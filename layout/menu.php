<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<!--<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-desktop"></i>						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-bar-chart-o "></i>						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cog"></i>						</button>
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>					</div>
				</div>--><!-- /.sidebar-shortcuts -->

				<ul class="nav nav-list">
					<li class="<?php if ($menu==1){ echo "active";}?>">
						<a href="./">
							<i class="menu-icon fa fa-tachometer red"></i><span class="menu-text"> Inicio </span>
						</a>
					</li>
					
					<li class="<?php if ($menu==2){ echo "active";}?>">
						<a href="fichas.php">
							<i class="menu-icon fa fa-file-text-o red"></i><span class="menu-text">Ficha Catastral</span>
						</a>
					</li>

					
					<li class="<?php if ($menu==3){ echo "active";}?>">
						<a href="pago_solvencias.php">
							<i class="menu-icon fa  fa-money red"></i><span class="menu-text">Pago de Solvencias</span>
						</a>
					</li>
					
					<li class="<?php if ($menu==4){ echo "active";}?>">
						<a href="inspecciones.php">
							<i class="menu-icon fa fa-eye red"></i><span class="menu-text">Inspecciones</span>
						</a>
					</li>
					
					<li class="<?php if ($menu==6){ echo "active";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa 	fa-file red"></i><span class="menu-text">Constancias</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
									<li class=""><a href="linderos.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-file red"></i> Certifiacion de Linderos
										</a>
										<b class="arrow"></b>
									</li>
									
									<li class=""><a href="solvencias.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-file red"></i> Solvencias
										</a>
										<b class="arrow"></b>
									</li>
									
									<!-- <li class=""><a href="">
										<i class="menu-icon fa fa-caret-right red"></i>
											Ficha Catastral
										</a>
										<b class="arrow"></b>
									</li>
									<li class=""><a href="">
										<i class="menu-icon fa fa-caret-right red"></i>
											Boleta Catastral
										</a>
										<b class="arrow"></b>
									</li> -->
								</ul>
					</li>

					<li class="<?php if ($menu==10){ echo "active";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa 	fa fa-file-pdf-o red"></i><span class="menu-text">Reportes</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
									<li class=""><a href="ficha_reporte.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-file-pdf-o red"></i> Fichas Catastrales
										</a>
										<b class="arrow"></b>
									</li>
									<li class=""><a href="inspecciones_reporte.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-file-pdf-o red"></i> Inspecciones
										</a>
										<b class="arrow"></b>
									</li>
									<li class=""><a href="solvencia_reporte.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-file-pdf-o red"></i> Solvencias
										</a>
										<b class="arrow"></b>
									</li>
									<li class=""><a href="certificacion_reporte.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-file-pdf-o red"></i> Certificacion de Linderos
										</a>
										<b class="arrow"></b>
									</li>
									
								</ul>
					</li>

					<li class="<?php if ($menu==9){ echo "active";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa 	fa fa-bar-chart-o red"></i><span class="menu-text">Estadisticas</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
									<li class=""><a href="estadisticas_terreno.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-bar-chart-o red"></i> Estadisticas Terreno
										</a>
										<b class="arrow"></b>
									</li>

									<li class=""><a href="estadisticas_construccion.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-bar-chart-o red"></i> Estadisticas Construccion
										</a>
										<b class="arrow"></b>
									</li>
									
									<li class=""><a href="estadisticas.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa fa-bar-chart-o red"></i> Estadisticas Generales
										</a>
										<b class="arrow"></b>
									</li>
								</ul>
					</li>
					<!-- <li class="<?php if ($menu==9){ echo "active";}?>">
						<a href="estadisticas.php">
							<i class="menu-icon fa fa-bar-chart-o red"></i><span class="menu-text">Estadisticas</span>
						</a>
					</li> -->
					
					<li class="<?php if ($menu==8){ echo "active";}?>">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa 	fa-cog red"></i><span class="menu-text">Configuraciones</span>
							<b class="arrow fa fa-angle-down"></b>
						</a>
						<ul class="submenu">
									<li class=""><a href="pedul.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Sectores
										</a>
										<b class="arrow"></b>
									</li>
									
									<li class=""><a href="parroquias.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Parroquias
										</a>
										<b class="arrow"></b>
									</li>
									
									<!-- <li class=""><a href="parcelas.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											Parcelas
										</a>
										<b class="arrow"></b>
									</li> -->
									<li class=""><a href="calles.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Calles
										</a>
										<b class="arrow"></b>
									</li>
									<li class=""><a href="inspectores.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Inspectores
										</a>
										<b class="arrow"></b>
									</li>

									<li class=""><a href="propietarios_ocupantes.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Propietario/Ocupante
										</a>
										<b class="arrow"></b>
									</li>

									<li class=""><a href="unidad_tributaria.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											<i class="menu-icon fa 	fa-cog red"></i> Unidad Tributaria
										</a>
										<b class="arrow"></b>
									</li>

									<!-- <li class=""><a href="unidades.php">
										<i class="menu-icon fa fa-caret-right red"></i>
											Unidad Tributaria
										</a>
										<b class="arrow"></b>
									</li> -->

								</ul>
					</li>
					<li class="<?php if ($menu==7){ echo "active";}?>">
						<a href="usuarios.php">
							<i class="menu-icon fa fa-users red"></i><span class="menu-text">Usuarios</span>
						</a>
					</li>

					<li class="">
						<a href="#" class="btnSalir">
							<i class="menu-icon fa fa-power-off red"></i><span class="menu-text">Salir</span>
						</a>
					</li>
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>				</div>
			</div>