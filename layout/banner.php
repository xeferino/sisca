<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>				</button>

				<div class="navbar-header pull-left">
					<a href="./" class="navbar-brand">
						<small>
							<i class="fa fa-cogs"></i>
							Sisca - Admin						</small>					</a>				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="">
								<img class="nav-user-photo" src="../assets/images/avatars/ava.png" alt="Jason's Photo" />
								<span class="user-info">
									<small>Bienvenido(a)</small>
									<?php echo @$_SESSION['nombre']." ".@$_SESSION['apellido'];?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

								<li>
									<a href="#" class="btnPerfil">
										<i class="ace-icon fa fa-user red"></i>
										Perfil									</a>								</li>

								<li class="divider"></li>

								<li>
									<a href="#" class="btnSalir">
										<i class="ace-icon fa fa-power-off red"></i>
										Salir									</a>								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>