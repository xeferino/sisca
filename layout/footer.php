	<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<!--
							<button class="btn btn-success">
							<i class="ace-icon fa fa-desktop"></i>						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-bar-chart-o "></i>						</button>

						<button class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>						</button>

						<button class="btn btn-danger">
							<i class="ace-icon fa fa-cog"></i>						</button>
					</div>-->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>			</a>		
	</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='../assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		
		<script src="../assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="../assets/js/jquery-ui.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>

		<!--[if lte IE 8]>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="../assets/js/jquery-ui.custom.min.js"></script>
		<script src="../assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="../assets/js/jquery.easypiechart.min.js"></script>
		<script src="../assets/js/jquery.sparkline.index.min.js"></script>
		<script src="../assets/js/jquery.flot.min.js"></script>
		<script src="../assets/js/jquery.flot.pie.min.js"></script>
		<script src="../assets/js/jquery.flot.resize.min.js"></script>
		
		<!-- page specific plugin scripts -->
		<!-- datatables -->
		<script src="../assets/datatables/media/js/jquery.dataTables.min.js"></script>
		<script src="../assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

		<!-- ace scripts -->
		<script src="../assets/js/ace-elements.min.js"></script>
		<script src="../assets/js/ace.min.js"></script>
		
		<!-- Select2 -->
		<script src="../assets/select2/select2.full.min.js"></script>
		<!-- inline scripts related to this page -->

		<!-- page specific plugin scripts -->
		<script src="../assets/js/wizard.min.js"></script>
		<script src="../assets/js/jquery.validate.min.js"></script>
		<script src="../assets/js/jquery-additional-methods.min.js"></script>
		<script src="../assets/js/bootbox.js"></script>
		<script src="../assets/js/jquery.maskedinput.min.js"></script>

<!-- 		 <script src="../assets/morris/jquery-1.10.2.js"></script>
 -->		 <!-- MORRIS CHART SCRIPTS -->
		 <script src="../assets/morris/raphael-2.1.0.min.js"></script>
		 <script src="../assets/morris/morris.js"></script>
		 <script src="../assets/function/fichacatastral.js"></script>		
		
	<script>
	//jquery tabs
	$( "#tabs" ).tabs();

    $(document).ready(function() {
        $('#dynamic-table').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[6]}]
        });
		//swal("Oops... Something went wrong!");
    });
    </script>
	
	

<script>
	$(document).on('ready',function(event){
	event.preventDefault();
	
	$('.btnSalir').click(function(){
	
	swal({
		title: "<?php echo @$_SESSION['rol'].": ".@$_SESSION['nombre']." ".@$_SESSION['apellido'];?>",
		text: "Desea Salir del Sistema",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		closeOnConfirm: false,
		confirmButtonText: 'Aceptar'
	},
	function(){
		$(location).attr('href','http:../logout.php');
	});
	});
	
	$('.btnPerfil').click(function(){
	
	swal({
		title: "Alerta!",
		text: "Desea Actualizar sus Datos de Acceso",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		closeOnConfirm: false,
		confirmButtonText: 'Aceptar'
	},
	function(){
		$(location).attr('href','http:perfil.php?id=<?php echo $_SESSION['conectado'];?>');
	});
	});
});
 </script>
 
 