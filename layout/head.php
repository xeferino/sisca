<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Sisca - Admin</title>
		<link rel="shortcut icon" type="image/x-icon" href="../assets/images/img/pa.ico">

		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="../assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="../assets/font-awesome/4.5.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
		<link rel="stylesheet" href="../assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="../assets/css/jquery-ui.min.css" />

		<!-- text fonts -->
		<link rel="stylesheet" href="../assets/css/fonts.googleapis.com.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="../assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="../assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="../assets/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->
		<!-- Select2 -->
		<link rel="stylesheet" href="../assets/select2/select2.min.css">
				<!-- inline styles related to this page -->
		<!-- datatables-->
		<link href="../assets/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">
		<link href="../assets/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
		
		<!-- datepicker-->
		<!-- datepicker-->
		<script src="../assets/datepicker/jquery-ui.js"></script>
		<script src="../assets/datepicker/datepicker-es.js"></script>
		
		<!-- sweetalert -->
	    <script src="../assets/sweetalert/dist/sweetalert-dev.js"></script>
		<!--<link rel="stylesheet" href="../assets/sweetalert/example/example.css">-->
	    <link rel="stylesheet" href="../assets/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" href="../assets/sweetalert/themes/facebook/facebook.css">
		<!-- ace settings handler -->
		<script src="../assets/js/ace-extra.min.js"></script>
		<script src="../assets/js/jquery-2.1.4.min.js"></script>
		<script src="../assets/jquery.validate/jquery.formvalidate.js"></script>
		<!-- graficas -->
		<script src="../assets/graficas/Chart.js"></script>
		<link rel="stylesheet" href="../assets/graficas/style-gra.css" type="text/css" />
		<!-- Calendar -->
		<link rel="stylesheet" href="../assets/calendar/clndr.css" type="text/css" />
		<script src="../assets/calendar/underscore-min.js" type="text/javascript"></script>
		<script src= "../assets/calendar/moment-2.2.1.js" type="text/javascript"></script>
		<script src="../assets/calendar/clndr.js" type="text/javascript"></script>
		<script src="../assets/calendar/site.js" type="text/javascript"></script>
		
		<!-- MORRIS CHART STYLES-->
    	<link href="../assets/morris/morris-0.4.3.min.css" rel="stylesheet" />
    	



		<script>
			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#campofecha1").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});
		</script>
		<style type="text/css">
			.error{
				color:#f00!important;			}

		</style>
	</head>