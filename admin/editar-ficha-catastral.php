<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");

$objeto = new Ficha();
$objeto->servicios();

$parroquia = new Parroquia();
$parroquia->parroquias();

if (isset($_GET['ficha'])) {

	$ficha = $_GET['ficha'];
	$objeto->datosFichaCastastral($ficha);	
	
}

if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {

	$array = array($_POST);
	$objeto->actualizarFichaCastastral($array);	
	
}
	
?>
<!DOCTYPE html>
<html lang="en">
<?php include('../layout/head.php');?>
<style type="text/css">
.select2{
width: 100% !important;
}

.error-message, label.error { color: #a10052;  display: block; font-size: 1em !important;font-weight:bold; }
</style>
<body class="no-skin">
<?php include('../layout/banner.php');?>
<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=2; include('../layout/menu.php');?>
<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="#">Home</a>							</li>
<li class="active">Ficha Catastral</li>
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">

<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
<a href="fichas.php"><span class="btn btn-primary pull-right" title="Listado de Fichas Catastrales"><i class="ace-icon fa fa-list"></i></span></a><br><br><br>
<div class="widget-box">
<div class="widget-header widget-header-blue widget-header-flat">
	<h4 class="widget-title lighter"><i class="ace-icon fa 	fa-file-text-o"></i> Nueva Ficha Catastral</h4>
</div>
<div class="widget-body">
			<?php if($objeto->mensaje==1){?>
			<script>
				$(document).ready(function() {
					swal({
						title: "",
						text: "Ficha Actualizada Exitosamente",
						type: "success",
						confirmButtonText: "Aceptar",
						timer: "3000"
					},
					function(){
						$(location).attr('href','http:fichas.php');
					});

				});
			</script>
			<?php }?>
	<div class="row">
		<form action="" method="post" id="fom-fichacatastral">
			<input type="hidden" name="submit" value="edit" />

			<input type="hidden" name="valor" id="sector_valor" value="<?php echo $objeto->row['numero_sector'];?>" />
			<input type="hidden" name="ficha" id="ficha" value="<?php echo $ficha;?>" />
			<input type="hidden" name="idterreno" value="<?php echo $objeto->row['idterreno'];?>" />

			<input type="hidden" name="idconstruccion" value="<?php echo $objeto->row['idconstruccion'];?>" />

			<input type="hidden" name="idubicacioncomunitaria" 
			value="<?php echo $objeto->row['idubicacioncomunitaria'];?>" />

			<input type="hidden" name="idregistro" 
			value="<?php echo $objeto->row['idregistro'];?>" />

			<input type="hidden" name="ocupante_viejo" 
			value="<?php echo $objeto->row['poid'];?>" />

			<input type="hidden" name="idinmueble" 
			value="<?php echo $objeto->row['idinmueble'];?>" />

			

			
		<!-- FIN DE SEGUNDA COLUMNA -->
		<br><br>
		<div class="col-md-12">
		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion Solicitada.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Control Archivo</label>
					<input class="form-control number" name="control_archivo" id="control_archivo"  value="<?php echo $objeto->row['numeroarchivo'];?>" placeholder="EJ:02" type="text" required="required" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha Inscripcion</label>
					<input class="form-control " id="fecha-ficha" name="fecha_ficha" value="<?php echo $objeto->row['fechainscripcion'];?>" placeholder="00-00-0000" type="text" required="required"  readonly="" required="required">
				</div>
				</div>

			</div>
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion de Ubicacion Comunitaria.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Region Federal de Gobierno</label>
					<input class="form-control " name="region_rederal_gobierno"   value="<?php echo $objeto->row['region_consejofg'];?>" type="text" required="required" >
					
					
				</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>Comuna</label>
					<input class="form-control " name="comuna"   value="<?php echo $objeto->row['comuna'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2" style="text-align: center;">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_comuna"   value="<?php echo $objeto->row['numero_comuna'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					<label>Consejo Comunal</label>
					<input class="form-control " name="consejo_comunal"   value="<?php echo $objeto->row['consejo_comunal'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_consejo_comunal"   value="<?php echo $objeto->row['numero_consejo_comunal'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>Com. de Tierra Urb.</label>
					<input class="form-control " name="comite_tierra_urbano"   value="<?php echo $objeto->row['comite_tierrau'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_comite_urbano"   value="<?php echo $objeto->row['numero_comite_urbano'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Registro MVV</label>
					<input class="form-control number" name="registro_mvv"   value="<?php echo $objeto->row['numero_rmv'];?>" type="text" required="required">
				</div>
				</div>

			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion de Ubicacion Del Inmueble.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<select class="form-control  select2" name="parroquia" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$parroquia1= pg_query("SELECT * FROM  tb_parroquia");
						?>			
						<?php while($reg=pg_fetch_object($parroquia1)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['parroquia'] == $reg->codigo){echo " selected='selected'";}
						?> value="<?php echo $reg->codigo;?>"><?php echo $reg->nombre;?></option>
						<?php }?>
					</select>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<input class="form-control " name="ciudad"   value="<?php echo $objeto->row['ciudad'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<input class="form-control " name="localidad"   value="<?php echo $objeto->row['localidad'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizacion</label>
					<input class="form-control " name="urbanizacion"   value="<?php echo $objeto->row['urbanizacion'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<input class="form-control " name="consejo_residencial"   value="<?php echo $objeto->row['consejo_residencial'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<input class="form-control " name="barrio"   value="<?php echo $objeto->row['barrio'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<input class="form-control number" name="numero_civico"   value="<?php echo $objeto->row['numero_civico'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<select class="form-control  select2" id="sector" name="sector" required="required">
						<?php 
							$i=0;
							$pedul= pg_query("SELECT * FROM  tb_pedul");
							?>			
						<?php while($reg=pg_fetch_object($pedul)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['numero_sector'] == $reg->id){
							echo "selected='selected'";
						}?>
							value="<?php echo $reg->id;?>"><?php echo $reg->nombre;?></option>
						<?php }?>
					</select>
				</div>
				</div>

				<div class="row">
					<div id="calles_sector"></div>
				</div>
	
				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<input class="form-control " name="punto_referencia"   value="<?php echo $objeto->row['puntoreferencia'];?>" type="text" required="required">
				</div>
				</div>

				
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los datos del registro publico.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Numero</label>
					<textarea class="form-control required number" name="numero_registro" cols="" rows="" ><?php echo $objeto->row['numero'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Folio</label>
					<textarea class="form-control " name="folio_registro" cols="" rows="" required="required"><?php echo $objeto->row['folio'];?></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tomo</label>
					<textarea class="form-control " name="tomo_registro" cols="" rows="" required="required"><?php echo $objeto->row['tomo'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Protocolo</label>
					<textarea class="form-control " name="protocolo_registro" cols="" rows="" required="required"><?php echo $objeto->row['protocolo'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha</label>
					<input class="form-control " id="registro-fecha" name="registro_fecha" value="<?php echo $objeto->row['fecharp'];?>" placeholder="00-00-0000" type="text"  rows="" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Area (M2) Terreno</label>
					<textarea class="form-control number" name="registro_aream2terreno" cols="" rows="" required="required"><?php echo $objeto->row['aream2cont'];?></textarea>
					
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Area (M2) Construccion</label>
					<textarea class="form-control number" name="registro_aream2const" cols="" rows="" required="required"><?php echo $objeto->row['aream2cont'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Monto (BS)</label>
					<textarea class="form-control number" name="registro_monto" cols="" rows="" required="required"><?php echo $objeto->row['valorbs'];?></textarea>
				</div>
				</div>
			</div>



				<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ocupante de la Ficha: (<?php 
			echo $objeto->row['ponacionalidad']."-".$objeto->row['pocedula']." ".$objeto->row['pon1']." ".$objeto->row['pon2']." ".$objeto->row['poa1']." ".$objeto->row['poa2'];?>) </strong>
				</p>
			</div>
			<div class="col-md-12">
							<div class="form-group">
					<label>Cambiar Ocupante</label>
					<select class="form-control   select2" name="ocupante" >
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$persona1= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante' and chequeado!=1");
						?>			
						<?php while($reg=pg_fetch_object($persona1)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['poid'] == $reg->id){
							echo "selected='selected'";
						}?> value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>

					
				</div>
				<?php 

					$idocupante = $objeto->row['poid'];
					$idpropietario = $objeto->row['ppid'];

					$sql_valida= pg_query("SELECT * FROM  tb_inmueble 
					WHERE idfichacatastral='$ficha'");

					$valida=pg_fetch_object($sql_valida);

					if($valida->idocupante != $valida->idpropietario){

				?>
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion del Propietario.</strong>
				</p>
				
				<div class="col-md-12">
							<div class="form-group">
					<label>Propietario</label>
					<select class="form-control select2" name="propietario" id="propietario">
						<?php 
							$i=0;
							$persona= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'");
						?>			
						<?php while($reg=pg_fetch_object($persona)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['ppid'] == $reg->id){
							echo "selected='selected'";
						}?>
						value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
					
				</div>

			</div>
		<div class="col-md-12" id="propietario_direccion">
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Direccion del Propietario.</strong>
				</p>
			
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<textarea class="form-control" name="parroquia_persona" id="parroquia_persona" cols="" rows=""><?php echo $objeto->row['nombre_parroquia'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<textarea class="form-control" name="ciudad_persona" id="ciudad_persona" cols="" rows=""><?php echo $objeto->row['dppciudad'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<textarea class="form-control" id="localidad_persona" name="localidad_persona" cols="" rows=""><?php echo $objeto->row['dpplocalidad'];?></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizacion</label>
					<textarea class="form-control" id="urbanizacion_persona" name="urbanizacion_persona" cols="" rows=""><?php echo $objeto->row['dppurbanizacion'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<textarea class="form-control" id="consejo_residencial_persona" name="consejo_residencial_persona" cols="" rows=""><?php echo $objeto->row['dppconsejo_residencial'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<textarea class="form-control"  id="barrio_persona" name="barrio_persona" cols="" rows=""><?php echo $objeto->row['dppbarrio'];?></textarea>
				</div>
				</div>


				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<textarea class="form-control number" id="numero_civico_persona" name="numero_civico_persona" cols="" rows=""><?php echo $objeto->row['dppnumero_civico'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Nombre Calle</label>
					<textarea class="form-control" id="nombre_calle_persona" name="nombre_calle_persona" cols="" rows=""><?php echo $objeto->row['dppnombrecalle'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<textarea class="form-control" id="sector_persona" name="sector_persona" cols="" rows=""><?php echo $objeto->row['sector_nombre'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tipo de Calle</label>
						<select class="form-control  tipocalle" name="tipo_calle_persona" id="tipo_calle_persona" >
					<option 
					<?php if($objeto->row['tipo_calle'] == "Av"){
							echo "selected='selected'";
						}?> value="Av">Av</option>
					<option <?php if($objeto->row['tipo_calle'] == "Clle"){
							echo "selected='selected'";
						}?>
						value="Clle">Clle</option>
					<option <?php if($objeto->row['tipo_calle'] == "Crr"){
							echo "selected='selected'";
						}?> 
						value="Crr">Crr</option>
					<option <?php if($objeto->row['tipo_calle'] == "Trav"){
							echo "selected='selected'";
						}?>
						value="Trav">Trav</option>
					<option <?php if($objeto->row['tipo_calle'] == "Prol"){
							echo "selected='selected'";
						}?>
						value="Prol">Prol</option>
					<option <?php if($objeto->row['tipo_calle'] == "Cjn"){
							echo "selected='selected'";
						}?>
						value="Cjn">Cjn</option>
					<option <?php if($objeto->row['tipo_calle'] == "Psje"){
							echo "selected='selected'";
						}?>
						value="Psje">Psje</option>
					<option <?php if($objeto->row['tipo_calle'] == "Blv"){
							echo "selected='selected'";
						}?>
						value="Blv">Blv</option>
					<option <?php if($objeto->row['tipo_calle'] == "Vda"){
							echo "selected='selected'";
						}?>
						value="Vda">Vda</option>
					<option <?php if($objeto->row['tipo_calle'] == "Esc"){
							echo "selected='selected'";
						}?>
						value="Esc">Esc</option>
					<option <?php if($objeto->row['tipo_calle'] == "Snd"){
							echo "selected='selected'";
						}?>
						value="Snd">Snd</option>
					<option <?php if($objeto->row['tipo_calle'] == "Tcal"){
							echo "selected='selected'";
						}?>
						value="Tcal">Tcal</option>
					<option <?php if($objeto->row['tipo_calle'] == "Cno"){
							echo "selected='selected'";
						}?>
						value="Cno">Cno</option>

						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<textarea class="form-control" id="punto_referencia_persona" name="punto_referencia_persona" cols="" rows=""><?php echo $objeto->row['dpppuntoreferencia'];?></textarea>
				</div>
				</div>

			</div>
		</div>
		<?php }?>

			<div class="col-md-12">
			<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion del Terreno.</strong>
				</p>
				<div class="col-md-4">
					<div class="form-group">
					<label>Topografia</label>
					<select class="form-control  select2" required="required" name="topografia" >
						<option <?php if($objeto->row['topografia'] == "Plano"){
							echo "selected='selected'";
						}?>
						value="Plano">Plano</option>
						<option <?php if($objeto->row['topografia'] == "Sobre Nivel"){
							echo "selected='selected'";
						}?>
						value="Sobre Nivel">Sobre Nivel</option>
						<option <?php if($objeto->row['topografia'] == "Bajo Nivel"){
							echo "selected='selected'";
						}?>
						value="Bajo Nivel">Bajo Nivel</option>
						<option <?php if($objeto->row['topografia'] == "Corte"){
							echo "selected='selected'";
						}?>
						value="Corte">Corte</option>
						<option <?php if($objeto->row['topografia'] == "Relleno"){
							echo "selected='selected'";
						}?>
						value="Relleno">Relleno</option>
						<option <?php if($objeto->row['topografia'] == "Inclinado/Pend. Suave"){
							echo "selected='selected'";
						}?>
						value="Inclinado/Pend. Suave">Inclinado/Pend. Suave</option>
						<option <?php if($objeto->row['topografia'] == "Inclinado/Pend. Media"){
							echo "selected='selected'";
						}?>
						value="Inclinado/Pend. Media">Inclinado/Pend. Media</option>
						<option <?php if($objeto->row['topografia'] == "Inclinado/Pend. Fuerte"){
							echo "selected='selected'";
						}?>
						value="Inclinado/Pend. Fuerte">Inclinado/Pend. Fuerte</option>
						<option <?php if($objeto->row['topografia'] == "Irregular"){
							echo "selected='selected'";
						}?>
						value="Irregular">Irregular</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Acceso</label>
					<select class="form-control  select2" required="required" name="acceso">
						<option <?php if($objeto->row['acceso'] == "Calle Pavimentada"){
							echo "selected='selected'";
						}?>
						value="Calle Pavimentada">Calle Pavimentada</option>
						<option <?php if($objeto->row['acceso'] == "Calle Engranzonada"){
							echo "selected='selected'";
						}?>
						value="Calle Engranzonada">Calle Engranzonada</option>
						<option <?php if($objeto->row['acceso'] == "Calle de Tierra"){
							echo "selected='selected'";
						}?>
						value="Calle de Tierra">Calle de Tierra</option>
						<option <?php if($objeto->row['acceso'] == "Escalera Pavimento"){
							echo "selected='selected'";
						}?>
						value="Escalera Pavimento">Escalera Pavimento</option>
						<option <?php if($objeto->row['acceso'] == "Escalera de Tierra"){
							echo "selected='selected'";
						}?>
						value="Escalera de Tierra">Escalera de Tierra</option>
						<option <?php if($objeto->row['acceso'] == "Paso de Servidumbre"){
							echo "selected='selected'";
						}?>
						value="Paso de Servidumbre">Paso de Servidumbre</option>
						<option <?php if($objeto->row['acceso'] == "Vereda"){
							echo "selected='selected'";
						}?>
						value="Vereda">Vereda</option>
						<option <?php if($objeto->row['acceso'] == "Sendero"){
							echo "selected='selected'";
						}?>
						value="Sendero">Sendero</option>
						<option <?php if($objeto->row['acceso'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Forma</label>
					<select class="form-control  select2" required="required" name="forma">
						<option <?php if($objeto->row['forma'] == "Regular"){
							echo "selected='selected'";
						}?>
						value="Regular">Regular</option>
						<option <?php if($objeto->row['forma'] == "Irregular"){
							echo "selected='selected'";
						}?>
						value="Irregular">Irregular</option>
						<option <?php if($objeto->row['forma'] == "Muy Irregular"){
							echo "selected='selected'";
						}?>
						value="Muy Irregular">Muy Irregular</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Ubicacion</label>
					<select class="form-control  select2" required="required" name="ubicacion">
						<option <?php if($objeto->row['ubicacion'] == "Convencional"){
							echo "selected='selected'";
						}?>
						value="Convencional">Convencional</option>
						<option <?php if($objeto->row['ubicacion'] == "Esquina"){
							echo "selected='selected'";
						}?>
						value="Esquina">Esquina</option>
						<option <?php if($objeto->row['ubicacion'] == "Interior de Manzana"){
							echo "selected='selected'";
						}?>
						value="Interior de Manzana">Interior de Manzana</option>
						<option <?php if($objeto->row['ubicacion'] == "Oculta"){
							echo "selected='selected'";
						}?>
						value="Oculta">Oculta</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Entorno Fisico</label>
					<select class="form-control  select2" required="required" name="entorno_fisico">
						
						<option <?php if($objeto->row['entornofisico'] == "Zona Urbanizada"){
							echo "selected='selected'";
						}?>
						value="Zona Urbanizada">Zona Urbanizada</option>
						<option <?php if($objeto->row['entornofisico'] == "Zona no Urbanizada"){
							echo "selected='selected'";
						}?>
						value="Zona no Urbanizada">Zona no Urbanizada</option>
						<option <?php if($objeto->row['entornofisico'] == "Rio/Quebrada"){
							echo "selected='selected'";
						}?>
						value="Rio/Quebrada">Rio/Quebrada</option>
						<option <?php if($objeto->row['entornofisico'] == "Barranco/Talud"){
							echo "selected='selected'";
						}?>
						value="Barranco/Talud">Barranco/Talud</option>
						<option <?php if($objeto->row['entornofisico'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Mejoras al Terreno</label>
					<select class="form-control  select2" required="required" name="mejoras_terreno">
						<option <?php if($objeto->row['mejorasterreno'] == "Muro de Contencion"){
							echo "selected='selected'";
						}?>
						value="Muro de Contencion">Muro de Contencion</option>
						<option <?php if($objeto->row['mejorasterreno'] == "Nivelacion"){
							echo "selected='selected'";
						}?>
						value="Nivelacion">Nivelacion</option>
						<option <?php if($objeto->row['mejorasterreno'] == "Cercado"){
							echo "selected='selected'";
						}?>
						value="Cercado">Cercado</option>
						<option <?php if($objeto->row['mejorasterreno'] == "Pozo Septico"){
							echo "selected='selected'";
						}?>
						value="Pozo Septico">Pozo Septico</option>
						<option <?php if($objeto->row['mejorasterreno'] == "Lagunas Artificiales"){
							echo "selected='selected'";
						}?>
						value="Lagunas Artificiales">Lagunas Artificiales</option>
						<option <?php if($objeto->row['mejorasterreno'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Tenencia Terreno</label>
					<select class="form-control  select2" required="required" name="tenencia_terreno">
						<option <?php if($objeto->row['teneciaterreno'] == "Propiedad"){
							echo "selected='selected'";
						}?>
						value="Propiedad">Propiedad</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Arrendamiento"){
							echo "selected='selected'";
						}?>
						value="Arrendamiento">Arrendamiento</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Comodato"){
							echo "selected='selected'";
						}?>
						value="Comodato">Comodato</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Anticresis"){
							echo "selected='selected'";
						}?>
						value="Anticresis">Anticresis</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Enfiteusis"){
							echo "selected='selected'";
						}?>
						value="Enfiteusis">Enfiteusis</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Usufructo"){
							echo "selected='selected'";
						}?>
						value="Usufructo">Usufructo</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Derechho de Hab."){
							echo "selected='selected'";
						}?>
						value="Derechho de Hab.">Derechho de Hab.</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Concesion de Uso"){
							echo "selected='selected'";
						}?>
						value="Concesion de Uso">Concesion de Uso</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Admin. de Estado N/E/M"){
							echo "selected='selected'";
						}?>
						value="Admin. de Estado N/E/M">Admin. de Estado N/E/M</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Posesion con Tit./Sin Tit."){
							echo "selected='selected'";
						}?>
						value="Posesion con Tit./Sin Tit.">Posesion con Tit./Sin Tit.</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Posesion Certificada TU"){
							echo "selected='selected'";
						}?>
						value="Posesion Certificada TU">Posesion Certificada TU</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Ocupacion"){
							echo "selected='selected'";
						}?>
						value="Ocupacion">Ocupacion</option>
						<option <?php if($objeto->row['teneciaterreno'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Reg. de Propiedad</label>
					<select class="form-control  select2" required="required" name="regimen_propiedad">
						<option <?php if($objeto->row['registropropiedad'] == "Ejido"){
							echo "selected='selected'";
						}?>
						value="Ejido">Ejido</option>
						<option <?php if($objeto->row['registropropiedad'] == "Municipal Propio"){
							echo "selected='selected'";
						}?>
						value="Municipal Propio">Municipal Propio</option>
						<option <?php if($objeto->row['registropropiedad'] == "Nacional"){
							echo "selected='selected'";
						}?>
						value="Nacional">Nacional</option>
						<option <?php if($objeto->row['registropropiedad'] == "Baldio"){
							echo "selected='selected'";
						}?>
						value="Baldio">Baldio</option>
						<option <?php if($objeto->row['registropropiedad'] == "Estadal"){
							echo "selected='selected'";
						}?>
						value="Estadal">Estadal</option>
						<option <?php if($objeto->row['registropropiedad'] == "Priv. Individual"){
							echo "selected='selected'";
						}?>
						value="Priv. Individual">Priv. Individual</option>
						<option <?php if($objeto->row['registropropiedad'] == "Priv. Condominio"){
							echo "selected='selected'";
						}?>
						value="Priv. Condominio">Priv. Condominio</option>
						<option <?php if($objeto->row['registropropiedad'] == "Priv. Posesion"){
							echo "selected='selected'";
						}?>
						value="Priv. Posesion">Priv. Posesion</option>
						<option <?php if($objeto->row['registropropiedad'] == "Familiar"){
							echo "selected='selected'";
						}?>
						value="Familiar">Familiar</option>
						<option <?php if($objeto->row['registropropiedad'] == "Colectiva"){
							echo "selected='selected'";
						}?>
						value="Colectiva">Colectiva</option>
						<option <?php if($objeto->row['registropropiedad'] == "Multifamiliar"){
							echo "selected='selected'";
						}?>
						value="Multifamiliar">Multifamiliar</option>
						<option <?php if($objeto->row['registropropiedad'] == "Comunal"){
							echo "selected='selected'";
						}?>
						value="Comunal">Comunal</option>
						<option <?php if($objeto->row['registropropiedad'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Uso Actual</label>
					<select class="form-control  select2" required="required" name="uso_actual_terreno">
						<option <?php if($objeto->row['usoactual'] == "Residencial"){
							echo "selected='selected'";
						}?>
						value="Residencial">Residencial</option>
						<option <?php if($objeto->row['usoactual'] == "Comercial"){
							echo "selected='selected'";
						}?>
						value="Comercial">Comercial</option>
						<option <?php if($objeto->row['usoactual'] == "Industrial"){
							echo "selected='selected'";
						}?>
						value="Industrial">Industrial</option>
						<option <?php if($objeto->row['usoactual'] == "Recreativo"){
							echo "selected='selected'";
						}?>
						value="Recreativo">Recreativo</option>
						<option <?php if($objeto->row['usoactual'] == "Deportivo"){
							echo "selected='selected'";
						}?>
						value="Deportivo">Deportivo</option>
						<option <?php if($objeto->row['usoactual'] == "Asistencial/salud"){
							echo "selected='selected'";
						}?>
						value="Asistencial/salud">Asistencial/salud</option>
						<option <?php if($objeto->row['usoactual'] == "Educacional"){
							echo "selected='selected'";
						}?>
						value="Educacional">Educacional</option>
						<option <?php if($objeto->row['usoactual'] == "Turistico"){
							echo "selected='selected'";
						}?>
						value="Turistico">Turistico</option>
						<option <?php if($objeto->row['usoactual'] == "Social/Cultural"){
							echo "selected='selected'";
						}?>
						value="Social/Cultural">Social/Cultural</option>
						<option <?php if($objeto->row['usoactual'] == "Religioso"){
							echo "selected='selected'";
						}?>
						value="Religioso">Religioso</option>
						<option <?php if($objeto->row['usoactual'] == "Alimentacion"){
							echo "selected='selected'";
						}?>
						value="Alimentacion">Alimentacion</option>
						<option <?php if($objeto->row['usoactual'] == "Servicio Comunal"){
							echo "selected='selected'";
						}?>
						value="Servicio Comunal">Servicio Comunal</option>
						<option <?php if($objeto->row['usoactual'] == "Gubernamental/institucional"){
							echo "selected='selected'";
						}?>
						value="Gubernamental/institucional">Gubernamental/institucional</option>
						<option <?php if($objeto->row['usoactual'] == "Pesquero"){
							echo "selected='selected'";
						}?>
						value="Pesquero">Pesquero</option>
						<option <?php if($objeto->row['usoactual'] == "Agroindustrial"){
							echo "selected='selected'";
						}?>
						value="Agroindustrial">Agroindustrial</option>
						<option <?php if($objeto->row['usoactual'] == "Agroforestal"){
							echo "selected='selected'";
						}?>
						value="Agroforestal">Agroforestal</option>
						<option <?php if($objeto->row['usoactual'] == "Agricola"){
							echo "selected='selected'";
						}?>
						value="Agricola">Agricola</option>
						<option <?php if($objeto->row['usoactual'] == "Pecuario"){
							echo "selected='selected'";
						}?>
						value="Pecuario">Pecuario</option>
						<option <?php if($objeto->row['usoactual'] == "Forestal"){
							echo "selected='selected'";
						}?>
						value="Forestal">Forestal</option>
						<option <?php if($objeto->row['usoactual'] == "Minero"){
							echo "selected='selected'";
						}?>
						value="Minero">Minero</option>
						<option <?php if($objeto->row['usoactual'] == "Sin Uso"){
							echo "selected='selected'";
						}?>
						value="Sin Uso">Sin Uso</option>
						<option <?php if($objeto->row['usoactual'] == "Otro"){
							echo "selected='selected'";
						}?>
						value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-12">
					<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Valoracion del Terreno</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="Observaciones_economia_terreno" cols="" rows="" required="required"><?php echo $objeto->row['observacionterreno'];?></textarea>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label> % Porcentaje (U.T. M2)</label>
					<input class="form-control number" name="valor_ajustado"   value="<?php echo $objeto->row['valorajustadometroscuadrados'];?>" type="text" required="required">
				</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los servicios que posee el terreno.</strong>
				</p>
				<div class="col-md-12">
					<div class="form-group">
					<label>Servicios</label>
					<select name="servicios[]" class="form-control  select2" required="required" multiple="multiple" data-placeholder="--Seleccione--" style=" width: 100%;">
				 <?php 

				 $idterreno = $objeto->row['idterreno'];
				 $sql=pg_query("SELECT idservicios FROM tb_terreno as tr
							 	LEFT JOIN tb_terrenoservicios as tserv on tserv.idterreno = tr.id
							 	LEFT JOIN tb_servicio as serv on serv.id = tserv.idservicios
							 	WHERE tserv.idterreno ='$idterreno'");

							$selcionados = array();
							$i=0;
							while ($row = pg_fetch_array($sql)){
								$selcionados[$i] = $row['idservicios'];
								$i++;
							}//fin 
				 $servicios= pg_query("SELECT * FROM  tb_servicio");

				 while($reg=pg_fetch_object($servicios)){?>
					<?php $i++;?>
					<option <?php if(in_array($reg->id, $selcionados)){ echo "selected='selected'";}?>
						value="<?php  echo $reg->id;?>"> <?php echo $reg->tipo;?></option>
				  <?php }?>
				</select>
				</div>
				</div>	
		</div>


		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion de General de la Construccion.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo</label>
					<select class="form-control  select2" required="required" name="tipo_construccion">
						<option <?php if($objeto->row['tipo'] == "Quinta"){
							echo "selected='selected'";}?>
							value="Quinta">Quinta</option>
						<option <?php if($objeto->row['tipo'] == "Casa/Quinta"){
							echo "selected='selected'";}?>
							value="Casa/Quinta">Casa/Quinta</option>
						<option <?php if($objeto->row['tipo'] == "Chalet"){
							echo "selected='selected'";}?>
							value="Chalet">Chalet</option>
						<option <?php if($objeto->row['tipo'] == "Town House"){
							echo "selected='selected'";}?>
							value="Town House">Town House</option>
						<option <?php if($objeto->row['tipo'] == "Casa Tradicional"){
							echo "selected='selected'";}?>
							value="Casa Tradicional">Casa Tradicional</option>
						<option <?php if($objeto->row['tipo'] == "Casa Convencional"){
							echo "selected='selected'";}?>
							value="Casa Convencional">Casa Convencional</option>
						<option <?php if($objeto->row['tipo'] == "Casa Economica"){
							echo "selected='selected'";}?>
							value="Casa Economica">Casa Economica</option>
						<option <?php if($objeto->row['tipo'] == "Rancho"){
							echo "selected='selected'";}?>
							value="Rancho">Rancho</option>
						<option <?php if($objeto->row['tipo'] == "Edificio"){
							echo "selected='selected'";}?>
							value="Edificio">Edificio</option>
						<option <?php if($objeto->row['tipo'] == "Apartamento"){
							echo "selected='selected'";}?>
							value="Apartamento">Apartamento</option>
						<option <?php if($objeto->row['tipo'] == "Centro Comercial"){
							echo "selected='selected'";}?>
							value="Centro Comercial">Alimentacion</option>
						<option <?php if($objeto->row['tipo'] == "Local Comercial"){
							echo "selected='selected'";}?>
							value="Local Comercial">Local Comercial</option>
						<option <?php if($objeto->row['tipo'] == "Galpon"){
							echo "selected='selected'";}?>
							value="Galpon">Galpon</option>
						<option <?php if($objeto->row['tipo'] == "Barraca"){
							echo "selected='selected'";}?>
							value="Barraca">Barraca</option>
						<option <?php if($objeto->row['tipo'] == "Vaqueras"){
							echo "selected='selected'";}?>
							value="Vaqueras">Vaqueras</option>
						<option <?php if($objeto->row['tipo'] == "Cochineras"){
							echo "selected='selected'";}?>
							value="Cochineras">Cochineras</option>
						<option <?php if($objeto->row['tipo'] == "Corrales y Anexos"){
							echo "selected='selected'";}?>
							value="Corrales y Anexos">Corrales y Anexos</option>
						<option <?php if($objeto->row['tipo'] == "Bebederos"){
							echo "selected='selected'";}?>
							value="Bebederos">Bebederos</option>
						<option <?php if($objeto->row['tipo'] == "Comederos"){
							echo "selected='selected'";}?>
							value="Comederos">Comederos</option>
						<option <?php if($objeto->row['tipo'] == "Tanques"){
							echo "selected='selected'";}?>
							value="Tanques">Tanques</option>
						<option <?php if($objeto->row['tipo'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Descripcion de Uso</label>
					<select class="form-control  select2" required="required" name="descripcion_uso">
						<option <?php if($objeto->row['descripcionuso'] == "Unifamiliar"){
							echo "selected='selected'";}?>
							value="Unifamiliar">Unifamiliar</option>
						<option <?php if($objeto->row['descripcionuso'] == "Bifamiliar"){
							echo "selected='selected'";}?>
							value="Bifamiliar">Bifamiliar</option>
						<option <?php if($objeto->row['descripcionuso'] == "Multifamiliar"){
							echo "selected='selected'";}?>
							value="Multifamiliar">Multifamiliar</option>
						<option <?php if($objeto->row['descripcionuso'] == "Comercio al Detal"){
							echo "selected='selected'";}?>
							value="Comercio al Detal">Comercio al Detal</option>
						<option <?php if($objeto->row['descripcionuso'] == "Comercio al Mayor"){
							echo "selected='selected'";}?>
							value="Comercio al Mayor">Comercio al Mayor</option>
						<option <?php if($objeto->row['descripcionuso'] == "Mercado Libre"){
							echo "selected='selected'";}?>
							value="Mercado Libre">Mercado Libre</option>
						<option <?php if($objeto->row['descripcionuso'] == "Oficinas"){
							echo "selected='selected'";}?>
							value="Oficinas">Oficinas</option>
						<option <?php if($objeto->row['descripcionuso'] == "Industrial"){
							echo "selected='selected'";}?>
							value="Industrial">Industrial</option>
						<option <?php if($objeto->row['descripcionuso'] == "Servicio"){
							echo "selected='selected'";}?>
							value="Servicio">Servicio</option>
						<option <?php if($objeto->row['descripcionuso'] == "Agropecuario"){
							echo "selected='selected'";}?>
							value="Agropecuario">Agropecuario</option>
						<option <?php if($objeto->row['descripcionuso'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tenencia Const.</label>
					<select name="tenencia_construccion" class="form-control  select2" required="required">
						<option <?php if($objeto->row['teneciaconstruccion'] == "Propiedad"){
							echo "selected='selected'";}?>
							value="Propiedad">Propiedad</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Arrendamiento"){
							echo "selected='selected'";}?>
							value="Arrendamiento">Arrendamiento</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Comodato"){
							echo "selected='selected'";}?>
							value="Comodato">Comodato</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Anticresis"){
							echo "selected='selected'";}?>
							value="Anticresis">Anticresis</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Enfiteusis"){
							echo "selected='selected'";}?>
							value="Enfiteusis">Enfiteusis</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Usufructo"){
							echo "selected='selected'";}?>
							value="Usufructo">Usufructo</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Derecho de Hab."){
							echo "selected='selected'";}?>
							value="Derecho de Hab.">Derecho de Hab.</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Concesion de Uso"){
							echo "selected='selected'";}?>
							value="Concesion de Uso">Concesion de Uso</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Admin. de Estado N/E/M"){
							echo "selected='selected'";}?>
							value="Admin. de Estado N/E/M">Admin. de Estado N/E/M</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Posesion con Tit./Sin Tit."){
							echo "selected='selected'";}?>
							value="Posesion con Tit./Sin Tit.">Posesion con Tit./Sin Tit.</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Ocupacion"){
							echo "selected='selected'";}?>
							value="Ocupacion">Ocupacion</option>
						<option <?php if($objeto->row['teneciaconstruccion'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Reg. de Propiedad.</label>
					<select name="reg_propiedad_const" class="form-control  select2" required="required">
						<option <?php if($objeto->row['regimenpropiedad'] == "Municipal Propio"){
							echo "selected='selected'";}?>
							value="Municipal Propio">Municipal Propio</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Nacional"){
							echo "selected='selected'";}?>
							value="Nacional">Nacional</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Estadal"){
							echo "selected='selected'";}?>
							value="Estadal">Estadal</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Priv. Individual"){
							echo "selected='selected'";}?>
							value="Priv. Individual">Priv. Individual</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Priv. Condominio"){
							echo "selected='selected'";}?>
							value="Priv. Condominio">Priv. Condominio</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Posesion"){
							echo "selected='selected'";}?>
							value="Posesion">Posesion</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Familiar"){
							echo "selected='selected'";}?>
							value="Familiar">Familiar</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Multifamiliar"){
							echo "selected='selected'";}?>
							value="Multifamiliar">Multifamiliar</option>
						<option <?php if($objeto->row['regimenpropiedad'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>
			</div>
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Estructurales de Construccion.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Soporte</label>
					<select class="form-control  select2" required="required" name="soporte_const">
						<option <?php if($objeto->row['soporteestructural'] == "Concreto Armado"){
							echo "selected='selected'";}?>
							value="Concreto Armado">Concreto Armado</option>
						<option <?php if($objeto->row['soporteestructural'] == "Metalica"){
							echo "selected='selected'";}?>
							value="Metalica">Metalica</option>
						<option <?php if($objeto->row['soporteestructural'] == "Madera"){
							echo "selected='selected'";}?>
							value="Madera">Madera</option>
						<option <?php if($objeto->row['soporteestructural'] == "Paredes de Carga"){
							echo "selected='selected'";}?>
							value="Paredes de Carga">Paredes de Carga</option>
						<option <?php if($objeto->row['soporteestructural'] == "Prefabricado"){
							echo "selected='selected'";}?>
							value="Prefabricado">Prefabricado</option>
						<option <?php if($objeto->row['soporteestructural'] == "Machones"){
							echo "selected='selected'";}?>
							value="Machones">Machones</option>
						<option <?php if($objeto->row['soporteestructural'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Techo</label>
					<select class="form-control  select2" required="required" name="techo_const">
						<option <?php if($objeto->row['techoestructural'] == "Concreto Armado"){
							echo "selected='selected'";}?>
							value="Concreto Armado">Concreto Armado</option>
						<option <?php if($objeto->row['techoestructural'] == "Metalica"){
							echo "selected='selected'";}?>
							value="Metalica">Metalica</option>
						<option <?php if($objeto->row['techoestructural'] == "Madera"){
							echo "selected='selected'";}?>
							value="Madera">Madera</option>
						<option <?php if($objeto->row['techoestructural'] == "Varas"){
							echo "selected='selected'";}?>
							value="Varas">Varas</option>
						<option <?php if($objeto->row['techoestructural'] == "Cerchas"){
							echo "selected='selected'";}?>
							value="Cerchas">Cerchas</option>
						<option <?php if($objeto->row['techoestructural'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Interna</label>
					<select class="form-control  select2" required="required" name="cubierta_interna">
						<option <?php if($objeto->row['cubiertainterna'] == "Plafon"){
							echo "selected='selected'";}?>
							value="Plafon">Plafon</option>
						<option <?php if($objeto->row['cubiertainterna'] == "Cielo Raso (Laminas)"){
							echo "selected='selected'";}?>
							value="Cielo Raso (Laminas)">Cielo Raso (Laminas)</option>
						<option <?php if($objeto->row['cubiertainterna'] == "Cielo Raso (Econom.)"){
							echo "selected='selected'";}?>
							value="Cielo Raso (Econom.)">Cielo Raso (Econom.)</option>
						<option <?php if($objeto->row['cubiertainterna'] == "Machihembrado (Madera)"){
							echo "selected='selected'";}?>
							value="Machihembrado (Madera)">Machihembrado (Madera)</option>
						<option <?php if($objeto->row['cubiertainterna'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Externa</label>
					<select class="form-control  select2" required="required" name="cubierta_externa">
						<option <?php if($objeto->row['cubiertaexterna'] == "Madera/Teja"){
							echo "selected='selected'";}?>
							value="Madera/Teja">Madera/Teja</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Placa/Teja"){
							echo "selected='selected'";}?>
							value="Placa/Teja">Placa/Teja</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Platabanda"){
							echo "selected='selected'";}?>
							value="Platabanda">Platabanda</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Tejas"){
							echo "selected='selected'";}?>
							value="Tejas">Tejas</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Caña Brava"){
							echo "selected='selected'";}?>
							value="Caña Brava">Caña Brava</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Asbesto"){
							echo "selected='selected'";}?>
							value="Asbesto">Asbesto</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Aluminio"){
							echo "selected='selected'";}?>
							value="Aluminio">Aluminio</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Zinc"){
							echo "selected='selected'";}?>
							value="Zinc">Zinc</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Acerolit"){
							echo "selected='selected'";}?>
							value="Acerolit">Acerolit</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Tabelon"){
							echo "selected='selected'";}?>
							value="Tabelon">Tabelon</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Acero Galbanizado"){
							echo "selected='selected'";}?>
							value="Acero Galbanizado">Acero Galbanizado</option>
						<option <?php if($objeto->row['cubiertaexterna'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>
		</div>


		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construccion.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pared</label>
					<select class="form-control  select2" required="required" name="tipo_pared">
						<option <?php if($objeto->row['paredestipo'] == "Bloque de Arcilla"){
							echo "selected='selected'";}?>
							value="Bloque de Arcilla">Bloque de Arcilla</option>
						<option <?php if($objeto->row['paredestipo'] == "Bloque de Cemento"){
							echo "selected='selected'";}?>
							value="Bloque de Cemento">Bloque de Cemento</option>
						<option <?php if($objeto->row['paredestipo'] == "Madera Aserrada"){
							echo "selected='selected'";}?>
							value="Madera Aserrada">Madera Aserrada</option>
						<option <?php if($objeto->row['paredestipo'] == "Laton"){
							echo "selected='selected'";}?>
							value="Laton">Laton</option>
						<option <?php if($objeto->row['paredestipo'] == "Ladrillo"){
							echo "selected='selected'";}?>
							value="Ladrillo">Ladrillo</option>
						<option <?php if($objeto->row['paredestipo'] == "Prefabricada"){
							echo "selected='selected'";}?>
							value="Prefabricada">Prefabricada</option>
						<option <?php if($objeto->row['paredestipo'] == "Bahareque"){
							echo "selected='selected'";}?>
							value="Bahareque">Bahareque</option>
						<option <?php if($objeto->row['paredestipo'] == "Adobe"){
							echo "selected='selected'";}?>
							value="Adobe">Adobe</option>
						<option <?php if($objeto->row['paredestipo'] == "Tapia"){
							echo "selected='selected'";}?>
							value="Tapia">Tapia</option>
						<option <?php if($objeto->row['paredestipo'] == "Vidrio"){
							echo "selected='selected'";}?>
							value="Vidrio">Vidrio</option>
						<option <?php if($objeto->row['paredestipo'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Acabado (Pared)</label>
					<select class="form-control  select2" required="required" name="acabado_pared">
						<option <?php if($objeto->row['paredesacabado'] == "Sin Friso"){
							echo "selected='selected'";}?>
							value="Sin Friso">Sin Friso</option>
						<option <?php if($objeto->row['paredesacabado'] == "Friso Liso"){
							echo "selected='selected'";}?>
							value="Friso Liso">Friso Liso</option>
						<option <?php if($objeto->row['paredesacabado'] == "Friso Rustico"){
							echo "selected='selected'";}?>
							value="Friso Rustico">Friso Rustico</option>
						<option <?php if($objeto->row['paredesacabado'] == "Obra Limpia"){
							echo "selected='selected'";}?>
							value="Obra Limpia">Obra Limpia</option>
						<option <?php if($objeto->row['paredesacabado'] == "Cemento Blanco"){
							echo "selected='selected'";}?>
							value="Cemento Blanco">Cemento Blanco</option>
						<option <?php if($objeto->row['paredesacabado'] == "Ceramica"){
							echo "selected='selected'";}?>
							value="Ceramica">Ceramica</option>
						<option <?php if($objeto->row['paredesacabado'] == "Vidrio Molido"){
							echo "selected='selected'";}?>
							value="Vidrio Molido">Vidrio Molido</option>
						<option <?php if($objeto->row['paredesacabado'] == "Yeso/Cal"){
							echo "selected='selected'";}?>
							value="Yeso/Cal">Yeso/Cal</option>
						<option <?php if($objeto->row['paredesacabado'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pintura (Pared)</label>
					<select class="form-control  select2" required="required" name="tipo_pintura_pared">
						<option <?php if($objeto->row['paredespintura'] == "Caucho"){
							echo "selected='selected'";}?>
							value="Caucho">Caucho</option>
						<option <?php if($objeto->row['paredespintura'] == "Oleo"){
							echo "selected='selected'";}?>
							value="Oleo">Oleo</option>
						<option <?php if($objeto->row['paredespintura'] == "Lechada"){
							echo "selected='selected'";}?>
							value="Lechada">Lechada</option>
						<option <?php if($objeto->row['paredespintura'] == "Pasta"){
							echo "selected='selected'";}?>
							value="Pasta">Pasta</option>
						<option <?php if($objeto->row['paredespintura'] == "Asbestina"){
							echo "selected='selected'";}?>
							value="Asbestina">Asbestina</option>
						<option <?php if($objeto->row['paredespintura'] == "Sin Pintura"){
							echo "selected='selected'";}?>
							value="Sin Pintura">Sin Pintura</option>
						<option <?php if($objeto->row['paredespintura'] == "Papel Tapiz"){
							echo "selected='selected'";}?>
							value="Papel Tapiz">Papel Tapiz</option>
					</select>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Inst. Electricas (Pared)</label>
					<select class="form-control  select2" required="required" name="instalacciones_electricas">
						<option <?php if($objeto->row['paredesinstelectricas'] == "Embutidas"){
							echo "selected='selected'";}?>
							value="Embutidas">Embutidas</option>
						<option <?php if($objeto->row['paredesinstelectricas'] == "Externa"){
							echo "selected='selected'";}?>
							value="Externa">Externa</option>
						<option <?php if($objeto->row['paredesinstelectricas'] == "Industrial"){
							echo "selected='selected'";}?>
							value="Industrial">Industrial</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Pisos</label>
					<select class="form-control  select2" required="required" name="pisos_construccion">
						<option <?php if($objeto->row['pisotipo'] == "Cemento Pulido"){
							echo "selected='selected'";}?>
							value="Cemento Pulido">Cemento Pulido</option>
						<option <?php if($objeto->row['pisotipo'] == "Cemento Rustico"){
							echo "selected='selected'";}?>
							value="Cemento Rustico">Cemento Rustico</option>
						<option <?php if($objeto->row['pisotipo'] == "Sin Piso (Tierra)"){
							echo "selected='selected'";}?>
							value="Sin Piso (Tierra)">Sin Piso (Tierra)</option>
						<option <?php if($objeto->row['pisotipo'] == "Ceramica"){
							echo "selected='selected'";}?>
							value="Ceramica">Ceramica</option>
						<option <?php if($objeto->row['pisotipo'] == "Granito"){
							echo "selected='selected'";}?>
							value="Granito">Granito</option>
						<option <?php if($objeto->row['pisotipo'] == "Ladrillos"){
							echo "selected='selected'";}?>
							value="Ladrillos">Ladrillos</option>
						<option <?php if($objeto->row['pisotipo'] == "Parque"){
							echo "selected='selected'";}?>
							value="Parque">Parque</option>
						<option <?php if($objeto->row['pisotipo'] == "Marmol"){
							echo "selected='selected'";}?>
							value="Marmol">Marmol</option>
						<option <?php if($objeto->row['pisotipo'] == "Vinil"){
							echo "selected='selected'";}?>
							value="Vinil">Vinil</option>
						<option <?php if($objeto->row['pisotipo'] == "Mosaico"){
							echo "selected='selected'";}?>
							value="Mosaico">Mosaico</option>
						<option <?php if($objeto->row['pisotipo'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
				</div>
				<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construccion (Baños)</strong>
				</p>
				<div class="col-md-3">
				<div class="form-group">
					<label>WC</label>
					<input class="form-control number" name="banoWC" value="<?php echo $objeto->row['banowc'];?>" type="text" required="required">	
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comunitario</label>
					<input class="form-control number" name="bano_comunitario" value="<?php echo $objeto->row['banocomunitario'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Ducha</label>
					<input class="form-control number" name="bano_ducha" value="<?php echo $objeto->row['banoducha'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Lavamanos</label>
					<input class="form-control number" name="bano_lavamanos" value="<?php echo $objeto->row['banolavamanos'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Bañera</label>
					<input class="form-control number" name="bano_banera" value="<?php echo $objeto->row['banobanera'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Bidet</label>
					<input class="form-control number" name="bano_bidet" value="<?php echo $objeto->row['banobidet'];?>" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label>Letrina</label>
					<input class="form-control number" name="bano_letrina" value="<?php echo $objeto->row['banoletrina'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Cer. 1era</label>
					<input class="form-control number" name="bano_ceramica1" value="<?php echo $objeto->row['banoceramica1'];?>" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Cer. 2da</label>
					<input class="form-control number" name="bano_ceramica2" value="<?php echo $objeto->row['banoceramica2'];?>" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construccion (Puertas)</strong>
				</p>
				<br>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Metalica</label>
					<input class="form-control number" name="puerta_metalica" value="<?php echo $objeto->row['puertametalica'];?>" placeholder="" type="text" required="required">	
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Madera Cepillada</label>
					<input class="form-control number" name="puerta_madera_cep" value="<?php echo $objeto->row['puertamaderacepillada'];?>" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Madera Ent. Ecoc.</label>
					<input class="form-control number" name="puerta_madera_ecoc" value="<?php echo $objeto->row['puertamadeconomica'];?>" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Entamborada Fina</label>
					<input class="form-control number" name="puerta_entamborada" value="<?php echo $objeto->row['puertaentamboradafina'];?>" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>De Seguridad</label>
					<input class="form-control number" name="puerta_seguridad" value="<?php echo $objeto->row['puertaseguridad'];?>" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Otra</label>
					<input class="form-control" name="puerta_otra" value="<?php echo $objeto->row['puertaotra'];?>" placeholder="" type="text" >
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad</label>
					<input class="form-control number" name="puerta_otra_cantidad" value="<?php echo $objeto->row['puertaotrac'];?>" placeholder="" type="text" >
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construccion (Ventanas)</strong>
				</p>
				<br>
			</div>


			<div class="col-md-12">
				<div class="col-md-2">
					<label>Ventanal (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_hierro" value="<?php echo $objeto->row['ventanalhierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Ventanal (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_aluminio" value="<?php echo $objeto->row['ventanalaluminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Ventanal (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_madera" value="<?php echo $objeto->row['ventanalmadera'];?>" placeholder="M" type="text" required="required">
					</div>
				</div>


				<div class="col-md-2">
					<label>Basculante (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_hierro" value="<?php echo $objeto->row['basculantehierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Basculante (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_aluminio" value="<?php echo $objeto->row['basculantealuminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Basculante (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_madera" value="<?php echo $objeto->row['basculantemadera'];?>" placeholder="M" type="text" required="required">
					</div>
				</div>


				<div class="col-md-2">
					<label>Batiente (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_hierro" value="<?php echo $objeto->row['satientehierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Batiente (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_aluminio" value="<?php echo $objeto->row['satientealuminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Batiente (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_madera" value="<?php echo $objeto->row['satientemadera'];?>" placeholder="M" type="text" required="required" >
					</div>
				</div>


				<div class="col-md-2">
					<label>Celosia (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_hierro" value="<?php echo $objeto->row['celosiahierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Celosia (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_aluminio" value="<?php echo $objeto->row['celosiaaluminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Celosia (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_madera" value="<?php echo $objeto->row['celosiamadera'];?>" placeholder="M" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_hierro" value="<?php echo $objeto->row['correderahierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_aluminio" value="<?php echo $objeto->row['correderaaluminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_madera" value="<?php echo $objeto->row['correderamadera'];?>" placeholder="M" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_hierro" value="<?php echo $objeto->row['panoramicahierro'];?>" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_aluminio" value="<?php echo $objeto->row['panoramicaaluminio'];?>" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_madera" value="<?php echo $objeto->row['panoramicamadera'];?>" placeholder="M" type="text" required="required">
					</div>
				</div>


			</div>

			<div class="col-md-12">
					<br>
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Otros Datos (Const.)</strong>
				</p>
				<br>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Año Construccion</label>
					<input class="form-control number" name="anos_const" value="<?php echo $objeto->row['anoconstruccion'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Porcentaje (%) Refaccion</label>
					<input class="form-control number" name="refaccion_const" value="<?php echo $objeto->row['porcentajerefaccion'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>N° de Niveles</label>
					<input class="form-control number" name="numeroniveles_const" value="<?php echo $objeto->row['numeroniveles'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Años Refaccion</label>
					<input class="form-control number" name="anosrefaccion_const" value="<?php echo $objeto->row['anorefaccion'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Edad Efectiva</label>
					<input class="form-control number" name="edadefectiva_const" value="<?php echo $objeto->row['edadefectiva'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>


			<div class="col-md-6">
				<div class="form-group">
					<label>N° de Edificaciones</label>
					<input class="form-control number" name="edificaciones_const" value="<?php echo $objeto->row['numeroedificacion'];?>" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Estado de Conservacion</label>
					<select class="form-control  select2" required="required" name="estado_conservacion_const">
						<option <?php if($objeto->row['estadoconservacion'] == "Excelente"){
							echo "selected='selected'";}?>
							value="Excelente">Excelente</option>
						<option <?php if($objeto->row['estadoconservacion'] == "Bueno"){
							echo "selected='selected'";}?>
							value="Bueno">Bueno</option>
						<option <?php if($objeto->row['estadoconservacion'] == "Regular"){
							echo "selected='selected'";}?>
							value="Regular">Regular</option>
						<option <?php if($objeto->row['estadoconservacion'] == "Malo"){
							echo "selected='selected'";}?>
							value="Malo">Malo</option>
						<option <?php if($objeto->row['estadoconservacion'] == "Otro"){
							echo "selected='selected'";}?>
							value="Otro">Otro</option>
					</select>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="observaciones_const" cols="" rows="" required="required"><?php echo $objeto->row['observacionconstruccion'];?></textarea>
				</div>
			</div>

			
		</div>





	<div class="row">
		 <br><br>
		<div class="col-md-12">
		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ubicacion de parcela (Linderos Actuales).</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Largo del Terreno</label>
					<input class="form-control number" name="largo_terreno"   value="<?php echo $objeto->row['largo_terreno'];?>" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ancho del Terreno</label>
					<input class="form-control number" name="ancho_terreno"   value="<?php echo $objeto->row['ancho_terreno'];?>" type="text" required="required">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Norte</label>
					<textarea class="form-control " name="norte_linderos" cols="" rows="" required="required"><?php echo $objeto->row['linderonorte'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sur</label>
					<textarea class="form-control " name="sur_linderos" cols="" rows="" required="required"><?php echo $objeto->row['linderosur'];?></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Este</label>
					<textarea class="form-control " name="este_linderos" cols="" rows="" required="required"><?php echo $objeto->row['linderoeste'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Oeste</label>
					<textarea class="form-control " name="oeste_linderos" cols="" rows="" required="required"><?php echo $objeto->row['linderooeste'];?></textarea>
				</div>
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Coordenadas UTM (REGVEN)</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Norte (M)</label>
					<textarea class="form-control number" name="norte_coord" cols="" rows="" required="required"><?php echo $objeto->row['coordenadasutmnorte'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Este (M)</label>
					<textarea class="form-control number" name="este_coord" cols="" rows="" required="required"><?php echo $objeto->row['coordenadasutmeste'];?></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Huso</label>
					<textarea class="form-control number" name="huso_coord" cols="" rows="" required="required"><?php echo $objeto->row['coordenadasutmhuso'];?></textarea>
				</div>
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Inspectores que realizaron la Visita.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha de Primera Visita</label>
					<input class="form-control " id="fecha-visita" name="fechavisita" value="<?php echo $objeto->row['fechavisita'];?>" placeholder="00-00-0000" type="text" required="required"  readonly="">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha del lavantamiento</label>
					<input class="form-control " id="fecha-levantamiento" name="fechalevantamiento" value="<?php echo $objeto->row['fechalevantamiento'];?>" placeholder="00-00-0000" type="text" required="required"  readonly="">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Elaborado Por</label>
					<select class="form-control select2 " name="inspertor_elaborador" required="required">
						<?php 
							$i=0;
							$persona3= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'");
						?>			
						<?php while($reg=pg_fetch_object($persona3)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['peid'] == $reg->id){
							echo "selected='selected'";}?>
							value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Revisado Por</label>
					<select class="form-control select2 " name="inspertor_revisor" required="required">
						<?php 
							$i=0;
							$persona2= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'");
						?>			
						<?php while($reg=pg_fetch_object($persona2)){?>
						<?php $i++;?>
						
						<option <?php if($objeto->row['prid'] == $reg->id){
							echo "selected='selected'";}?>
							value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="observaciones_ficha" cols="" rows="" required="required"><?php echo $objeto->row['fichaobservacion'];?></textarea>
				</div>

				<div align="center"><br>
				<a href="./" class="btnNew"><span class="btn btn-default" title="Cancelar"><i class="ace-icon fa fa-remove"></i>Cancelar</span></a>
				<button type="submit" class="btn btn-primary" title=""><i class="fa fa-pencil"></i> Actualizar</button>

			</div>
			<br>
			</form>
				</div>
			</div>
			</div>
	</div>



			
</div>
</div>
<!-- PAGE CONTENT ENDS -->
</div>
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
</div>
</div>
<?php include('../layout/footer.php');?>


<script>

	$(document).ready(function(event) {
	 sector = $("#sector_valor").val(); 
	 ficha = $("#ficha").val(); 
	//alert (ficha);
	cargarSector(sector, ficha);

	function cargarSector(sector){
				 sector = $("#sector_valor").val();
				 ficha = $("#ficha").val(); 
				$("#calles_sector").html("<i class='fa fa-spinner'></i> Cargando...");
	        	$.post("calles_sector.php", {"sector":sector, "ficha":ficha}, function(data){
					$("#calles_sector").html(data);
				});
	    	}
});


$(".select2").select2();
$(".select22").select2();


$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#registro-fecha").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-ficha").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-visita").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-levantamiento").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

</script>

<script type="text/javascript">
	$(".tipocalle").select2();

</script>
</body>

</html>
