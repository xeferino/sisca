<?php 
include_once("seguridad.php");
include_once("clases/conexion.php");
include_once("clases/ficha.php");
include_once("clases/linderos.php");
include_once("clases/dashboard.php");

$ficha = new Ficha();
$ficha->fichaEstadistica();

$lindero = new Linderos();
$lindero->linderoEstadistica();

$dashboard = new Dashboard();

$dashboard->ultimosRegistrosFicha();

$dashboard->ultimosRegistroslinderos();

$dashboard->ultimosRegistrosolvencias();

$dashboard->ultimosRegistrosInspecciones();

$dashboard->sectoresCount();

$dashboard->callesCount();

$dashboard->parroquiaCount();

$dashboard->inspectoresCount();


$dashboard->fichasCount();

$dashboard->inspeccionesCount();

$dashboard->solvenciasCount();

$dashboard->certificacionesCount();

//var_dump($dashboard->ultimosRegistrosFicha());die();


?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

	<?php include('../layout/banner.php');?>
<style type="text/css">
	.black{
		color: #999;
	}
</style>
	<div class="main-container ace-save-state" id="main-container">
		<script type="text/javascript">
			try{ace.settings.loadState('main-container')}catch(e){}
		</script>

		<?php $menu=1; include('../layout/menu.php');?>

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Home</a>							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="widget-box widget-color-red2">
									<div class="widget-header widget-header-small">
										<h5 class="widget-title smaller">Bienvenido al Sistema de Informaci&oacute;n para el Registro y Control de los Datos Catastrales del Municipio Ribero, Edo. Sucre.</h5>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											Este sistema tiene como objeto prestar servicios de consultas de inforamci&oacute;n de la data de castastro del Municipio, en funci&oacute;n de mejorar los tiempos de respuestas de las solicitudes que se realizan a diario en la oficina.
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
<!-- 
								<div class="col-sm-6 infobox-container">
									<div id="morris-donut-chart"></div>	
								</div> -->
								
								<div class="col-sm-6 infobox-container">
										<div class="infobox infobox-green">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-share"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->sector;?></span>
												<div class="infobox-content"><h4>Sectores</h4></div>
											</div>

										</div>

										<div class="infobox infobox-blue">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-share"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->calle;?></span>
												<div class="infobox-content"><h4>Calles</h4></div>
											</div>

										</div>

										<div class="infobox infobox-pink">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-share"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->parroquia;?></span>
												<div class="infobox-content"><h4>Parroquias</h4></div>
											</div>
										</div>

										<div class="infobox infobox-red">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-user"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->inspector;?></span>
												<div class="infobox-content"><h4>Inspectores</h4></div>
											</div>
										</div>	
									</div>

									<div class="col-sm-6 infobox-container">
										<div class="infobox infobox-green">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-map"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->fichas;?></span>
												<div class="infobox-content"><h4>Fichas</h4></div>
											</div>

										</div>

										<div class="infobox infobox-blue">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-map"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->inspeccion;?></span>
												<div class="infobox-content"><h4>Inspecciones</h4></div>
											</div>

										</div>

										<div class="infobox infobox-pink">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-map"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->solvencia;?></span>
												<div class="infobox-content"><h4>Solvencias</h4></div>
											</div>
										</div>

										<div class="infobox infobox-red">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-user"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number"><?php echo  $dashboard->certificacion;?></span>
												<div class="infobox-content"><h4>Certificaciones</h4></div>
											</div>
										</div>	
									</div>
									
							</div>
						</div>
						<br>

								<div class="row">
									<div class="col-sm-12">
										<div class="widget-box transparent" id="recent-box">
									<div class="col-sm-6">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title lighter">
													<a href="#" class="btn btn-app btn-grey  btn-xs">
														<i class="ace-icon fa 	fa-bar-chart-o   bigger-160"></i>
													</a>
													<b><span class="black"> Ultimas 10 Fichas Registradas</span></b>
												</h4>
											</div>

											<div class="widget-body" style="display: block;">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>#
																</th>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>N° Archivo
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>N° Catastral
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Fecha Insc.
																</th>
															</tr>
														</thead>

														<tbody>
														<?php 
															$i = 0;
															while($row=pg_fetch_object($dashboard->ficha)){
																$i++;
															?>
															<tr>
																<td><?php echo $i ?></td>
																<td>
																	<button class="btn btn-minier btn-purple"><?php echo $row->numeroarchivo ?></button>
																	</td>
																<td>
																	<a class="" href="reportes/fichaCatastralPdf.php?id=<?php echo $row->id; ?>" target="_blank" title="PDF">

																	<button class="btn btn-minier btn-yellow"><?php  echo $row->numerocatastral  ?></button></a>
																	</td>
																<td>
																	<button class="btn btn-minier btn-grey"><?php  echo $row->fechainscripcion  ?></button>
																</td>
															</tr>
														<?php } ?>
														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div>

									<div class="col-sm-6">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title lighter">
													<a href="#" class="btn btn-app btn-grey  btn-xs">
														<i class="ace-icon fa 	fa-bar-chart-o   bigger-160"></i>
													</a>
													<b><span class="black"> Ultimas 10 Certificaciones de Linderos</span></b>
												</h4>
											</div>

											<div class="widget-body" style="display: block;">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>#
																</th>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>Propietario
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Ubicaci&oacute;n
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Fecha
																</th>
															</tr>
														</thead>

														<tbody>
														<?php 
															$i = 0;
															while($row=pg_fetch_object($dashboard->linderos)){
																$i++;
															?>
															<tr>
																<td><?php echo $i ?></td>
																<td>
																	<a class="" href="reportes/certificacion_linderos.php?id=<?php echo $row->id; ?>" target="_blank" title="PDF">
																	<button class="btn btn-minier btn-purple"><?php echo $row->ppn."-".$row->ppc; ?></button>
																	</td>
																	</a>
																<td>
																	<button class="btn btn-minier btn-yellow"><?php  echo $row->ubicacion  ?></button>
																	</td>
																<td>
																	<button class="btn btn-minier btn-grey"><?php  echo $row->fecha  ?></button>
																</td>
															</tr>
														<?php } ?>
														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div>
							</div>
								<br><br><br><br>
							<div class="col-sm-12">
									<div class="col-sm-6">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title lighter">
													<a href="#" class="btn btn-app btn-grey  btn-xs">
														<i class="ace-icon fa 	fa-bar-chart-o   bigger-160"></i>
													</a>
													<b><span class="black"> Ultimas 10 Solvencias Pagadas</span></b>
												</h4>
											</div>

											<div class="widget-body" style="display: block;">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>#
																</th>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>Propietario
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Anualidad
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Estatus
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Fecha
																</th>
															</tr>
														</thead>

														<tbody>
														<?php 
															$i = 0;
															while($row=pg_fetch_object($dashboard->solvencias)){
																$i++;
															?>
															<tr>
																<td><?php echo $i ?></td>
																<td>
																	<a class="" href="reportes/solvencia_pdf.php?id=<?php echo $row->id; ?>" target="_blank" title="PDF">
																	<button class="btn btn-minier btn-purple"><?php echo $row->ppn."-".$row->ppc; ?></button>
																	</td>
																	</a>
																<td>
																	<button class="btn btn-minier btn-yellow"><?php  echo $row->anualidad  ?></button>
																</td>

																<td>
																	<button class="btn btn-minier btn-success"><?php  echo $row->estatus  ?></button>
																</td>

																<td>
																	<button class="btn btn-minier btn-grey"><?php  echo $row->fecha  ?></button>
																</td>
															</tr>
														<?php } ?>
														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div>

									<div class="col-sm-6">
										<div class="widget-box transparent">
											<div class="widget-header widget-header-flat">
												<h4 class="widget-title lighter">
													<a href="#" class="btn btn-app btn-grey  btn-xs">
														<i class="ace-icon fa 	fa-bar-chart-o   bigger-160"></i>
													</a>
													<b><span class="black"> Ultimas 10 Inspecciones Realizadas</span></b>
												</h4>
											</div>

											<div class="widget-body" style="display: block;">
												<div class="widget-main no-padding">
													<table class="table table-bordered table-striped">
														<thead class="thin-border-bottom">
															<tr>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>#
																</th>
																<th>
																	<i class="ace-icon fa fa-caret-right red"></i>Propietario
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Tipo
																</th>

																<th class="hidden-480">
																	<i class="ace-icon fa fa-caret-right red"></i>Fecha
																</th>
															</tr>
														</thead>

														<tbody>
														<?php 
															$i = 0;
															while($row=pg_fetch_object($dashboard->inspecciones)){
																$i++;
															?>
															<tr>
																<td><?php echo $i ?></td>
																<td>
																	<a class="" href="reportes/inspecciones_pdf.php?id=<?php echo $row->id; ?>" target="_blank" title="PDF">
																	<button class="btn btn-minier btn-purple"><?php echo $row->ppn."-".$row->ppc; ?></button>
																	</td>
																	</a>
																<td>
																	<button class="btn btn-minier btn-yellow"><?php  echo $row->tipo  ?></button>
																</td>

																<td>
																	<button class="btn btn-minier btn-grey"><?php  echo $row->fecha  ?></button>
																</td>
															</tr>
														<?php } ?>
														</tbody>
													</table>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
									</div>
								</div>
											

											<!-- /.widget-body -->
										<!-- /.col -->
									</div><!-- /.row -->
									<!-- PAGE CONTENT ENDS -->
								</div><!-- /.col -->
							</div><!-- /.row -->
						<!-- /.page-content -->
					</div>
				</div><!-- /.main-content -->

				<?php include('../layout/footer.php');?>
				<script type="text/javascript">

					Morris.Bar({
						element: 'morris-bar-chart',
						data: [
						{ y: 'Ene', a: 100}, 
						{ y: 'Feb', a: 75},
						{ y: 'Mar', a: 50},
						{ y: 'Abr', a: 75},
						{ y: 'May', a: 50},
						{ y: 'Jun', a: 75},
						{ y: 'Jul', a: 100},
						{ y: 'Ago', a: 100},
						{ y: 'Sep', a: 100},
						{ y: 'Oct', a: 100},
						{ y: 'Nov', a: 100},
						{ y: 'Dic', a: 100}
						],
						xkey: 'y',
						ykeys: ['a'],
						labels: ['Ficha Registradas'],
						barColors: ['#0b62a4'],
						hideHover: 'auto',
						resize: true
					});


					Morris.Line({
						element: 'morris-line-chart',
						data: [{
							y: '2006',
							a: 100,
							b: 90
						}, {
							y: '2007',
							a: 75,
							b: 65
						}, {
							y: '2008',
							a: 50,
							b: 40
						}, {
							y: '2009',
							a: 75,
							b: 65
						}, {
							y: '2010',
							a: 50,
							b: 40
						}, {
							y: '2011',
							a: 75,
							b: 65
						}, {
							y: '2012',
							a: 100,
							b: 90
						}],
						xkey: 'y',
						ykeys: ['a', 'b'],
						labels: ['Series A', 'Series B'],
						lineColors: ['#e91313', '#0b62a4'],
						hideHover: 'auto',
						resize: true
					});

					Morris.Area({
						element: 'morris-area-chart',
						data: [{
							period: '2010 Q1',
							iphone: 2666,
							ipad: null,
							itouch: 2647
						}, {
							period: '2010 Q2',
							iphone: 2778,
							ipad: 2294,
							itouch: 2441
						}, {
							period: '2010 Q3',
							iphone: 4912,
							ipad: 1969,
							itouch: 2501
						}, {
							period: '2010 Q4',
							iphone: 3767,
							ipad: 3597,
							itouch: 5689
						}, {
							period: '2011 Q1',
							iphone: 6810,
							ipad: 1914,
							itouch: 2293
						}, {
							period: '2011 Q2',
							iphone: 5670,
							ipad: 4293,
							itouch: 1881
						}, {
							period: '2011 Q3',
							iphone: 4820,
							ipad: 3795,
							itouch: 1588
						}, {
							period: '2011 Q4',
							iphone: 15073,
							ipad: 5967,
							itouch: 5175
						}, {
							period: '2012 Q1',
							iphone: 10687,
							ipad: 4460,
							itouch: 2028
						}, {
							period: '2012 Q2',
							iphone: 8432,
							ipad: 5713,
							itouch: 1791
						}],
						xkey: 'period',
						ykeys: ['iphone', 'ipad', 'itouch'],
						labels: ['iPhone', 'iPad', 'iPod Touch'],
						pointSize: 2,
						hideHover: 'auto',
						resize: true
					});


				</script>

				<script type="text/javascript">

					Morris.Donut({
						element: 'morris-donut-chart',
						data: [{
							value: <?php echo $lindero->estadistica[0];?>,
							color: "#e91313",
							highlight: "#000",
							label: "Certificacion de Linderos"
						},
						{
							value: <?php echo $ficha->estadistica[0];?>,
							color: "#0b62a4",
							highlight: "#000",
							label: "Fichas Catastrales"
						}],
						resize: true
					});

				</script>

			</body>
			</html>
