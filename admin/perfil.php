<?php include_once("seguridad.php"); ?>
<?php 
	include_once("clases/motor.php");
	$objeto = new Persona();

	if(isset($_GET['id']))
	{
		$id= $_SESSION['conectado'];

		$objeto->perfil($id);
		
		//echo $id;
	}
	else
	{
		header("Location: ./");
	}
		
	if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {
	
		$id= $_SESSION['conectado'];
		$clavea =  $_POST['clavea'];
		$clave1 =  $_POST['clave1'];
		$clave2 =  $_POST['clave2'];
		$objeto->update_acces($id, $clavea, $clave1, $clave2);
		
	}

?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-user"></i> Usuarios</li> <li class="active"><i class="ace-icon fa fa-user"></i>  Perfil</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="usuarios.php"><span class="btn btn-primary pull-right" title="Lista de Usuario"><i class="ace-icon fa fa-users"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-user"></i> <?php echo $objeto->role;?></h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<i class="ace-icon fa 	fa-edit"></i> <strong>Actualize sus Datos de Acceso.</strong>
														</p>
													
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Datos Actualizados.",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
															
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Las Claves no Coinciden.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: " Error, en la Clave Actual.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==4){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos En Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
						<?php }?>
							<form action="" method="post" id="form-usuario">
								<input type="hidden" name="submit" value="edit" />
									<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											<label class="block clearfix"> Cuenta 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="cuenta" value="<?php echo  $objeto->cuenta;?>"  placeholder="" type="text" disabled="disabled">
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
								<label class="block clearfix"> Estatus 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="cuenta" value="<?php echo $objeto->estatus;?>"  placeholder="" type="text" disabled="disabled">
															<i class="ace-icon fa fa-check-circle-o"></i>
														</span>
													</label>											
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											<label class="block clearfix"> Clave Actual
														<span class="block input-icon input-icon-right">
															<input type="password" name="clavea" id="ca" class="form-control" placeholder="Password" required="required" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
												<label class="block clearfix"> Clave Nueva
														<span class="block input-icon input-icon-right">
															<input type="password" name="clave1" id="cn" class="form-control" placeholder="Password" required="required" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											<label class="block clearfix"> Confirmar Clave
														<span class="block input-icon input-icon-right">
															<input type="password" name="clave2" id="cc" class="form-control" placeholder="Password" required="required" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
			
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											
											<button type="submit" class="btn btn-primary pull-right" title="Actualizar"><i class="fa fa-pencil"></i> Actualizar</button>
										</div>
										<div class="form-group col-sm-2"></div>
									<div>
										<br><br><br>
									</div>
								</form>
							
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			
			<?php include('../layout/footer.php');?>
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('clave', function(value, element){
					return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
				});
			    $("#form-usuario").validate({
			        rules: {
			            ca: { required: true, clave: true, minlength: 4, maxlength:8 },
			            cn: { required:true, clave: true, minlength: 4,  maxlength:8 },
			            cc: { required:true, clave: true, minlength: 4,  maxlength:8 },
			        },
			        messages: {
			            ca: {
			            		required: 'la clave actual es requerida',
			            		clave: 'la clave solo acepta numeros y letras ',
			            		minlength:'la clave debe tener como minimo 4 caracter',
			            		maxlength:'la clave tiene un maximo permitido son 8 caracteres'
			            },
			            cn: {
			            		required: 'la clave nueva es requerida',
			            		clave: 'la clave solo acepta numeros y letras ',
			            		minlength:'la clave debe tener como minimo 4 caracter',
			            		maxlength:'la clave tiene un maximo permitido son 8 caracteres'
			            },
			            cc: {
			            		required: 'el campo es requerida',
			            		clave: 'la clave solo acepta numeros y letras ',
			            		minlength:'la clave debe tener como minimo 4 caracter',
			            		maxlength:'la clave tiene un maximo permitido son 8 caracteres'
			            },
			        }
			    });
			    $("#form-usuario").submit(function(){
			        $post("perfil.php");
			        return false;
			    });
			});
		</script>
			
	</body>
</html>
