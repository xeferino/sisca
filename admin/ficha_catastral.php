<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");

$objeto = new Ficha();
$objeto->servicios();

$parroquia = new Parroquia();
$parroquia->parroquias();

if (isset($_POST['submit']) && $_POST['submit'] == 'new') {

	$array = array($_POST);
	$objeto->registrarFichaCastastral($array);	
	
}

	
?>
<!DOCTYPE html>
<html lang="en">
<?php include('../layout/head.php');?>
<style type="text/css">
.select2{
width: 100% !important;
}

.error-message, label.error { color: #a10052;  display: block; font-size: 1em !important;font-weight:bold; }
</style>
<body class="no-skin">
<?php include('../layout/banner.php');?>
<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=2; include('../layout/menu.php');?>
<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="#">Home</a>							</li>
<li class="active">Ficha Catastral</li>
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">

<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
<a href="fichas.php"><span class="btn btn-primary pull-right" title="Listado de Fichas Catastrales"><i class="ace-icon fa fa-list"></i></span></a><br><br><br>
<div class="widget-box">
<div class="widget-header widget-header-blue widget-header-flat">
	<h4 class="widget-title lighter"><i class="ace-icon fa 	fa-file-text-o"></i> Nueva Ficha Catastral</h4>
</div>
<div class="widget-body">
			<?php if($objeto->mensaje==1){?>
			<script>
				$(document).ready(function() {
					swal({
						title: "",
						text: "Ficha Registrada Exitosamente",
						type: "success",
						confirmButtonText: "Aceptar",
						timer: "3000"
					}/*,
					function(){
						$(location).attr('href','http:fichas.php');
					}*/);

				});
			</script>
			<?php }?>
	<div class="row">
		<form action="" method="post" id="fom-fichacatastral">
			<input type="hidden" name="submit" value="new" />
		<!-- FIN DE SEGUNDA COLUMNA -->
		<br><br>
		<div class="col-md-12">
		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n Solicitada.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Control Archivo</label>
					<input class="form-control number" name="control_archivo" id="control_archivo"  value="" placeholder="EJ:02" type="text" required="required" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha Inscripci&oacute;n</label>
					<input class="form-control " id="fecha-ficha" name="fecha_ficha" value="" placeholder="00-00-0000" type="text" required="required"  readonly="" required="required">
				</div>
				</div>

			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n de la Ubicacion Comunitaria.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Regi&oacute;n Federal de Gobierno</label>
					<input class="form-control " name="region_rederal_gobierno"   value="" type="text" required="required" >
					
					
				</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>Comuna</label>
					<input class="form-control " name="comuna"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2" style="text-align: center;">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_comuna"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
					<label>Consejo Comunal</label>
					<input class="form-control " name="consejo_comunal"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2" style="text-align: center;">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_consejo_comunal"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					<label>Comite de Tierra Urbano</label>
					<input class="form-control " name="comite_tierra_urbano"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-2" style="text-align: center;">
					<div class="form-group">
					<label>N°</label>
					<input class="form-control number" name="numero_comite_urbano"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-12">

				<div class="col-md-6">
					<div class="form-group">
					<label>Registro MVV</label>
					<input class="form-control number" name="registro_mvv"   value="" type="text" required="required">
				</div>
				</div>

			</div>
			</div>


			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n de Ubicacion Del Inmueble.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<select class="form-control  select2" name="parroquia" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$parroquia1= pg_query("SELECT * FROM  tb_parroquia");
						?>			
						<?php while($reg=pg_fetch_object($parroquia1)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->codigo;?>"><?php echo $reg->nombre;?></option>
						<?php }?>
					</select>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<input class="form-control " name="ciudad"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<input class="form-control " name="localidad"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizaci&oacute;n</label>
					<input class="form-control " name="urbanizacion"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<input class="form-control " name="consejo_residencial"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<input class="form-control " name="barrio"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<input class="form-control number" name="numero_civico"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<select class="form-control  select2" id="sector" name="sector" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$pedul= pg_query("SELECT * FROM  tb_pedul");
							?>			
						<?php while($reg=pg_fetch_object($pedul)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->id;?>"><?php echo $reg->nombre;?></option>
						<?php }?>
					</select>
				</div>
				</div>

				<div class="row">
					<div id="calles_sector"></div>
				</div>
	
				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<input class="form-control " name="punto_referencia"   value="" type="text" required="required">
				</div>
				</div>

				
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los datos del Registro Publico.</strong>
				</p>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
						<label>Numero</label>
						<textarea class="form-control required number" name="numero_registro" cols="" rows="" ></textarea>
					</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Folio</label>
						<textarea class="form-control " name="folio_registro" cols="" rows="" required="required"></textarea>
					</div>
					</div>
			   </div>
			   <div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
						<label>Tomo</label>
						<textarea class="form-control " name="tomo_registro" cols="" rows="" required="required"></textarea>
					</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Protocolo</label>
						<textarea class="form-control " name="protocolo_registro" cols="" rows="" required="required"></textarea>
					</div>
					</div>
				</div>

				<div class="col-md-12">

					<div class="col-md-6">
						<div class="form-group">
						<label>Fecha</label>
						<input class="form-control " id="registro-fecha" name="registro_fecha" value="" placeholder="00-00-0000" type="text"  rows="" required="required">
					</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Area (M2) Terreno</label>
						<textarea class="form-control number" name="registro_aream2terreno" id="at" cols="" rows="" required="required"></textarea>
						
					</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
						<label>Area (M2) TConstrucci&oacute;n</label>
						<textarea class="form-control number" name="registro_aream2const" cols="" rows="" required="required"></textarea>
					</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
						<label>Monto (BS)</label>
						<textarea class="form-control number" name="registro_monto" cols="" rows="" required="required"></textarea>
					</div>
					</div>
				</div>

			</div>



				<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione el Ocupante, en caso de ser el mismo propietario del inmueble no es necesario llenar el Formulario inferior.</strong>
				</p>
			</div>
			<div class="col-md-12">
							<div class="form-group">
					<label>Ocupante</label>
					<select class="form-control   select2" name="ocupante" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$persona1= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante' and chequeado!=1");
						?>			
						<?php while($reg=pg_fetch_object($persona1)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>

					
				</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n del Propietario.</strong>
				</p>
				<div class="col-md-12">
							<div class="form-group">
					<label>Propietario</label>
					<select class="form-control select2" name="propietario" id="propietario">
					<option value="0">--Selecccione--</option>
						<?php 
							$i=0;
							$persona= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'");
						?>			
						<?php while($reg=pg_fetch_object($persona)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
					
				</div>

			</div>
		<div class="col-md-12" id="propietario_direccion">
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Direcci&oacute;n del Propietario.</strong>
				</p>
			
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<textarea class="form-control" name="parroquia_persona" id="parroquia_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<textarea class="form-control" name="ciudad_persona" id="ciudad_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<textarea class="form-control" id="localidad_persona" name="localidad_persona" cols="" rows=""></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizaci&oacute;n</label>
					<textarea class="form-control" id="urbanizacion_persona" name="urbanizacion_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<textarea class="form-control" id="consejo_residencial_persona" name="consejo_residencial_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<textarea class="form-control"  id="barrio_persona" name="barrio_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<textarea class="form-control number" id="numero_civico_persona" name="numero_civico_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Nombre Calle</label>
					<textarea class="form-control" id="nombre_calle_persona" name="nombre_calle_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<textarea class="form-control" id="sector_persona" name="sector_persona" cols="" rows=""></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tipo de Calle</label>
						<select class="form-control  tipocalle" name="tipo_calle_persona" id="tipo_calle_persona" >
							<option value="">--Selecccione--</option>
							<option value="Av">Av</option>
							<option value="Clle">Clle</option>
							<option value="Crr">Crr</option>
							<option value="Trav">Trav</option>
							<option value="Prol">Prol</option>
							<option value="Cjn">Cjn</option>
							<option value="Psje">Psje</option>
							<option value="Blv">Blv</option>
							<option value="Vda">Vda</option>
							<option value="Esc">Esc</option>
							<option value="Snd">Snd</option>
							<option value="Tcal">Tcal</option>
							<option value="Cno">Cno</option>
						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<textarea class="form-control" id="punto_referencia_persona" name="punto_referencia_persona" cols="" rows=""></textarea>
				</div>
				</div>

			</div>
		</div>

			<div class="col-md-12">
			<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n del Terreno.</strong>
				</p>
				<div class="col-md-4">
					<div class="form-group">
					<label>Topografia</label>
					<select class="form-control  select2" required="required" name="topografia" >
						<option value="">--Selecccione--</option>
						<option value="Plano">Plano</option>
						<option value="Sobre Nivel">Sobre Nivel</option>
						<option value="Bajo Nivel">Bajo Nivel</option>
						<option value="Corte">Corte</option>
						<option value="Relleno">Relleno</option>
						<option value="Inclinado/Pend. Suave">Inclinado/Pend. Suave</option>
						<option value="Inclinado/Pend. Media">Inclinado/Pend. Media</option>
						<option value="Inclinado/Pend. Fuerte">Inclinado/Pend. Fuerte</option>
						<option value="Irregular">Irregular</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Acceso</label>
					<select class="form-control  select2" required="required" name="acceso">
						<option value="">--Selecccione--</option>
						<option value="Calle Pavimentada">Calle Pavimentada</option>
						<option value="Calle Engranzonada">Calle Engranzonada</option>
						<option value="Calle de Tierra">Calle de Tierra</option>
						<option value="Escalera Pavimento">Escalera Pavimento</option>
						<option value="Escalera de Tierra">Escalera de Tierra</option>
						<option value="Paso de Servidumbre">Paso de Servidumbre</option>
						<option value="Vereda">Vereda</option>
						<option value="Sendero">Sendero</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Forma</label>
					<select class="form-control  select2" required="required" name="forma">
						<option value="">--Selecccione--</option>
						<option value="Regular">Regular</option>
						<option value="Irregular">Irregular</option>
						<option value="Muy Irregular">Muy Irregular</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Ubicaci&oacute;n</label>
					<select class="form-control  select2" required="required" name="ubicacion">
						<option value="">--Selecccione--</option>
						<option value="Convencional">Convencional</option>
						<option value="Esquina">Esquina</option>
						<option value="Interior de Manzana">Interior de Manzana</option>
						<option value="Oculta">Oculta</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Entorno Fisico</label>
					<select class="form-control  select2" required="required" name="entorno_fisico">
						<option value="">--Selecccione--</option>
						<option value="Zona Urbanizada">Zona Urbanizada</option>
						<option value="Zona no Urbanizada">Zona no Urbanizada</option>
						<option value="Rio/Quebrada">Rio/Quebrada</option>
						<option value="Barranco/Talud">Barranco/Talud</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Mejoras al Terreno</label>
					<select class="form-control  select2" required="required" name="mejoras_terreno">
						<option value="">--Selecccione--</option>
						<option value="Muro de Contencion">Muro de Contenci&oacute;n</option>
						<option value="Nivelacion">Nivelaci&oacute;n</option>
						<option value="Cercado">Cercado</option>
						<option value="Pozo Septico">Pozo Septico</option>
						<option value="Lagunas Artificiales">Lagunas Artificiales</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Tenencia Terreno</label>
					<select class="form-control  select2" required="required" name="tenencia_terreno">
						<option value="">--Selecccione--</option>
						<option value="Propiedad">Propiedad</option>
						<option value="Arrendamiento">Arrendamiento</option>
						<option value="Comodato">Comodato</option>
						<option value="Anticresis">Anticresis</option>
						<option value="Enfiteusis">Enfiteusis</option>
						<option value="Usufructo">Usufructo</option>
						<option value="Derechho de Hab.">Derechho de Hab.</option>
						<option value="Concesion de Uso">Concesion de Uso</option>
						<option value="Admin. de Estado N/E/M">Admin. de Estado N/E/M</option>
						<option value="Posesion con Tit./Sin Tit.">Posesion con Tit./Sin Tit.</option>
						<option value="Posesion Certificada TU">Posesion Certificada TU</option>
						<option value="Ocupacion">Ocupacion</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Reg. de Propiedad</label>
					<select class="form-control  select2" required="required" name="regimen_propiedad">
						<option value="">--Selecccione--</option>
						<option value="Ejido">Ejido</option>
						<option value="Municipal Propio">Municipal Propio</option>
						<option value="Nacional">Nacional</option>
						<option value="Baldio">Baldio</option>
						<option value="Estadal">Estadal</option>
						<option value="Priv. Individual">Priv. Individual</option>
						<option value="Priv. Condominio">Priv. Condominio</option>
						<option value="Priv. Posesion">Priv. Posesion</option>
						<option value="Familiar">Familiar</option>
						<option value="Colectiva">Colectiva</option>
						<option value="Multifamiliar">Multifamiliar</option>
						<option value="Comunal">Comunal</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-4">
					<div class="form-group">
					<label>Uso Actual</label>
					<select class="form-control  select2" required="required" name="uso_actual_terreno">
						<option value="">--Selecccione--</option>
						<option value="Residencial">Residencial</option>
						<option value="Comercial">Comercial</option>
						<option value="Industrial">Industrial</option>
						<option value="Recreativo">Recreativo</option>
						<option value="Deportivo">Deportivo</option>
						<option value="Asistencial/salud">Asistencial/salud</option>
						<option value="Educacional">Educacional</option>
						<option value="Turistico">Turistico</option>
						<option value="Social/Cultural">Social/Cultural</option>
						<option value="Religioso">Religioso</option>
						<option value="Alimentacion">Alimentacion</option>
						<option value="Servicio Comunal">Servicio Comunal</option>
						<option value="Gubernamental/institucional">Gubernamental/institucional</option>
						<option value="Pesquero">Pesquero</option>
						<option value="Agroindustrial">Agroindustrial</option>
						<option value="Agroforestal">Agroforestal</option>
						<option value="Agricola">Agricola</option>
						<option value="Pecuario">Pecuario</option>
						<option value="Forestal">Forestal</option>
						<option value="Minero">Minero</option>
						<option value="Sin Uso">Sin Uso</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-12">
					<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Valoracion del Terreno</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="Observaciones_economia_terreno" cols="" rows="" required="required"></textarea>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label> % Porcentaje (U.T. M2)</label>
					<input class="form-control number" name="valor_ajustado"   value="" type="text" required="required">
				</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los servicios que posee el terreno.</strong>
				</p>
				<div class="col-md-12">
					<div class="form-group">
					<label>Servicios</label>
					<select name="servicios[]" class="form-control  select2" required="required" multiple="multiple" data-placeholder="--Seleccione--" style=" width: 100%;">
				 <?php 
				 $servicios= pg_query("SELECT * FROM  tb_servicio");

				 while($reg=pg_fetch_object($servicios)){?>
					<?php $i++;?>
					<option value="<?php  echo $reg->id;?>"> <?php echo $reg->tipo;?></option>
				  <?php }?>
				</select>
				</div>
				</div>	
		</div>


		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informaci&oacute;n General de la Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo</label>
					<select class="form-control  select2" required="required" name="tipo_construccion">
						<option value="">--Selecccione--</option>
						<option value="Quinta">Quinta</option>
						<option value="Casa/Quinta">Casa/Quinta</option>
						<option value="Chalet">Chalet</option>
						<option value="Town House">Town House</option>
						<option value="Casa Tradicional">Casa Tradicional</option>
						<option value="Casa Convencional">Casa Convencional</option>
						<option value="Casa Economica">Casa Economica</option>
						<option value="Rancho">Rancho</option>
						<option value="Edificio">Edificio</option>
						<option value="Apartamento">Apartamento</option>
						<option value="Centro Comercial">Alimentacion</option>
						<option value="Local Comercial">Local Comercial</option>
						<option value="Galpon">Galpon</option>
						<option value="Barraca">Barraca</option>
						<option value="Vaqueras">Vaqueras</option>
						<option value="Cochineras">Cochineras</option>
						<option value="Corrales y Anexos">Corrales y Anexos</option>
						<option value="Bebederos">Bebederos</option>
						<option value="Comederos">Comederos</option>
						<option value="Tanques">Tanques</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Descripci&oacute;n de Uso</label>
					<select class="form-control  select2" required="required" name="descripcion_uso">
						<option value="">--Selecccione--</option>
						<option value="Unifamiliar">Unifamiliar</option>
						<option value="Bifamiliar">Bifamiliar</option>
						<option value="Multifamiliar">Multifamiliar</option>
						<option value="Comercio al Detal">Comercio al Detal</option>
						<option value="Comercio al Mayor">Comercio al Mayor</option>
						<option value="Mercado Libre">Mercado Libre</option>
						<option value="Oficinas">Oficinas</option>
						<option value="Industrial">Industrial</option>
						<option value="Servicio">Servicio</option>
						<option value="Agropecuario">Agropecuario</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tenencia Construcci&oacute;n</label>
					<select name="tenencia_construccion" class="form-control  select2" required="required">
						<option value="">--Selecccione--</option>
						<option value="Propiedad">Propiedad</option>
						<option value="Arrendamiento">Arrendamiento</option>
						<option value="Comodato">Comodato</option>
						<option value="Anticresis">Anticresis</option>
						<option value="Enfiteusis">Enfiteusis</option>
						<option value="Usufructo">Usufructo</option>
						<option value="Derecho de Hab.">Derecho de Hab.</option>
						<option value="Concesion de Uso">Concesion de Uso</option>
						<option value="Admin. de Estado N/E/M">Admin. de Estado N/E/M</option>
						<option value="Posesion con Tit./Sin Tit.">Posesion con Tit./Sin Tit.</option>
						<option value="Ocupacion">Ocupacion</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Reg. de Propiedad.</label>
					<select name="reg_propiedad_const" class="form-control  select2" required="required">
						<option value="">--Selecccione--</option>
						<option value="Municipal Propio">Municipal Propio</option>
						<option value="Nacional">Nacional</option>
						<option value="Estadal">Estadal</option>
						<option value="Priv. Individual">Priv. Individual</option>
						<option value="Priv. Condominio">Priv. Condominio</option>
						<option value="Posesion">Posesion</option>
						<option value="Familiar">Familiar</option>
						<option value="Multifamiliar">Multifamiliar</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
			</div>
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Estructurales de Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Soporte</label>
					<select class="form-control  select2" required="required" name="soporte_const">
						<option value="">--Selecccione--</option>
						<option value="Concreto Armado">Concreto Armado</option>
						<option value="Metalica">Metalica</option>
						<option value="Madera">Madera</option>
						<option value="Paredes de Carga">Paredes de Carga</option>
						<option value="Prefabricado">Prefabricado</option>
						<option value="Machones">Machones</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Techo</label>
					<select class="form-control  select2" required="required" name="techo_const">
						<option value="">--Selecccione--</option>
						<option value="Concreto Armado">Concreto Armado</option>
						<option value="Metalica">Metalica</option>
						<option value="Madera">Madera</option>
						<option value="Varas">Varas</option>
						<option value="Cerchas">Cerchas</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Interna</label>
					<select class="form-control  select2" required="required" name="cubierta_interna">
						<option value="">--Selecccione--
						</option>
						<option value="Plafon">Plafon</option>
						<option value="Cielo Raso (Laminas)">Cielo Raso (Laminas)</option>
						<option value="Cielo Raso (Econom.)">Cielo Raso (Econom.)</option>
						<option value="Machihembrado (Madera)">Machihembrado (Madera)</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Externa</label>
					<select class="form-control  select2" required="required" name="cubierta_externa">
						<option value="">--Selecccione--</option>
						<option value="Madera/Teja">Madera/Teja</option>
						<option value="Placa/Teja">Placa/Teja</option>
						<option value="Platabanda">Platabanda</option>
						<option value="Tejas">Tejas</option>
						<option value="Caña Brava">Caña Brava</option>
						<option value="Asbesto">Asbesto</option>
						<option value="Aluminio">Aluminio</option>
						<option value="Zinc">Zinc</option>
						<option value="Acerolit">Acerolit</option>
						<option value="Tabelon">Tabelon</option>
						<option value="Acero Galbanizado">Acero Galbanizado</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
		</div>


		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pared</label>
					<select class="form-control  select2" required="required" name="tipo_pared">
						<option value="">--Selecccione--</option>
						<option value="Bloque de Arcilla">Bloque de Arcilla</option>
						<option value="Bloque de Cemento">Bloque de Cemento</option>
						<option value="Madera Aserrada">Madera Aserrada</option>
						<option value="Laton">Laton</option>
						<option value="Ladrillo">Ladrillo</option>
						<option value="Prefabricada">Prefabricada</option>
						<option value="Bahareque">Bahareque</option>
						<option value="Adobe">Adobe</option>
						<option value="Tapia">Tapia</option>
						<option value="Vidrio">Vidrio</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Acabado (Pared)</label>
					<select class="form-control  select2" required="required" name="acabado_pared">
						<option value="">--Selecccione--</option>
						<option value="Sin Friso">Sin Friso</option>
						<option value="Friso Liso">Friso Liso</option>
						<option value="Friso Rustico">Friso Rustico</option>
						<option value="Obra Limpia">Obra Limpia</option>
						<option value="Cemento Blanco">Cemento Blanco</option>
						<option value="Ceramica">Ceramica</option>
						<option value="Vidrio Molido">Vidrio Molido</option>
						<option value="Yeso/Cal">Yeso/Cal</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pintura (Pared)</label>
					<select class="form-control  select2" required="required" name="tipo_pintura_pared">
						<option value="">--Selecccione--</option>
						<option value="Caucho">Caucho</option>
						<option value="Oleo">Oleo</option>
						<option value="Lechada">Lechada</option>
						<option value="Pasta">Pasta</option>
						<option value="Asbestina">Asbestina</option>
						<option value="Sin Pintura">Sin Pintura</option>
						<option value="Papel Tapiz">Papel Tapiz</option>
					</select>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Inst. Electricas (Pared)</label>
					<select class="form-control  select2" required="required" name="instalacciones_electricas">
						<option value="">--Selecccione--</option>
						<option value="Embutidas">Embutidas</option>
						<option value="Externa">Externa</option>
						<option value="Industrial">Industrial</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Pisos</label>
					<select class="form-control  select2" required="required" name="pisos_construccion">
						<option value="">--Selecccione--</option>
						<option value="Cemento Pulido">Cemento Pulido</option>
						<option value="Cemento Rustico">Cemento Rustico</option>
						<option value="Sin Piso (Tierra)">Sin Piso (Tierra)</option>
						<option value="Ceramica">Ceramica</option>
						<option value="Granito">Granito</option>
						<option value="Ladrillos">Ladrillos</option>
						<option value="Parque">Parque</option>
						<option value="Marmol">Marmol</option>
						<option value="Vinil">Vinil</option>
						<option value="Mosaico">Mosaico</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
				<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construcci&oacute;n (Baños)</strong>
				</p>
				<div class="col-md-3">
				<div class="form-group">
					<label>WC</label>
					<input class="form-control number" name="banoWC" value="" type="text" required="required">	
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Comunitario</label>
					<input class="form-control number" name="bano_comunitario" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Ducha</label>
					<input class="form-control number" name="bano_ducha" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Lavamanos</label>
					<input class="form-control number" name="bano_lavamanos" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Bañera</label>
					<input class="form-control number" name="bano_banera" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Bidet</label>
					<input class="form-control number" name="bano_bidet" value="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label>Letrina</label>
					<input class="form-control number" name="bano_letrina" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Cer. 1era</label>
					<input class="form-control number" name="bano_ceramica1" value="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label>Cer. 2da</label>
					<input class="form-control number" name="bano_ceramica2" value="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construcci&oacute;n (Puertas)</strong>
				</p>
				<br>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Metalica</label>
					<input class="form-control number" name="puerta_metalica" value="" placeholder="" type="text" required="required">	
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Madera Cepillada</label>
					<input class="form-control number" name="puerta_madera_cep" value="" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Madera Ent. Ecoc.</label>
					<input class="form-control number" name="puerta_madera_ecoc" value="" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Entamborada Fina</label>
					<input class="form-control number" name="puerta_entamborada" value="" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>De Seguridad</label>
					<input class="form-control number" name="puerta_seguridad" value="" placeholder="" type="text" required="required">
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Otra</label>
					<input class="form-control" name="puerta_otra" value="" placeholder="" type="text" >
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>Cantidad</label>
					<input class="form-control number" name="puerta_otra_cantidad" value="" placeholder="" type="text" >
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construcci&oacute;n (Ventanas)</strong>
				</p>
				<br>
			</div>


			<div class="col-md-12">
				<div class="col-md-2">
					<label>Ventanal (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Ventanal (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Ventanal (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Ventanal_madera" value="" placeholder="M" type="text" required="required">
					</div>
				</div>


				<div class="col-md-2">
					<label>Basculante (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Basculante (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Basculante (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Basculante_madera" value="" placeholder="M" type="text" required="required">
					</div>
				</div>


				<div class="col-md-2">
					<label>Batiente (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Batiente (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Batiente (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Batiente_madera" value="" placeholder="M" type="text" required="required" >
					</div>
				</div>


				<div class="col-md-2">
					<label>Celosia (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Celosia (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Celosia (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Celosia_madera" value="" placeholder="M" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Corredera (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Corredera_madera" value="" placeholder="M" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (H)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_hierro" value="" placeholder="H" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (A)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_aluminio" value="" placeholder="A" type="text" required="required">
					</div>
				</div>

				<div class="col-md-2">
					<label>Panoramica (M)</label>
					<div class="form-group">
						<input class="form-control number" name="Panoramica_madera" value="" placeholder="M" type="text" required="required">
					</div>
				</div>


			</div>

			<div class="col-md-12">
					<br>
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Otros Datos (Const.)</strong>
				</p>
				<br>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Año Construcci&oacute;n</label>
					<input class="form-control number" name="anos_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Porcentaje (%) Refacci&oacute;n</label>
					<input class="form-control number" name="refaccion_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>N° de Niveles</label>
					<input class="form-control number" name="numeroniveles_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Años Refacci&oacute;n</label>
					<input class="form-control number" name="anosrefaccion_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Edad Efectiva</label>
					<input class="form-control number" name="edadefectiva_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>


			<div class="col-md-6">
				<div class="form-group">
					<label>N° de Edificaciones</label>
					<input class="form-control number" name="edificaciones_const" value="" placeholder="" type="text" required="required" >
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Estado de Conservaci&oacute;n</label>
					<select class="form-control  select2" required="required" name="estado_conservacion_const">
						<option value="">--Selecccione--</option>
						<option value="Excelente">Excelente</option>
						<option value="Bueno">Bueno</option>
						<option value="Regular">Regular</option>
						<option value="Malo">Malo</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="observaciones_const" cols="" rows="" required="required"></textarea>
				</div>
			</div>

			
		</div>





	<div class="row">
		 <br><br>
		<div class="col-md-12">
		<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ubicaci&oacute;n de parcela (Linderos Actuales).</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Largo del Terreno</label>
					<input class="form-control number" name="largo_terreno"   value="" type="text" required="required">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ancho del Terreno</label>
					<input class="form-control number" name="ancho_terreno"   value="" type="text" required="required">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Norte</label>
					<textarea class="form-control " name="norte_linderos" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sur</label>
					<textarea class="form-control " name="sur_linderos" cols="" rows="" required="required"></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Este</label>
					<textarea class="form-control " name="este_linderos" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Oeste</label>
					<textarea class="form-control " name="oeste_linderos" cols="" rows="" required="required"></textarea>
				</div>
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Coordenadas UTM (REGVEN)</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Norte (M)</label>
					<textarea class="form-control number" name="norte_coord" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Este (M)</label>
					<textarea class="form-control number" name="este_coord" cols="" rows="" required="required"></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Huso</label>
					<textarea class="form-control number" name="huso_coord" cols="" rows="" required="required"></textarea>
				</div>
				</div>
			</div>

			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Inspectores que realizar&oacute;n la Visita.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha de Primera Visita</label>
					<input class="form-control " id="fecha-visita" name="fechavisita" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Fecha del lavantamiento</label>
					<input class="form-control " id="fecha-levantamiento" name="fechalevantamiento" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Elaborado Por</label>
					<select class="form-control select2 " name="inspertor_elaborador" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$persona3= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'");
						?>			
						<?php while($reg=pg_fetch_object($persona3)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Revisado Por</label>
					<select class="form-control select2 " name="inspertor_revisor" required="required">
					<option value="">--Selecccione--</option>
						<?php 
							$i=0;
							$persona2= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'");
						?>			
						<?php while($reg=pg_fetch_object($persona2)){?>
						<?php $i++;?>
						
						<option value="<?php echo $reg->id;?>"><?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></option>
						<?php }?>
					</select>
				</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label>Observaciones Generales</label>
					<textarea class="form-control " name="observaciones_ficha" cols="" rows="" required="required"></textarea>
				</div>

				<div align="center"><br>
				<a href="./" class="btnNew"><span class="btn btn-default" title="Cancelar"><i class="ace-icon fa fa-remove"></i>Cancelar</span></a>
				<button type="submit" class="btn btn-primary" title=""><i class="fa fa-check-circle-o"></i> Registrar</button>

			</div>
			<br>
			</form>
				</div>
			</div>
			</div>
	</div>



			
</div>
</div>
<!-- PAGE CONTENT ENDS -->
</div>
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
</div>
</div>
<?php include('../layout/footer.php');?>
<script>


$(".select2").select2();
$(".select22").select2();


$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#registro-fecha").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-ficha").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-visita").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-levantamiento").datepicker({
firstDay: 0,
maxDate:0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

</script>

<script type="text/javascript">
	$(".tipocalle").select2();

</script>
</body>

</html>
