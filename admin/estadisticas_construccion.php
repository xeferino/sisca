<?php 
include_once("seguridad.php");
include_once("clases/conexion.php");
include_once("clases/ficha.php");
include_once("clases/linderos.php");

$ficha = new Ficha();
$ficha->fichaEstadistica();

$lindero = new Linderos();
$lindero->linderoEstadistica();


?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=9; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="#">Home</a>							</li>
<li class="active">Estadisticas</li>
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
	<!-- PAGE CONTENT BEGINS -->
	<div class="alert alert-block alert-info">
		<i class="ace-icon fa fa-check"></i>
		Bienvenido Módulo de estadistica donde podra realizar los reportes correspodientes segun sus necesidades.
		<strong class="green">
			<small></small>									</strong>
		</div>
		<!-- /.row -->
		<!-- /.row -->

		<div class="row">
			<form action="reportes/graficas_construccion.php" method="post" id="fom-construccion-estadistica" target="_blank">
			<div class="col-sm-12">
				<div class="widget-box transparent" id="recent-box">
					<div class="col-sm-12" style="text-transform: uppercase;">
						<div class="panel panel-default">
							<div class="panel-heading">
								<b><i class="ace-icon fa fa-filter"></i> Selecciones los Filtros Correspondientes para el Reporte por Construcci&oacute;n</b>
							</div>
							<div class="panel-body">

									<div class="col-md-12">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Fecha de Inicio</label>
										<input class="form-control " id="fechaci" name="fechai" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label>Fecha Fin</label>
										<input class="form-control " id="fechacf" name="fechaf" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label>Sector</label>
										<select class="form-control  select2" id="sector" name="sector" required="required">
											<option value="">--Selecccione--</option>
											<?php 
											$i=0;
											$pedul= pg_query("SELECT * FROM  tb_pedul");
											?>			
											<?php while($reg=pg_fetch_object($pedul)){?>
											<?php $i++;?>

											<option value="<?php echo $reg->id;?>"><?php echo $reg->nombre;?></option>
											<?php }?>
										</select>
									</div>
								</div>
							</div>
							<div class="col-md-12">
				<p class="alert alert" style="background-color: #f5f5f5; important!
											background-color: #f5f5f5;
											border-color: #ddd;
											text-decoration-color: #333; important!

										">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Informacion de General de la Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo</label>
					<select class="form-control  select2" required="required" name="tipo_construccion">
						<option value="">--Selecccione--</option>
						<option value="Quinta">Quinta</option>
						<option value="Casa/Quinta">Casa/Quinta</option>
						<option value="Chalet">Chalet</option>
						<option value="Town House">Town House</option>
						<option value="Casa Tradicional">Casa Tradicional</option>
						<option value="Casa Convencional">Casa Convencional</option>
						<option value="Casa Economica">Casa Economica</option>
						<option value="Rancho">Rancho</option>
						<option value="Edificio">Edificio</option>
						<option value="Apartamento">Apartamento</option>
						<option value="Centro Comercial">Alimentacion</option>
						<option value="Local Comercial">Local Comercial</option>
						<option value="Galpon">Galpon</option>
						<option value="Barraca">Barraca</option>
						<option value="Vaqueras">Vaqueras</option>
						<option value="Cochineras">Cochineras</option>
						<option value="Corrales y Anexos">Corrales y Anexos</option>
						<option value="Bebederos">Bebederos</option>
						<option value="Comederos">Comederos</option>
						<option value="Tanques">Tanques</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Descripcion de Uso</label>
					<select class="form-control  select2" required="required" name="descripcion_uso">
						<option value="">--Selecccione--</option>
						<option value="Unifamiliar">Unifamiliar</option>
						<option value="Bifamiliar">Bifamiliar</option>
						<option value="Multifamiliar">Multifamiliar</option>
						<option value="Comercio al Detal">Comercio al Detal</option>
						<option value="Comercio al Mayor">Comercio al Mayor</option>
						<option value="Mercado Libre">Mercado Libre</option>
						<option value="Oficinas">Oficinas</option>
						<option value="Industrial">Industrial</option>
						<option value="Servicio">Servicio</option>
						<option value="Agropecuario">Agropecuario</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tenencia Const.</label>
					<select name="tenencia_construccion" class="form-control  select2" required="required">
						<option value="">--Selecccione--</option>
						<option value="Propiedad">Propiedad</option>
						<option value="Arrendamiento">Arrendamiento</option>
						<option value="Comodato">Comodato</option>
						<option value="Anticresis">Anticresis</option>
						<option value="Enfiteusis">Enfiteusis</option>
						<option value="Usufructo">Usufructo</option>
						<option value="Derecho de Hab.">Derecho de Hab.</option>
						<option value="Concesion de Uso">Concesion de Uso</option>
						<option value="Admin. de Estado N/E/M">Admin. de Estado N/E/M</option>
						<option value="Posesion con Tit./Sin Tit.">Posesion con Tit./Sin Tit.</option>
						<option value="Ocupacion">Ocupacion</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Reg. de Propiedad.</label>
					<select name="reg_propiedad_const" class="form-control  select2" required="required">
						<option value="">--Selecccione--</option>
						<option value="Municipal Propio">Municipal Propio</option>
						<option value="Nacional">Nacional</option>
						<option value="Estadal">Estadal</option>
						<option value="Priv. Individual">Priv. Individual</option>
						<option value="Priv. Condominio">Priv. Condominio</option>
						<option value="Posesion">Posesion</option>
						<option value="Familiar">Familiar</option>
						<option value="Multifamiliar">Multifamiliar</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
			</div>
			<div class="col-md-12">
				<p class="alert alert" style="background-color: #f5f5f5; important!
											background-color: #f5f5f5;
											border-color: #ddd;
											text-decoration-color: #333; important!

										">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Estructurales de Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Soporte</label>
					<select class="form-control  select2" required="required" name="soporte_const">
						<option value="">--Selecccione--</option>
						<option value="Concreto Armado">Concreto Armado</option>
						<option value="Metalica">Metalica</option>
						<option value="Madera">Madera</option>
						<option value="Paredes de Carga">Paredes de Carga</option>
						<option value="Prefabricado">Prefabricado</option>
						<option value="Machones">Machones</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Techo</label>
					<select class="form-control  select2" required="required" name="techo_const">
						<option value="">--Selecccione--</option>
						<option value="Concreto Armado">Concreto Armado</option>
						<option value="Metalica">Metalica</option>
						<option value="Madera">Madera</option>
						<option value="Varas">Varas</option>
						<option value="Cerchas">Cerchas</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Interna</label>
					<select class="form-control  select2" required="required" name="cubierta_interna">
						<option value="">--Selecccione--
						</option>
						<option value="Plafon">Plafon</option>
						<option value="Cielo Raso (Laminas)">Cielo Raso (Laminas)</option>
						<option value="Cielo Raso (Econom.)">Cielo Raso (Econom.)</option>
						<option value="Machihembrado (Madera)">Machihembrado (Madera)</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Cubierta Externa</label>
					<select class="form-control  select2" required="required" name="cubierta_externa">
						<option value="">--Selecccione--</option>
						<option value="Madera/Teja">Madera/Teja</option>
						<option value="Placa/Teja">Placa/Teja</option>
						<option value="Platabanda">Platabanda</option>
						<option value="Tejas">Tejas</option>
						<option value="Caña Brava">Caña Brava</option>
						<option value="Asbesto">Asbesto</option>
						<option value="Aluminio">Aluminio</option>
						<option value="Zinc">Zinc</option>
						<option value="Acerolit">Acerolit</option>
						<option value="Tabelon">Tabelon</option>
						<option value="Acero Galbanizado">Acero Galbanizado</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
		</div>


		<div class="col-md-12">
				<p class="alert alert" style="background-color: #f5f5f5; important!
											background-color: #f5f5f5;
											border-color: #ddd;
											text-decoration-color: #333; important!

										">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Datos Complementarios de la Construcci&oacute;n.</strong>
				</p>
				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pared</label>
					<select class="form-control  select2" required="required" name="tipo_pared">
						<option value="">--Selecccione--</option>
						<option value="Bloque de Arcilla">Bloque de Arcilla</option>
						<option value="Bloque de Cemento">Bloque de Cemento</option>
						<option value="Madera Aserrada">Madera Aserrada</option>
						<option value="Laton">Laton</option>
						<option value="Ladrillo">Ladrillo</option>
						<option value="Prefabricada">Prefabricada</option>
						<option value="Bahareque">Bahareque</option>
						<option value="Adobe">Adobe</option>
						<option value="Tapia">Tapia</option>
						<option value="Vidrio">Vidrio</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

					<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Acabado (Pared)</label>
					<select class="form-control  select2" required="required" name="acabado_pared">
						<option value="">--Selecccione--</option>
						<option value="Sin Friso">Sin Friso</option>
						<option value="Friso Liso">Friso Liso</option>
						<option value="Friso Rustico">Friso Rustico</option>
						<option value="Obra Limpia">Obra Limpia</option>
						<option value="Cemento Blanco">Cemento Blanco</option>
						<option value="Ceramica">Ceramica</option>
						<option value="Vidrio Molido">Vidrio Molido</option>
						<option value="Yeso/Cal">Yeso/Cal</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Tipo de Pintura (Pared)</label>
					<select class="form-control  select2" required="required" name="tipo_pintura_pared">
						<option value="">--Selecccione--</option>
						<option value="Caucho">Caucho</option>
						<option value="Oleo">Oleo</option>
						<option value="Lechada">Lechada</option>
						<option value="Pasta">Pasta</option>
						<option value="Asbestina">Asbestina</option>
						<option value="Sin Pintura">Sin Pintura</option>
						<option value="Papel Tapiz">Papel Tapiz</option>
					</select>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Inst. Electricas (Pared)</label>
					<select class="form-control  select2" required="required" name="instalacciones_electricas">
						<option value="">--Selecccione--</option>
						<option value="Embutidas">Embutidas</option>
						<option value="Externa">Externa</option>
						<option value="Industrial">Industrial</option>
					</select>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Pisos</label>
					<select class="form-control  select2" required="required" name="pisos_construccion">
						<option value="">--Selecccione--</option>
						<option value="Cemento Pulido">Cemento Pulido</option>
						<option value="Cemento Rustico">Cemento Rustico</option>
						<option value="Sin Piso (Tierra)">Sin Piso (Tierra)</option>
						<option value="Ceramica">Ceramica</option>
						<option value="Granito">Granito</option>
						<option value="Ladrillos">Ladrillos</option>
						<option value="Parque">Parque</option>
						<option value="Marmol">Marmol</option>
						<option value="Vinil">Vinil</option>
						<option value="Mosaico">Mosaico</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
				</div>
				<div class="col-md-12">
										<p class="alert alert" style="background-color: #f5f5f5; important!
											background-color: #f5f5f5;
											border-color: #ddd;
											text-decoration-color: #333; important!

										">
											<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione el Tipo de Grafica</strong>
										</p>
										<div class="form-group">

											<div class="row">
												<div class="col-xs-3">
													<label>
														<input name="graficatorta" class="ace ace-switch ace-switch-4" type="checkbox"
														value="1">
														<span class="lbl">&nbsp;&nbsp;Grafica de Torta</span>
													</label>
												</div>

												<div class="col-xs-3">
													<label>
														<input name="graficabarrahorizontal" class="ace ace-switch ace-switch-4" type="checkbox"
														value="2">
														<span class="lbl">&nbsp;&nbsp;Grafica de Barra </span>
													</label>
												</div>

												<div class="col-xs-3">
													<label>
														<input name="graficalinea" class="ace ace-switch ace-switch-4" type="checkbox"
														value="3">
														<span class="lbl">&nbsp;&nbsp;Grafica de Lineas </span>
													</label>
												</div>
												
											</div>
										</div>

										<div align="right"><br>
										<button type="submit" class="btn btn-primary" title=""><i class="fa fa-cubes"></i> Generar</button>

									</div>
									</div>


						</div>

					</div>
					</form>			
					<!-- /.widget-body -->
				</div><!-- /.widget-box -->
			</div><!-- /.col -->
			<!-- /.col -->
		</div><!-- /.row -->
		<!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->

<?php include('../layout/footer.php');?>

<script>
$(".select2").select2();
$(".select22").select2();


$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#registro-fecha").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-ficha").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-visita").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fecha-levantamiento").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});



$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fechaci").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

$(function (){
$.datepicker.setDefaults($.datepicker.regional["es"]);
$("#fechacf").datepicker({
firstDay: 0,
changeMonth: true,
changeYear : true,
dateFormat: "dd-mm-yy",
yearRange: "1900:2099"
});
});

</script>
<script type="text/javascript">

Morris.Bar({
element: 'morris-bar-chart',
data: [
{ y: 'Ene', a: 100}, 
{ y: 'Feb', a: 75},
{ y: 'Mar', a: 50},
{ y: 'Abr', a: 75},
{ y: 'May', a: 50},
{ y: 'Jun', a: 75},
{ y: 'Jul', a: 100},
{ y: 'Ago', a: 100},
{ y: 'Sep', a: 100},
{ y: 'Oct', a: 100},
{ y: 'Nov', a: 100},
{ y: 'Dic', a: 100}
],
xkey: 'y',
ykeys: ['a'],
labels: ['Ficha Registradas'],
barColors: ['#0b62a4'],
hideHover: 'auto',
resize: true
});


Morris.Line({
element: 'morris-line-chart',
data: [{
y: '2006',
a: 100,
b: 90
}, {
y: '2007',
a: 75,
b: 65
}, {
y: '2008',
a: 50,
b: 40
}, {
y: '2009',
a: 75,
b: 65
}, {
y: '2010',
a: 50,
b: 40
}, {
y: '2011',
a: 75,
b: 65
}, {
y: '2012',
a: 100,
b: 90
}],
xkey: 'y',
ykeys: ['a', 'b'],
labels: ['Series A', 'Series B'],
lineColors: ['#e91313', '#0b62a4'],
hideHover: 'auto',
resize: true
});

Morris.Area({
element: 'morris-area-chart',
data: [{
period: '2010 Q1',
iphone: 2666,
ipad: null,
itouch: 2647
}, {
period: '2010 Q2',
iphone: 2778,
ipad: 2294,
itouch: 2441
}, {
period: '2010 Q3',
iphone: 4912,
ipad: 1969,
itouch: 2501
}, {
period: '2010 Q4',
iphone: 3767,
ipad: 3597,
itouch: 5689
}, {
period: '2011 Q1',
iphone: 6810,
ipad: 1914,
itouch: 2293
}, {
period: '2011 Q2',
iphone: 5670,
ipad: 4293,
itouch: 1881
}, {
period: '2011 Q3',
iphone: 4820,
ipad: 3795,
itouch: 1588
}, {
period: '2011 Q4',
iphone: 15073,
ipad: 5967,
itouch: 5175
}, {
period: '2012 Q1',
iphone: 10687,
ipad: 4460,
itouch: 2028
}, {
period: '2012 Q2',
iphone: 8432,
ipad: 5713,
itouch: 1791
}],
xkey: 'period',
ykeys: ['iphone', 'ipad', 'itouch'],
labels: ['iPhone', 'iPad', 'iPod Touch'],
pointSize: 2,
hideHover: 'auto',
resize: true
});


</script>

<script type="text/javascript">

Morris.Donut({
element: 'morris-donut-chart',
data: [{
value: <?php echo $lindero->estadistica[0];?>,
color: "#e91313",
highlight: "#000",
label: "Certificacion de Linderos"
},
{
value: <?php echo $ficha->estadistica[0];?>,
color: "#0b62a4",
highlight: "#000",
label: "Fichas Catastrales"
}],
resize: true
});

</script>

</body>
</html>
