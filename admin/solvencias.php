<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");
$objeto = new Solvencia();
$objeto->solvenciasMuncipales();
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=6; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="./">Home</a>							</li>
<li class="active"><i class="ace-icon fa fa-money"></i> Solvencias</li> 
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<?php if($objeto->consulta>0):?>
<div class="row">

<div class="col-xs-12" >
<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nuevo Pago"><i class="ace-icon fa fa-plus"></i></span></a><br><br><br>
<!-- PAGE CONTENT BEGINS -->
<table id="dynamic-t" class="table table-striped">
			<thead>
				<tr>
					<th><i class="fa fa-credit-card"></i> Cedula</th>
					<th><i class="fa fa-pencil"></i> Nombre y Apellido</th>
					<th><i class="fa fa-calendar"></i> Fecha Pagada</th>
					<th><i class="fa fa-calendar"></i> Anualidad</th>
					<th><i class="fa fa-money"></i> Monto</th>
					<th><i class="fa fa-money"></i> Estatus</th>
					<th><i class="fa fa-check-square-o"></i> Acciones</th>
				</tr>
			</thead>
			
			<tbody>
				
			<?php foreach ($objeto->solvencias as $solvencia ) {
				# code...
			?>
			<?php $i++;?>
				<tr>
					<td><?php echo $solvencia->nacionalidad."-".$solvencia->cedula;?></td>
					<td><?php echo $solvencia->nombre1." ".$solvencia->nombre2." ".$rsolvenciaeg->apellido1." ".$solvencia->apellido2;?></td>
					<td><?php echo $solvencia->fecha;?></td>
					<td><?php echo $solvencia->anualidad;?></td>
					<td><?php echo $solvencia->monto;?></td>
					<?php if ($solvencia->estatus=="Pagada"){?>
					<td><span class="label label-info"><?php echo $solvencia->estatus;?></span>
					</td>
					<?php }else{?>
					<td><span class="label label-warning"><?php echo $solvencia->estatus;?></span>
					</td>
					<?php }?>
					<td>	
					<a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="edit-solvencia.php?id=<?php echo $solvencia->id;?>" title="Editar">
							<i class="ace-icon fa fa-pencil bigger-130"></i>
					</a>
					<!-- <a class="btn btn-primary btn-xs btnVer<?php echo $i;?>" href="#" title="ver">
							<i class="ace-icon fa fa-search bigger-130"></i>
					</a> -->
					<a class="btn btn-primary btn-xs btnPdf<?php echo $i;?>" href="reportes/solvencia_pdf.php?id=<?php echo $solvencia->id;?>" target="blank">
							<i class="ace-icon fa fa-file-pdf-o bigger-130"></i>
					</a>
				
					</td>
				</tr>
			<?php }?>
			</tbody>
		</table>
<!-- PAGE CONTENT ENDS -->
</div>
<!-- /.col -->
</div><!-- /.row -->
<?php else: ?>
<script>
$(document).ready(function() {
swal({
	title: "Alerta!",
	text: "No hay Solvencias Registradas en Sistema, Desea Registrar Presione Aceptar",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	confirmButtonText: 'Aceptar'
},
function(){
	$(location).attr('href','http:pago_solvencias.php');
});//swal

});///dom
</script>
<?php endif; ?>
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<?php include('../layout/footer.php');?>
<script>
$(document).ready(function() {	
$('#dynamic-t').DataTable({
responsive: true,
aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
});	
$('.btnNew').click(function(){

swal({
title: "Alerta!",
text: "Nuevo Pago",
type: "info",
showCancelButton: true,
closeOnConfirm: false,
confirmButtonText: 'Aceptar'
},
function(){
$(location).attr('href','http:pago_solvencias.php');
});//swal

});//click

});///dom
</script>
</body>
</html>
