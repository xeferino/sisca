<div class="modal fade" id="certificacion<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alerta alert alert-info" style="display:block;">
          <span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> 
          <strong> Datos del Propietario </strong> 
        </div>
        <div class="row">
         <div class="col-xs-12 ">
          <div class="space visible-xs"></div>

          <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
              <div class="profile-info-name"> Cedula </div>

              <div class="profile-info-value">
                <span><?php echo $reg->nacionalidad."-".$reg->cedula;?></span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Nombre y Apellido </div>

              <div class="profile-info-value">
                <span><?php echo $reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>

      <div class="alerta alert alert-info" style="display:block;">
        <span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> 
        <strong> Datos de los Linderos </strong> 
      </div>
      <div class="row">
         <div class="col-xs-12 ">
          <div class="space visible-xs"></div>

          <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
              <div class="profile-info-name"> Norte </div>

              <div class="profile-info-value">
                <span><?php echo $reg->lnorte;?> M</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Sur </div>

              <div class="profile-info-value">
                <span><?php echo $reg->lsur;?> M</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Este </div>

              <div class="profile-info-value">
                <span><?php echo $reg->leste;?> M</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Oeste </div>

              <div class="profile-info-value">
                <span><?php echo $reg->loeste;?> M</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Area M2 </div>

              <div class="profile-info-value">
                <span><?php echo $reg->area;?> M2</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Fecha </div>

              <div class="profile-info-value">
                <span><?php echo $reg->fecha;?> (Registro)</span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Observacion </div>

              <div class="profile-info-value">
                <span><?php echo $reg->observacion;?> </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="inspeccion<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alerta alert alert-info" style="display:block;">
          <span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> 
          <strong> Datos del Propietario </strong> 
        </div>
        <div class="row">
         <div class="col-xs-12 ">
          <div class="space visible-xs"></div>

          <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
              <div class="profile-info-name"> Cedula </div>

              <div class="profile-info-value">
                <span><?php echo $n."-".$ci;?></span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Nombre y Apellido </div>

              <div class="profile-info-value">
                <span><?php echo $nombre1." ".$nombre2." ".$apellido1." ".$apellido2;?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>

      <div class="alerta alert alert-info" style="display:block;">
        <span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> 
        <strong> Datos de la Inspeccion </strong> 
      </div>
      <div class="row">
         <div class="col-xs-12 ">
          <div class="space visible-xs"></div>

          <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
              <div class="profile-info-name"> Tipo </div>

              <div class="profile-info-value">
                <span><?php echo $tipo;?> </span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Fecha </div>

              <div class="profile-info-value">
                <span><?php echo $fecha;?> </span>
              </div>
            </div>
            
            <div class="profile-info-row">
              <div class="profile-info-name"> Observacion </div>

              <div class="profile-info-value">
                <span><?php echo $observacion;?> </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="modal fade" id="cedula<?php echo $reg->ficha;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
           <div class="alerta alert alert-info" style="display:block;">
          <span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> 
          <strong> Datos del Propietario </strong> 
        </div>
        <div class="row">
         <div class="col-xs-12 ">
          <div class="space visible-xs"></div>

          <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
              <div class="profile-info-name"> Cedula </div>

              <div class="profile-info-value">
                <span><?php echo $reg->ppnacionalidad."-".$reg->ppcedula;?></span>
              </div>
            </div>
            <div class="profile-info-row">
              <div class="profile-info-name"> Nombre y Apellido </div>

              <div class="profile-info-value">
                <span><?php echo $reg->ppn1." ".$reg->ppn2." ".$reg->ppa1." ".$reg->ppa2;?></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>