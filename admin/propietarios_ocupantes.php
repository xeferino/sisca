<?php include_once("seguridad.php"); ?>
<?php 
		include_once("clases/motor.php");
		$objeto = new Persona();
		$objeto->propietarios();
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=8; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-user"></i> Propietarios/Ocupantes</li> 
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
						<?php if ($objeto->mensaje==1){?>
								<script>
									$(document).on('ready',function(event){
									event.preventDefault();
									
									swal({
										title: "Alerta!",
										text: "No Existen Propietarios/Ocupantes Registrados en el Sistema",
										type: "info",
										confirmButtonText: 'Aceptar'
									},
									function(){
										$(location).attr('href','http:propietario_ocupante.php');
									});//swal
								});
							</script>
							<?php }
								  else{
							?>
							<div class="col-xs-12" >
								<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nuevo Inspector"><i class="ace-icon fa fa-user-plus"></i></span></a><br><br><br>
								<!-- PAGE CONTENT BEGINS -->
								<table id="dynamic-t" class="table table-striped">
												<thead>
													<tr>
														<th class="center">ID</th>
														<th><i class="fa fa-credit-card"></i> Cedula</th>
														<th><i class="fa fa-pencil"></i> Nombre y Apellido</th>
														<th><i class="fa fa-check-square-o"></i> Acciones</th>
													</tr>
												</thead>
												
												<tbody>
												<?php $i=0;?>			
												<?php while($reg=pg_fetch_object($objeto->consulta2)){?>
												<?php $i++;?>
													<tr>
														<td><?php echo $i;?></td>
														<td><?php echo $reg->nacionalidad."-".$reg->cedula;?></td>
														<td><?php echo $reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></td>
														<td>	
														<a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="edit-propietario_ocupante.php?id=<?php echo $reg->id;?>">
																<i class="ace-icon fa fa-pencil bigger-130"></i>
														</a>
														
														</td>
													</tr>
												<?php }?>
												</tbody>
											</table>
								<!-- PAGE CONTENT ENDS -->
							</div>
							<?php }	?>
							<!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<?php include('../layout/footer.php');?>
	<script>
    $(document).ready(function() {	
	 $('#dynamic-t').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });	
		$('.btnNew').click(function(){
	
			swal({
				title: "Alerta!",
				text: "Nuevo propietario/Ocupante",
				type: "info",
				showCancelButton: true,
				closeOnConfirm: false,
				confirmButtonText: 'Aceptar'
			},
			function(){
				$(location).attr('href','http:propietario_ocupante.php');
			});//swal
	
	});//click
	
    });///dom
    </script>
	</body>
</html>
