<?php include_once("seguridad.php"); ?>
<?php 
		include_once("clases/motor.php");
		include_once ('clases/pedul.php');
		$objeto = new Pedul();
		$objeto->parcelas();
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=8; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-map"></i> Parcelas</li> 
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12" >
								
								<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nueva Parcela"><i class="ace-icon fa fa-plus"></i></span></a><br><br><br>
								<!-- PAGE CONTENT BEGINS -->
								<table id="dynamic-table1" class="table table-striped">
												<thead>
													<tr>
														<th class="center">ID</th>
														<th><i class="fa fa-pencil"></i> Pacela</th>
														<th><i class="fa fa-pencil"></i> Manzana</th>
														<th><i class="fa fa-pencil"></i> Desripcion</th>
														<th><i class="fa fa-check-square-o"></i> Acciones</th>
													</tr>
												</thead>
												
												<tbody>
												<?php $i=0;?>			
												<?php while($reg=pg_fetch_object($objeto->consulta)){?>
												<?php $i++;?>
													<tr>
														<td><?php echo $i;?></td>
														<td><?php echo $reg->parcela;?></td>
														<td><?php echo $reg->manzana;?></td>
														<td><?php echo $reg->descripcion;?></td>
														<td>	
															<!-- <a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="#">
																<i class="ace-icon fa fa-pencil bigger-130"></i> -->
															<script>
																$(document).on('ready',function(event){
																event.preventDefault();
																
																$('.btnEdit<?php echo $i;?>').click(function(){
																
																swal({
																	title: "Actualizar",
																	text: "<?php echo "Parcela: ".$reg->parcela;?>",
																	type: "info",
																	showCancelButton: true,
																	confirmButtonColor: '#DD6B55',
																	closeOnConfirm: false,
																	confirmButtonText: 'Aceptar'
																},
																function(){
																	$(location).attr('href','http:edit-parcela.php?cod=<?php echo $reg->codigo;?>');
																});
																});
															});
															 </script>
															</a>
														</td>
													</tr>
												<?php }?>
												</tbody>
											</table>
								<!-- PAGE CONTENT ENDS -->
							</div>
							<!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<?php include('../layout/footer.php');?>
			<script>
    $(document).ready(function() {
        $('#dynamic-table1').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
        });
		
		$('.btnNew').click(function(){
	
			swal({
				title: "Alerta!",
				text: "Nueva Parcela",
				type: "info",
				showCancelButton: true,
				closeOnConfirm: false,
				confirmButtonText: 'Aceptar'
			},
			function(){
				$(location).attr('href','http:nueva_parcela.php');
			});//swal
	
	});//click
    });
    </script>
	</body>
</html>
