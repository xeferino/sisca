<?php 
include_once("seguridad.php");
include_once("clases/conexion.php");
include_once("clases/pedul.php");

$objeto = new Pedul();
$sector = $_POST['sector'];

$objeto->sector_calles($sector);
$objeto->datos($sector);

$fichaCatastral = new Ficha();
$ficha = $_POST['ficha'];

//var_dump($ficha);die();

$idpp = $_POST['idpp'];

$fichaCatastral->datosDireccionPersona($idpp);


?>
<?php 

$validar = pg_num_rows($fichaCatastral->query);

//var_dump($validar);die();

	if($validar>0){
?>
			<div class="row">
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Direccion del Propietario.</strong>
				</p>
			
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<textarea class="form-control" name="parroquia_persona" id="parroquia_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['parroquia'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<textarea class="form-control" name="ciudad_persona" id="ciudad_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['ciudad'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<textarea class="form-control" id="localidad_persona" name="localidad_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['localidad'];?></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizacion</label>
					<textarea class="form-control" id="urbanizacion_persona" name="urbanizacion_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['urbanizacion'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<textarea class="form-control" id="consejo_residencial_persona" name="consejo_residencial_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['consejo_residencial'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<textarea class="form-control"  id="barrio_persona" name="barrio_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['barrio'];?></textarea>
				</div>
				</div>


				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<textarea class="form-control number" id="numero_civico_persona" name="numero_civico_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['numero_civico'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Nombre Calle</label>
					<textarea class="form-control" id="nombre_calle_persona" name="nombre_calle_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['nombre_calle'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<textarea class="form-control" id="sector_persona" name="sector_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['sector'];?></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tipo de Calle</label>
						<select class="form-control  tipocalle" name="tipo_calle_persona" id="tipo_calle_persona" required="required">
					<option 
					<?php if($fichaCatastral->row['tipo_calle'] == "Av"){
							echo "selected='selected'";
						}?> value="Av">Av</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Clle"){
							echo "selected='selected'";
						}?>
						value="Clle">Clle</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Crr"){
							echo "selected='selected'";
						}?> 
						value="Crr">Crr</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Trav"){
							echo "selected='selected'";
						}?>
						value="Trav">Trav</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Prol"){
							echo "selected='selected'";
						}?>
						value="Prol">Prol</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cjn"){
							echo "selected='selected'";
						}?>
						value="Cjn">Cjn</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Psje"){
							echo "selected='selected'";
						}?>
						value="Psje">Psje</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Blv"){
							echo "selected='selected'";
						}?>
						value="Blv">Blv</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Vda"){
							echo "selected='selected'";
						}?>
						value="Vda">Vda</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Esc"){
							echo "selected='selected'";
						}?>
						value="Esc">Esc</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Snd"){
							echo "selected='selected'";
						}?>
						value="Snd">Snd</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Tcal"){
							echo "selected='selected'";
						}?>
						value="Tcal">Tcal</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cno"){
							echo "selected='selected'";
						}?>
						value="Cno">Cno</option>

						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<textarea class="form-control" id="punto_referencia_persona" name="punto_referencia_persona" cols="" rows="" required="required"><?php echo $fichaCatastral->row['puntoreferencia'];?></textarea>
				</div>
				</div>

			</div>
		</div>

<?php } else{
	?>
<div class="row">
			<div class="col-md-12">
				<p class="alert alert-info">
					<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese la Direccion del Propietario.</strong>
				</p>
			
				<div class="col-md-6">
					<div class="form-group">
					<label>Parroquia</label>
					<textarea class="form-control" name="parroquia_persona" id="parroquia_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Ciudad</label>
					<textarea class="form-control" name="ciudad_persona" id="ciudad_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Localidad</label>
					<textarea class="form-control" id="localidad_persona" name="localidad_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label>Urbanizacion</label>
					<textarea class="form-control" id="urbanizacion_persona" name="urbanizacion_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Consejo Residencial</label>
					<textarea class="form-control" id="consejo_residencial_persona" name="consejo_residencial_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Barrio</label>
					<textarea class="form-control"  id="barrio_persona" name="barrio_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>


				<div class="col-md-6">
					<div class="form-group">
					<label>N° Civico</label>
					<textarea class="form-control number" id="numero_civico_persona" name="numero_civico_persona" cols="" rows=""  required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Nombre Calle</label>
					<textarea class="form-control" id="nombre_calle_persona" name="nombre_calle_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
					<label>Sector</label>
					<textarea class="form-control" id="sector_persona" name="sector_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label>Tipo de Calle</label>
						<select class="form-control  tipocalle" name="tipo_calle_persona" id="tipo_calle_persona" required="required">
							<option value="">--Selecccione--</option>
							<option value="Av">Av</option>
							<option value="Clle">Clle</option>
							<option value="Crr">Crr</option>
							<option value="Trav">Trav</option>
							<option value="Prol">Prol</option>
							<option value="Cjn">Cjn</option>
							<option value="Psje">Psje</option>
							<option value="Blv">Blv</option>
							<option value="Vda">Vda</option>
							<option value="Esc">Esc</option>
							<option value="Snd">Snd</option>
							<option value="Tcal">Tcal</option>
							<option value="Cno">Cno</option>
						</select>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group">
					<label>Punto de Referencia</label>
					<textarea class="form-control" id="punto_referencia_persona" name="punto_referencia_persona" cols="" rows="" required="required"></textarea>
				</div>
				</div>

			</div>
		</div>
<?php } ?>

<script type="text/javascript">
	$(".tipocalle").select2();
	$(".manzana").select2();
</script>