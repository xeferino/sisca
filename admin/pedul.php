<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");
include_once ('clases/pedul.php');
$objeto = new Pedul();
$objeto->pedules();
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=8; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="./">Home</a>							</li>
<li class="active"><i class="ace-icon fa fa-map"></i> Sectores</li> 
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">
<div class="col-xs-12" >

<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nuevo Pedul"><i class="ace-icon fa fa-plus"></i></span></a><br><br><br>
<!-- PAGE CONTENT BEGINS -->
<table id="dynamic-table1" class="table table-striped">
<thead>
	<tr>
		<th class="center">ID</th>
		<th><i class="fa fa-credit-card"></i> Codigo</th>
		<th><i class="fa fa-pencil"></i> Sector</th>
		<th><i class="fa fa-pencil"></i> Calles</th>
		<th><i class="fa fa-pencil"></i> Cat. de Manzanas</th>
		<th><i class="fa fa-pencil"></i> Desripcion</th>
		<th><i class="fa fa-check-square-o"></i> Acciones</th>
	</tr>
</thead>

<tbody>
<?php $i=0;?>			
<?php while($reg=pg_fetch_object($objeto->consulta)){

	$consulta = "SELECT 
		cl.nombre as calle
		FROM
		tb_manzana as cl
		LEFT JOIN tb_pedul_manzana as clm on clm.id_m = cl.id
		LEFT JOIN tb_pedul as pcl on pcl.id = clm.id_p
		WHERE clm.id_p ='$reg->id_sector'";
	$query = pg_query($consulta);
	$validar = pg_num_rows($query);

	
	?>
<?php $i++;?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $reg->codigo_sector;?></td>
		<td><?php echo $reg->sector_nombre;?></td>
		<td>			
			<?php
			if ($validar>0){
			while($c=pg_fetch_object($query)){
			 	echo $c->calle.", ";
			 }
			}
			else
			{
				echo "No posee calles asignadas";
			}
			?>
		</td>
		<td align="center"><?php echo $reg->cantidad;?></td>
		<td><?php echo $reg->descripcion_sector;?></td>
		<td>	
			<a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="edit-pedul.php?cod=<?php echo $reg->id_sector;?>">
				<i class="ace-icon fa fa-pencil bigger-130"></i>
			
			</a>
		</td>
	</tr>
<?php }?>
</tbody>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
<!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<?php include('../layout/footer.php');?>
<script>
$(document).ready(function() {
$('#dynamic-table1').DataTable({
responsive: true,
aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
});

$('.btnNew').click(function(){

swal({
title: "Alerta!",
text: "Nuevo Sector",
type: "info",
showCancelButton: true,
closeOnConfirm: false,
confirmButtonText: 'Aceptar'
},
function(){
$(location).attr('href','http:nuevo_pedul.php');
});//swal

});//click
});
</script>
</body>
</html>
