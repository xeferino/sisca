<?php 
include_once("seguridad.php");
include_once("clases/conexion.php");
include_once("clases/pedul.php");

$objeto = new Pedul();
$sector = $_POST['sector'];

$objeto->sector_calles($sector);
$objeto->datos($sector);

$fichaCatastral = new Ficha();
$ficha = $_POST['ficha'];

//var_dump($ficha);die();

$fichaCatastral->datosFichaCastastral($ficha);


?>
<?php 

$validar = pg_num_rows($objeto->pgQuery);
	if($validar>0){
?>
<div class="col-md-12">
	<div class="col-md-12">
		<div class="form-group">
			<label>Tipo de Calle</label>
			<select class="form-control  calle" name="tipo_calle" required="required">
				<?php if(isset($ficha) && isset($sector)){?>
					<option 
					<?php if($fichaCatastral->row['tipo_calle'] == "Av"){
							echo "selected='selected'";
						}?> value="Av">Av</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Clle"){
							echo "selected='selected'";
						}?>
						value="Clle">Clle</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Crr"){
							echo "selected='selected'";
						}?> 
						value="Crr">Crr</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Trav"){
							echo "selected='selected'";
						}?>
						value="Trav">Trav</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Prol"){
							echo "selected='selected'";
						}?>
						value="Prol">Prol</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cjn"){
							echo "selected='selected'";
						}?>
						value="Cjn">Cjn</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Psje"){
							echo "selected='selected'";
						}?>
						value="Psje">Psje</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Blv"){
							echo "selected='selected'";
						}?>
						value="Blv">Blv</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Vda"){
							echo "selected='selected'";
						}?>
						value="Vda">Vda</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Esc"){
							echo "selected='selected'";
						}?>
						value="Esc">Esc</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Snd"){
							echo "selected='selected'";
						}?>
						value="Snd">Snd</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Tcal"){
							echo "selected='selected'";
						}?>
						value="Tcal">Tcal</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cno"){
							echo "selected='selected'";
						}?>
						value="Cno">Cno</option>
					<?php }elseif(isset($sector)){ ?>

						<option value="">--Selecccione--</option>
						<option value="Av">Av</option>
						<option value="Clle">Clle</option>
						<option value="Crr">Crr</option>
						<option value="Trav">Trav</option>
						<option value="Prol">Prol</option>
						<option value="Cjn">Cjn</option>
						<option value="Psje">Psje</option>
						<option value="Blv">Blv</option>
						<option value="Vda">Vda</option>
						<option value="Esc">Esc</option>
						<option value="Snd">Snd</option>
						<option value="Tcal">Tcal</option>
						<option value="Cno">Cno</option>
					<?php }?>
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Calles</label>
			<select class="form-control  calle" name="nombre_calle" required="required">
				<?php if(isset($ficha) && isset($sector)){?>
				<?php $i=0;?>
				<?php while($reg=pg_fetch_object($objeto->pgQuery)){?>
				<?php $i++;?>
					<option <?php if($fichaCatastral->row['nombre_calle'] == $reg->calle){
							echo "selected='selected'";
						}?>
						value="<?php echo $reg->calle;?>"><?php echo $reg->calle;?></option>
				<?php }?>
				<?php }elseif(isset($sector)){?>
				<option value="">--Selecccione--</option>
				<?php while($reg=pg_fetch_object($objeto->pgQuery)){?>
				<?php $i++;?>
					<option value="<?php echo $reg->calle;?>"><?php echo $reg->calle;?></option>
				<?php }?>
				<?php }?>
			</select>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label>Manzanas</label>
			<select class="form-control  manzana" name="manzanas" required="required">
				
			<?php if(isset($ficha) && isset($sector)){?>
				<?php for($i=1; $i<=$objeto->cantidad_manzana; $i++){?>
					<option <?php if($fichaCatastral->row['manzana'] == $i){
							echo "selected='selected'";
						}?>
						value="<?php echo $i;?>"><?php echo " Manzana: ".$i;?></option>
				<?php }?>
			<?php }elseif(isset($sector)){?>

				<option value="">--Selecccione--</option>
				<?php for($i=1; $i<=$objeto->cantidad_manzana; $i++){?>
					<option value="<?php echo $i;?>"><?php echo " Manzana: ".$i;?></option>
				<?php }?>
				<?php }?>
			</select>
		</div>
	</div>
</div>

<?php } else{
	?>

<div class="col-md-12">
	<div class="col-md-12">
		<div class="form-group">
			<label>Tipo de Calle</label>
			<select class="form-control  calle" name="tipo_calle" required="required">
				<?php if(isset($ficha) && isset($sector)){?>
					<option 
					<?php if($fichaCatastral->row['tipo_calle'] == "Av"){
							echo "selected='selected'";
						}?> value="Av">Av</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Clle"){
							echo "selected='selected'";
						}?>
						value="Clle">Clle</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Crr"){
							echo "selected='selected'";
						}?> 
						value="Crr">Crr</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Trav"){
							echo "selected='selected'";
						}?>
						value="Trav">Trav</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Prol"){
							echo "selected='selected'";
						}?>
						value="Prol">Prol</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cjn"){
							echo "selected='selected'";
						}?>
						value="Cjn">Cjn</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Psje"){
							echo "selected='selected'";
						}?>
						value="Psje">Psje</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Blv"){
							echo "selected='selected'";
						}?>
						value="Blv">Blv</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Vda"){
							echo "selected='selected'";
						}?>
						value="Vda">Vda</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Esc"){
							echo "selected='selected'";
						}?>
						value="Esc">Esc</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Snd"){
							echo "selected='selected'";
						}?>
						value="Snd">Snd</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Tcal"){
							echo "selected='selected'";
						}?>
						value="Tcal">Tcal</option>
					<option <?php if($fichaCatastral->row['tipo_calle'] == "Cno"){
							echo "selected='selected'";
						}?>
						value="Cno">Cno</option>
					<?php }elseif(isset($sector)){ ?>

						<option value="">--Selecccione--</option>
						<option value="Av">Av</option>
						<option value="Clle">Clle</option>
						<option value="Crr">Crr</option>
						<option value="Trav">Trav</option>
						<option value="Prol">Prol</option>
						<option value="Cjn">Cjn</option>
						<option value="Psje">Psje</option>
						<option value="Blv">Blv</option>
						<option value="Vda">Vda</option>
						<option value="Esc">Esc</option>
						<option value="Snd">Snd</option>
						<option value="Tcal">Tcal</option>
						<option value="Cno">Cno</option>
					<?php }?>
			</select>
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<label>Calle</label>
			<?php if(isset($ficha) && isset($sector)){?>
				<input class="form-control " name="nombre_calle"   value="<?php echo $fichaCatastral->row['nombre_calle'];?>" type="text" required="required">
			<?php }elseif(isset($sector)){?>
				<input class="form-control " name="nombre_calle"   value="" type="text" required="required">
			<?php }?>
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
			<label>Manzanas</label>
			<select class="form-control  manzana" name="manzanas" required="required">
				
			<?php if(isset($ficha) && isset($sector)){?>
				<?php for($i=1; $i<=$objeto->cantidad_manzana; $i++){?>
					<option <?php if($fichaCatastral->row['manzana'] == $i){
							echo "selected='selected'";
						}?>
						value="<?php echo $i;?>"><?php echo " Manzana: ".$i;?></option>
				<?php }?>
			<?php }elseif(isset($sector)){?>

				<option value="">--Selecccione--</option>
				<?php for($i=1; $i<=$objeto->cantidad_manzana; $i++){?>
					<option value="<?php echo $i;?>"><?php echo " Manzana: ".$i;?></option>
				<?php }?>
				<?php }?>
			</select>
		</div>
	</div>
</div>

<?php } ?>

<script type="text/javascript">
	$(".calle").select2();
	$(".manzana").select2();
</script>