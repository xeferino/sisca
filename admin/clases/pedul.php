<?php
	class Pedul
	{
			public $id;
			public $codigo;
			public $nombre;
			public $descripcion;
			public $calle;
			
			public $mensaje;
			
			public function cargar($id, $codigo, $nombre, $descripcion, $cantidad_manzana, $calle)
			{
				$this->id=$id;
				$this->codigo=$codigo;
				$this->nombre=$nombre;
				$this->descripcion=$descripcion;
				$this->cantidad_manzana=$cantidad_manzana;
				$this->calle=$calle;	
			}//fin de function

			public function datos($cod)
		{
			$this->cod=$cod;
			
			$query=pg_query("SELECT 
  				* 
				FROM 
				public.tb_pedul 
				WHERE 
				tb_pedul.id='$this->cod'");
				$reg=pg_fetch_object($query);
				$this->id=$reg->id;
				$this->codigo=$reg->codigo;
				$this->nombre=$reg->nombre;
				$this->cantidad_manzana=$reg->cantidad_manzana;
				$this->descripcion = $reg->descripcion;
		}//fin de function
		
		public function registrar()
		{
			$c = $this->calle;
			
			if (($this->codigo!='')&&($this->nombre!='')&&($this->descripcion!='') &&($this->cantidad_manzana!='') /*&&($this->calle!='')*/){
					
				$consulta = pg_num_rows (pg_query("SELECT codigo FROM tb_pedul WHERE codigo='$this->codigo'"));
				if($consulta==0){
						
						$pedul = pg_query("insert into tb_pedul values (default,'$this->codigo', '$this->nombre', '$this->descripcion', '$this->cantidad_manzana')");				
						$validar = pg_affected_rows($pedul);
						
						if($validar==1){
													
							$query = pg_query("SELECT id FROM tb_pedul ORDER BY id DESC");
							$row = pg_fetch_array($query);
							$cod  = $row['id'];
							
							for ($i=0;$i<count($c);$i++){ 
							  $pedul_manzana = pg_query("insert into tb_pedul_manzana values (default, '$cod', '$c[$i]')");
							}
						}						
						$this->codigo="";
						$this->nombre="";
						$this->descripcion="";
						$this->cantidad_manzana="";
						$this->mensaje=1;
				}
				else{
					$this->mensaje=2;
				}	
			}
			else{
				$this->mensaje=3;
			}
		}//fin de function

		public function actualizar($idPedul)
		{
			$c = $this->calle;
			$this->idPedul = $idPedul;
						
			if (($this->codigo!='')&&($this->nombre!='')&&($this->descripcion!='') &&($this->cantidad_manzana!='')){
					
				$consulta = pg_num_rows (pg_query("SELECT id FROM tb_pedul WHERE id='$this->idPedul'"));
				
				if($consulta>0){

					$pedul = "update tb_pedul set codigo='$this->codigo', nombre='$this->nombre', descripcion='$this->descripcion', cantidad_manzana='$this->cantidad_manzana' where id='$this->idPedul'";
					$sql = pg_query ($pedul);
					

					//var_dump($this->idPedul);die();

					$validar = pg_affected_rows($sql);

					$delete_actualizar = "delete from tb_pedul_manzana where id_p='$this->idPedul'";
					$query = pg_query ($delete_actualizar);

					$delete = pg_affected_rows($query);

					if($delete){
						for ($i=0;$i<count($c);$i++){ 
							$pedul_manzana = pg_query("insert into tb_pedul_manzana values (default, '$this->idPedul', '$c[$i]')");
						}
					}
					//$this->mensaje=1;
				}						
						$this->codigo="";
						$this->nombre="";
						$this->descripcion="";
						$this->cantidad_manzana="";
						$this->calle[]="";
						$this->mensaje=1;
				/*}
				else{
					$this->mensaje=2;
				}*/	
			}
			else{
				$this->mensaje=3;
			}
		}//fin de function
	
		public function pedules()
		{
			$this->consulta= pg_query("SELECT
										tb_pedul.id AS id_sector,
										tb_pedul.codigo AS codigo_sector,
										tb_pedul.nombre AS sector_nombre,
										tb_pedul.descripcion AS descripcion_sector,
										tb_pedul.cantidad_manzana AS cantidad

										FROM
										tb_pedul");
		}//fin de function

		public function pedules_calles($idsector)
		{
			$this->idsector=$idsector;
			$this->consulta= pg_query("SELECT 
				cl.nombre as calle
				FROM
				tb_manzana as cl
				LEFT JOIN tb_pedul_manzana as clm on clm.id_m = cl.id
				LEFT JOIN tb_pedul as pcl on pcl.id = clm.id_p
				WHERE clm.id_p ='$this->idsector'");

			$this->calles = array();
			while ($row = pg_fetch_array($this->consulta)){
				$this->calles[] = $row;
			}//fin while

			return $this->calles;

		}//fin de function

		public function sector_calles($idsector)
		{
			$this->idsector = $idsector;

			$consulta = "SELECT 
				clm.id_m as idcalle,
				cl.nombre AS calle
				FROM
				tb_manzana as cl
				LEFT JOIN tb_pedul_manzana as clm on clm.id_m = cl.id
				LEFT JOIN tb_pedul as pcl on pcl.id = clm.id_p
				WHERE clm.id_p ='$this->idsector'";
			$this->pgQuery = pg_query($consulta);

		}//fin de function

		public function datosParelas($cod)
		{
			$this->cod=$cod;
			
			$query=pg_query("SELECT 
  				* 
				FROM 
				public.tb_manzana,
				public.tb_parcela
				WHERE 
				tb_manzana.id = tb_pparecla.codigo='$this->cod'");
				$reg=pg_fetch_object($query);
				$this->codigo=$reg->codigo;
				$this->nombre=$reg->nombre;
				$this->nombre=$reg->nombre;
				$this->descripcion = $reg->descripcion;
		}//fin de function
		
		public function registrarParcela($id, $nombre, $descripcion)
		{
			$this->id = $id;
			$this->nombre = $nombre;
			$this->descripcion = $descripcion;
			
			if (($this->id!='')&&($this->nombre!='')&&($this->descripcion!='') ){
					
						$parcela = pg_query("insert into tb_parcela values (default,'$this->nombre',
						'$this->descripcion', '$this->id')");								
						$this->nombre="";
						$this->descripcion="";
						$this->mensaje=1;	
			}
			else{
				$this->mensaje=3;
			}
		}//fin de function

		public function actualizarParecla($idPedul)
		{
			//$this->cargar();

			$c = $this->calle;
			$this->idPedul = $idPedul;
			//echo $this->idPedul;
			
			if (($this->codigo!='')&&($this->nombre!='')&&($this->descripcion!='') &&($this->calle!='')){
					
				$consulta = pg_num_rows (pg_query("SELECT id FROM tb_pedul WHERE id='$this->idPedul'"));
				if($consulta>0){

					/*$codigo = pg_num_rows (pg_query("SELECT codigo FROM tb_pedul WHERE codigo='$this->codigo'"));
					if($codigo==0){*/
						
						$pedul = "update tb_pedul set codigo='$this->codigo', nombre='$this->nombre', descripcion='$this->descripcion' where id='$this->idPedul'";
							$sql = pg_query ($pedul);
							$this->mensaje=1;

						$validar = pg_affected_rows($sql);

					/*}
					else{
						$this->mensaje=2;
					}*/

						/*$pedul = pg_query("insert into tb_pedul values (default,'$this->codigo', '$this->nombre', 
						'$this->descripcion')");				
						$validar = pg_affected_rows($pedul);
						
						if($validar==1){
													
							$query = pg_query("SELECT codigo FROM tb_pedul ORDER BY codigo DESC");
							$row = pg_fetch_array($query);
							$cod  = $row['codigo'];
							
							for ($i=0;$i<count($c);$i++){ 
							  $pedul_manzana = pg_query("insert into tb_pedul_manzana values (default, '$cod', '$c[$i]')");
							}*/
				}						
						$this->codigo="";
						$this->nombre="";
						$this->descripcion="";
						$this->mensaje=1;
				/*}
				else{
					$this->mensaje=2;
				}*/	
			}
			else{
				$this->mensaje=3;
			}
		}//fin de function
	
		public function parcelas()
		{
			$this->consulta= pg_query("SELECT 
										tb_manzana.codigo AS codmanzana, 
										tb_manzana.nombre AS manzana, 
										tb_parcela.codigo, 
										tb_parcela.nombre AS parcela, 
										tb_parcela.descripcion, 
										tb_parcela.id_m, 
										tb_manzana.id
										FROM 
										public.tb_manzana, 
										public.tb_parcela
										WHERE 
										tb_manzana.id = tb_parcela.id_m
			");
		}//fin de function
	}//fin de class
	?>