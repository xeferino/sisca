<?php
	class Solvencia
	{
			public $id;
			public $monto;
			public $estatus;
			public $fecha;
			public $anualidad;
			public $id_persona;
			
			public $mensaje;
		
			public function cargar($id,$monto,$estatus,$fecha, $anualidad, $id_persona)
			{
				$this->id = $id;
				$this->monto = $monto;
				$this->estatus = $estatus;
				$this->fecha = $fecha;
				$this->anualidad = $anualidad;
				$this->id_persona = $id_persona;
			}

			public function registrar()
			{
				if($this->monto!='' && $this->estatus!='0' && $this->fecha!='' && $this->anualidad!='0' ){

						$validarSolevncia = pg_num_rows (pg_query("SELECT * FROM tb_persona as p, tb_solvencia_municipal as s WHERE p.id=s.id_persona AND anualidad = '$this->anualidad' AND s.id_persona ='$this->id_persona'"));
						if($validarSolevncia==0){

							$solvencia = "insert into tb_solvencia_municipal
							values (default, '$this->monto', '$this->estatus', '$this->fecha', '$this->id_persona', '$this->anualidad')";
							$consulta = pg_query ($solvencia);
							$this->mensaje=1;
							$this->monto="";
							$this->fecha="";
						}
						else{
							
							$this->mensaje=2;
						}

					//$validar=pg_affected_rows($persona);
				}
				else{
					
					$this->mensaje=3;
				}
			}

			public function editar($id_solvencia)
			{
				$this->id_solvencia = $id_solvencia;

				//var_dump($id_solvencia);die();

				if($this->monto!='' && $this->estatus!='0' && $this->fecha!='' && $this->anualidad!='0' )
				{

					$solvencia = "UPDATE tb_solvencia_municipal SET monto='$this->monto', fecha='$this->fecha', id_persona='$this->id_persona', anualidad='$this->anualidad' WHERE id='$this->id_solvencia'";
					$consulta = pg_query ($solvencia);
					$this->mensaje=1;	
				}
				else{
					
					$this->mensaje=3;
				}
			}

			public function solvenciasMuncipales()
			{
				
				$this->query = pg_query("SELECT * from view_solvencia_municipal");
				$this->consulta = pg_num_rows($this->query);
				$this->solvencias = array();
				while ($row = pg_fetch_object($this->query)){
					$this->solvencias[] = $row;
				}//fin while			
			}

			public function datoSolvencia($id_solvencia)
			{
				
				$this->id_solvencia = $id_solvencia;
				$datoSolvencia = pg_query("SELECT
												p.id,
												p.nacionalidad,
												p.cedula,
												p.nombre1,
												p.apellido1,
												s.id as id_solvencia,
												s.monto,
												s.estatus,
												s.fecha,
												s.anualidad
												FROM
												tb_persona AS P,
												tb_solvencia_municipal AS S
												WHERE
												P.id = S.id_persona
												AND S.id = '$this->id_solvencia'
												");

					$reg=pg_fetch_array($datoSolvencia);
					$this->id=$reg['id'];
					$this->id_solvencia=$reg['id_solvencia'];
					$this->nacionalidad=$reg['nacionalidad'];
					$this->cedula=$reg['cedula'];
					$this->nombre1=$reg['nombre1'];
					$this->apellido1=$reg['apellido1'];
					$this->monto=$reg['monto'];
					$this->fecha=$reg['fecha'];
					$this->anualidad=$reg['anualidad'];
							
			}
		
	}//fin de class
?>