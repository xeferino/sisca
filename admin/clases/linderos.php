<?php
	class linderos
	{
			/*public $nacionalidad;
			public $cedula;
			public $nombre1;
			public $nombre2;
			public $apellido1;
			public $apellido2;
			public $telefono1;
			public $telefono2;
			*/
			public $area;
			public $snorte;
			public $ssur;
			public $seste;
			public $soeste;
			public $lnorte;
			public $lsur;
			public $leste;
			public $loeste;
			public $fecha;
			public $ubicacion;
			public $parroquia;
			public $observacion;
			public $id_persona;
			
			public $mensaje;
		
			public function cargar($area,$snorte,$ssur,$seste,$soeste,$lnorte,$lsur,$leste,$loeste,$fecha, $ubicacion,$observacion, $id_persona, $parroquia)
			{
				/*$this->nacionalidad = $nacionalidad;
				$this->cedula = $cedula;
				$this->nombre1 = $nombre1;
				$this->nombre2 = $nombre2;
				$this->apellido1 = $apellido1;
				$this->apellido2 = $apellido2;
				$this->telefono1 = $telefono1;
				$this->telefono2 = $telefono2;*/
				$this->area = $area;
				$this->snorte = $snorte;
				$this->ssur = $ssur;
				$this->seste = $seste;
				$this->soeste = $soeste;
				$this->lnorte = $lnorte;
				$this->lsur = $lsur;
				$this->leste = $leste;
				$this->loeste = $loeste;
				$this->fecha = $fecha;
				$this->ubicacion = $ubicacion;
				$this->observacion = $observacion;
				$this->id_persona = $id_persona;
				$this->parroquia = $parroquia;
			}

			public function datoCertificacion($id_certificacion)
			{
				
				$this->id_certificacion = $id_certificacion;
				$datoCertificacion = pg_query("SELECT
					tb_certificacion_linderos.id,
					tb_certificacion_linderos.area,
					tb_certificacion_linderos.norte,
					tb_certificacion_linderos.sur,
					tb_certificacion_linderos.este,
					tb_certificacion_linderos.oeste,
					tb_certificacion_linderos.lnorte,
					tb_certificacion_linderos.lsur,
					tb_certificacion_linderos.leste,
					tb_certificacion_linderos.loeste,
					tb_certificacion_linderos.fecha,
					tb_certificacion_linderos.observacion,
					tb_certificacion_linderos.ubicacion,
					tb_certificacion_linderos.id_parroquia,
					tb_persona.id as id_persona,
					tb_persona.nacionalidad,
					tb_persona.cedula,
					tb_persona.nombre1,
					tb_persona.nombre2,
					tb_persona.apellido1,
					tb_persona.apellido2
					FROM
					tb_certificacion_linderos as tb_certificacion_linderos
					LEFT JOIN tb_persona as tb_persona on tb_persona.id = tb_certificacion_linderos.id_persona
					LEFT JOIN tb_parroquia as tb_parroquia on tb_parroquia.id = tb_certificacion_linderos.id_parroquia
					WHERE
					tb_certificacion_linderos.id = '$this->id_certificacion'");

					$reg=pg_fetch_array($datoCertificacion);
					$this->id=$reg['id'];
					$this->id_persona=$reg['id_persona'];
					$this->nacionalidad=$reg['nacionalidad'];
					$this->cedula=$reg['cedula'];
					$this->nombre1=$reg['nombre1'];
					$this->nombre1=$reg['nombre2'];
					$this->apellido1=$reg['apellido1'];
					$this->apellido1=$reg['apellido2'];

					$this->snorte=$reg['lnorte'];
					$this->ssur=$reg['lsur'];
					$this->seste=$reg['leste'];
					$this->soeste=$reg['loeste'];
					$this->norte=$reg['norte'];
					$this->sur=$reg['sur'];
					$this->este=$reg['este'];
					$this->oeste=$reg['oeste'];
					$this->area=$reg['area'];
					$this->fecha=$reg['fecha'];
					$this->ubicacion=$reg['ubicacion'];
					$this->observacion=$reg['observacion'];
					$this->id_parroquia=$reg['id_parroquia'];
							
			}
		

			public function registrar()
			{
				if($this->snorte!='' && $this->ssur!='' && $this->seste!='' && $this->soeste!='' &&$this->lnorte!='' && $this->lsur!='' && $this->leste!='' && $this->loeste!='' && $this->fecha!='' && $this->ubicacion!='' && $this->observacion!='' && $this->id_persona!='' && $this->parroquia!=''){

					$lindero = "insert into tb_certificacion_linderos 
							values (default,'$this->snorte', '$this->ssur','$this->seste', 
							'$this->soeste','$this->area','$this->lnorte', '$this->lsur',
							'$this->leste', '$this->loeste','$this->ubicacion','$this->fecha', 
							'$this->observacion','$this->id_persona', '$this->parroquia')";
							//echo $lindero;
							$consulta1 = pg_query ($lindero);
							$this->mensaje=1;
						
							$this->snorte = '';
							$this->ssur = '';
							$this->seste = '';
							$this->soeste = '';
							$this->lnorte = '';
							$this->lsur = '';
							$this->leste = '';
							$this->loeste = '';
							$this->fecha = '';
							$this->observacion = '';
							$this->ubicacion = '';
							$this->parroquia = '';

				
			}
			else{
					$this->mensaje=3;
				}
		}

		public function editar($id_certificacion)
		{

			$this->id_certificacion = $id_certificacion;

				if($this->snorte!='' && $this->ssur!='' && $this->seste!='' && $this->soeste!='' &&$this->lnorte!='' && $this->lsur!='' && $this->leste!='' && $this->loeste!='' && $this->fecha!='' && $this->ubicacion!='' && $this->observacion!='' && $this->id_persona!=''){

					$lindero = "update tb_certificacion_linderos 
							set area='$this->area', lnorte='$this->snorte', lsur='$this->ssur',
							leste='$this->seste', loeste='$this->soeste', norte='$this->lnorte', sur='$this->lsur',	este='$this->leste', oeste='$this->loeste', fecha='$this->fecha', observacion='$this->observacion', id_persona='$this->id_persona', id_parroquia='$this->parroquia', ubicacion='$this->ubicacion' WHERE id='$this->id_certificacion'";
							$consulta = pg_query ($lindero);
							$this->mensaje=1;
							
							$this->snorte = '';
							$this->ssur = '';
							$this->seste = '';
							$this->soeste = '';
							$this->lnorte = '';
							$this->lsur = '';
							$this->leste = '';
							$this->loeste = '';
							$this->fecha = '';
							$this->observacion = '';
							$this->ubicacion = '';
			}
			else{
					$this->mensaje=3;
				}
		}
		
		public function linderos_personas()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  view_certificacion_linderos"));
			if ($this->validar>0){
				$this->consulta= pg_query("SELECT
					tb_certificacion_linderos.id,
					tb_certificacion_linderos.area,
					tb_certificacion_linderos.norte,
					tb_certificacion_linderos.sur,
					tb_certificacion_linderos.este,
					tb_certificacion_linderos.oeste,
					tb_certificacion_linderos.lnorte,
					tb_certificacion_linderos.lsur,
					tb_certificacion_linderos.leste,
					tb_certificacion_linderos.loeste,
					tb_certificacion_linderos.fecha,
					tb_certificacion_linderos.observacion,
					tb_certificacion_linderos.ubicacion,
					tb_certificacion_linderos.id_parroquia,
					tb_persona.id as id_persona,
					tb_persona.nacionalidad,
					tb_persona.cedula,
					tb_persona.nombre1,
					tb_persona.nombre2,
					tb_persona.apellido1,
					tb_persona.apellido2
					FROM
					tb_certificacion_linderos as tb_certificacion_linderos
					LEFT JOIN tb_persona as tb_persona on tb_persona.id = tb_certificacion_linderos.id_persona
					LEFT JOIN tb_parroquia as tb_parroquia on tb_parroquia.id = tb_certificacion_linderos.id_parroquia");
			}
			else{
				$this->mensaje=1;
			}
		}//fin de function

		public function linderoEstadistica()
		{
			
				$lindero= pg_query("SELECT COUNT(*) FROM  tb_certificacion_linderos");
				$this->estadistica = pg_fetch_array($lindero);
		}//fin de function
		
	}//fin de class
	?>