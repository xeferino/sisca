<?php
	class Usuario
	{
			public $id;
			public $nick;
			public $pass;
			public $role;
			public $estatus;
			public $id_persona;
			public $mensaje;
		
		public function inicializar_datos ( $id, $nick, $pass, $role, $estatus, $id_persona)
			{
				$this->id=$id;
				$this->nick=$nick;
				$this->pass=$pass;
				$this->role=$role;
				$this->estatus=$estatus;
				$this->id_persona=$id_persona;
				
				$this->mensaje=$mensaje;
			}//fin de function
			
		public function login ()
		{	
			if(( $this->nick !='' ) && ( $this->pass !='' ))
			{
				//$clave = md5($this->clave);
				$consulta= pg_query("SELECT 
									  tb_persona.ci, 
									  tb_persona.n, 
									  tb_persona.nombre, 
									  tb_persona.apellido, 
									  tb_persona.correo, 
									  tb_persona.tlf, 
									  tb_usuario.nick, 
									  tb_usuario.pass, 
									  tb_usuario.role, 
									  tb_usuario.estatus, 
									  tb_usuario.id_usuario, 
									  tb_persona.id_persona, 
									  tb_usuario.id_persona
									FROM 
									  public.tb_persona, 
									  public.tb_usuario
									WHERE 
									  tb_persona.id_persona = tb_usuario.id_persona 
									  AND tb_usuario.nick='$this->nick' AND tb_usuario.pass='$this->pass';");
				$row = pg_num_rows ($consulta);
				if($row>0)
				{	
					$reg=pg_fetch_object($consulta);
	
					if($reg->estatus == "Activo")
					{	
						$_SESSION['role']=$reg->role;
						$_SESSION['nick']=$reg->nick;
						$_SESSION['nombre']=$reg->nombre;
						$_SESSION['apellido']=$reg->apellido;
						$_SESSION['conectado'] = $reg->id_usuario;
						header("Location: ./");
					}
					else 
					{
						$this->mensaje=1;
					}
				}
				else 
				{
					$this->mensaje=2;
				}
			}
			else 
			{
				$this->mensaje=3;
			}
		}//fin de function
		
		public function logout()
		{
			session_destroy();
			header("Location: .././login.php");
		}//fin de function
		
		public function usuarios()
		{
			$this->consulta= pg_query("SELECT *	FROM 
										  public.tb_persona, 
										  public.tb_usuario
										WHERE 
										  tb_persona.id_persona = tb_usuario.id_persona
										ORDER BY id_usuario asc;");
		}//fin de function

	}//fin de class
	?>