<?php
	class Ficha
	{
		public $control_archivo;
		public $fecha_ficha;
		public $region_rederal_gobierno;
		public $comuna;
		public $consejo_comunal;
		public $comite_tierra_urbano;
		public $registro_mvv;
		public $manzanas;
		public $parroquia;
		public $ciudad;
		public $localidad;
		public $urbanizacion;
		public $consejo_residencial;
		public $barrio;
		public $sector;
		public $numero_civico;
		public $punto_referencia;
		public $servicios;
		public $topografia;
		public $acceso;
		public $forma;
		public $ubicacion;
		public $entorno_fisico;
		public $mejoras_terreno;
		public $tenencia_terreno;
		public $regimen_propiedad;
		public $uso_actual_terreno;
		public $tipo_construccion;
		public $descripcion_uso;
		public $tenencia_construccion;
		public $reg_propiedad_const;
		public $soporte_const;
		public $techo_const;
		public $cubierta_interna;
		public $cubierta_externa;
		public $tipo_pared;
		public $acabado_pared;
		public $tipo_pintura_pared;
		public $instalacciones_electricas;
		public $pisos_construccion;
		public $banoWC;
		public $bano_comunitario;
		public $bano_ducha;
		public $bano_lavamanos;
		public $bano_banera;
		public $bano_bidet;
		public $bano_letrina;
		public $bano_ceramica1;
		public $bano_ceramica2;
		public $puerta_metalica;
		public $puerta_madera_cep;
		public $puerta_madera_ecoc;
		public $puerta_entamborada;
		public $puerta_seguridad;
		public $puerta_otra;
		public $puerta_otra_cantidad;
		public $Ventanal_hierro;
		public $Ventanal_madera;
		public $Ventanal_aluminio;
		public $Basculante_hierro;
		public $Basculante_aluminio; 
		public $Basculante_madera;
		public $Bastiente_hierro;
		public $Bastiente_aluminio;
		public $Bastiente_madera;
		public $Celosia_hierro;
		public $Celosia_aluminio;
		public $Celosia_madera;
		public $Corredera_hierro;
		public $Corredera_aluminio;
		public $Corredera_madera;
		public $Panoramica_hierro;
		public $Panoramica_aluminio;
		public $Panoramica_madera;
		public $anos_const;
		public $refaccion_const;
		public $numeroniveles_const;
		public $anosrefaccion_const;
		public $edadefectiva_const;
		public $edificaciones_const;
		public $estado_conservacion_const;
		public $observaciones_const;
		public $ocupante;
		public $propietario;
		public $parroquia_persona;
		public $ciudad_persona;
		public $localidad_persona;
		public $urbanizacion_persona;
		public $consejo_residencial_persona;
		public $barrio_persona;
		public $sector_persona;
		public $numero_civico_persona;
		public $punto_referencia_persona;
		public $numero_registro;
		public $folio_registro;
		public $tomo_registro;
		public $protocolo_registro;
		public $registro_fecha;
		public $registro_aream2terreno;
		public $registro_aream2const;
		public $registro_monto;
		public $area_metros_valor;
		public $valor_unitario_valor;
		public $sector_valor;
		public $factor_ajuste_valor_forma;
		public $factor_ajuste_valor_area;
		public $valor_ajustado_bs_valor;
		public $Observaciones_economia_terreno;
		public $tipologia_economia;
		public $area_mt_economia;
		public $valor_mt2_economia;
		public $depreciacion_economia;
		public $valor_actual_economia;
		public $Observaciones_economia;
		public $norte_linderos;
		public $sur_linderos;
		public $este_linderos;
		public $oeste_linderos;
		public $norte_coord;
		public $este_coord;
		public $huso_coord;
		public $Observaciones_generales;
		public $fechavisita;
		public $fechalevantamiento;
		public $inspertor_revisor;
		public $inspertor_elaborador;

		public $mensaje;
			
			public function cargar ($control_archivo, $fecha_ficha, $region_rederal_gobierno, 
				$comuna, $consejo_comunal, $comite_tierra_urbano, $registro_mvv, $manzanas,$parroquia, $ciudad,
				$localidad, $urbanizacion, $consejo_residencial, $barrio, $sector, $numero_civico,
				$punto_referencia, $servicios, $topografia, $acceso, $forma, $ubicacion,
				$entorno_fisico, $mejoras_terreno, $tenencia_terreno, $regimen_propiedad,
				$uso_actual_terreno, $tipo_construccion, $descripcion_uso, $tenencia_construccion,
				$reg_propiedad_const, $soporte_const, $techo_const, $cubierta_interna,
				$cubierta_externa, $tipo_pared, $acabado_pared, $tipo_pintura_pared, 
				$instalacciones_electricas, $pisos_construccion, $banoWC, $bano_comunitario,
				$bano_ducha, $bano_lavamanos, $bano_banera, $bano_bidet, $bano_letrina,
				$bano_ceramica1, $bano_ceramica2, $puerta_metalica, $puerta_madera_cep,
				$puerta_madera_ecoc, $puerta_entamborada, $puerta_seguridad, $puerta_otra,
				$puerta_otra_cantidad, $Ventanal_hierro, $Ventanal_madera, $Ventanal_aluminio,
				$Basculante_hierro, $Basculante_aluminio, $Basculante_madera, $Bastiente_hierro,
				$Bastiente_aluminio, $Bastiente_madera, $Celosia_hierro, $Celosia_aluminio,
				$Celosia_madera, $Corredera_hierro, $Corredera_aluminio, $Corredera_madera,
				$Panoramica_hierro, $Panoramica_aluminio, $Panoramica_madera, $anos_const,
				$refaccion_const, $numeroniveles_const, $anosrefaccion_const, $edadefectiva_const,
				$edificaciones_const, $estado_conservacion_const, $observaciones_const, $ocupante,
				$propietario, $parroquia_persona, $ciudad_persona, $localidad_persona,
				$urbanizacion_persona, $consejo_residencial_persona, $barrio_persona,
				$sector_persona, $numero_civico_persona, $punto_referencia_persona, $numero_registro,
				$folio_registro, $tomo_registro, $protocolo_registro, $registro_fecha, 
				$registro_aream2terreno, $registro_aream2const, $registro_monto, $area_metros_valor,
				$valor_unitario_valor, $sector_valor, $factor_ajuste_valor_forma,
				$factor_ajuste_valor_area, $valor_ajustado_bs_valor, $Observaciones_economia_terreno,
				$tipologia_economia, $area_mt_economia, $valor_mt2_economia, $depreciacion_economia,
				$valor_actual_economia, $Observaciones_economia, $norte_linderos, $sur_linderos,
				$este_linderos, $oeste_linderos, $norte_coord, $este_coord, $huso_coord,
				$Observaciones_generales, $fechavisita,  $fechalevantamiento, $inspertor_revisor, 
				$inspertor_elaborador)
				
			{
				$this->control_archivo = $control_archivo;
				$this->fecha_ficha = $fecha_ficha;
				$this->region_rederal_gobierno = $region_rederal_gobierno;
				$this->comuna = $comuna;
				$this->consejo_comunal = $consejo_comunal;
				$this->comite_tierra_urbano = $comite_tierra_urbano;
				$this->registro_mvv = $registro_mvv;
				$this->manzanas = $manzanas;
				$this->parroquia = $parroquia;
				$this->ciudad = $ciudad;
				$this->localidad = $localidad;
				$this->urbanizacion = $urbanizacion;
				$this->consejo_residencial = $consejo_residencial;
				$this->barrio = $barrio;
				$this->sector = $sector;
				$this->numero_civico = $numero_civico;
				$this->punto_referencia = $punto_referencia;
				$this->servicios = $servicios;
				$this->topografia = $topografia;
				$this->acceso = $acceso;
				$this->forma = $forma;
				$this->ubicacion = $ubicacion;
				$this->entorno_fisico = $entorno_fisico;
				$this->mejoras_terreno = $mejoras_terreno;
				$this->tenencia_terreno = $tenencia_terreno;
				$this->regimen_propiedad = $regimen_propiedad;
				$this->uso_actual_terreno = $uso_actual_terreno;
				$this->tipo_construccion = $tipo_construccion;
				$this->descripcion_uso = $descripcion_uso;
				$this->tenencia_construccion = $tenencia_construccion;
				$this->reg_propiedad_const = $reg_propiedad_const;
				$this->soporte_const = $soporte_const;
				$this->techo_const = $techo_const;
				$this->cubierta_interna = $cubierta_interna;
				$this->cubierta_externa = $cubierta_externa;
				$this->tipo_pared = $tipo_pared;
				$this->acabado_pared = $acabado_pared;
				$this->tipo_pintura_pared = $tipo_pintura_pared;
				$this->instalacciones_electricas = $instalacciones_electricas;
				$this->pisos_construccion = $pisos_construccion;
				$this->banoWC = $banoWC;
				$this->bano_comunitario = $bano_comunitario;
				$this->bano_ducha = $bano_ducha;
				$this->bano_lavamanos = $bano_lavamanos;
				$this->bano_banera = $bano_banera;
				$this->bano_bidet = $bano_bidet;
				$this->bano_letrina = $bano_letrina;
				$this->bano_ceramica1 = $bano_ceramica1;
				$this->bano_ceramica2 = $bano_ceramica2;
				$this->puerta_metalica = $puerta_metalica;
				$this->puerta_madera_cep = $puerta_madera_cep;
				$this->puerta_madera_ecoc = $puerta_madera_ecoc;
				$this->puerta_entamborada = $puerta_entamborada;
				$this->puerta_seguridad = $puerta_seguridad;
				$this->puerta_otra = $puerta_otra;
				$this->puerta_otra_cantidad = $puerta_otra_cantidad;
				$this->Ventanal_hierro = $Ventanal_hierro;
				$this->Ventanal_madera = $Ventanal_madera;
				$this->Ventanal_aluminio = $Ventanal_aluminio;
				$this->Basculante_hierro = $Basculante_hierro;
				$this->Basculante_aluminio = $Basculante_aluminio; 
				$this->Basculante_madera = $Basculante_madera;
				$this->Bastiente_hierro = $Bastiente_hierro;
				$this->Bastiente_aluminio = $Bastiente_aluminio;
				$this->Bastiente_madera = $Bastiente_madera;
				$this->Celosia_hierro = $Celosia_hierro;
				$this->Celosia_aluminio =$Celosia_aluminio;
				$this->Celosia_madera = $Celosia_madera;
				$this->Corredera_hierro = $Corredera_hierro;
				$this->Corredera_aluminio = $Corredera_aluminio;
				$this->Corredera_madera = $Corredera_madera;
				$this->Panoramica_hierro = $Panoramica_hierro;
				$this->Panoramica_aluminio = $Panoramica_aluminio;
				$this->Panoramica_madera = $Panoramica_madera;
				$this->anos_const = $anos_const;
				$this->refaccion_const = $refaccion_const;
				$this->numeroniveles_const = $numeroniveles_const;
				$this->anosrefaccion_const = $anosrefaccion_const;
				$this->edadefectiva_const = $edadefectiva_const;
				$this->edificaciones_const = $edificaciones_const;
				$this->estado_conservacion_const = $estado_conservacion_const;
				$this->observaciones_const = $observaciones_const;
				$this->ocupante = $ocupante;
				$this->propietario = $propietario;
				$this->parroquia_persona = $parroquia_persona;
				$this->ciudad_persona = $ciudad_persona;
				$this->localidad_persona = $localidad_persona;
				$this->urbanizacion_persona = $urbanizacion_persona;
				$this->consejo_residencial_persona = $consejo_residencial_persona;
				$this->barrio_persona = $barrio_persona;
				$this->sector_persona = $sector_persona;
				$this->numero_civico_persona = $numero_civico_persona;
				$this->punto_referencia_persona= $punto_referencia_persona;
				$this->numero_registro = $numero_registro;
				$this->folio_registro = $folio_registro;
				$this->tomo_registro = $tomo_registro;
				$this->protocolo_registro = $protocolo_registro;
				$this->registro_fecha = $registro_fecha;
				$this->registro_aream2terreno = $registro_aream2terreno;
				$this->registro_aream2const = $registro_aream2const;
				$this->registro_monto = $registro_monto;
				$this->area_metros_valor = $area_metros_valor;
				$this->valor_unitario_valor = $valor_unitario_valor;
				$this->sector_valor = $sector_valor;
				$this->factor_ajuste_valor_forma = $factor_ajuste_valor_forma;
				$this->factor_ajuste_valor_area = $factor_ajuste_valor_area;
				$this->valor_ajustado_bs_valor = $valor_ajustado_bs_valor;
				$this->Observaciones_economia_terreno = $Observaciones_economia_terreno;
				$this->tipologia_economia = $tipologia_economia;
				$this->area_mt_economia = $area_mt_economia;
				$this->valor_mt2_economia = $valor_mt2_economia;
				$this->depreciacion_economia = $depreciacion_economia;
				$this->valor_actual_economia = $valor_actual_economia;
				$this->Observaciones_economia = $Observaciones_economia;
				$this->norte_linderos  = $norte_linderos;
				$this->sur_linderos = $sur_linderos;
				$this->este_linderos = $este_linderos;
				$this->oeste_linderos = $oeste_linderos;
				$this->norte_coord = $norte_coord;
				$this->este_coord = $este_coord;
				$this->huso_coord = $huso_coord;
				$this->Observaciones_generales = $Observaciones_generales;
				$this->fechavisita = $fechavisita;
				$this->fechalevantamiento =$fechalevantamiento;
				$this->inspertor_revisor = $inspertor_revisor;
				$this->inspertor_elaborador = $inspertor_elaborador;
			}//fin de function

	public function datosFichaCastastral($ficha){
		
		$this->ficha = $ficha;

		$query=pg_query("SELECT
					ib.id as idinmueble,
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr.id AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					tr.largo_terreno,
					tr.ancho_terreno,
					ct.id as idconstruccion,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.id as idregistro,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.id as idubicacioncomunitaria,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector as numero_sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.id as ppid,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.id as poid,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.id as peid,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.id as prid,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					where
					fc. id = '$this->ficha'");
		$this->row=pg_fetch_array($query);

	}

	public function datosDireccionPersona($idpp){
		
		$this->idpp = $idpp;

		$this->query=pg_query("SELECT	* FROM tb_direccion_persona
		WHERE idpersona='$this->idpp'");
		$this->row=pg_fetch_array($this->query);

	}

	public function registrarFichaCastastral($data = array()){

		$this->data = $data;
		//print_r($data);

		foreach ($data as $dat) {
			#REGISTRO DE FICHA CATASTRAL
			$ambito = '01';
			$numerocatastral = $ambito."-".$dat['sector']."-".$dat['manzanas']."-".$dat['parroquia'];

		$ficha_catastral = pg_query(" INSERT INTO tb_ficha_catastral
		VALUES (default, '$numerocatastral', '".$dat['control_archivo']."', 
		'".$dat['fecha_ficha']."', 'CROQUIS', '".$dat['fechavisita']."', 
		'".$dat['fechalevantamiento']."', '".$dat['inspertor_elaborador']."',
		'".$dat['inspertor_revisor']."', '".$dat['observaciones_ficha']."'
		)");
		$pg_affected_ficha = pg_affected_rows($ficha_catastral);

		$areametroscuadrados = $dat['largo_terreno']*$dat['ancho_terreno'];

		#REGISTRO DE TERRENO
		$this->consulta= pg_query("SELECT * FROM  tb_unidad");
		$reg=pg_fetch_object($this->consulta);
		$this->precio=$reg->precio;

		$valor_total_terreno = ($areametroscuadrados*($dat['valor_ajustado']*$this->precio));

		$total = bcdiv($valor_total_terreno, "1", "2");
		//var_dump($total);

		$terreno = pg_query("INSERT INTO tb_terreno 
		VALUES (default, '".$dat['topografia']."', '".$dat['acceso']."',
		'".$dat['forma']."', '".$dat['ubicacion']."', 
		'".$dat['entorno_fisico']."', '".$dat['mejoras_terreno']."',
		'".$dat['tenencia_terreno']."', '".$dat['regimen_propiedad']."',
		'".$dat['uso_actual_terreno']."', '".$dat['norte_linderos']."',
		'".$dat['sur_linderos']."', '".$dat['este_linderos']."',
		'".$dat['oeste_linderos']."', '".$dat['norte_coord']."', 
		'".$dat['este_coord']."', '".$dat['huso_coord']."', 
		'$areametroscuadrados', '$this->precio', '', '', '".$dat['sector']."', 
		'".$dat['valor_ajustado']."', '$total', 
		'".$dat['Observaciones_economia_terreno']."', '".$dat['largo_terreno']."',
		'".$dat['ancho_terreno']."','$this->precio')");

		$pg_affected_terrreno = pg_affected_rows($terreno);

		#REGISTRO DE TERRENO SERVICIOS
		if ($pg_affected_terrreno) {
			# code...
			foreach ($dat['servicios'] as $servicio){
				$query = pg_query("SELECT id 
											FROM tb_terreno ORDER BY id DESC");
				$row = pg_fetch_array($query);
				$idterreno  = $row['id'];
					
				$terrenoservicio = pg_query("INSERT INTO tb_terrenoservicios
					VALUES (default, '".$servicio."', '$idterreno')");
			}
		}

		#REGISTRO DE CONSTRUCCION
		$construccion = pg_query("INSERT INTO tb_construccion 
		VALUES (default,'".$dat['tipo_construccion']."',
		'".$dat['descripcion_uso']."', '".$dat['tenencia_construccion']."',
		'".$dat['reg_propiedad_const']."', '".$dat['soporte_const']."', 
		'".$dat['techo_const']."', '".$dat['cubierta_interna']."', 
		'".$dat['cubierta_externa']."',	'".$dat['tipo_pared']."',
		'".$dat['acabado_pared']."', '".$dat['tipo_pintura_pared']."', 
		'".$dat['instalacciones_electricas']."', '".$dat['pisos_construccion']."', '".$dat['banoWC']."', '".$dat['bano_comunitario']."', 
		'".$dat['bano_ducha']."', '".$dat['bano_lavamanos']."', 
		'".$dat['bano_banera']."', '".$dat['bano_bidet']."', 
		'".$dat['bano_letrina']."', '".$dat['bano_ceramica1']."', 
		'".$dat['bano_ceramica2']."', '".$dat['puerta_metalica']."', 
		'".$dat['puerta_madera_cep']."', '".$dat['puerta_madera_ecoc']."', 
		'".$dat['puerta_entamborada']."', '".$dat['puerta_seguridad']."', 
		'".$dat['puerta_otra']."', '".$dat['puerta_otra_cantidad']."', '".$dat['Ventanal_aluminio']."',	'".$dat['Ventanal_hierro']."', 
		'".$dat['Ventanal_madera']."', '".$dat['Basculante_aluminio']."', 
		'".$dat['Basculante_hierro']."', '".$dat['Basculante_madera']."', 
		'".$dat['Batiente_aluminio']."', '".$dat['Batiente_hierro']."', 
		'".$dat['Batiente_madera']."',	'".$dat['Celosia_aluminio']."', 
		'".$dat['Celosia_hierro']."', '".$dat['Celosia_madera']."', 
		'".$dat['Corredera_aluminio']."', '".$dat['Corredera_hierro']."', 
		'".$dat['Corredera_madera']."',	'".$dat['Panoramica_aluminio']."', 
		'".$dat['Panoramica_hierro']."', '".$dat['Panoramica_madera']."', 
		'".$dat['estado_conservacion_const']."', '".$dat['anos_const']."', 
		'".$dat['refaccion_const']."', '".$dat['numeroniveles_const']."',
		'".$dat['anosrefaccion_const']."',	'".$dat['edadefectiva_const']."', 
		'".$dat['edificaciones_const']."', '".$dat['observaciones_const']."', 
		' ', ' ', ' ', ' ', ' ', ' ',' ', ' ', ' ',' ')");
		
		$pg_affected_construccion = pg_affected_rows($construccion);

		#UBICACION COMUNITARIA
		$ubicacionComunitaria = pg_query("INSERT INTO tb_ubicacion_comunitaria 
		VALUES (default, '".$dat['registro_mvv']."', 
		'".$dat['region_rederal_gobierno']."', '".$dat['comuna']."', 
		'".$dat['comite_tierra_urbano']."', '".$dat['consejo_comunal']."',
		'".$dat['ciudad']."', '".$dat['localidad']."', '".$dat['urbanizacion']."', '".$dat['consejo_residencial']."', '".$dat['barrio']."',
		'".$dat['numero_civico']."', '".$dat['punto_referencia']."', 
		'".$dat['manzanas']."', '".$dat['parroquia']."', '".$dat['sector']."',
		'".$dat['numero_comuna']."', '".$dat['numero_comite_urbano']."',
		'".$dat['numero_consejo_comunal']."', '".$dat['tipo_calle']."',
		'".$dat['nombre_calle']."')");
		
		$pg_affected_ubicacion = pg_affected_rows($ubicacionComunitaria);

		#UBICACION COMUNITARIA PROPIETARIO
		if($dat['propietario']!="" and $dat['propietario']>0){

			$query=pg_query("SELECT	* FROM tb_direccion_persona
			WHERE idpersona='".$dat['propietario']."'");
		    $validar = pg_num_rows($query);
		    if($validar>0){
		    	$updatePropietario = pg_query("UPDATE tb_direccion_persona SET 
				ciudad='".$dat['ciudad_persona']."', 
				localidad='".$dat['localidad_persona']."', 
				urbanizacion='".$dat['urbanizacion_persona']."',
				consejo_residencial='".$dat['consejo_residencial_persona']."', 
				barrio='".$dat['barrio_persona']."', 
				numero_civico='".$dat['numero_civico_persona']."', 
				puntoreferencia='".$dat['punto_referencia_persona']."', 
				parroquia='".$dat['parroquia_persona']."',
				sector='".$dat['sector_persona']."', 
				tipo_calle='".$dat['tipo_calle_persona']."',
				nombre_calle='".$dat['nombre_calle_persona']."' 
				WHERE idpersona='".$dat['propietario']."'");
				$pg_affected_updatePropietario = pg_affected_rows($updatePropietario);

		    }else{

				$ubicacionPropietario = pg_query("
												   INSERT INTO tb_direccion_persona 
				VALUES (default, '".$dat['ciudad_persona']."', 
				'".$dat['localidad_persona']."', '".$dat['urbanizacion_persona']."',
				'".$dat['consejo_residencial_persona']."', '".$dat['barrio_persona']."', '".$dat['numero_civico_persona']."', 
				'".$dat['punto_referencia_persona']."', '".$dat['parroquia_persona']."',
				'".$dat['sector_persona']."', '".$dat['tipo_calle_persona']."',
				'".$dat['nombre_calle_persona']."', '".$dat['propietario']."' )");

				$pg_affected_ubicacionPropietario = pg_affected_rows($ubicacionPropietario);
			}
		}

		#REGISTRO PUBLICO
		$registro = pg_query("INSERT INTO tb_registro_publico 
		VALUES (default, '".$dat['numero_registro']."', 
		'".$dat['folio_registro']."', '".$dat['tomo_registro']."',
		'".$dat['protocolo_registro']."', '".$dat['registro_fecha']."',
		'".$dat['registro_aream2terreno']."','".$dat['registro_aream2const']."',
		'".$dat['registro_monto']."')");
		$pg_affected_registro = pg_affected_rows($registro);


		if(($pg_affected_ficha==1) && ($pg_affected_terrreno==1) 
			&& ($pg_affected_construccion==1) && ($pg_affected_ubicacion==1)){

			$q = pg_query("SELECT id FROM tb_ficha_catastral ORDER BY id DESC");
			$r = pg_fetch_array($q);
			$this->idfichacatastral  = $r['id'];

			$q1 = pg_query("SELECT id FROM tb_terreno ORDER BY id DESC");
			$r1 = pg_fetch_array($q1);
			$this->idterreno  = $r1['id'];

			$q2 = pg_query("SELECT id FROM tb_construccion ORDER BY id DESC");
			$r2 = pg_fetch_array($q2);
			$this->idconstruccion  = $r2['id'];

			$q3 = pg_query("SELECT id FROM tb_ubicacion_comunitaria ORDER BY id DESC");
			$r3 = pg_fetch_array($q3);
			$this->idubicacioncomunitaria  = $r3['id'];

			$q4 = pg_query("SELECT id FROM tb_registro_publico ORDER BY id DESC");
			$r4 = pg_fetch_array($q4);
			$this->idregistro  = $r4['id'];

			
			if($pg_affected_ubicacionPropietario){
				$this->propietario = $dat['propietario'];
			}elseif ($pg_affected_updatePropietario) {
				$this->propietario = $dat['propietario'];
			}else{
				$this->propietario = $dat['ocupante'];
			}
			#DATOS INMUEBLE
			$inmueble = pg_query("INSERT INTO tb_inmueble 
			VALUES (default, '$this->idfichacatastral', '".$dat['ocupante']."', 
			'$this->propietario', '$this->idterreno', '$this->idconstruccion',
			'$this->idregistro', '$this->idubicacioncomunitaria')");

			#CHEQUEADO NUEVO
			$chequeado = pg_query("UPDATE tb_persona SET chequeado='1' 
			WHERE id='".$dat['ocupante']."'");

			$this->mensaje=1;
		}
		
			
		}//FOREACH DATA
	}//function

	public function actualizarFichaCastastral($data = array()){

		$this->data = $data;
		//print_r($data);

		foreach ($data as $dat) {
			#REGISTRO DE FICHA CATASTRAL
			$ambito = '01';
			$numerocatastral = $ambito."-".$dat['sector']."-".$dat['manzanas']."-".$dat['parroquia'];

		$ficha_catastral = pg_query(" UPDATE tb_ficha_catastral SET
		numerocatastral='$numerocatastral', numeroarchivo='".$dat['control_archivo']."', fechainscripcion='".$dat['fecha_ficha']."', 
		fechavisita='".$dat['fechavisita']."', 
		fechalevantamiento='".$dat['fechalevantamiento']."',
		elaboradopor='".$dat['inspertor_elaborador']."',
		revisadopor='".$dat['inspertor_revisor']."', 
		observaciones='".$dat['observaciones_ficha']."' WHERE id=".$dat['ficha']." ");
		$pg_affected_ficha = pg_affected_rows($ficha_catastral);

		$areametroscuadrados = $dat['largo_terreno']*$dat['ancho_terreno'];

		#REGISTRO DE TERRENO
		$this->consulta= pg_query("SELECT * FROM  tb_unidad");
		$reg=pg_fetch_object($this->consulta);
		$this->precio=$reg->precio;

		$valor_total_terreno = ($areametroscuadrados*($dat['valor_ajustado']*$this->precio));

		$total = bcdiv($valor_total_terreno, "1", "2");

		#REGISTRO DE TERRENO
		$terreno = pg_query(" UPDATE tb_terreno SET
		topografia='".$dat['topografia']."', acceso='".$dat['acceso']."',
		forma='".$dat['forma']."', ubicacion='".$dat['ubicacion']."', 
		entornofisico='".$dat['entorno_fisico']."', 
		mejorasterreno='".$dat['mejoras_terreno']."',
		teneciaterreno='".$dat['tenencia_terreno']."', 
		registropropiedad='".$dat['regimen_propiedad']."',
		usoactual='".$dat['uso_actual_terreno']."', 
		linderonorte='".$dat['norte_linderos']."',
		linderosur='".$dat['sur_linderos']."', linderoeste='".$dat['este_linderos']."',
		linderooeste='".$dat['oeste_linderos']."', 
		coordenadasutmnorte='".$dat['norte_coord']."', 
		coordenadasutmeste='".$dat['este_coord']."', 
		coordenadasutmhuso='".$dat['huso_coord']."', 
		areametroscuadrados='$areametroscuadrados',
		valorunitariometroscuadrados = '$this->precio', sector = '".$dat['sector']."',
		valorajustadometroscuadrados = '".$dat['valor_ajustado']."',
		valortotal='$total',
		observacioneconomica='".$dat['Observaciones_economia_terreno']."', 
		largo_terreno='".$dat['largo_terreno']."',
		ancho_terreno='".$dat['ancho_terreno']."',
		unidad_tributaria='$this->precio' WHERE id='".$dat['idterreno']."'");

		$pg_affected_terrreno = pg_affected_rows($terreno);

		#REGISTRO DE TERRENO SERVICIOS
		$update = pg_query("DELETE FROM tb_terrenoservicios 
		WHERE idterreno = '".$dat['idterreno']."'");
		$pg_affected_update = pg_affected_rows($update);

		if ($pg_affected_update) {
			# code...
			foreach ($dat['servicios'] as $servicio){
					
				$terrenoservicio = pg_query("INSERT INTO tb_terrenoservicios
					VALUES (default, '".$servicio."', '".$dat['idterreno']."')");
			}
		}

		#REGISTRO DE CONSTRUCCION
		$construccion = pg_query("UPDATE tb_construccion SET 
		tipo='".$dat['tipo_construccion']."',
		descripcionuso= '".$dat['descripcion_uso']."', 
		teneciaconstruccion='".$dat['tenencia_construccion']."',
		regimenpropiedad='".$dat['reg_propiedad_const']."', 
		soporteestructural='".$dat['soporte_const']."', 
		techoestructural='".$dat['techo_const']."', 
		cubiertainterna='".$dat['cubierta_interna']."', 
		cubiertaexterna='".$dat['cubierta_externa']."',	
		paredestipo='".$dat['tipo_pared']."',
		paredesacabado='".$dat['acabado_pared']."', 
		paredespintura='".$dat['tipo_pintura_pared']."', 
		paredesinstelectricas='".$dat['instalacciones_electricas']."', 
		pisotipo='".$dat['pisos_construccion']."', 
		banowc='".$dat['banoWC']."', banocomunitario='".$dat['bano_comunitario']."', 
		banoducha='".$dat['bano_ducha']."', banolavamanos='".$dat['bano_lavamanos']."', banobanera='".$dat['bano_banera']."', 
		banobidet='".$dat['bano_bidet']."', 
		banoletrina='".$dat['bano_letrina']."', 
		banoceramica1='".$dat['bano_ceramica1']."', 
		banoceramica2='".$dat['bano_ceramica2']."', 
		puertametalica='".$dat['puerta_metalica']."', 
		puertamaderacepillada='".$dat['puerta_madera_cep']."', 
		puertamadeconomica='".$dat['puerta_madera_ecoc']."', 
		puertaentamboradafina='".$dat['puerta_entamborada']."', 
		puertaseguridad='".$dat['puerta_seguridad']."', 
		puertaotra='".$dat['puerta_otra']."', 
		puertaotrac='".$dat['puerta_otra_cantidad']."', 
		ventanalaluminio='".$dat['Ventanal_aluminio']."',	
		ventanalhierro='".$dat['Ventanal_hierro']."', 
		ventanalmadera='".$dat['Ventanal_madera']."', 
		basculantealuminio='".$dat['Basculante_aluminio']."', 
		basculantehierro='".$dat['Basculante_hierro']."', 
		basculantemadera='".$dat['Basculante_madera']."', 
		satientealuminio='".$dat['Batiente_aluminio']."', 
		satientehierro='".$dat['Batiente_hierro']."', 
		satientemadera='".$dat['Batiente_madera']."',	
		celosiaaluminio='".$dat['Celosia_aluminio']."', 
		celosiahierro='".$dat['Celosia_hierro']."', 
		celosiamadera='".$dat['Celosia_madera']."', 
		correderaaluminio='".$dat['Corredera_aluminio']."', 
		correderahierro='".$dat['Corredera_hierro']."', 
		correderamadera='".$dat['Corredera_madera']."',	
		panoramicaaluminio='".$dat['Panoramica_aluminio']."', 
		panoramicahierro='".$dat['Panoramica_hierro']."', 
		panoramicamadera='".$dat['Panoramica_madera']."', 
		estadoconservacion='".$dat['estado_conservacion_const']."', 
		anoconstruccion='".$dat['anos_const']."', 
		porcentajerefaccion='".$dat['refaccion_const']."', 
		numeroniveles='".$dat['numeroniveles_const']."',
		anorefaccion='".$dat['anosrefaccion_const']."',	
		edadefectiva='".$dat['edadefectiva_const']."', 
		numeroedificacion='".$dat['edificaciones_const']."', 
		observacionesconts='".$dat['observaciones_const']."' 
		WHERE id='".$dat['idconstruccion']."'");

		//var_dump($construccion);die();
		
		$pg_affected_construccion = pg_affected_rows($construccion);

		#UBICACION COMUNITARIA
		$ubicacionComunitaria = pg_query("UPDATE tb_ubicacion_comunitaria SET 
		numero_rmv='".$dat['registro_mvv']."', 
		region_consejofg='".$dat['region_rederal_gobierno']."', 
		comuna='".$dat['comuna']."', comite_tierrau='".$dat['comite_tierra_urbano']."', consejo_comunal='".$dat['consejo_comunal']."',
		ciudad='".$dat['ciudad']."', localidad='".$dat['localidad']."', 
		urbanizacion='".$dat['urbanizacion']."', 
		consejo_residencial='".$dat['consejo_residencial']."', 
		barrio='".$dat['barrio']."', numero_civico='".$dat['numero_civico']."', 
		puntoreferencia='".$dat['punto_referencia']."', 
		manzana='".$dat['manzanas']."', parroquia='".$dat['parroquia']."', 
		sector='".$dat['sector']."', numero_comuna='".$dat['numero_comuna']."',
		numero_comite_urbano='".$dat['numero_comite_urbano']."',
		numero_consejo_comunal='".$dat['numero_consejo_comunal']."', 
		tipo_calle='".$dat['tipo_calle']."', nombre_calle='".$dat['nombre_calle']."'
		WHERE id='".$dat['idubicacioncomunitaria']."'");

		//var_dump($ubicacionComunitaria);die();
		
		$pg_affected_ubicacion = pg_affected_rows($ubicacionComunitaria);

		#UBICACION COMUNITARIA PROPIETARIO
		if($dat['propietario']!="0"){

			$query=pg_query("SELECT	* FROM tb_direccion_persona
			WHERE idpersona='".$dat['propietario']."'");
		    $validar = pg_num_rows($query);
		    if($validar>0){
		    	$updatePropietario = pg_query("UPDATE tb_direccion_persona SET 
				ciudad='".$dat['ciudad_persona']."', 
				localidad='".$dat['localidad_persona']."', 
				urbanizacion='".$dat['urbanizacion_persona']."',
				consejo_residencial='".$dat['consejo_residencial_persona']."', 
				barrio='".$dat['barrio_persona']."', 
				numero_civico='".$dat['numero_civico_persona']."', 
				puntoreferencia='".$dat['punto_referencia_persona']."', 
				parroquia='".$dat['parroquia_persona']."',
				sector='".$dat['sector_persona']."', 
				tipo_calle='".$dat['tipo_calle_persona']."',
				nombre_calle='".$dat['nombre_calle_persona']."' 
				WHERE idpersona='".$dat['propietario']."'");
				$pg_affected_updatePropietario = pg_affected_rows($updatePropietario);

				
		    }else{

				$ubicacionPropietario = pg_query("
												   INSERT INTO tb_direccion_persona 
				VALUES (default, '".$dat['ciudad_persona']."', 
				'".$dat['localidad_persona']."', '".$dat['urbanizacion_persona']."',
				'".$dat['consejo_residencial_persona']."', '".$dat['barrio_persona']."', '".$dat['numero_civico_persona']."', 
				'".$dat['punto_referencia_persona']."', '".$dat['parroquia_persona']."',
				'".$dat['sector_persona']."', '".$dat['tipo_calle_persona']."',
				'".$dat['nombre_calle_persona']."', '".$dat['propietario']."')");
				$pg_affected_ubicacionPropietario = pg_affected_rows($ubicacionPropietario);
			}
		}

		#REGISTRO PUBLICO
		$registro = pg_query("UPDATE tb_registro_publico SET
		numero='".$dat['numero_registro']."', 
		folio='".$dat['folio_registro']."', 
		tomo='".$dat['tomo_registro']."',
		protocolo='".$dat['protocolo_registro']."', 
		fecha='".$dat['registro_fecha']."',
		aream2terreno='".$dat['registro_aream2terreno']."',
		aream2cont='".$dat['registro_aream2const']."',
		valorbs='".$dat['registro_monto']."' WHERE id='".$dat['idregistro']."'");
		$pg_affected_registro = pg_affected_rows($registro);
	
			
		$sql_valida= pg_query("SELECT * FROM  tb_inmueble 
					WHERE id='".$dat['idinmueble']."'");
		$valida=pg_fetch_object($sql_valida);

		if($dat['ocupante']!="" && $dat['propietario']>0){

			if($dat['ocupante_viejo'] != $dat['ocupante']){

				$inmueble = pg_query("UPDATE tb_inmueble SET
				idocupante='".$dat['ocupante']."', idpropietario='".$dat['propietario']."'
				WHERE id='".$dat['idinmueble']."'");

				#CHEQUEADO NUEVO
				$chequeado = pg_query("UPDATE tb_persona SET chequeado='1' 
				WHERE id='".$dat['ocupante']."'");

				$chequeado_viejo = pg_query("UPDATE tb_persona SET chequeado='0' 
				WHERE id='".$dat['ocupante_viejo']."'");
			}
		}elseif($dat['ocupante']=="" && $dat['propietario']>0){

			$inmueble = pg_query("UPDATE tb_inmueble SET
			idocupante='".$dat['ocupante_viejo']."', idpropietario='".$dat['propietario']."'
			WHERE id='".$dat['idinmueble']."'");

		}elseif($dat['ocupante_viejo']==$dat['propietario']){

			$inmueble = pg_query("UPDATE tb_inmueble SET
			idocupante='".$dat['ocupante_viejo']."', idpropietario='".$dat['propietario']."'
			WHERE id='".$dat['idinmueble']."'");

		}elseif($dat['ocupante']==""){

			$inmueble = pg_query("UPDATE tb_inmueble SET
			idocupante='".$dat['ocupante_viejo']."'	WHERE id='".$dat['idinmueble']."'");

		}elseif($dat['ocupante']!=""){

			$inmueble = pg_query("UPDATE tb_inmueble SET
			idocupante='".$dat['ocupante']."', idpropietario='".$dat['ocupante']."'
			WHERE id='".$dat['idinmueble']."'");

			#CHEQUEADO NUEVO
				$chequeado = pg_query("UPDATE tb_persona SET chequeado='1' 
				WHERE id='".$dat['ocupante']."'");

				$chequeado_viejo = pg_query("UPDATE tb_persona SET chequeado='0' 
				WHERE id='".$dat['ocupante_viejo']."'");

				$query=pg_query("SELECT	* FROM tb_direccion_persona
				WHERE idpersona='".$dat['ocupante']."'");
			    $validar = pg_num_rows($query);
			    if($validar == 0){
				$ubp = pg_query("INSERT INTO tb_direccion_persona 
				VALUES (default, 'Generico','Generico', 'Generico',
				'Generico', 'Generico', '9999', 'Generico', 'Generico',
				'Generico', 'Generico',	'Generico', '".$dat['ocupante']."')");
				}
		}		
			$this->mensaje=1;
			
		}//FOREACH DATA
	}//function
		
		public function servicios()
		{
				$this->servicios= pg_query("SELECT * FROM  tb_servicio");
		}//fin de function

		public function fichasCatastrales()
		{
				$this->ficha= pg_query("SELECT
					fc.id as ficha,
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					po.id as poid,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2
					
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante");
		}//fin de function

		public function fichaEstadistica()
		{
			
				$ficha= pg_query("SELECT COUNT(*) FROM  tb_ficha_catastral");
				$this->estadistica = pg_fetch_array($ficha);
		}//fin de function


		public function terrenosEstadisticas()
		{
			
				$ficha= pg_query("SELECT COUNT(*) FROM  tb_ficha_catastral");
				$this->estadistica = pg_fetch_array($ficha);
		}//fin de function

		public function construccionEstadisticas()
		{
			
				$ficha= pg_query("SELECT COUNT(*) FROM  tb_ficha_catastral");
				$this->estadistica = pg_fetch_array($ficha);
		}//fin de function
		
	}//fin de class
	?>