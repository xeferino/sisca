<?php
	class Dashboard
	{
	
			
		public function sectoresCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_pedul"));
				if($consulta>0){
						$this->sector = pg_num_rows (pg_query("SELECT * from tb_pedul"));
				}
		}//fin de function

		public function callesCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_manzana"));
				if($consulta>0){
						$this->calle = pg_num_rows (pg_query("SELECT * from tb_manzana"));
				}
		}//fin de function

		public function parroquiaCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_parroquia"));
				if($consulta>0){
						$this->parroquia = pg_num_rows (pg_query("SELECT * from tb_parroquia"));
				}
		}//fin de function

		public function inspectoresCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_persona where tipo_p ='Inspector'"));
				if($consulta>0){
						$this->inspector = pg_num_rows (pg_query("SELECT * from tb_persona where tipo_p ='Inspector'"));
				}
		}//fin de function


		public function fichasCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_ficha_catastral"));
				if($consulta>0){
						$this->fichas = pg_num_rows (pg_query("SELECT * from tb_ficha_catastral"));
				}
		}//fin de function

		public function inspeccionesCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_inspecciones"));
				if($consulta>0){
						$this->inspeccion = pg_num_rows (pg_query("SELECT * from tb_inspecciones"));
				}
		}//fin de function

		public function solvenciasCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_solvencia_municipal"));
				if($consulta>0){
						$this->solvencia = pg_num_rows (pg_query("SELECT * from tb_solvencia_municipal"));
				}
		}//fin de function

		public function certificacionesCount()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_certificacion_linderos"));
				if($consulta>0){
						$this->certificacion = pg_num_rows (pg_query("SELECT * from tb_certificacion_linderos"));
				}
		}//fin de function
				
		public function ultimosRegistrosFicha()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_ficha_catastral"));
				if($consulta>0){
						$this->ficha = pg_query("SELECT * from tb_ficha_catastral ORDER BY id DESC limit 10");
				}
		}//fin de function

		public function ultimosRegistroslinderos()
		{
			
					
				$consulta = pg_num_rows (pg_query("SELECT * from tb_certificacion_linderos"));
				if($consulta>0){
						$this->linderos = pg_query("SELECT
														cf.id as id,
														cf.fecha,
														cf.ubicacion,
														cf.observacion,
														pp.nacionalidad AS ppn,
														pp.cedula AS ppc,
														pp.nombre1 AS ppn1,
														pp.nombre2 AS ppn2,
														pp.apellido1 AS ppa1,
														pp.apellido2 asppa2
													FROM
														tb_certificacion_linderos AS cf
													INNER JOIN tb_persona AS pp ON pp. ID = cf.id_persona
													ORDER BY cf.id DESC limit 10");
				}
		}//fin de function

		public function ultimosRegistrosolvencias()
		{
			
				$consulta = pg_num_rows (pg_query("SELECT * from tb_solvencia_municipal"));
				if($consulta>0){
						$this->solvencias = pg_query("SELECT
								sm.id as id,
								sm.estatus,
								sm.anualidad,
								sm.fecha,
								sm.monto,
								pp.nacionalidad as ppn,
								pp.cedula as ppc,
								pp.nombre1 as ppn1,
								pp.nombre2  as ppn2,
								pp.apellido1 as ppa1,
								pp.apellido2 asppa2
							FROM
								tb_solvencia_municipal as sm
							INNER JOIN tb_persona as pp on pp.id = sm.id_persona
							ORDER BY sm.id DESC limit 10");
				}
		}//fin de function



		public function ultimosRegistrosInspecciones()
		{
			
				$consulta = pg_num_rows (pg_query("SELECT * from tb_inspecciones"));
				if($consulta>0){
						$this->inspecciones = pg_query("SELECT
							ip.id as id,
							ip.tipo,
							ip.observaciones,
							ip.fecha,
							pp.nacionalidad as ppn,
							pp.cedula as ppc,
							pp.nombre1 as ppn1,
							pp.nombre2  as ppn2,
							pp.apellido1 as ppa1,
							pp.apellido2 asppa2,
							pp1.nacionalidad as pp1n,
							pp1.cedula as pp1c,
							pp1.nombre1 as pp1n1,
							pp1.nombre2  as pp1n2,
							pp1.apellido1 as pp1a1,
							pp1.apellido2 as pp1a2
						FROM
							tb_inspecciones as ip
						LEFT JOIN tb_persona as pp on pp.id = ip.id_inspector
						LEFT JOIN tb_persona_inspeccion as ppi on ppi.id_inspeccion = ip.id
						LEFT JOIN tb_persona as pp1 on pp1.id = ppi.id_persona
							ORDER BY ip.id DESC limit 10");
				}
		}//fin de function
		
		
	}//fin de class
	?>