<?php
	class Persona
	{
			public $id;
			public $nacionalidad;
			public $cedula;
			public $nombre1;
			public $nombre2;
			public $apellido1;
			public $apellido2;
			public $estado_civil;
			public $correo;
			public $telefono1;
			public $telefono2;
			public $tipo;
			
			public $role;
			public $cuenta;
			public $clave;
			public $clave2;
			public $estatus;
			
			public $inspeccionar;
			public $fecha;
			public $observacion;
			public $id_persona;
			public $id_inspector;

			public $mensaje;
		
			public function datos_inspecciones($inspeccionar,$fecha,$observacion,$id_persona,$id_inspector)
			{
				$this->inspeccionar = $inspeccionar;
				$this->fecha = $fecha;
				$this->observacion = $observacion;
				$this->id_persona = $id_persona;
				$this->id_inspector = $id_inspector;
			}
			
			public function cargar ($id, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $estado_civil, 
			$correo, $telefono1, $telefono2, $tipo, $role, $cuenta, $clave, $clave2, $estatus)
			{
		
				$this->id=$id;
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->estado_civil=$estado_civil;
				$this->correo=$correo;
				$this->estado_civil=$estado_civil;
				$this->correo=$correo;
				$this->telefono1=$telefono1;
				$this->telefono2=$telefono2;
				$this->tipo=$tipo;
				
				$this->role=$role;
				$this->cuenta=$cuenta;
				$this->clave=$clave;
				$this->clave2=$clave2;
				$this->estatus=$estatus;
				
			}//fin de function
		
			public function login ($cuenta, $clave)
			{				
				$this->cuenta=$cuenta;
				$this->clave=$clave;
				
				if(( $this->cuenta !='' ) && ( $this->clave !='' ))
				{
					$clave = md5($this->clave);
					$consulta= pg_query("SELECT 
										  id, role, nombre1, apellido1, cuenta, clave, estatus
										FROM 
										  tb_usuario
										WHERE 
										cuenta='$this->cuenta' AND clave='$clave'");
					$row = pg_num_rows ($consulta);
					if($row>0)
					{	
						$reg=pg_fetch_object($consulta);
		
						if($reg->estatus == "Activo")
						{
							
							$_SESSION['conectado'] = $reg->id;
							$_SESSION['rol']=$reg->role;
							$_SESSION['nombre']=$reg->nombre1;
							$_SESSION['apellido']=$reg->apellido1;
							
							$this->mensaje=4;
							//header("Location: admin/");
						}
						else 
						{
							$this->mensaje=1;
						}
					}
					else 
					{
						$this->mensaje=2;
					}
				}
				else 
				{
					$this->mensaje=3;
				}
		}//fin de function
		
		public function logout()
		{
			session_destroy();
			session_unset();
			header("Location: ./");
		}//fin de function
		
		public function perfil($id)
		{
			$this->id=$id;
			
			$query=pg_query("SELECT 
							  tb_usuario.id, 
							  tb_usuario.cuenta,
							  tb_usuario.estatus
							FROM 
							  tb_usuario
							WHERE 
							  tb_usuario.id='$this->id'");
							  $reg=pg_fetch_array($query);
							  $this->id=$reg['id'];
							  $this->estatus=$reg['estatus'];
							  $this->cuenta=$reg['cuenta'];
				
		}//fin de function
		
		public function update_acces($id, $clavea, $clave1, $clave2)
		{
			$this->id=$id;
			$this->clavea=$clavea;
			$this->clave1=$clave1;
			$this->clave2=$clave2;
			
			if ($this->clavea!='' && $this->clave1!='' && $this->clave2!=""){
				$clave = md5($this->clavea);
				$cons = pg_num_rows (pg_query("SELECT clave	FROM tb_usuario	WHERE clave='$clave'"));
				if ($cons>0){
					if($this->clave1==$this->clave2){
						$clave = md5($this->clave1);
						$update = pg_query("update tb_usuario set clave='$clave' where id='$this->id'");
						$this->mensaje=1;
						$this->clavea=" ";
						$this->clave1=" ";
						$this->clave2=" ";
					}
					else{
						$this->mensaje=2;
					}
				}
				else{
					$this->mensaje=3;
				}
			}
			else{
				$this->mensaje=4;
			}
		}//fin de function
		
		public function new_cuenta()
		{
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')
			&&($this->apellido2!='')&&($this->cuenta!='')&&($this->clave!='')&&($this->clave2!='')&&($this->role!='0')
			&&($this->estatus!='0'))
			{
					if($this->clave==$this->clave2)
					{	
					 	$cedula = pg_num_rows (pg_query("SELECT 
														  tb_usuario.nacionalidad, 
														  tb_usuario.cedula
														FROM 
														  tb_usuario
														WHERE tb_usuario.nacionalidad='$this->nacionalidad' 
														AND tb_usuario.cedula='$this->cedula'"));
						if($cedula==0)
						{
							$cuenta = pg_num_rows (pg_query("SELECT 
														  *
														FROM  
														  tb_usuario
														WHERE 
														  cuenta='$this->cuenta'"));
							if($cuenta==0)
							{
								$clave = md5($this->clave);
								$persona = "insert into tb_usuario 
								values (default,'$this->nacionalidad', '$this->cedula', '$this->nombre1', '$this->nombre2', 
								'$this->apellido1', '$this->apellido2', 'NULL', 'NULL', 'NULL', 'NULL', '$this->tipo', 
								'$this->role', '$this->cuenta', '$clave', '$this->estatus')";
								$consulta = pg_query ($persona);
								
									$this->id=$id;
									$this->nacionalidad="0";
									$this->cedula="";
									$this->nombre1="";
									$this->nombre2="";
									$this->apellido1="";
									$this->apellido2="";						
									$this->role="0";
									$this->cuenta="";
									
									$this->mensaje=2;
							}
							else {$this->mensaje=3;}
						}
						else {$this->mensaje=4;}
					}
					else{$this->mensaje=5;}
			}//fin comparar claves
				else{$this->mensaje=6;}
		}//fin de function
		
		public function update_cuenta($id, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $role, $cuenta, $clave, $clave2, $estatus)
		{	
				$this->id=$id;
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->role=$role;
				$this->cuenta=$cuenta;
				$this->clave=$clave;
				$this->clave2=$clave2;
				$this->estatus=$estatus;
	
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')
			&&($this->apellido2!='')&&($this->cuenta!='')&&($this->clave!='')&&($this->clave2!='')&&($this->role!='0')
			&&($this->estatus!='0')){
			
				if($this->clave==$this->clave2){	
					$cedula = pg_num_rows (pg_query("SELECT 
														  tb_usuario.nacionalidad, 
														  tb_usuario.cedula, cuenta
														FROM 
														  tb_usuario
														WHERE tb_usuario.nacionalidad='$this->nacionalidad' 
														AND tb_usuario.cedula='$this->cedula'"));
					if($cedula>0){
						$cuenta = pg_num_rows (pg_query("SELECT 
														  *
														FROM  
														  tb_usuario
														WHERE 
														  cuenta='$this->cuenta'"));
						if($cuenta>0){
							$query=pg_query("SELECT id FROM tb_usuario WHERE nacionalidad='$this->nacionalidad' 
							AND	cedula='$this->cedula' AND cuenta='$this->cuenta'");
					
							$reg=pg_fetch_object($query);
							$id=$reg->id;
							$clave = md5($this->clave);
							$usuario = "update tb_usuario  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
							nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1',
							apellido2='$this->apellido2', cuenta='$this->cuenta', role='$this->role', clave='$clave', 
							estatus='$this->estatus' where id='$id'";
							$consulta = pg_query ($usuario);
							if($consulta){$this->mensaje=1;}
							else{
							$this->error=$this->mensaje=8;
							$this->error=pg_last_error();
							}
						}//fin comparar cuenta
						else{
								$clave = md5($this->clave);
								$usuario = "update tb_usuario  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
								nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1',
								apellido2='$this->apellido2', cuenta='$this->cuenta', role='$this->role', clave='$clave', 
								estatus='$this->estatus' where id='$id'";
								$consulta = pg_query ($usuario);
								if($consulta){$this->mensaje=1;}
							else{
							$this->error=$this->mensaje=8;
							$this->error=pg_last_error();
							}
							}
					}//fin comparar cedula
					else{
							$clave = md5($this->clave);
							$usuario = "update tb_usuario  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
							nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1',
							apellido2='$this->apellido2', cuenta='$this->cuenta', role='$this->role', clave='$clave', 
							estatus='$this->estatus' where id='$id'";
							$consulta = pg_query ($usuario);
							
							if($consulta){$this->mensaje=1;}
							else{
							$this->error=$this->mensaje=8;
							$this->error=pg_last_error();
							}
					}
				}//fin de comparar claves
				else{
					$this->mensaje=5;
					}	
			}//fin comparar campos en blanco
			else{
				$this->mensaje=6;
			}
			
		}//fin de function

		public function data_usuario($id)
		{
				$this->id=$id;
			
				$query=pg_query("SELECT * FROM tb_usuario WHERE id='$this->id'");
				$reg=pg_fetch_array($query);
				$this->id=$reg['id'];
				$this->nacionalidad=$reg['nacionalidad'];
				$this->cedula=$reg['cedula'];
				$this->nombre1=$reg['nombre1'];
				$this->nombre2=$reg['nombre2'];
				$this->apellido1=$reg['apellido1'];
				$this->apellido2=$reg['apellido2'];
				
				$this->estado_civil=$reg['estado_civil'];
				$this->correo=$reg['correo'];
				
				$this->telefono1=$reg['telefono1'];
				$this->telefono2=$reg['telefono2'];
				$this->tipo=$reg['tipo_p'];
				
				$this->role=$reg['role'];
				$this->cuenta=$reg['cuenta'];
				
				$this->estatus=$reg['estatus'];
				
				/*id 	nacionalidad 	cedula 	nombre1 	nombre2 	apellido1 	apellido2 	estado_civil 	correo 	telefono1 	telefono2 	tipo_p 	role 	cuenta 	clave 	estatus*/
		}//fin de function
		
		public function data_persona($id)
		{
				$this->id=$id;
			
				$query=pg_query("SELECT * FROM tb_persona WHERE id='$this->id'");
				$reg=pg_fetch_array($query);
				$this->id=$reg['id'];
				$this->nacionalidad=$reg['nacionalidad'];
				$this->cedula=$reg['cedula'];
				$this->nombre1=$reg['nombre1'];
				$this->nombre2=$reg['nombre2'];
				$this->apellido1=$reg['apellido1'];
				$this->apellido2=$reg['apellido2'];
				
				$this->estado_civil=$reg['estado_civil'];
				$this->correo=$reg['correo'];
				
				$this->telefono1=$reg['telefono1'];
				$this->telefono2=$reg['telefono2'];
				$this->tipo=$reg['tipo_p'];
				
				$this->role=$reg['role'];
				$this->cuenta=$reg['cuenta'];
				
				$this->estatus=$reg['estatus'];
				
				/*id 	nacionalidad 	cedula 	nombre1 	nombre2 	apellido1 	apellido2 	estado_civil 	correo 	telefono1 	telefono2 	tipo_p 	role 	cuenta 	clave 	estatus*/
		}//fin de function
		
		public function new_inspector($nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $tipo, $estatus)
		{
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->tipo=$tipo;
				$this->estatus=$estatus;
				
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')
			&&($this->apellido2!=''))
			{
						
				$cedula = pg_num_rows (pg_query("SELECT 
												  tb_persona.nacionalidad, 
												  tb_persona.cedula
												FROM 
												  tb_persona
												WHERE tb_persona.nacionalidad='$this->nacionalidad' 
												AND tb_persona.cedula='$this->cedula'"));
				if($cedula==0)
				{
					$inspector = "insert into tb_persona 
					values (default,'$this->nacionalidad', '$this->cedula', '$this->nombre1', '$this->nombre2', 
					'$this->apellido1', '$this->apellido2', 'NULL', 'NULL', 'NULL', 'NULL', 'Inspector', 'NULL', 
					'NULL', 'NULL', 'Activo')";
					$consulta = pg_query ($inspector);
					
					$this->mensaje=1;
					$this->nacionalidad='';
					$this->cedula='';
					$this->nombre1='';
					$this->nombre2='';
					$this->apellido1='';
					$this->apellido2='';
				}//fin cedula
				else {$this->mensaje=2;}
			}//fin campos en blanco
			else{$this->mensaje=3;}
		}//fin de function
		
		public function update_inspector($id, $n, $ci, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $estatus)
		{
				$this->id=$id;
				$this->n=$n;
				$this->ci=$ci;
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->estatus=$estatus;
				
			
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')
			&&($this->apellido2!='')&&($this->estatus!='0') )
			{
				$cedula = pg_num_rows (pg_query("SELECT nacionalidad, cedula FROM tb_persona 
				WHERE nacionalidad='$this->nacionalidad' AND cedula='$this->cedula'"));
				if($cedula>0){
					$query=pg_query("SELECT id FROM tb_persona WHERE nacionalidad='$this->nacionalidad' AND
					cedula='$this->cedula'");
					
					$reg=pg_fetch_object($query);
					$id=$reg->id;
					
					$inspector = "update tb_persona  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
					nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1', apellido2='$this->apellido2',
					estatus='$this->estatus' where id='$id'";
					$consulta = pg_query ($inspector);
					
					$this->mensaje=1;
				}
				else{
					$inspector = "update tb_persona  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
					nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1', apellido2='$this->apellido2',
					estatus='$this->estatus' where id='$id'";
					$consulta = pg_query ($inspector);
					$this->mensaje=1;
				}
			}//fin campos en blanco
			else{$this->mensaje=2;}
		}//fin de function

		public function datoInspeccion($id)
		{
				$this->id=$id;
				//var_dump($this->id);die();

				$query=pg_query("SELECT * FROM tb_inspecciones WHERE id='$this->id'");
				$reg=pg_fetch_array($query);
				$this->id=$reg['id'];
				$this->tipo=$reg['tipo'];
				$this->fecha=$reg['fecha'];
				$this->observaciones=$reg['observaciones'];
				$this->id_inspector=$reg['id_inspector'];
		}//fin de function

		public function datoPersonaInspeccion($id)
		{
				$this->id=$id;
				//var_dump($this->id);die();

				$query=pg_query("SELECT * FROM tb_persona_inspeccion WHERE id_inspeccion='$this->id'");
				$reg=pg_fetch_array($query);
				$this->id=$reg['id'];
				$this->id_persona=$reg['id_persona'];
				$this->id_inspeccion=$reg['id_inspeccion'];
		}//fin de function
		public function registrar_inspeccion()
			{
				if($this->inspeccionar!='0' && $this->fecha!='' && $this->observacion!='' && $this->id_persona!='' && $this->id_inspector!=''){

						$inspeccion = "insert into tb_inspecciones values (default,'$this->inspeccionar', '$this->fecha', '$this->observacion', '$this->id_inspector')";
						$con = pg_query ($inspeccion);

						$validar = pg_affected_rows($con);

						if($validar==true){

							$query = pg_query("select * from tb_inspecciones order by id desc");
							$row = pg_fetch_array($query);
							$id = $row['id'];

							$personainspeccion = "insert into tb_persona_inspeccion values (default,'$this->id_persona', '$id')";
							$consulta = pg_query ($personainspeccion);
						}
						
						$this->mensaje=1;

						$this->telefono2 = '';
						$this->lnorte = '';
						$this->lsur = '';
						$this->leste = '';
						$this->loeste = '';
						$this->fecha = '';
						$this->observacion = '';
				}
				else{
					$this->mensaje=3;
				}
			}

		public function actualizar_inspeccion($id_inspeccion)
		{
			$this->id_inspeccion = $id_inspeccion;
			//var_dump($this->id_persona);die();

			if($this->inspeccionar!='0' && $this->fecha!='' && $this->observacion!='' && $this->id_persona!='' && $this->id_inspector!=''){

					$inspeccion = "update tb_inspecciones set tipo='$this->inspeccionar', fecha='$this->fecha', observaciones='$this->observacion', id_inspector='$this->id_inspector' WHERE id='$this->id_inspeccion'";
					$con = pg_query ($inspeccion);

					$validar = pg_affected_rows($con);

					if($validar==true){

						$personainspeccion = "update tb_persona_inspeccion set id_persona='$this->id_persona' WHERE id_inspeccion='$this->id_inspeccion'";
						$consulta = pg_query ($personainspeccion);
					}
					
					$this->mensaje=1;

					$this->telefono2 = '';
					$this->lnorte = '';
					$this->lsur = '';
					$this->leste = '';
					$this->loeste = '';
					$this->fecha = '';
					$this->observacion = '';
			}
			else{
				$this->mensaje=3;
			}
		}

		public function new_propietario_ocupante($nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $correo, $telefono1, $telefono2, $tipo, $estatus)
		{
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->correo=$correo;
				$this->telefono1=$telefono1;
				$this->telefono2=$telefono2;
				$this->tipo=$tipo;
				$this->estatus=$estatus;
				
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')&&($this->apellido2!='') &&($this->correo!='')&&($this->telefono1!='')&&($this->telefono2!=''))
			{
						
				$cedula = pg_num_rows (pg_query("SELECT 
												  tb_persona.nacionalidad, 
												  tb_persona.cedula
												FROM 
												  tb_persona
												WHERE tb_persona.nacionalidad='$this->nacionalidad' 
												AND tb_persona.cedula='$this->cedula'"));
				if($cedula==0)
				{
					$inspector = "insert into tb_persona 
					values (default,'$this->nacionalidad', '$this->cedula', '$this->nombre1', '$this->nombre2', 
					'$this->apellido1', '$this->apellido2', 'NULL', '$this->correo', '$this->telefono1', '$this->telefono2', '$this->tipo', 'NULL', 
					'NULL', 'NULL', 'Activo', '', '0')";
					$consulta = pg_query ($inspector);
					
					$this->mensaje=1;
					$this->nacionalidad='';
					$this->cedula='';
					$this->nombre1='';
					$this->nombre2='';
					$this->apellido1='';
					$this->apellido2='';
					$this->correo='';
					$this->telefono1='';
					$this->telefono2='';
				}//fin cedula
				else {$this->mensaje=2;}
			}//fin campos en blanco
			else{$this->mensaje=3;}
		}//fin de function
		
		public function update_propietario_ocupante($id, $n, $ci, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $correo, $telefono1, 
				$telefono2, $estatus)
		{
				$this->id=$id;
				$this->n=$n;
				$this->ci=$ci;
				$this->nacionalidad=$nacionalidad;
				$this->cedula=$cedula;
				$this->nombre1=$nombre1;
				$this->nombre2=$nombre2;
				$this->apellido1=$apellido1;
				$this->apellido2=$apellido2;
				$this->correo=$correo;
				$this->telefono1=$telefono1;
				$this->telefono2=$telefono2;
				$this->estatus=$estatus;
				
			
			if (($this->nacionalidad!='0')&&($this->cedula!='')&&($this->nombre1!='')&&($this->nombre2!='')&&($this->apellido1!='')
			&&($this->apellido2!='') && ($this->correo!='') && ($this->telefono1!='') &&($this->telefono2!='') &&($this->estatus!='0') )
			{
				$cedula = pg_num_rows (pg_query("SELECT nacionalidad, cedula FROM tb_persona 
				WHERE nacionalidad='$this->nacionalidad' AND cedula='$this->cedula'"));
				if($cedula>0){
					$query=pg_query("SELECT id FROM tb_persona WHERE nacionalidad='$this->nacionalidad' AND
					cedula='$this->cedula'");
					
					$reg=pg_fetch_object($query);
					$id=$reg->id;
					
					$inspector = "update tb_persona  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
					nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1', apellido2='$this->apellido2', correo='$this->correo', telefono1='$this->telefono1', telefono2='$this->telefono2',	estatus='$this->estatus' where id='$id'";
					$consulta = pg_query ($inspector);
					
					$this->mensaje=1;
				}
				else{
					$inspector = "update tb_persona  set nacionalidad='$this->nacionalidad', cedula='$this->cedula', 
					nombre1='$this->nombre1', nombre2='$this->nombre2', apellido1='$this->apellido1', apellido2='$this->apellido2', correo='$this->correo', telefono1='$this->telefono1', telefono2='$this->telefono2',	estatus='$this->estatus' where id='$id'";
					$consulta = pg_query ($inspector);
					$this->mensaje=1;
				}
			}//fin campos en blanco
			else{$this->mensaje=2;}
		}//fin de function
	
		public function usuarios()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  tb_usuario WHERE tipo_p='Usuario'"));
			if ($this->validar>0){
				$this->consulta= pg_query("SELECT * FROM  tb_usuario WHERE tipo_p='Usuario'");
			}
			else{
				$this->mensaje=1;
			}
			
		}//fin de function
		
		public function inspectores()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'"));
			if ($this->validar>0){
				$this->consulta1= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Inspector'");
			}
			else{
				$this->mensaje=1;
			}
			
		}//fin de function

		public function inspecciones()
		{
				$this->validar = pg_num_rows (pg_query("SELECT
										 			  tb_inspecciones.id as ip, 
													  tb_inspecciones.tipo, 
													  tb_inspecciones.fecha, 
													  tb_inspecciones.observaciones, 
													  tb_persona.nacionalidad, 
													  tb_persona.cedula, 
													  tb_persona.nombre1, 
													  tb_persona.nombre2, 
													  tb_persona.apellido1, 
													  tb_persona.apellido2
													FROM 
													  public.tb_inspecciones, 
													  public.tb_persona, 
													  public.tb_persona_inspeccion
													WHERE 
													  tb_inspecciones.id = tb_persona_inspeccion.id_inspeccion AND
													  tb_inspecciones.id_inspector = tb_persona.id
													"));
				if ($this->validar>0){
				$this->inspecciones= pg_query("SELECT 
													  tb_inspecciones.id as ip, 
													  tb_inspecciones.tipo, 
													  tb_inspecciones.fecha, 
													  tb_inspecciones.observaciones, 
													  tb_persona.nacionalidad, 
													  tb_persona.cedula, 
													  tb_persona.nombre1, 
													  tb_persona.nombre2, 
													  tb_persona.apellido1, 
													  tb_persona.apellido2
													FROM 
													  public.tb_inspecciones, 
													  public.tb_persona, 
													  public.tb_persona_inspeccion
													WHERE 
													  tb_inspecciones.id = tb_persona_inspeccion.id_inspeccion AND 
													  tb_inspecciones.id_inspector = tb_persona.id
													");
			}
			else{
				$this->mensaje=1;
			}
			
		}//fin de function
		
		public function propietarios()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'"));
			if ($this->validar>0){
				$this->consulta2= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'");
			}
			else{
				$this->mensaje=1;
			}
			
		}//fin de function

		public function ocupantes_propietarios()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'"));
			if ($this->validar>0){
				$this->persona= pg_query("SELECT * FROM  tb_persona WHERE tipo_p='Propietario/Ocupante'");
			}
			else{
				$this->mensaje=1;
			}
			
		}//fin de function
	}//fin de class
	?>