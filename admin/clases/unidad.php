<?php
	class Unidad
	{
		public $codigo;
		public $precio;
		public $fecha;
		public $estatus;

		
		public $mensaje;
			
		public function cargar ($codigo, $precio, $fecha, $estatus)
		{
	
			$this->codigo=$codigo;
			$this->precio=$precio;
			$this->fecha=$fecha;
			$this->estatus=$estatus;

		}//fin de function
				
		public function registrar()
		{
			if (($this->precio!='')){
					
				$this->cosulta = pg_num_rows (pg_query("SELECT estatus FROM tb_unidad WHERE estatus='Aperturada'"));
				if($this->cosulta==0){
						$unidad = "insert into tb_unidad values (default,'$this->precio', '$this->fecha', '$this->estatus')";
						$sql = pg_query ($unidad);
						
						$this->precio="";
						
						$this->mensaje=1;
				}
				else{
					$this->mensaje=2;
				}
				
			}
			else{
				$this->mensaje=3;
				}
		}//fin de function
		
		public function actualizar($id, $precio)
		{
			$this->id=$id;
			$this->precio=$precio;			
			
			if (($this->precio!='')){
				
				$unidad = "update tb_unidad set precio='$this->precio' where codigo='$this->id'";
				$sql = pg_query ($unidad);
					
				$this->mensaje=1;
			}
			else{
				$this->mensaje=3;
			}
		}//fin de function
	
		public function unidades()
		{
			$this->validar = pg_num_rows (pg_query("SELECT * FROM  tb_unidad"));
			if ($this->validar>0){
				$this->consulta= pg_query("SELECT * FROM  tb_unidad");
				$reg=pg_fetch_object($this->consulta);
				$this->precio=$reg->precio;
				$this->id=$reg->codigo;
				$this->boton = "edit";
			}
			else{
				$this->boton = "new";
			}
		}//fin de function

		public function cerrar($cod, $fecha)
		{
			$this->cod=$cod;
			$this->fecha=$fecha;

			$cerrar = "update tb_unidad set fecha='$this->fecha', estatus='Cerrada' 
						where codigo='$this->cod'";
			$sql = pg_query ($cerrar);

			header("Location: ./unidades.php");

			$this->cierre=pg_affected_rows($cerrar);
			
		}//fin de function
		
		public function datos($edit)
		{
			$this->edit=$edit;
			
			$query=pg_query("SELECT * FROM  tb_unidad WHERE codigo='$this->edit'");
			$reg=pg_fetch_object($query);
			
			$this->id=$reg->codigo;
			$this->precio=$reg->precio;
		}//fin de function
	}//fin de class
	?>