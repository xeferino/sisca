<?php include_once("seguridad.php"); ?>
<?php
	include_once("clases/conexion.php");
	include_once("clases/linderos.php");
	
	$objeto = new linderos();
	$persona = new Persona();
	$persona->propietarios();

	$parroquia = new Parroquia();
	$parroquia->parroquias();
		
		
		if(isset($_GET['id']))
		{
			
			$id= $_GET['id'];
			$objeto->datoCertificacion($id);
		}
		

		if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {	
			
			/*$nacionalidad =  $_POST['nacionalidad'];
			$cedula =  $_POST['cedula'];
			$nombre1 =  $_POST['nombre1'];
			$nombre2 =  $_POST['nombre2'];
			$apellido1 =  $_POST['apellido1'];
			$apellido2 =  $_POST['apellido2'];
			$telefono1 =  $_POST['telefono1'];
			$telefono2 =  $_POST['telefono2'];
*/
			$snorte =  $_POST['norte'];
			$ssur =  $_POST['sur'];
			$seste =  $_POST['este'];
			$soeste =  $_POST['oeste'];

			$lnorte =  $_POST['lnorte'];
			$lsur =  $_POST['lsur'];
			$leste =  $_POST['leste'];
			$loeste =  $_POST['loeste'];
			$fecha =  $_POST['fecha'];
			$ubicacion =  $_POST['ubicacion'];
			$observacion =  $_POST['observacion'];
			$id_persona =  $_POST['persona'];
			$parroquia =  $_POST['parroquia'];

			$area1 = ($_POST['norte']+$_POST['sur'])/2;
			$area2 = ($_POST['este']+$_POST['oeste'])/2;

			$area =  $area1*$area2;

			$id_certificacion = $_POST['id_certificacion'];
					
			$objeto->cargar($area,$snorte,$ssur,$seste,$soeste,$lnorte,$lsur,$leste,$loeste,$fecha, $ubicacion,$observacion, $id_persona, $parroquia);
			$objeto->editar($id_certificacion);

		}
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=6; include('../layout/menu.php'); ?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-map"></i> Certificacion Linderos </li> <li class="active"><i class="ace-icon fa fa-map"></i> Actualizar </li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="linderos.php"><span class="btn btn-primary pull-right" title="Lista de Certificaciones"><i class="ace-icon fa fa-th-list"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Formulario de Actualizacion</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione el Propietario/Ocupante.</strong>
														</p>
													
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Actualizacion Exitosa.",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																},
																function(){
																	$(location).attr('href','http:linderos.php');
																});
															
														});
														</script>
														<?php }?>
													
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "  Cedula de Identidad no Disponible.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos en Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
							<form action="" method="post" id="form-certificacion">
								<input type="hidden" name="submit" value="edit" />
								<input type="hidden" name="id_certificacion" value="<?php  echo $objeto->id;?>">
								<fieldset>
										<div class="col-md-6">
											<div class="form-group">
												<label>Propietario/Ocupante</label>
												
												<select name="persona" class="form-control select2">
													<?php while($reg=pg_fetch_object($persona->consulta2)){?>
													<?php $i++;?>
													<option <?php if ($objeto->id_persona == $reg->id){echo "selected='selected'";}?> value="<?php  echo $reg->id;?>"> <?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->apellido1;?></option>
													<?php }?>
												</select>
											</div>
										</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Fecha</label>
											<input class="form-control" id="campofecha1" name="fecha" value="<?php echo $objeto->fecha;?>" placeholder="" type="text">
										</div>
									</div>
									</fieldset>


									<!-- <p class="alert alert-info">
									<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los Direccion.</strong>
									</p>

									<fieldset>
										<div class="col-md-12">
											<div class="form-group">
												<label>Direccion</label>
												
											</div>
										</div>
									</fieldset> -->
									

								<p class="alert alert-info">
									<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los Datos de los Linderos del Terreno.</strong>
								</p>
									<fieldset>
										<div class="form-group col-sm-3">
										<div class="form-group">
											<label>Superficie Norte</label>
											<input class="form-control" id="norte" name="norte" value="<?php echo $objeto->snorte;?>" placeholder="" type="text">
										</div>
									</div>

									<div class="form-group col-sm-3">
										<div class="form-group">
											<label>Superficie Sur</label>
											<input class="form-control" id="sur" name="sur" value="<?php echo $objeto->ssur;?>" placeholder="" type="text">
										</div>
									</div>
									<div class="form-group col-sm-3">
										<div class="form-group">
											<label>Superficie Este</label>
											<input class="form-control" id="este" name="este" value="<?php echo $objeto->seste;?>" placeholder="" type="text">
										</div>
									</div>

									<div class="form-group col-sm-3">
										<div class="form-group">
											<label>Superficie Oeste</label>
											<input class="form-control" id="oeste" name="oeste" value="<?php echo $objeto->soeste;?>" placeholder="" type="text">
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Lindero Norte</label>
											<textarea class="form-control" name="lnorte" id="lnorte"><?php echo $objeto->norte;?></textarea>
										</div>
									</div>
										
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Lindero Sur</label>
											<textarea class="form-control" name="lsur" id="lsur
											"><?php echo $objeto->sur;?></textarea>
										</div>
									</div>

									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Lidero Este</label>
											<textarea class="form-control" name="leste" id="leste"><?php echo $objeto->este;?></textarea>
										</div>
									</div>

									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Lidero Oeste</label>
											<textarea class="form-control" name="loeste" id="loeste"><?php echo $objeto->oeste;?></textarea>
										</div>
									</div>

									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Comunidad</label>
											<textarea class="form-control required" name="ubicacion" id="ubicacion"><?php echo $objeto->ubicacion;?></textarea>
										</div>
									</div>

									<div class="col-md-6">
											<div class="form-group">
												<label>Parroquia</label>
												<select name="parroquia" class="form-control select2 required">
													<?php while($reg=pg_fetch_object($parroquia->consulta)){?>
													<?php $i++;?>
													<option <?php if ($objeto->id_parroquia == $reg->id){echo "selected='selected'";}?> value="<?php  echo $reg->id;?>"> <?php echo $reg->nombre;?></option>
													<?php }?>
												</select>
											</div>
										</div>
										
										<div class="form-group col-sm-12">
										<div class="form-group">
											<label>Observacion</label>
											<textarea class="form-control required" name="observacion" id="observacion"><?php echo $objeto->ubicacion;?></textarea>
										</div>
									</div>
										
									</fieldset>
									<div>
										<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-pencil"></i> Actualizar</button>
									</div>
									<br><br>
								</form>
						</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ0-9,.()\s]+$/.test(value);
				});

				$.validator.addMethod('cuenta', function(value, element){
					return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
				});
			    $("#form-certificacion").validate({
			        rules: {
			        	campofecha1: { required: true},
			        	lnorte: { required: true, letras: true, minlength: 2, maxlength:500 },
			        	lsur: { required: true, letras: true, minlength: 2, maxlength:500 },
			        	leste: { required: true, letras: true, minlength: 2, maxlength:500 },
			        	loeste: { required: true, letras: true, minlength: 2, maxlength:500 },
			        	norte: { required: true, digits: true, minlength: 1, maxlength:5 },
			        	sur: { required: true, digits: true, minlength: 1, maxlength:5 },
			        	este: { required: true, digits: true, minlength: 1, maxlength:5 },
			        	oeste: { required: true, digits: true, minlength: 1, maxlength:5 },
			        	ubicacion: { required: true, letras: true, minlength: 5, maxlqqength:500 },
			            observacion: { required: true, letras: true, minlength: 10, maxlength:500 },
			        },
			        messages: {
			            campofecha1: {
			            		required: 'la fecha es requerida'
			            },
			            norte: {
			            		required: 'el campo es requerido',
			            		digits: 'el campo solo acepta numeros',
			            		minlength:'el campo debe tener como minimo 1 caracter',
			            		maxlength:'el maximo permitido son 5 caracteres'
			            },
			            sur: {
			            		required: 'el campo es requerido',
			            		digits: 'el campo solo acepta numeros',
			            		minlength:'el campo debe tener como minimo 1 caracter',
			            		maxlength:'el maximo permitido son 5 caracteres'
			            },
			            este: {
			            		required: 'el campo es requerido',
			            		digits: 'el campo solo acepta numeros',
			            		minlength:'el campo debe tener como minimo 1 caracter',
			            		maxlength:'el maximo permitido son 5 caracteres'
			            },
			            oeste: {
			            		required: 'el campo es requerido',
			            		digits: 'el campo solo acepta numeros',
			            		minlength:'el campo debe tener como minimo 1 caracter',
			            		maxlength:'el maximo permitido son 5 caracteres'
			            },
			            lnorte: {
			            		required: 'el campo es requerido',
			            		letras: 'el campo solo acepta numeros, letras y (.,) ',
			            		minlength:'el campo debe tener como minimo 2 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			            lsur: {
			            		required: 'el campo es requerido',
			            		letras: 'el campo solo acepta numeros, letras y (.,) ',
			            		minlength:'el campo debe tener como minimo 2 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			            leste: {
			            		required: 'el campo es requerido',
			            		letras: 'el campo solo acepta numeros, letras y (.,) ',
			            		minlength:'el campo debe tener como minimo 2 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			            loeste: {
			            		required: 'el campo es requerido',
			            		letras: 'el campo solo acepta numeros, letras y (.,) ',
			            		minlength:'el campo debe tener como minimo 2 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			            observacion: {
			            		required: 'la observacion es requerida',
			            		letras: 'la observacion solo acepta numeros, letras y (.,) ',
			            		minlength:'la observacion debe tener como minimo 10 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			            ubicacion: {
			            		required: 'la ubicacion es requerida',
			            		letras: 'la ubicacion solo acepta numeros, letras y (.,) ',
			            		minlength:'la ubicacion debe tener como minimo 5 caracter',
			            		maxlength:'el maximo permitido son 500 caracteres'
			            },
			        }
			    });
			    $("#form-certificacion").submit(function(){
			        $post("edit-certificacion.php");
			        return false;
			    });
			});
		</script>
			<?php include('../layout/footer.php');?>
			<script type="text/javascript">
				$(".select2").select2();
			</script>
	</body>
</html>
