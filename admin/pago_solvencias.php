<?php include_once("seguridad.php"); ?>
<?php
	include_once("clases/motor.php");

		$sol = new Solvencia();
		$objeto = new Persona();
		$objeto->propietarios();
		
		if (isset($_POST['submit']) && $_POST['submit'] == 'new') {	
			
			$monto =  $_POST['monto'];
			$fecha =  $_POST['fecha'];
			$estatus =  "Pagada";
			$anualidad =  $_POST['anualidad'];
			$id_persona =  $_POST['persona'];

			//echo $monto."-".$fecha."-".$estatus."-".$id_persona;
			
			$sol->cargar($id, $monto, $estatus, $fecha, $anualidad, $id_persona);
			$sol->registrar();
		}
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=3; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-money"></i> Solvencias </li> <li class="active"><i class="ace-icon fa fa-money"></i> Nuevo Pago</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="solvencias.php"><span class="btn btn-primary pull-right" title="Solvencias"><i class="ace-icon fa fa-th-list"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Formulario de Registro</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione el Propietario/Ocupante.</strong>
														</p>
													
														<?php if($sol->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Registro Exitoso.",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
															
														});
														</script>
														<?php }?>
													
														<?php if($sol->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Advertencia!",
																  text: "  El usuario seleccionado ya posee  solvencia municipal pagada para el año que Selecciono.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "7000"
																});
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos en Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
							<form action="" method="post" id="form-solvencia">
								<input type="hidden" name="submit" value="new" />
									<fieldset>
										<div class="col-md-12">
											<div class="form-group">
												<label>Propietario/Ocupante</label>
												<select name="persona" class="form-control select2 required">
													<option value="">Seleccione</option>
													<?php while($reg=pg_fetch_object($objeto->consulta2)){?>
													<?php $i++;?>
													<option value="<?php  echo $reg->id;?>"> <?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->apellido1;?></option>
													<?php }?>
												</select>
											</div>
										</div>
									</fieldset>
									
								<p class="alert alert-info">
									<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los Datos del Pago de Solvencia.</strong>
								</p>
									<fieldset>
										
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Fecha</label>
											<input class="form-control required" id="campofecha1" name="fecha" value="<?php echo $sol->fecha;?>" placeholder="0000-00-00" type="text" readonly="">
										</div>
									</div>

									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Monto</label>
											<input class="form-control required number" name="monto" id="monto" value="<?php echo $sol->monto;?>" placeholder="" type="text">
										</div>
									</div>
										
									</fieldset>
									<fieldset>
										<!-- <div class="col-md-6">
											<div class="form-group">
												<label>Estatus de la Solvencia</label>
												<select name="estatus" class="form-control select2" data-placeholder="--Seleccione--" style=" width: 100%;">
													<option value="Pagada">Pagada</option>
													<option value="Pendiente">Pendiente</option>
												</select>
											</div>
										</div> -->

										<div class="col-md-6">
											<div class="form-group">
												<label>Anualidad</label>
												<select name="anualidad" class="form-control select2 required" data-placeholder="--Seleccione--" style=" width: 100%;">
													<option value="">--Seleccione--</option>
													<option value="2010">2010</option>
													<option value="2011">2011</option>
													<option value="2012">2012</option>
													<option value="2013">2013</option>
													<option value="2014">2014</option>
													<option value="2015">2015</option>
													<option value="2016">2016</option>
													<option value="2017">2017</option>
													<option value="2018">2018</option>
													<option value="2019">2019</option>
													<option value="2020">2020</option>
													<option value="2021">2021</option>
													<option value="2022">2022</option>
													<option value="2023">2023</option>
													<option value="2024">2024</option>
													<option value="2025">2025</option>
													<option value="2026">2026</option>
													<option value="2027">2027</option>
													<option value="2028">2028</option>
													<option value="2029">2029</option>
													<option value="2030">2030</option>
												</select>
											</div>
										</div>
									</fieldset>
									
									<div>
										<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-ok-sign"></i> Registrar</button>
									</div>
									<br><br>
								</form>
						</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<!-- <script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ0-9,.\s]+$/.test(value);
				});

				$.validator.addMethod('cuenta', function(value, element){
					return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
				});
			    $("#form-solvencia").validate({
			        rules: {
			        	campofecha1: { required: true},
			            monto: { required: true, digits: true, minlength: 2, maxlength:10 },
			        },
			        messages: {
			            campofecha1: {
			            		required: 'la fecha es requerida'
			            },
			            monto: {
			            		required: 'el monto es requerido',
			            		digits: 'el monto solo acepta numeros',
			            		minlength:'la monto debe tener como minimo 2 caracter',
			            		maxlength:'el maximo permitido son 10 caracteres'
			            },
			        }
			    });
			    $("#form-solvencia").submit(function(){
			        $post("pago_solvencias.php.php");
			        return false;
			    });
			});
		</script> -->
			<?php include('../layout/footer.php');?>
			<script type="text/javascript">
				$(".select2").select2();
			</script>
	</body>
</html>
