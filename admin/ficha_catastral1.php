<?php include_once("seguridad.php"); ?>
<?php include_once("clases/motor.php");
		$objeto = new Persona();
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=1; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>							</li>
							<li class="active">Ficha Catastral</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">

							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
									<div class="widget-box">
									<div class="widget-header widget-header-blue widget-header-flat">
										<h4 class="widget-title lighter"><i class="ace-icon fa 	fa-file-text-o"></i> Nueva Ficha Catastral</h4>

										<div class="widget-toolbar">
											<label>
												<small class="green">
													<b>Validation</b>
												</small>

												<input id="skip-validation" type="checkbox" class="ace ace-switch ace-switch-4" />
												<span class="lbl middle"></span>
											</label>
										</div>
									</div>

									<div class="widget-body">
										<div class="widget-main">
											<div id="fuelux-wizard-container">
												<div>
													<ul class="steps">
														<li data-step="1" class="active">
															<span class="step">1</span>
															<span class="title">Informacion General</span>
														</li>

														<li data-step="2">
															<span class="step">2</span>
															<span class="title">Ocupante/Propietario</span>
														</li>

														<li data-step="3">
															<span class="step">3</span>
															<span class="title">Datos del Terreno</span>
														</li>

														<li data-step="4">
															<span class="step">4</span>
															<span class="title">Datos de la Construccion</span>
														</li>
														<li data-step="5">
															<span class="step">5</span>
															<span class="title">Datos Complementarios Const.</span>
														</li>
														<li data-step="6">
															<span class="step">6</span>
															<span class="title">Other Info</span>
														</li>
														<li data-step="7">
															<span class="step">7</span>
															<span class="title">Other Info</span>
														</li>
														<li data-step="8">
															<span class="step">8</span>
															<span class="title">Other Info</span>
														</li>
														<li data-step="8">
															<span class="step">8</span>
															<span class="title">Other Info</span>
														</li>
														<li data-step="9">
															<span class="step">9</span>
															<span class="title">Other Info</span>
														</li>
													</ul>
												</div>

												<hr />

												<div class="step-content pos-rel">
													

													<div class="step-pane" data-step="1">
													

							<form action="" method="POST">
								<input type="hidden" name="submit" value="new" />
								
									<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Datos de Ubicacion Comunitaria</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Region Federal de Gobierno</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>

														<div class="form-group">
															<label>Comuna</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Consejo Comunal</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Comite de Tierra Urbano</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>N° Registro MVV</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Direccion del Inmueble</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Parroquia</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>

														<div class="form-group">
															<label>Ciudad</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Localidad</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Urbanizacion</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Consejo Residencial</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Barrio</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Sector</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>N° Civico</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Punto de Referencia</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
													</div>
												</div>
											</div>
										</div>
								</form>
							</div>

													<div class="step-pane" data-step="2">
														<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Datos del Ocupante</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Cedula:</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Primer Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Segundo Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>

															<div class="form-group">
															<label>Primer Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Segundo Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Telefono:</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Telefono Celular</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Datos del Propietario</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Cedula:</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Primer Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Segundo Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>

															<div class="form-group">
															<label>Primer Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Segundo Nombre</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
															<div class="form-group">
															<label>Tipo</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Telefono</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Parroquia</label>
															<input class="form-control" name="archivo" value="" placeholder="EJ:02" type="text" maxlength="2">
														</div>

														<div class="form-group">
															<label>Ciudad</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Localidad</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Urbanizacion</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Consejo Residencial</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Barrio</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Sector</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>N° Civico</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
														<div class="form-group">
															<label>Punto de Referencia</label>
															<input class="form-control" name="fecha" value="" placeholder="EJ:01-01-2010" type="text" maxlength="2">
														</div>
													</div>
												</div>
											</div>
										</div>
													</div>

													<div class="step-pane" data-step="3">
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Topografia</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Acceso</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Forma</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Ubicacion</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Entorno Fisico</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Mejoras al Terreno</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Tenencia Terreno</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Registro Propiedad</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Uso Actual</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>


										<div class="col-xs-12 col-sm-12">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Servicios Publicos</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

													</div>
													<div class="step-pane" data-step="4">
														<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Tipo</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Descripcion de uso</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Tenencia Construccion</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Regimen de Propiedad</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-8">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Datos Estructurales</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Soporte </label>
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
														<div class="form-group">
															<label>Soporte </label>
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Cubierta Externa</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Cubierta Interna</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

									</div>

													<div class="step-pane" data-step="5">
														<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Tipo</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Descripcion de uso</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Tenencia Construccion</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-4">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Regimen de Propiedad</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-8">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Datos Estructurales</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<label>Soporte </label>
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
														<div class="form-group">
															<label>Soporte </label>
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Cubierta Externa</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>

										<div class="col-xs-12 col-sm-6">
											<div class="widget-box">
												<div class="widget-header">
													<h4 class="widget-title"> <i class="ace-icon fa 	fa-pencil"></i>Cubierta Interna</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<div class="form-group">
															<select class="form-control" name="estatus">
																<option value="Activo">Activo</option>
															</select>
														</div>
												</div>
												</div>
											</div>
										</div>
													</div>

													<div class="step-pane" data-step="6">
														<div class="center">
															<h3 class="blue lighter">This is step 6</h3>
														</div>
													</div>

													<div class="step-pane" data-step="7">
														<div class="center">
															<h3 class="blue lighter">This is step 7</h3>
														</div>
													</div>

													<div class="step-pane" data-step="8">
														<div class="center">
															<h3 class="blue lighter">This is step 8</h3>
														</div>
													</div>

													<div class="step-pane" data-step="9">
														<div class="center">
															<h3 class="blue lighter">This is step 9</h3>
														</div>
													</div>

												</div>
											</div>

											<hr />
											<div class="wizard-actions">
												<button class="btn btn-prev">
													<i class="ace-icon fa fa-arrow-left"></i>
													Anterior
												</button>

												<button class="btn btn-primary btn-next" data-last="Finish">
													Siguiente
													<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
												</button>
											</div>
										</div><!-- /.widget-main -->
									</div><!-- /.widget-body -->
								</div>
								<!-- PAGE CONTENT ENDS -->
								</div>
							</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<?php include('../layout/footer.php');?>
			
	</body>
</html>
