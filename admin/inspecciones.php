<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");
$objeto = new Persona();
$objeto->inspecciones();
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=4; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="./">Home</a>							</li>
<li class="active"><i class="ace-icon fa fa-eye"></i> Inspeccciones</li> 
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">
<?php if ($objeto->mensaje==1){?>
<script>
$(document).on('ready',function(event){
event.preventDefault();

swal({
title: "Alerta!",
text: "No Existen Inspeccciones Registradas en el Sistema",
type: "info",
confirmButtonText: 'Aceptar'
},
function(){
$(location).attr('href','http:nueva_inspeccion.php');
});//swal
});
</script>
<?php }
else{
?>
<div class="col-xs-12" >
<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nueva Inspeccion"><i class="ace-icon fa fa-eye"></i></span></a><br><br><br>
<!-- PAGE CONTENT BEGINS -->
<table id="dynamic-t" class="table table-striped">
<thead>
	<tr>
		<th class="center">ID</th>
		<th><i class="fa fa-credit-card"></i> Cedula</th>
		<th><i class="fa fa-user"></i> Nombre y Apellido (Inspector)</th>
		<th><i class="fa fa-calendar"></i> Fecha de Inspecci&oacute;n</th>
		<th><i class="fa fa-check-square-o"></i> Acciones</th>
	</tr>
</thead>

<tbody>
<?php $i=0;?>			
<?php while($reg=pg_fetch_object($objeto->inspecciones)){?>
<?php 
	$i++;
$query = pg_query("SELECT 
					  tb_inspecciones.tipo AS t, 
					  tb_inspecciones.fecha AS f, 
					  tb_inspecciones.observaciones AS o, 
					  tb_persona.nacionalidad AS n, 
					  tb_persona.cedula AS ci, 
					  tb_persona.nombre1 AS n1, 
					  tb_persona.nombre2 AS n2, 
					  tb_persona.apellido1 AS a1, 
					  tb_persona.apellido2 AS a2
					FROM 
					  public.tb_inspecciones, 
					  public.tb_persona, 
					  public.tb_persona_inspeccion
					WHERE 
					  tb_inspecciones.id = tb_persona_inspeccion.id_inspeccion AND
					  tb_persona_inspeccion.id_persona = tb_persona.id AND tb_inspecciones.id='$reg->ip';");
$row = pg_fetch_array($query);
$tipo = $row['t'];
$fecha = $row['f'];
$observacion = $row['o'];
$n = $row['n'];
$ci = $row['ci'];
$nombre1 = $row['n1'];
$nombre2 = $row['n2'];
$apellido1 = $row['a1'];
$apellido2 = $row['a2'];

?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $reg->nacionalidad."-".$reg->cedula;?></td>
		<td><?php echo $reg->nombre1." ".$reg->nombre2." ".$reg->apellido1." ".$reg->apellido2;?></td>
		<td><span class="label label-warning"><?php echo $reg->fecha;?></span></td>

		<td>	
		<a class="btn btn-primary btn-xs btnEdit<?php echo $reg->ip;?>" href="edit-inspeccion.php?id=<?php echo $reg->ip;?>">
				<i class="ace-icon fa fa-pencil bigger-130"></i>
		</a>
		<a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#inspeccion<?php echo $i;?>" href="#" title="Ver">
				<i class="ace-icon fa fa-search bigger-130"></i>
		</a>
		<a class="btn btn-primary btn-xs" href="reportes/inspecciones_pdf.php?id=<?php echo $reg->ip;?>" target="_blank" title="PDF">
				<i class="ace-icon fa fa-file-pdf-o bigger-130"></i>
		</a>
		
		</td>
	</tr>
	<?php include('modales.php');?>
<?php }?>
</tbody>
</table>
<!-- PAGE CONTENT ENDS -->
</div>
<?php }	?>
<!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<?php include('../layout/footer.php');?>
<script>
$(document).ready(function() {	
$('#dynamic-t').DataTable({
responsive: true,
aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
});	
$('.btnNew').click(function(){

swal({
title: "Alerta!",
text: "Nueva Inspeccion",
type: "info",
showCancelButton: true,
closeOnConfirm: false,
confirmButtonText: 'Aceptar'
},
function(){
$(location).attr('href','http:nueva_inspeccion.php');
});//swal

});//click

});///dom
</script>
</body>
</html>
