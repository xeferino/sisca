<?php 
		include_once("seguridad.php");
		include_once("clases/motor.php");
		$objeto = new Unidad();
		$objeto->unidades();

		if(isset($_GET['cerrar'])){

			$cod = $_GET['cerrar'];
			$fecha = date('d/m/Y');
			$objeto->cerrar($cod, $fecha);
		}

		if(isset($_GET['cerrar'])){

			$cod = $_GET['cerrar'];
			$fecha = date('d/m/Y');
			$objeto->cerrar($cod, $fecha);
		}
		if(isset($_GET['edit'])){

			$edit = $_GET['edit'];
			$objeto->datos($edit);
		}

		if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {	
			
		
		$precio =  $_POST['precio'];
		$id = $_POST['id'];
		$objeto->actualizar($id, $precio);
	}
?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=8; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-money"></i> Unidades Tributarias</li> 
						</ul><!-- /.breadcrumb -->
					</div>
					<?php if(!$_GET['edit']){?>

						<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<?php if ($objeto->cierre==true){?>

							<script>
								$(document).ready(function() {
									swal({
										title: "",
										text: "Unidad Tributaria Cerrada Exitosamente",
										type: "success",
										confirmButtonText: "Aceptar",
										timer: "3000"
									});
								});
							</script>

							<?php }?>

							<?php if ($objeto->mensaje==1){?>
								<script>
									$(document).on('ready',function(event){
									event.preventDefault();
									
									swal({
										title: "Alerta!",
										text: "No Existen Unidades Tributarias (U.T.) Registradas en el Sistema",
										type: "info",
										confirmButtonText: 'Aceptar'
									},
									function(){
										$(location).attr('href','http:unidad_tributaria.php');
									});//swal
								});
							</script>
							<?php }
								  else{
							?>
							<div class="col-xs-12" >
								<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nueva Unidad Tributaria"><i class="ace-icon fa fa-plus"></i></span></a><br><br><br>
								<!-- PAGE CONTENT BEGINS -->
								<table id="dynamic-table1" class="table table-striped">
												<thead>
													<tr>
														<th class="center">ID</th>
														<th><i class="fa fa-pencil"></i> Precio</th>
														<th><i class="fa fa-calendar"></i> Fecha</th>
														<th><i class="fa fa-pencil"></i> Estatus</th>
														<th><i class="fa fa-check-square-o"></i> Acciones</th>
													</tr>
												</thead>
												
												<tbody>
												<?php $i=0;?>			
												<?php while($reg=pg_fetch_object($objeto->consulta)){?>
												<?php $i++;?>
													<tr>
														<td><?php echo $i;?></td>
														<td><?php echo $reg->precio." Bs";?></td>
														<td><?php echo $reg->fecha;?></td>
														<td><?php echo $reg->estatus;?></td>

														<td>	
															
															<a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="#">
																<i class="ace-icon fa fa-pencil bigger-130"></i>
															<script>
																$(document).on('ready',function(event){
																event.preventDefault();
																
																$('.btnEdit<?php echo $i;?>').click(function(){
																
																swal({
																	title: "Actualizar",
																	text: "<?php echo "Unidad Tributaria: ".$reg->precio;?>",
																	type: "info",
																	showCancelButton: true,
																	confirmButtonColor: '#DD6B55',
																	closeOnConfirm: false,
																	confirmButtonText: 'Aceptar'
																},
																function(){
																	$(location).attr('href','http:unidades.php?edit=<?php echo $reg->codigo;?>');
																});
																});
															});
															 </script>
															</a>
															<?php if ($reg->estatus =='Aperturada'){ ?>
															<a class="btn btn-primary btn-xs btnut<?php echo $i;?>" href="#">
																<i class="ace-icon fa fa-unlock bigger-130"></i>
															<script>
																$(document).on('ready',function(event){
																event.preventDefault();
																
																$('.btnut<?php echo $i;?>').click(function(){
																
																swal({
																	title: "Cerrar Unidad Tributaria",
																	text: "<?php echo "U.T.: ".$reg->precio." Bs.";?>",
																	type: "info",
																	showCancelButton: true,
																	confirmButtonColor: '#DD6B55',
																	closeOnConfirm: false,
																	confirmButtonText: 'Aceptar'
																},
																function(){
																	$(location).attr('href','http:unidades.php?cerrar=<?php echo $reg->codigo;?>');
																});
																});
															});
															 </script>
															</a>
															<?php } else {?>
																<a class="btn btn-primary btn-xs" href="#" title="Unidad Tributaria Cerrada">
																<i class="ace-icon fa fa-lock bigger-130"></i></a>
															<?php } ?>
														</td>
													</tr>
												<?php }?>
												</tbody>
											</table>
								<!-- PAGE CONTENT ENDS -->
							</div>
							<!-- /.col -->
							<?php }//fin else?>
						</div><!-- /.row -->
					</div><!-- /.page-content -->

					<?php } else{?>

						<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								  <a href="unidades.php"><span class="btn btn-primary pull-right" title="Lista de Parroquias"><i class="ace-icon fa fa-list"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-money"></i> Formulario de Actualizacion</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<i class="ace-icon fa 	fa-edit"></i> <strong>Ingrese los Datos para el Actualizacion.</strong>
														</p>
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Unidad Tributaria Actualizada Exitosamente",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																},
																function(){
																	$(location).attr('href','http:unidades.php');
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Para Registrar una (UT), debe Cerrar la Aperturada",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos En Blancos",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
															
							<form action="" method="post">
								<input type="hidden" name="submit" value="edit" />
								<input type="hidden" name="id" value="<?php echo $objeto->id;?>">
									<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											<label class="block clearfix"> Precio 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="precio" value="<?php echo $objeto->precio;?>"  placeholder="" type="text" maxlength="">
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								
							
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											
											<button type="submit" class="btn btn-primary pull-right" title="Actualizar"><i class="fa fa-pencil"></i> Editar</button>
										</div>
										<div class="form-group col-sm-2"></div>
									<div>
										<br><br><br>
									</div>
								</form>
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->

					<?php } ?>
				</div>
			</div><!-- /.main-content -->
			<?php include('../layout/footer.php');?>
	<script>
    $(document).ready(function() {
        $('#dynamic-table1').DataTable({
                responsive: true,
				aoColumnDefs:[{'bSortable':false,'aTargets':[3]}]
        });
		
		$('.btnNew').click(function(){
	
			swal({
				title: "Alerta!",
				text: "Nueva U.T.",
				type: "info",
				showCancelButton: true,
				closeOnConfirm: false,
				confirmButtonText: 'Aceptar'
			},
			function(){
				$(location).attr('href','http:unidad_tributaria.php');
			});//swal
	
	});//click
	
    });///dom
    </script>
	</body>
</html>
