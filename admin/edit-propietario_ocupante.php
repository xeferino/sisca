<?php include_once("seguridad.php"); ?>
<?php
	include_once("clases/motor.php");
	
	$objeto = new Persona();
		if(isset($_GET['id']))
		{
			$id= $_GET['id'];
			$objeto->data_persona($id);
		}
		else
		{
			header("Location: ./");
		}
			
		if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {	
			
			$nacionalidad =  $_POST['nacionalidad'];
			$cedula =  $_POST['cedula'];
			$nombre1 =  $_POST['nombre1'];
			$nombre2 =  $_POST['nombre2'];
			$apellido1 =  $_POST['apellido1'];
			$apellido2 =  $_POST['apellido2'];
			$correo =  $_POST['correo'];
			$telefono1 =  $_POST['telefono1'];
			$telefono2 =  $_POST['telefono2'];
			
			$estatus =  $_POST['estatus'];
			if ($estatus== "0"){$estatus='Activo';}
			if ($estatus== "1"){$estatus='Inactivo';}
			$id= $_POST['id'];
			$ci= $_POST['ci'];
			$n= $_POST['n'];
						
			$objeto->update_propietario_ocupante($id, $n, $ci, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $correo, $telefono1, 
				$telefono2, $estatus);
		}

?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=7; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-user"></i> Priopietario/Ocupante </li> <li class="active"><i class="ace-icon fa fa-user"></i> Editar </li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="inspectores.php"><span class="btn btn-primary pull-right" title="Lista de Inspectores"><i class="ace-icon fa fa-users"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Actualizar Priopietario/Ocupante</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Actualize los Datos del Priopietario/Ocupante.</strong>
														</p>
													
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Priopietario/Ocupante Actualizado Exitosamente.",
																  type: "success",
																  confirmButtonText: "Aceptar"
																},
																function(){
																	$(location).attr('href','http:propietarios_ocupantes.php');
																});
															
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos en Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Cedula: <?php echo $objeto->nacionalidad."-".$objeto->cedula;?> No esta Registrada, Verifique!",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "2000"
																},
																function(){
																	$(location).attr('href','http:propietarios_ocupantes.php');
																});
														});
														</script>
														<?php }?>
							<form action="" method="post" id="form-propietario">
								<input type="hidden" name="submit" value="edit" />
								<input type="hidden" name="id" value="<?php echo $objeto->id;?>" />
								<input type="hidden" name="ci" value="<?php echo $objeto->cedula;?>" />
								<input type="hidden" name="n" value="<?php echo $objeto->nacionalidad;?>" />
									<fieldset>
									<div class="form-group col-sm-4">
										<div class="form-group">
											<?php 
										$nac = array('V' => 'V',
											'E' => 'E',
											'P' => 'P',
											'T' => 'T',
											'J' => 'J'
										);
										?>
											<label>Nacionalidad</label>

											<select name="nacionalidad" id="nacionalidad" class="form-control select2">
											<option value="">Seleccione</option>
											<?php foreach ($nac as $key => $value){?>
	
												<option <?php if ($key == $objeto->nacionalidad){echo "selected='selected'";}?> value="<?php echo $key?>"><?php echo $key?></option>
											<?php }?>
												
											</select>
										</div>
									</div>
									<div class="form-group col-sm-8">
										<div class="form-group">
											<label>Cedula de Identidad</label>
											<input class="form-control" name="cedula" id="cedula" value="<?php echo $objeto->cedula;?>" placeholder="EJ:12345678" type="text" >
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Nombre</label>
											<input class="form-control" name="nombre1" id="nombre1" value="<?php echo $objeto->nombre1;?>" placeholder="EJ: Juan" type="text">
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Nombre</label>
											<input class="form-control" name="nombre2" id="nombre2" value="<?php echo $objeto->nombre2;?>" placeholder="EJ: Miguel" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Apellido</label>
											<input class="form-control" name="apellido1" id="apellido1" value="<?php echo $objeto->apellido1;?>" placeholder="EJ: Lara" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Apellido</label>
											<input class="form-control" name="apellido2" id="apellido2" value="<?php echo $objeto->apellido2;?>" placeholder="EJ: Lopez" type="text">
										</div>
									</div>
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Telefono</label>
											<input class="form-control" name="telefono1" id="telefono1" value="<?php echo $objeto->telefono1;?>" placeholder="EJ: 04161234567" type="text">
										</div>
									</div>

									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Telefono Celular</label>
											<input class="form-control" name="telefono2" id="telefono2" value="<?php echo $objeto->telefono2;?>" placeholder="EJ: 04161234567" type="text">
										</div>
									</div>
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Correo</label>
											<input class="form-control" name="correo" id="correo" value="<?php echo $objeto->correo;?>" placeholder="EJ: prueba@prueba.com" type="email">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Cambiar Estatus</label>
											<select class="form-control select2" name="estatus">
											<?php if ($objeto->estatus=="Activo"){?>
													<option value="0">--Seleccione--</option>
													<option value="Inactivo">Inactivo</option>
											<?php } else {?>
											<option value="1">--Seleccione--</option>
											<option value="Activo">Activo</option>
											<?php }?>
												</select>
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Estatus</label>
											<input class="form-control" name="" value="<?php echo $objeto->estatus;?>" placeholder="EJ: Lopez" type="text" disabled="disabled">
										</div>
									</div>
										
									</fieldset>
									<div>
										<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-pencil"></i> Actualizar</button>
									</div>
									<br><br>
								</form>
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ\s]+$/.test(value);
				});
			    $("#form-propietario").validate({
			        rules: {
nacionalidad: { required: true},
cedula: { required: true, digits: true, minlength: 7, maxlength:9 },
nombre1: { required:true, letras: true, minlength: 4,  maxlength:50 },
nombre2: { required:true, letras: true, minlength: 4,  maxlength:50 },
apellido1: { required:true, letras: true, minlength: 4,  maxlength:50 },
apellido2: { required:true, letras: true, minlength: 4,  maxlength:50 },
telefono1: { required: true, digits: true, minlength: 11, maxlength:11 },
telefono2: { required: true, digits: true, minlength: 11, maxlength:11 },
correo: { required: true },
},
messages: {
nacionalidad: {
required: 'la nacionalidad es requerida'
},
cedula: {
required: 'la cedula es requerida',
digits: 'la cedula solo acepta numeros ',
minlength:'la cedula debe tener como minimo 7 caracter',
maxlength:'el maximo permitido son 9 caracteres'
},
nombre1: {
required: 'el primer nombre es requerido',
minlength:'el primer nombre debe tener como minimo 4 caracter',
maxlength:'el maximo permitido son 50 caracteres',
letras:'el primer nombre acepta solo letras'
},
nombre2: {
required: 'el segundo nombre es requerido',
minlength:'el segundo nombre debe tener como minimo 4 caracter',
maxlength:'el maximo permitido son 50 caracteres',
letras:'el segundo nombre acepta solo letras'
},
apellido1: {
required: 'el primer apellido es requerido',
minlength:'el primer apellido debe tener como minimo 4 caracter',
maxlength:'el maximo permitido son 50 caracteres',
letras:'el primer apellido acepta solo letras'
},
apellido2: {
required: 'el segundo apellido es requerido',
minlength:'el segundo apellido debe tener como minimo 4 caracter',
maxlength:'el maximo permitido son 50 caracteres',
letras:'el segundo apellido acepta solo letras'
},
telefono1: {
required: 'el telefono es requerido',
digits: 'el telefono solo acepta numeros ',
minlength:'el telefono debe tener como minimo 11 caracter',
maxlength:'el maximo permitido son 11 caracteres'
},
telefono2: {
required: 'el telefono celular es requerido',
digits: 'el telefono solo acepta numeros ',
minlength:'el telefono debe tener como minimo 11 caracter',
maxlength:'el maximo permitido son 11 caracteres'
},
correo: {
required: 'el correo es requerido'
},

}
			    });
			    $("#form-propietario").submit(function(){
			        $post("edit-propietario_ocupante.php");
			        return false;
			    });
			});
		</script>
			<?php include('../layout/footer.php');?>
			<script type="text/javascript">
				$(".select2").select2();
			</script>
	</body>
</html>
