<?php include_once("seguridad.php"); ?>
<?php
include_once("clases/conexion.php");
include_once("clases/persona.php");

$objeto = new Persona();
$objeto->inspectores();
$objeto->propietarios();

//var_dump($_GET['id']);die();
if(isset($_GET['id']))
{

$id = $_GET['id'];
	$objeto->datoInspeccion($id);
	$objeto->datoPersonaInspeccion($id);
}

if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {	

$inspeccionar =  $_POST['inspeccionar'];
$fecha =  $_POST['fecha'];
$observacion =  $_POST['observacion'];
$id_persona =  $_POST['persona'];
$id_inspector =  $_POST['ispector'];
$id_inspeccion = $_POST['id_inspeccion'];

$objeto->datos_inspecciones($inspeccionar,$fecha,$observacion,$id_persona, $id_inspector);
$objeto->actualizar_inspeccion($id_inspeccion);
}
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=4; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="./">Home</a>							</li>
<li class="active"><i class="ace-icon fa fa-user"></i> Inspecciones </li> <li class="active"><i class="ace-icon fa fa-user"></i> Editar </li>
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
<a href="inspecciones.php"><span class="btn btn-primary pull-right" title="Lista de Inspectores"><i class="ace-icon fa fa-users"></i></span></a><br><br><br>
			<div class="widget-box" id="widget-box-4">
				<div class="widget-header widget-header-large">
					<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Actualizar datos de la inspeccion</h4>
				</div>

				<div class="widget-body">
					<div class="widget-main">
						<p class="alert alert-info">
							<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione  el Inspector</strong>
						</p>
					
						<?php if($objeto->mensaje==1){?>
						<script>
						$(document).ready(function() {
							swal({
								  title: "",
								  text: "Actualizacion Exitosa.",
								  type: "success",
								  confirmButtonText: "Aceptar",
								  timer: "3000"
								},
								function(){
									$(location).attr('href','http:inspecciones.php');
								});
							
						});
						</script>
						<?php }?>
					
						<?php if($objeto->mensaje==2){?>
						<script>
						$(document).ready(function() {
							swal({
								  title: "Error!",
								  text: "  Cedula de Identidad no Disponible.",
								  type: "error",
								  confirmButtonText: "Aceptar",
								  timer: "3000"
								});
						});
						</script>
						<?php }?>
						
						<?php if($objeto->mensaje==3){?>
						<script>
						$(document).ready(function() {
							swal({
								  title: "Error!",
								  text: "Campos en Blancos.",
								  type: "error",
								  confirmButtonText: "Aceptar",
								  timer: "3000"
								});
						});
						</script>
						<?php }?>
						
<form action="" method="post" id="form-inspeccion">
<input type="hidden" name="submit" value="edit" />
<input type="hidden" name="id_inspeccion" value="<?php echo $id;?>" />
	<fieldset>
		<div class="col-md-12">
			<div class="form-group">
				<label>Inspector</label>
				<select name="ispector" class="form-control select2">
					<?php while($reg=pg_fetch_object($objeto->consulta1)){?>
					<?php $i++;?>
					<option <?php if ($objeto->id_inspector == $reg->id){echo "selected='selected'";}?> value="<?php  echo $reg->id;?>"> <?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->apellido1;?></option>
					<?php }?>
				</select>
			</div>
		</div>
	</fieldset>

	<fieldset>
		<p class="alert alert-info">
			<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Seleccione  El Propietario/Ocupante a Realizar la Inspeccion</strong>
		</p>
		<div class="col-md-12">
			<div class="form-group">
				<label>Propietario/Ocupante</label>
				<select name="persona" class="form-control select2" data-placeholder="--Seleccione--" style=" width: 100%;">
					<?php while($reg=pg_fetch_object($objeto->consulta2)){
						


					?>
					<?php $i++;?>
					<option <?php if ($objeto->id_persona == $reg->id){echo "selected='selected'";}?>  value="<?php  echo $reg->id;?>"> <?php echo $reg->nacionalidad."-".$reg->cedula." ".$reg->nombre1." ".$reg->apellido1;?></option>
					<?php }?>
				</select>
			</div>
		</div>
	</fieldset>

		<fieldset>
		
	<div class="form-group col-sm-6">
		<div class="form-group">
			<label>Fecha</label>
			<input class="form-control" id="campofecha1" name="fecha" value="<?php echo $objeto->fecha;?>" placeholder="0000-00-00" type="text">
		</div>
	</div>

	<div class="form-group col-sm-6">
		<div class="form-group">
			<label>Tipo</label>
			<select name="inspeccionar" class="form-control select2">
					
					<option <?php if ($objeto->tipo == "Lindero"){echo "selected='selected'";}?> value="Lindero">Lindero</option>
					<option <?php if ($objeto->tipo == "Terreno"){echo "selected='selected'";}?> value="Terreno">Terreno</option>
				</select>
		</div>
	</div>

	<div class="form-group col-sm-12">
		<div class="form-group">
			<label>Observacion</label>
			<textarea class="form-control" name="observacion" id="observacion"><?php echo $objeto->observaciones;?></textarea>
		</div>
	</div>
		
	</fieldset>

	<div>
		<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-pencil"></i> Actualizar</button>
	</div>
	<br><br>
</form>
					</div>
					
				</div>
			</div>
		</div>
<!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<script type="text/javascript"> 
$(document).ready(function(event) {

$.validator.addMethod('letras', function(value, element){
return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ0-9,.\s]+$/.test(value);
});

$.validator.addMethod('cuenta', function(value, element){
return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
});
$("#form-inspeccion").validate({
rules: {
campofecha1: { required: true},
observacion: { required: true, letras: true, minlength: 10, maxlength:500 },
},
messages: {
campofecha1: {
required: 'la fecha es requerida'
},
observacion: {
required: 'la observacion es requerida',
letras: 'la observacion solo acepta numeros, letras y (.,) ',
minlength:'la observacion debe tener como minimo 10 caracter',
maxlength:'el maximo permitido son 500 caracteres'
},
}
});
$("#form-inspeccion").submit(function(){
$post("nueva_inspeccion.php.php");
return false;
});
});
</script>
<?php include('../layout/footer.php');?>
<script type="text/javascript">
$(".select2").select2();
</script>
</body>
</html>
