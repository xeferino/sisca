<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if (isset($_POST['submit']) && $_POST['submit'] == 'pdf') {

	$fechainicio = $_POST['fechainicio'];
	$fechafin = $_POST['fechafin'];
	$sector = $_POST['sector'];

	if($sector=="todos"){
		$sector_nombre = "Todos";
		$query=pg_query("SELECT
						fc.numeroarchivo,
						fc.numerocatastral,
						fc.fechainscripcion,
						sec.nombre AS sector_nombre
					FROM
						tb_inmueble AS ib
					LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = ib.idfichacatastral
					LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul AS sec ON sec. ID = uc.sector
					LEFT JOIN tb_parroquia AS pa ON pa.codigo = uc.parroquia
					WHERE
						fc.fechainscripcion BETWEEN '".$fechainicio."'
					AND '".$fechafin."'"
				);
	}elseif ($sector!="todos") {
		# code...
		$query=pg_query("SELECT
							fc.numeroarchivo,
							fc.numerocatastral,
							fc.fechainscripcion,
							sec.nombre AS sector_nombre
							FROM
							tb_inmueble AS ib
							LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = ib.idfichacatastral
							LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = ib.idubicacioncomunitaria
							LEFT JOIN tb_pedul AS sec ON sec. ID = uc.sector
							LEFT JOIN tb_parroquia AS pa ON pa.codigo = uc.parroquia
							WHERE fc.fechainscripcion BETWEEN  '".$fechainicio."' AND '".$fechafin."'
							AND sec. ID = '".$sector."'"
				);
		$sec=pg_query("SELECT
							sec.nombre AS sector_nombre
							FROM
							tb_pedul AS sec
							WHERE sec. ID = '".$sector."'"
				);

		$fila = pg_fetch_array($sec);
		$sector_nombre = $fila['sector_nombre'];
	}

	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$title = "Listado de Ficha Catastral";
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sisca');
$pdf->SetSubject('PDF');
$pdf->SetTitle($title);
$pdf->SetKeywords('Listado de Ficha Catastral');
$cintillo = "pie.png";

$pdf->SetHeaderData($cintillo, "184", "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'República Bolivariana de Venezuela', '', 0, 'C', true, 0, false, false, 0);

//$pdf->Write(0, 'Ficha Catastral', '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica', '', 8);



// -----------------------------------------------------------------------------
#DATOS GENERALES
$encabezado = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<br><br><b> Listado de Ficha Catastral para el rango de fecha: '.$fechainicio.' - '.$fechafin.' Sector: '.$sector_nombre.'</b>
        </td>
    </tr>   
</table>
';

$html .= '
<table cellspacing="0" cellpadding="1" border="1" width="100%">
	<thead>
		<tr align="center" bgcolor="#eee" style="font-weight:bold;">
			<th width = "10%">ID</th>
			<th width = "30%">Sector</th>
			<th width = "20%">Numero Archivo</th>
			<th width = "20%">Numero Castastral</th>
			<th width = "20%">Fecha Inscripcion</th>
		</tr>
	<thead>
	<tbody>';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		
		$numeroarchivo = $row['numeroarchivo'];
		$numerocatastral = $row['numerocatastral'];
		$fechainscripcion = $row['fechainscripcion'];
		$sector_nombre = $row['sector_nombre'];

		$fecha = explode("-", $fechainscripcion);
		$dia = $fecha[2];
		$mes = $fecha[1];
		$anio = $fecha[0];

		$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

		$codigocatastral =   explode("-", $numerocatastral);
		$id ++;
		$total +=1;
$html .= '
	<tr align="center">
		<td width = "10%">'.$id.'</td>
		<td width = "30%">'.$sector_nombre.' </td>
		<td width = "20%">'.$numeroarchivo.' </td>
		<td width = "20%">'.$numerocatastral.'</td>
		<td width = "20%">'.$fecha_c.'</td>
	</tr>';
	}
$html .= '

	<tr align="center" border="0">
		<td  width = "">&nbsp; Total General ('.$total.') de las fichas consultadas segun los criterios de busqueda del reporte</td>
	</tr>
<tbody> 
</table>';

$pdf->writeHTML($encabezado, true, false, false, false, '');
$pdf->writeHTML($html, true, false, false, false, '');




// -----------------------------------------------------------------------------
$pdf->LastPage();

//Close and output PDF document
$pdf->Output('Listado de Ficha Catastral.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+



