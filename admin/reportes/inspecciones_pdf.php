<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if(isset($_GET['id']))
{
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$id =  $_GET['id'];
	$title = "Inspecciones Municipales";
	$inspecciones= pg_query("SELECT
								ins.tipo as tipo,
								ins.fecha as fecha,
								ins.observaciones as observaciones,
								p1.nacionalidad as nacionalidad,
								p1.cedula as cedula,
								p1.nombre1 as nombre1,
								p1.nombre2 as nombre2,
								p1.apellido1 as apellido1,
								p1.apellido2 as apellido2,
								p2.nacionalidad as nacionalidad,
								p2.cedula as cedula,
								p2.nombre1 as nombre1,
								p2.nombre2 as nombre2,
								p2.apellido1 apellido1,
								p2.apellido2 as apellido2
								FROM
								tb_persona_inspeccion as pi
								LEFT JOIN tb_persona as p1 ON p1.id = pi.id_persona
								LEFT JOIN tb_inspecciones as ins ON ins.id = pi.id_inspeccion
								LEFT JOIN tb_persona as p2 ON p2.id = ins.id_inspector
								WHERE ins.id='$id'");
	$reg=pg_fetch_array($inspecciones);
	$tipo = $reg['tipo'];
	$fecha = $reg['fecha'];
	$observaciones = $reg['observaciones'];
	$nPersona1 = $reg['nacionalidad'];
	$cPersona1 = $reg['cedula'];
	$n1Persona1 = $reg['nombre1'];
	$n2Persona1 = $reg['nombre2'];
	$a1Persona1 = $reg['apellido1'];
	$a2Persona1 = $reg['apellido2'];
	$nPersona2 = $reg['nacionalidad'];
	$cPersona2 = $reg['cedula'];
	$n1Persona2 = $reg['nombre1'];
	$n2Persona2 = $reg['nombre2'];
	$a1Persona2 = $reg['apellido1'];
	$a2Persona2 = $reg['apellido2'];
					
}


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);

$pdf->AddPage();

// Set some content to print
$html = '
	<img src="./cintillo.png">
	<h5 align="center">Inspeccion Municipal 0000'.$id.'</h5>
	<br>	
	<p align="justify">Quien suscribe Ing. Félix Aristimuño Coordinador de Catastro de la Alcaldía del Municipio Ribero del Estado Sucre.
</p>
<h5 align="center">HACE CONSTAR</h5>
<br>
	<p align="justify">
	Que el o la ciudadadano(a): <b>'.$n1Persona1.' '.$n2Persona1.' '.$a1Persona1.' '.$a2Persona1.',</b>&nbsp;&nbsp;&nbsp;&nbsp;Titular de la Cedula de identidad Nº <b>'.$nPersona1.'-'.$cPersona1.', SE LE REALIZO UN A INSPECCION DE TIPO: '.$tipo.'.</b>		
		<br>
		<br>
		<b>Nota:</b> la inspeccion aqui documentada se realizo: '.$fecha.'.
		<br>
		<b>Nota:</b> Inspector: '.$nPersona2.'-'.$cPersona2.' '.$n1Persona2.' '.$n2Persona2.' '.$a1Persona2.' '.$a2Persona2.'.
		<br>
		<b>Observaciones:&nbsp;&nbsp;&nbsp;</b>'.$observaciones.'.
		<br>
		<br>
		
		Constancia que se expide a solicitud de parte interesada en Cariaco, a los '.$d.' días del mes de '.$m.' del '.$a.'.
		
		<br>
		<br>
		<div align="center">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<b>Atentamente,

			<br>
		</div>
	</p>
	
	<img src="./pie.png">
';

	
	



// Print text using writeHTMLCell()
$pdf->writeHTML($html, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Certificacion de Linderos.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
