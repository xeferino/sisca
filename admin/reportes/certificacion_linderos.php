<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if(isset($_GET['id']))
{
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$id =  $_GET['id'];
	$title = "Certificacion de Linderos";
	$query=pg_query("SELECT
					tb_certificacion_linderos.id,
					tb_certificacion_linderos.area,
					tb_certificacion_linderos.norte,
					tb_certificacion_linderos.sur,
					tb_certificacion_linderos.este,
					tb_certificacion_linderos.oeste,
					tb_certificacion_linderos.lnorte,
					tb_certificacion_linderos.lsur,
					tb_certificacion_linderos.leste,
					tb_certificacion_linderos.loeste,
					tb_certificacion_linderos.fecha,
					tb_certificacion_linderos.observacion,
					tb_certificacion_linderos.ubicacion,
					tb_certificacion_linderos.id_parroquia,
					tb_persona.id as id_persona,
					tb_persona.nacionalidad,
					tb_persona.cedula,
					tb_persona.nombre1,
					tb_persona.nombre2,
					tb_persona.apellido1,
					tb_persona.apellido2,
					tb_parroquia.nombre
					FROM
					tb_certificacion_linderos as tb_certificacion_linderos
					LEFT JOIN tb_persona as tb_persona on tb_persona.id = tb_certificacion_linderos.id_persona
					LEFT JOIN tb_parroquia as tb_parroquia on tb_parroquia.id = tb_certificacion_linderos.id_parroquia
					WHERE
					tb_certificacion_linderos.id = '$id'");
	$reg=pg_fetch_array($query);
					$id=$reg['id'];
					$nacionalidad=$reg['nacionalidad'];
					$cedula=$reg['cedula'];
					$nombre1=$reg['nombre1'];
					$nombre1=$reg['nombre2'];
					$apellido1=$reg['apellido1'];
					$apellido1=$reg['apellido2'];

					$norte=$reg['norte'];
					$sur=$reg['sur'];
					$este=$reg['este'];
					$oeste=$reg['oeste'];
					$lnorte=$reg['lnorte'];
					$lsur=$reg['lsur'];
					$leste=$reg['leste'];
					$loeste=$reg['loeste'];
					$area=$reg['area'];
					$fecha=$reg['fecha'];
					$ubicacion=$reg['ubicacion'];
					$observacion=$reg['observacion'];
					$parroquia=$reg['nombre'];

					$hectaria = $area/10000;
}


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);

$pdf->AddPage();

// Set some content to print
$html = '
	<img src="./cintillo.png">
	<h3 align="center">A QUIEN PUEDA INTERESAR</h3>
	<p align="justify">Quien suscribe Ing. Félix Aristimuño Coordinador de Catastro de la Alcaldía del Municipio Ribero del Estado Sucre, por medio de la presente CERTIFICA: la existencia de un lote de terreno <b>Municipal</b> &nbsp; ocupado por los Ciudadano (a): <b>'.$nombre1.' '.$nombre2.' '.$apellido1.' '.$apellido2.'</b>, Titular de la Cedula de identidad Nº <b>'.$nacionalidad.'-'.$cedula.', Ubicado; </b> en la comunidad de '.$ubicacion.' de la Parroquia: '.$parroquia.' Municipio Ribero del Estado Sucre, y Alinderado de la manera siguiente: 

		<ul>
			<li><b>NORTE:</b> '.$norte.' ('.$lnorte.' mts.)</li>
			<li><b>SUR:</b> '.$sur.' ('.$lsur.' mts.)</li>
			<li><b>ESTE:</b> '.$este.' ('.$leste.' mts.)</li>
			<li><b>OESTE:</b> '.$oeste.' ('.$loeste.' mts.)</li>
		</ul>

	<b>Nota:</b> El Inmueble ante descrito esta enclavado en un area total de '.$area.' mt2 = '.$hectaria.' hectáreas, '.$observacion.' 
	<br><br>
	<b>Nota:</b> La información suministrada a esta dirección es responsabilidad exclusiva del consejo comunal, el cual certifica mediante acta la veracidad de los datos suministrados, por los ocupantes del terreno. La propiedad está fuera de la Poligonal urbana no está Catastrada.
		<br>
		<br>

		Constancia que se expide a solicitud de parte interesada en Cariaco, a los '.$d.' días del mes de '.$m.' del '.$a.'.
		<br>
		<div align="center">
			<b>Atentamente,
			
			<br>
			<br>
		</div>
	</p>
	
	<img src="./pie.png">
';

	
	



// Print text using writeHTMLCell()
$pdf->writeHTML($html, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Certificacion de Linderos.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
