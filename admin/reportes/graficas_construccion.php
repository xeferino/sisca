<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');
/*if ($_POST) {
*/	
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$fechainicio = $_POST['fechai'];
	$fechafin = $_POST['fechaf'];
	$sector = $_POST['sector'];

	$ptipoc = $_POST['tipo_construccion'];
	$pdesc = $_POST['descripcion_uso'];
	$ptenencia = $_POST['tenencia_construccion'];
	$preg = $_POST['reg_propiedad_const'];
	$psoporte = $_POST['soporte_const'];
	$ptecho = $_POST['techo_const'];
	$pcinterna = $_POST['cubierta_interna'];
	$pcexterna = $_POST['cubierta_externa'];
	$ppared = $_POST['tipo_pared'];
	$pacabado = $_POST['acabado_pared'];
	$ppintura = $_POST['tipo_pintura_pared'];
	$pelect = $_POST['instalacciones_electricas'];
	$ppisos = $_POST['pisos_construccion'];

	$pgraficatorta = $_POST['graficatorta'];
	$pgraficabarra = $_POST['graficabarrahorizontal'];
	$pgraficalinea = $_POST['graficalinea'];

	//var_dump($pgraficabarra);die();


	/*foreach ($_POST as $key => $value) {
		echo  $key.": ".$value."<br>";
	}die();*/

	$title = "Graficas de Construccion";					
	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_construccion AS cc ON cc.id = im.idconstruccion
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");

	$pedul= pg_query("SELECT * FROM  tb_pedul WHERE id='".$sector."'");
														
	$reg=pg_fetch_object($pedul);

	$nombre = $reg->nombre;

	$validar = pg_num_rows($sql);

	##GRAFICA DE BARRA DE TORTA TERRENO
	if($validar>0 and $pgraficatorta =="1"){

		require_once('libchart/libchart/classes/libchart.php');
		$chart = new PieChart(1000, 500);
		$dataSet = new XYDataSet();

		$tipoc = 0;
		$desc = 0;
		$tenencia = 0;
		$reg = 0;
		$soporte = 0;
		$techo = 0;
		$interna = 0;
		$externa = 0;
		$pared = 0;
		$acabado = 0;
		$pintura = 0;
		$pisos = 0;
		$elect = 0;

		while($fila = pg_fetch_array($sql)){
			foreach ($_POST as $key => $value) {
				if($fila['tipo'] == $value){$tipoc++;}
				if($fila['descripcionuso'] == $value){$desc++;}
				if($fila['teneciaconstruccion'] == $value){$tenencia++;}
				if($fila['regimenpropiedad'] == $value){$reg++;}
				if($fila['soporteestructural'] == $value){$soporte++;}
				if($fila['techoestructural'] == $value){$techo++;}
				if($fila['cubiertainterna'] == $value){$interna++;}
				if($fila['cubiertaexterna'] == $value){$externa++;}
				if($fila['paredestipo'] == $value){$pared++;}
				if($fila['paredesacabado'] == $value){$acabado++;}
				if($fila['paredespintura'] == $value){$pintura++;}
				if($fila['paredesinstelectricas'] == $value){$elect++;}
				if($fila['pisotipo'] == $value){$pisos++;}
			}
		}

		$dataSet->addPoint(new Point("Tipo :".$ptipoc." (".$tipoc.")", $tipoc));
		$dataSet->addPoint(new Point("Uso :".$pdesc." (".$desc.")", $desc));
		$dataSet->addPoint(new Point("Tenc. :".$ptenencia." (".$tenencia.")", $tenencia));
		$dataSet->addPoint(new Point("Prop. :".$preg." (".$reg.")", $reg));
		$dataSet->addPoint(new Point("Soporte:".$psoporte." (".$soporte.")", $soporte));
		$dataSet->addPoint(new Point("Techo :".$ptecho." (".$techo.")", $techo));
		$dataSet->addPoint(new Point("C. Int. :".$pinterna." (".$interna.")", $interna));
		$dataSet->addPoint(new Point("C. Ext. :".$pexterna." (".$externa.")", $externa));
		$dataSet->addPoint(new Point("Pared :".$ppared." (".$pared.")", $pared));
		$dataSet->addPoint(new Point("P. Acab. :".$pacabado." (".$acabado.")", $acabado));
		$dataSet->addPoint(new Point("P. Pint.:".$ppintura." (".$pintura.")", $pintura));
		$dataSet->addPoint(new Point("inst. Elect.:".$pelect." (".$elect.")", $elect));
		$dataSet->addPoint(new Point("Piso:".$ppisos." (".$pisos.")", $pisos));

		$chart->setDataSet($dataSet);
		$chart->getPlot()->setGraphPadding(new Padding(5, 30, 100, 160));
		$chart->setTitle("");
		$grafica1 = $chart->render("./libchart/demo/generated/grafica_torta_c.png");
	
}//fin grafica torta

	##GRAFICA DE BARRA HORIZONTAL TERRENO
	if($validar>0 and $pgraficabarra =="2"){


	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_construccion AS cc ON cc.id = im.idconstruccion
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");


	require_once('libchart/libchart/classes/libchart.php');
	$chart2 = new VerticalBarChart();
	$dataSet2 = new XYDataSet();

		$tipoc = 0;
		$desc = 0;
		$tenencia = 0;
		$reg = 0;
		$soporte = 0;
		$techo = 0;
		$interna = 0;
		$externa = 0;
		$pared = 0;
		$acabado = 0;
		$pintura = 0;
		$pisos = 0;
		$elect = 0;

		while($fila = pg_fetch_array($sql)){
			foreach ($_POST as $key => $value) {
				if($fila['tipo'] == $value){$tipoc++;}
				if($fila['descripcionuso'] == $value){$desc++;}
				if($fila['teneciaconstruccion'] == $value){$tenencia++;}
				if($fila['regimenpropiedad'] == $value){$reg++;}
				if($fila['soporteestructural'] == $value){$soporte++;}
				if($fila['techoestructural'] == $value){$techo++;}
				if($fila['cubiertainterna'] == $value){$interna++;}
				if($fila['cubiertaexterna'] == $value){$externa++;}
				if($fila['paredestipo'] == $value){$pared++;}
				if($fila['paredesacabado'] == $value){$acabado++;}
				if($fila['paredespintura'] == $value){$pintura++;}
				if($fila['paredesinstelectricas'] == $value){$elect++;}
				if($fila['pisotipo'] == $value){$pisos++;}
			}
		}

	$dataSet2->addPoint(new Point("Tipo :".$ptipoc."", $tipoc));
	$dataSet2->addPoint(new Point("Uso :".$pdesc."", $desc));
	$dataSet2->addPoint(new Point("Tenc. :".$ptenencia."", $tenencia));
	$dataSet2->addPoint(new Point("Prop. :".$preg."", $reg));
	$dataSet2->addPoint(new Point("Soporte:".$psoporte."", $soporte));
	$dataSet2->addPoint(new Point("Techo :".$ptecho."", $techo));
	$dataSet2->addPoint(new Point("C. Int. :".$pinterna."", $interna));
	$dataSet2->addPoint(new Point("C. Ext. :".$pexterna."", $externa));
	$dataSet2->addPoint(new Point("Pared :".$ppared."", $pared));
	$dataSet2->addPoint(new Point("P. Acab. :".$pacabado."", $acabado));
	$dataSet2->addPoint(new Point("P. Pint.:".$ppintura."", $pintura));
	$dataSet2->addPoint(new Point("inst. Elect.:".$pelect."", $elect));
	$dataSet2->addPoint(new Point("Piso:".$ppisos."", $pisos));	
	$chart2->setDataSet($dataSet2);
	$chart2->getPlot()->setGraphPadding(new Padding(5, 30, 100, 160));
	

	$chart2->setTitle("");
	$grafica2 = $chart2->render("./libchart/demo/generated/grafica_barra_c.png");
}//fin grafica DE BARRA


	##GRAFICA DE linea TERRENO
	if($validar>0 and $pgraficalinea =="3"){


	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_construccion AS cc ON cc.id = im.idconstruccion
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");


	require_once('libchart/libchart/classes/libchart.php');
	$chart3 = new LineChart();
	$dataSet3 = new XYDataSet();

	
		$tipoc = 0;
		$desc = 0;
		$tenencia = 0;
		$reg = 0;
		$soporte = 0;
		$techo = 0;
		$interna = 0;
		$externa = 0;
		$pared = 0;
		$acabado = 0;
		$pintura = 0;
		$pisos = 0;
		$elect = 0;

		while($fila = pg_fetch_array($sql)){
			foreach ($_POST as $key => $value) {
				if($fila['tipo'] == $value){$tipoc++;}
				if($fila['descripcionuso'] == $value){$desc++;}
				if($fila['teneciaconstruccion'] == $value){$tenencia++;}
				if($fila['regimenpropiedad'] == $value){$reg++;}
				if($fila['soporteestructural'] == $value){$soporte++;}
				if($fila['techoestructural'] == $value){$techo++;}
				if($fila['cubiertainterna'] == $value){$interna++;}
				if($fila['cubiertaexterna'] == $value){$externa++;}
				if($fila['paredestipo'] == $value){$pared++;}
				if($fila['paredesacabado'] == $value){$acabado++;}
				if($fila['paredespintura'] == $value){$pintura++;}
				if($fila['paredesinstelectricas'] == $value){$elect++;}
				if($fila['pisotipo'] == $value){$pisos++;}
			}
		}

	$dataSet3->addPoint(new Point("Tipo :".$ptipoc."", $tipoc));
	$dataSet3->addPoint(new Point("Uso :".$pdesc."", $desc));
	$dataSet3->addPoint(new Point("Tenc. :".$ptenencia."", $tenencia));
	$dataSet3->addPoint(new Point("Prop. :".$preg."", $reg));
	$dataSet3->addPoint(new Point("Soporte:".$psoporte."", $soporte));
	$dataSet3->addPoint(new Point("Techo :".$ptecho."", $techo));
	$dataSet3->addPoint(new Point("C. Int. :".$pinterna."", $interna));
	$dataSet3->addPoint(new Point("C. Ext. :".$pexterna."", $externa));
	$dataSet3->addPoint(new Point("Pared :".$ppared."", $pared));
	$dataSet3->addPoint(new Point("P. Acab. :".$pacabado."", $acabado));
	$dataSet3->addPoint(new Point("P. Pint.:".$ppintura."", $pintura));
	$dataSet3->addPoint(new Point("inst. Elect.:".$pelect."", $elect));
	$dataSet3->addPoint(new Point("Piso:".$ppisos."", $pisos));	
	
	$chart3->setDataSet($dataSet3);
	$chart3->getPlot()->setGraphPadding(new Padding(5, 20, 110, 100));
	

	$chart3->setTitle("");
	$grafica3 = $chart3->render("./libchart/demo/generated/grafica_lineas_c.png");

	}//fin grafica DE lineas

/*}*/


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);




$pdf->AddPage();

// Set some content to print
	
if($validar>0){

	###grafica de torta

	if($pgraficatorta ==NULL)
	{
		$html = '';
	}elseif($pgraficatorta =="1"){
		$html = '
			<center>
			<h3>Condiciones de Construccion para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_torta_c.png">
			</center>
		';
	}

	if($pgraficabarra == NULL)
	{
		$html1 = '';
	}elseif($pgraficabarra =="2"){
		$html1 = '
			<center>
			<h3>Condiciones de Construccion para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_barra_c.png">
			</center>
		';
	}

	if($pgraficalinea == NULL)
	{
		$html2 = '';
	}elseif($pgraficalinea =="3"){
		$html2 = '
			<center>
			<h3>Condiciones de Construccion para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_lineas_c.png">
			</center>
		';
	}
}
else{

	$html = '
	<p aling="center">No hay Resultados Obtenidos para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.' para el sector '.$nombre.'</p>
';
}

if($pgraficatorta ==NULL and $pgraficalinea == NULL and $pgraficabarra == NULL){

	$html4 = '
	<p aling="center">No hay Resultados Obtenidos, Debido a que no Seleccionó Ningún tipo de Gráfica a Mostrar... </p>';

}



	



// Print text using writeHTMLCell()
$pdf->writeHTML($html4, true, 0, true, 0);
$pdf->writeHTML($html, true, 0, true, 0);
$pdf->writeHTML($html1, true, 0, true, 0);

	if($pgraficatorta =="1" and $pgraficabarra==2){
		$pdf->AddPage();
	}

$pdf->writeHTML($html2, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('graficas.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
