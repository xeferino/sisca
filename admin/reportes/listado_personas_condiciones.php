<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

	$sector = $_POST['sector'];
	$persona = $_POST['persona'];
	$tipo = $_POST['tipo'];

	//var_dump($sector."-".$persona."-".$tipo);die();

if (isset($_POST['submit']) && $_POST['submit'] == 'pdf') {

	//var_dump($sector."-".$persona."-".$tipo);die();

	if($sector=="todos" && $persona=="todos"){
		
		$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id"
				);
	
	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);
	}elseif($sector!=="todos" && $persona=="todos"){
		
		$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					WHERE uc.sector = ".$sector.""
				);
	
	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);
	}elseif($sector=="todos" && $persona!="todos"){
		//var_dump($persona);die();
		$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					WHERE pp.id = ".$persona.""
				);
	
	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);
	}elseif($sector!="todos" && $persona!="todos"){
		//var_dump($persona);die();
		$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					WHERE pp.id = ".$persona." and uc.sector = ".$sector.""
				);
	
	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);
	}

	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$title = "Estadisticas Generales";
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sisca');
$pdf->SetSubject('PDF');
$pdf->SetTitle($title);
$pdf->SetKeywords('Estadisticas Generales');
$cintillo = "pie.png";

$pdf->SetHeaderData($cintillo, "184", "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'República Bolivariana de Venezuela', '', 0, 'C', true, 0, false, false, 0);

//$pdf->Write(0, 'Ficha Catastral', '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica', '', 8);



// -----------------------------------------------------------------------------
#DATOS GENERALES

if($_POST['persona']=="todos" && $_POST['sector']=="todos"){
$encabezado = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <!--<tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<b> Estadisticas Generales para el Tipo de Condicion: '.$_POST['tipo'].'</b>
        </td>
    </tr> --> 
</table>
';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		$ponacionalidad = $row['ponacionalidad'];
		$pocedula= $row['pocedula'];
		$pon1 = $row['pon1'];
		$pon2 = $row['pon2'];
		$poa1 = $row['poa1'];
		$poa2 = $row['poa2'];
		$sector_nombre= $row['sector_nombre'];

		$topografia = $row['topografia'];
		$acceso = $row['acceso'];
		$forma = $row['forma'];
		$ubicacion = $row['ubicacion'];
		$entornofisico = $row['entornofisico'];
		$mejorasterreno = $row['mejorasterreno'];
		$teneciaterreno = $row['teneciaterreno'];
		$registropropiedad= $row['registropropiedad'];
		$usoactual= $row['usoactual'];

		$tipo= $row['tipo'];
		$descripcionuso= $row['descripcionuso'];
		$teneciaconstruccion= $row['teneciaconstruccion'];
		$regimenpropiedad= $row['regimenpropiedad'];
		$soporteestructural= $row['soporteestructural'];
		$techoestructural= $row['techoestructural'];
		$cubiertainterna= $row['cubiertainterna'];
		$cubiertaexterna= $row['cubiertaexterna'];
		$paredestipo= $row['paredestipo'];
		$paredesacabado= $row['paredesacabado'];
		$paredespintura= $row['paredespintura'];
		$paredesinstelectricas= $row['paredesinstelectricas'];
		$pisotipo= $row['pisotipo'];
		$banowc= $row['banowc'];
		$banocomunitario= $row['banocomunitario'];
		$banoducha= $row['banoducha'];
		$banolavamanos= $row['banolavamanos'];
		$banobanera= $row['banobanera'];
		$banobidet= $row['banobidet'];
		$banoletrina= $row['banoletrina'];
		$banoceramica1= $row['banoceramica1'];
		$banoceramica2= $row['banoceramica2'];
		$puertametalica= $row['puertametalica'];
		$puertamaderacepillada= $row['puertamaderacepillada'];
		$puertamadeconomica= $row['puertamadeconomica'];
		$puertaentamboradafina= $row['puertaentamboradafina'];
		$puertaseguridad= $row['puertaseguridad'];
		$puertaotra= $row['puertaotra'];
		$puertaotrac= $row['puertaotrac'];
		$ventanalaluminio= $row['ventanalaluminio'];
		$ventanalhierro= $row['ventanalhierro'];
		$ventanalmadera= $row['ventanalmadera'];
		$basculantealuminio= $row['basculantealuminio'];
		$basculantehierro= $row['basculantehierro'];
		$basculantemadera= $row['basculantemadera'];
		$satientealuminio= $row['satientealuminio'];
		$satientehierro= $row['satientehierro'];
		$satientemadera= $row['satientemadera'];
		$celosiaaluminio= $row['celosiaaluminio'];
		$celosiahierro= $row['celosiahierro'];
		$celosiamadera= $row['celosiamadera'];
		$correderaaluminio= $row['correderaaluminio'];
		$correderahierro= $row['correderahierro'];
		$correderamadera= $row['correderamadera'];
		$panoramicaaluminio= $row['panoramicaaluminio'];
		$panoramicahierro= $row['panoramicahierro'];
		$panoramicamadera= $row['panoramicamadera'];
		$estadoconservacion= $row['estadoconservacion'];
		$anoconstruccion= $row['anoconstruccion'];
		$porcentajerefaccion= $row['porcentajerefaccion'];
		$numeroniveles= $row['numeroniveles'];
		$anorefaccion= $row['anorefaccion'];
		$edadefectiva= $row['edadefectiva'];
		$numeroedificacion= $row['numeroedificacion'];


	$html .= '
	<table cellspacing="0" cellpadding="1" border="1" width="100%">
		<thead>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Nombre del Propietario: </th>
				<th width = "70%">'.$ponacionalidad.'-'.$pocedula.' '.$pon1.' '.$pn2.' '.$poa1.' '.$poa2.'</th>
			</tr>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Sector: </th>
				<th width = "70%">'.$sector_nombre.'</th>
				
			</tr>
			<tr align="center" bgcolor="#eee"  >
				<th width = "100%" style="font-weight:bold;">Condiciones Generales </th>
			</tr>
			<tr align="center" border="0">
			<td  width = "50%" style="font-weight:bold;">&nbsp; Terreno</td>
			<td  width = "50%" style="font-weight:bold;">&nbsp;Construccion</td>
		</tr>
		<thead>
		<tbody>';
		

	$html .= '
		<tr align="center" border="0">
			<td  width = "50%">
			<ul>
				<li>Topografía: '.$topografia.'</li>
				<li>Acceso: '.$acceso.'</li>
				<li>Forma: '.$forma.'</li>
				<li>Ubicación: '.$ubicacion.'</li>
				<li>Entorno Fisico: '.$entornofisico.'</li>
				<li>Mejoras al Terreno: '.$mejorasterreno.'</li>
				<li>Tenencia Terreno: '.$teneciaterreno.'</li>
				<li>Rég. Propieda: '.$registropropiedad.'</li>
				<li>Uso Actual: '.$usoactual.'</li>
				<li>Servicios Públicos:</li>
				
			</ul>

			</td>
			<td  width = "50%">
			
			 <ul>
				<li>Tipo Casa: '.$tipo.'</li>
				<li>Descripción de Uso: '.$descripcionuso.'</li>
				<li>Tenencia Construcción: '.$teneciaconstruccion.'</li>
				<li>Regimen Propiedad: '.$regimenpropiedad.'</li>
				<li>Soporte: '.$soporteestructural.'</li>
				<li>Techo: '.$techoestructural.'</li>
				<li>Cubierta Externa: '.$cubiertaexterna.'</li>
				<li>Cubierta Ixterna: '.$cubiertainterna.'</li>
				<li>Paredes</li>
				<ul>
					<li>Tipo: '.$paredestipo.'</li>
					<li>Acabado: '.$paredesacabado.'</li>
					<li>Pintura: '.$paredesacabado.'</li>
					<li> Inst. Electricas: '.$paredesinstelectricas.'</li>
				</ul>
				<li>Pisos: '.$pisotipo.'</li>
				<li>Baños (WC: '.$banowc.' - Baño Comunitario: '.$banocomunitario.' -  Ducha: '.$banoducha.' - Lavamanos: '.$banolavamanos.' - Bañera: '.$banobanera.' - Bidet: '.$banobidet.' - Letrina: '.$banoletrina.' - Cerámica 1ra: '.$banoceramica1.' - Cerámica 2ra: '.$banoceramica2.') </li>
				
				<li>Ventanas ( Ventanal: [A:'.$ventanalaluminio.' - M:'.$ventanalmadera.' - H: '.$ventanalhierro.'], Basculante: [A:'.$basculantealuminio.' - M:'.$basculantemadera.' - H: '.$basculantehierro.'], Batiente: [A:'.$satientealuminio.' - M:'.$satientemadera.' - H: '.$satientehierro.'], Celosía: [A:'.$celosiaaluminio.' - M:'.$celosiamadera.' - H: '.$celosiahierro.'], Corredera: [A:'.$correderaaluminio.' - M:'.$correderamadera.' - H: '.$correderahierro.'], Panorámica: [A:'.$panoramicaaluminio.' - M:'.$panoramicamadera.' - H: '.$panoramicahierro.'])</li>
				
				<li>Puertas (Metálica: '.$puertametalica.' - Madera Cepillada: '.$puertamaderacepillada.' -   Mad. Ent. Ecón.: '.$puertamadeconomica.' -  Entamborada Fina: '.$puertaentamboradafina.' -  De Seguridad: '.$puertaseguridad.' - Otra '.$puertaotra.': '.$puertaotrac.') </li>
				
				<li>Estado Conservación: '.$estadoconservacion.'</li>
				<li>Otros Datos</li>
				<ul>
					<li>Año Construcción: '.$anoconstruccion.'</li>
					<li>% Refacción: '.$porcentajerefaccion.'</li>
					<li>N° de Niveles: '.$numeroniveles.'</li>
					<li>Años Refacción: '.$anorefaccion.'</li>
					<li>Edad Efectiva: '.$edadefectiva.'</li>
					<li>N° de Edificaciones: '.$numeroedificacion.'</li>
				</ul>
			</ul>
			</td>
		</tr>
	<tbody> 
	</table> <br><br>';
	}
}elseif( $_POST['sector']!="todos" && $_POST['persona']=="todos"){
$encabezado1 ='
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <!--<tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<b> Estadisticas Generales para el Tipo de Condicion: '.$_POST['tipo'].'</b>
        </td>
    </tr> --> 
</table>
';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		$ponacionalidad = $row['ponacionalidad'];
		$pocedula= $row['pocedula'];
		$pon1 = $row['pon1'];
		$pon2 = $row['pon2'];
		$poa1 = $row['poa1'];
		$poa2 = $row['poa2'];
		$sector_nombre= $row['sector_nombre'];

		$topografia = $row['topografia'];
		$acceso = $row['acceso'];
		$forma = $row['forma'];
		$ubicacion = $row['ubicacion'];
		$entornofisico = $row['entornofisico'];
		$mejorasterreno = $row['mejorasterreno'];
		$teneciaterreno = $row['teneciaterreno'];
		$registropropiedad= $row['registropropiedad'];
		$usoactual= $row['usoactual'];

		$tipo= $row['tipo'];
		$descripcionuso= $row['descripcionuso'];
		$teneciaconstruccion= $row['teneciaconstruccion'];
		$regimenpropiedad= $row['regimenpropiedad'];
		$soporteestructural= $row['soporteestructural'];
		$techoestructural= $row['techoestructural'];
		$cubiertainterna= $row['cubiertainterna'];
		$cubiertaexterna= $row['cubiertaexterna'];
		$paredestipo= $row['paredestipo'];
		$paredesacabado= $row['paredesacabado'];
		$paredespintura= $row['paredespintura'];
		$paredesinstelectricas= $row['paredesinstelectricas'];
		$pisotipo= $row['pisotipo'];
		$banowc= $row['banowc'];
		$banocomunitario= $row['banocomunitario'];
		$banoducha= $row['banoducha'];
		$banolavamanos= $row['banolavamanos'];
		$banobanera= $row['banobanera'];
		$banobidet= $row['banobidet'];
		$banoletrina= $row['banoletrina'];
		$banoceramica1= $row['banoceramica1'];
		$banoceramica2= $row['banoceramica2'];
		$puertametalica= $row['puertametalica'];
		$puertamaderacepillada= $row['puertamaderacepillada'];
		$puertamadeconomica= $row['puertamadeconomica'];
		$puertaentamboradafina= $row['puertaentamboradafina'];
		$puertaseguridad= $row['puertaseguridad'];
		$puertaotra= $row['puertaotra'];
		$puertaotrac= $row['puertaotrac'];
		$ventanalaluminio= $row['ventanalaluminio'];
		$ventanalhierro= $row['ventanalhierro'];
		$ventanalmadera= $row['ventanalmadera'];
		$basculantealuminio= $row['basculantealuminio'];
		$basculantehierro= $row['basculantehierro'];
		$basculantemadera= $row['basculantemadera'];
		$satientealuminio= $row['satientealuminio'];
		$satientehierro= $row['satientehierro'];
		$satientemadera= $row['satientemadera'];
		$celosiaaluminio= $row['celosiaaluminio'];
		$celosiahierro= $row['celosiahierro'];
		$celosiamadera= $row['celosiamadera'];
		$correderaaluminio= $row['correderaaluminio'];
		$correderahierro= $row['correderahierro'];
		$correderamadera= $row['correderamadera'];
		$panoramicaaluminio= $row['panoramicaaluminio'];
		$panoramicahierro= $row['panoramicahierro'];
		$panoramicamadera= $row['panoramicamadera'];
		$estadoconservacion= $row['estadoconservacion'];
		$anoconstruccion= $row['anoconstruccion'];
		$porcentajerefaccion= $row['porcentajerefaccion'];
		$numeroniveles= $row['numeroniveles'];
		$anorefaccion= $row['anorefaccion'];
		$edadefectiva= $row['edadefectiva'];
		$numeroedificacion= $row['numeroedificacion'];


	$html .= '
	<table cellspacing="0" cellpadding="1" border="1" width="100%">
		<thead>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Nombre del Propietario: </th>
				<th width = "70%">'.$ponacionalidad.'-'.$pocedula.' '.$pon1.' '.$pn2.' '.$poa1.' '.$poa2.'</th>
			</tr>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Sector: </th>
				<th width = "70%">'.$sector_nombre.'</th>
				
			</tr>
			<tr align="center" bgcolor="#eee"  >
				<th width = "100%" style="font-weight:bold;">Condiciones Generales </th>
			</tr>
			<tr align="center" border="0">
			<td  width = "50%" style="font-weight:bold;">&nbsp; Terreno</td>
			<td  width = "50%" style="font-weight:bold;">&nbsp;Construccion</td>
		</tr>
		<thead>
		<tbody>';
		

	$html .= '
		<tr align="center" border="0">
			<td  width = "50%">
			<ul>
				<li>Topografía: '.$topografia.'</li>
				<li>Acceso: '.$acceso.'</li>
				<li>Forma: '.$forma.'</li>
				<li>Ubicación: '.$ubicacion.'</li>
				<li>Entorno Fisico: '.$entornofisico.'</li>
				<li>Mejoras al Terreno: '.$mejorasterreno.'</li>
				<li>Tenencia Terreno: '.$teneciaterreno.'</li>
				<li>Rég. Propieda: '.$registropropiedad.'</li>
				<li>Uso Actual: '.$usoactual.'</li>
				<li>Servicios Públicos:</li>
				
			</ul>

			</td>
			<td  width = "50%">
			
			 <ul>
				<li>Tipo Casa: '.$tipo.'</li>
				<li>Descripción de Uso: '.$descripcionuso.'</li>
				<li>Tenencia Construcción: '.$teneciaconstruccion.'</li>
				<li>Regimen Propiedad: '.$regimenpropiedad.'</li>
				<li>Soporte: '.$soporteestructural.'</li>
				<li>Techo: '.$techoestructural.'</li>
				<li>Cubierta Externa: '.$cubiertaexterna.'</li>
				<li>Cubierta Ixterna: '.$cubiertainterna.'</li>
				<li>Paredes</li>
				<ul>
					<li>Tipo: '.$paredestipo.'</li>
					<li>Acabado: '.$paredesacabado.'</li>
					<li>Pintura: '.$paredesacabado.'</li>
					<li> Inst. Electricas: '.$paredesinstelectricas.'</li>
				</ul>
				<li>Pisos: '.$pisotipo.'</li>
				<li>Baños (WC: '.$banowc.' - Baño Comunitario: '.$banocomunitario.' -  Ducha: '.$banoducha.' - Lavamanos: '.$banolavamanos.' - Bañera: '.$banobanera.' - Bidet: '.$banobidet.' - Letrina: '.$banoletrina.' - Cerámica 1ra: '.$banoceramica1.' - Cerámica 2ra: '.$banoceramica2.') </li>
				
				<li>Ventanas ( Ventanal: [A:'.$ventanalaluminio.' - M:'.$ventanalmadera.' - H: '.$ventanalhierro.'], Basculante: [A:'.$basculantealuminio.' - M:'.$basculantemadera.' - H: '.$basculantehierro.'], Batiente: [A:'.$satientealuminio.' - M:'.$satientemadera.' - H: '.$satientehierro.'], Celosía: [A:'.$celosiaaluminio.' - M:'.$celosiamadera.' - H: '.$celosiahierro.'], Corredera: [A:'.$correderaaluminio.' - M:'.$correderamadera.' - H: '.$correderahierro.'], Panorámica: [A:'.$panoramicaaluminio.' - M:'.$panoramicamadera.' - H: '.$panoramicahierro.'])</li>
				
				<li>Puertas (Metálica: '.$puertametalica.' - Madera Cepillada: '.$puertamaderacepillada.' -   Mad. Ent. Ecón.: '.$puertamadeconomica.' -  Entamborada Fina: '.$puertaentamboradafina.' -  De Seguridad: '.$puertaseguridad.' - Otra '.$puertaotra.': '.$puertaotrac.') </li>
				
				<li>Estado Conservación: '.$estadoconservacion.'</li>
				<li>Otros Datos</li>
				<ul>
					<li>Año Construcción: '.$anoconstruccion.'</li>
					<li>% Refacción: '.$porcentajerefaccion.'</li>
					<li>N° de Niveles: '.$numeroniveles.'</li>
					<li>Años Refacción: '.$anorefaccion.'</li>
					<li>Edad Efectiva: '.$edadefectiva.'</li>
					<li>N° de Edificaciones: '.$numeroedificacion.'</li>
				</ul>
			</ul>
			</td>
		</tr>
	<tbody> 
	</table> <br><br>';
	}
}elseif( $_POST['sector']=="todos" && $_POST['persona']!="todos"){
$encabezado1 ='
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <!--<tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<b> Estadisticas Generales para el Tipo de Condicion: '.$_POST['tipo'].'</b>
        </td>
    </tr> --> 
</table>
';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		$ponacionalidad = $row['ponacionalidad'];
		$pocedula= $row['pocedula'];
		$pon1 = $row['pon1'];
		$pon2 = $row['pon2'];
		$poa1 = $row['poa1'];
		$poa2 = $row['poa2'];
		$sector_nombre= $row['sector_nombre'];

		$topografia = $row['topografia'];
		$acceso = $row['acceso'];
		$forma = $row['forma'];
		$ubicacion = $row['ubicacion'];
		$entornofisico = $row['entornofisico'];
		$mejorasterreno = $row['mejorasterreno'];
		$teneciaterreno = $row['teneciaterreno'];
		$registropropiedad= $row['registropropiedad'];
		$usoactual= $row['usoactual'];

		$tipo= $row['tipo'];
		$descripcionuso= $row['descripcionuso'];
		$teneciaconstruccion= $row['teneciaconstruccion'];
		$regimenpropiedad= $row['regimenpropiedad'];
		$soporteestructural= $row['soporteestructural'];
		$techoestructural= $row['techoestructural'];
		$cubiertainterna= $row['cubiertainterna'];
		$cubiertaexterna= $row['cubiertaexterna'];
		$paredestipo= $row['paredestipo'];
		$paredesacabado= $row['paredesacabado'];
		$paredespintura= $row['paredespintura'];
		$paredesinstelectricas= $row['paredesinstelectricas'];
		$pisotipo= $row['pisotipo'];
		$banowc= $row['banowc'];
		$banocomunitario= $row['banocomunitario'];
		$banoducha= $row['banoducha'];
		$banolavamanos= $row['banolavamanos'];
		$banobanera= $row['banobanera'];
		$banobidet= $row['banobidet'];
		$banoletrina= $row['banoletrina'];
		$banoceramica1= $row['banoceramica1'];
		$banoceramica2= $row['banoceramica2'];
		$puertametalica= $row['puertametalica'];
		$puertamaderacepillada= $row['puertamaderacepillada'];
		$puertamadeconomica= $row['puertamadeconomica'];
		$puertaentamboradafina= $row['puertaentamboradafina'];
		$puertaseguridad= $row['puertaseguridad'];
		$puertaotra= $row['puertaotra'];
		$puertaotrac= $row['puertaotrac'];
		$ventanalaluminio= $row['ventanalaluminio'];
		$ventanalhierro= $row['ventanalhierro'];
		$ventanalmadera= $row['ventanalmadera'];
		$basculantealuminio= $row['basculantealuminio'];
		$basculantehierro= $row['basculantehierro'];
		$basculantemadera= $row['basculantemadera'];
		$satientealuminio= $row['satientealuminio'];
		$satientehierro= $row['satientehierro'];
		$satientemadera= $row['satientemadera'];
		$celosiaaluminio= $row['celosiaaluminio'];
		$celosiahierro= $row['celosiahierro'];
		$celosiamadera= $row['celosiamadera'];
		$correderaaluminio= $row['correderaaluminio'];
		$correderahierro= $row['correderahierro'];
		$correderamadera= $row['correderamadera'];
		$panoramicaaluminio= $row['panoramicaaluminio'];
		$panoramicahierro= $row['panoramicahierro'];
		$panoramicamadera= $row['panoramicamadera'];
		$estadoconservacion= $row['estadoconservacion'];
		$anoconstruccion= $row['anoconstruccion'];
		$porcentajerefaccion= $row['porcentajerefaccion'];
		$numeroniveles= $row['numeroniveles'];
		$anorefaccion= $row['anorefaccion'];
		$edadefectiva= $row['edadefectiva'];
		$numeroedificacion= $row['numeroedificacion'];


	$html .= '
	<table cellspacing="0" cellpadding="1" border="1" width="100%">
		<thead>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Nombre del Propietario: </th>
				<th width = "70%">'.$ponacionalidad.'-'.$pocedula.' '.$pon1.' '.$pn2.' '.$poa1.' '.$poa2.'</th>
			</tr>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Sector: </th>
				<th width = "70%">'.$sector_nombre.'</th>
				
			</tr>
			<tr align="center" bgcolor="#eee"  >
				<th width = "100%" style="font-weight:bold;">Condiciones Generales </th>
			</tr>
			<tr align="center" border="0">
			<td  width = "50%" style="font-weight:bold;">&nbsp; Terreno</td>
			<td  width = "50%" style="font-weight:bold;">&nbsp;Construccion</td>
		</tr>
		<thead>
		<tbody>';
		

	$html .= '
		<tr align="center" border="0">
			<td  width = "50%">
			<ul>
				<li>Topografía: '.$topografia.'</li>
				<li>Acceso: '.$acceso.'</li>
				<li>Forma: '.$forma.'</li>
				<li>Ubicación: '.$ubicacion.'</li>
				<li>Entorno Fisico: '.$entornofisico.'</li>
				<li>Mejoras al Terreno: '.$mejorasterreno.'</li>
				<li>Tenencia Terreno: '.$teneciaterreno.'</li>
				<li>Rég. Propieda: '.$registropropiedad.'</li>
				<li>Uso Actual: '.$usoactual.'</li>
				<li>Servicios Públicos:</li>
				
			</ul>

			</td>
			<td  width = "50%">
			
			 <ul>
				<li>Tipo Casa: '.$tipo.'</li>
				<li>Descripción de Uso: '.$descripcionuso.'</li>
				<li>Tenencia Construcción: '.$teneciaconstruccion.'</li>
				<li>Regimen Propiedad: '.$regimenpropiedad.'</li>
				<li>Soporte: '.$soporteestructural.'</li>
				<li>Techo: '.$techoestructural.'</li>
				<li>Cubierta Externa: '.$cubiertaexterna.'</li>
				<li>Cubierta Ixterna: '.$cubiertainterna.'</li>
				<li>Paredes</li>
				<ul>
					<li>Tipo: '.$paredestipo.'</li>
					<li>Acabado: '.$paredesacabado.'</li>
					<li>Pintura: '.$paredesacabado.'</li>
					<li> Inst. Electricas: '.$paredesinstelectricas.'</li>
				</ul>
				<li>Pisos: '.$pisotipo.'</li>
				<li>Baños (WC: '.$banowc.' - Baño Comunitario: '.$banocomunitario.' -  Ducha: '.$banoducha.' - Lavamanos: '.$banolavamanos.' - Bañera: '.$banobanera.' - Bidet: '.$banobidet.' - Letrina: '.$banoletrina.' - Cerámica 1ra: '.$banoceramica1.' - Cerámica 2ra: '.$banoceramica2.') </li>
				
				<li>Ventanas ( Ventanal: [A:'.$ventanalaluminio.' - M:'.$ventanalmadera.' - H: '.$ventanalhierro.'], Basculante: [A:'.$basculantealuminio.' - M:'.$basculantemadera.' - H: '.$basculantehierro.'], Batiente: [A:'.$satientealuminio.' - M:'.$satientemadera.' - H: '.$satientehierro.'], Celosía: [A:'.$celosiaaluminio.' - M:'.$celosiamadera.' - H: '.$celosiahierro.'], Corredera: [A:'.$correderaaluminio.' - M:'.$correderamadera.' - H: '.$correderahierro.'], Panorámica: [A:'.$panoramicaaluminio.' - M:'.$panoramicamadera.' - H: '.$panoramicahierro.'])</li>
				
				<li>Puertas (Metálica: '.$puertametalica.' - Madera Cepillada: '.$puertamaderacepillada.' -   Mad. Ent. Ecón.: '.$puertamadeconomica.' -  Entamborada Fina: '.$puertaentamboradafina.' -  De Seguridad: '.$puertaseguridad.' - Otra '.$puertaotra.': '.$puertaotrac.') </li>
				
				<li>Estado Conservación: '.$estadoconservacion.'</li>
				<li>Otros Datos</li>
				<ul>
					<li>Año Construcción: '.$anoconstruccion.'</li>
					<li>% Refacción: '.$porcentajerefaccion.'</li>
					<li>N° de Niveles: '.$numeroniveles.'</li>
					<li>Años Refacción: '.$anorefaccion.'</li>
					<li>Edad Efectiva: '.$edadefectiva.'</li>
					<li>N° de Edificaciones: '.$numeroedificacion.'</li>
				</ul>
			</ul>
			</td>
		</tr>
	<tbody> 
	</table> <br><br>';
	}
}elseif( $_POST['sector']!="todos" && $_POST['persona']!="todos"){
$encabezado1 ='
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <!--<tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<b> Estadisticas Generales para el Tipo de Condicion: '.$_POST['tipo'].'</b>
        </td>
    </tr> --> 
</table>
';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		$ponacionalidad = $row['ponacionalidad'];
		$pocedula= $row['pocedula'];
		$pon1 = $row['pon1'];
		$pon2 = $row['pon2'];
		$poa1 = $row['poa1'];
		$poa2 = $row['poa2'];
		$sector_nombre= $row['sector_nombre'];

		$topografia = $row['topografia'];
		$acceso = $row['acceso'];
		$forma = $row['forma'];
		$ubicacion = $row['ubicacion'];
		$entornofisico = $row['entornofisico'];
		$mejorasterreno = $row['mejorasterreno'];
		$teneciaterreno = $row['teneciaterreno'];
		$registropropiedad= $row['registropropiedad'];
		$usoactual= $row['usoactual'];

		$tipo= $row['tipo'];
		$descripcionuso= $row['descripcionuso'];
		$teneciaconstruccion= $row['teneciaconstruccion'];
		$regimenpropiedad= $row['regimenpropiedad'];
		$soporteestructural= $row['soporteestructural'];
		$techoestructural= $row['techoestructural'];
		$cubiertainterna= $row['cubiertainterna'];
		$cubiertaexterna= $row['cubiertaexterna'];
		$paredestipo= $row['paredestipo'];
		$paredesacabado= $row['paredesacabado'];
		$paredespintura= $row['paredespintura'];
		$paredesinstelectricas= $row['paredesinstelectricas'];
		$pisotipo= $row['pisotipo'];
		$banowc= $row['banowc'];
		$banocomunitario= $row['banocomunitario'];
		$banoducha= $row['banoducha'];
		$banolavamanos= $row['banolavamanos'];
		$banobanera= $row['banobanera'];
		$banobidet= $row['banobidet'];
		$banoletrina= $row['banoletrina'];
		$banoceramica1= $row['banoceramica1'];
		$banoceramica2= $row['banoceramica2'];
		$puertametalica= $row['puertametalica'];
		$puertamaderacepillada= $row['puertamaderacepillada'];
		$puertamadeconomica= $row['puertamadeconomica'];
		$puertaentamboradafina= $row['puertaentamboradafina'];
		$puertaseguridad= $row['puertaseguridad'];
		$puertaotra= $row['puertaotra'];
		$puertaotrac= $row['puertaotrac'];
		$ventanalaluminio= $row['ventanalaluminio'];
		$ventanalhierro= $row['ventanalhierro'];
		$ventanalmadera= $row['ventanalmadera'];
		$basculantealuminio= $row['basculantealuminio'];
		$basculantehierro= $row['basculantehierro'];
		$basculantemadera= $row['basculantemadera'];
		$satientealuminio= $row['satientealuminio'];
		$satientehierro= $row['satientehierro'];
		$satientemadera= $row['satientemadera'];
		$celosiaaluminio= $row['celosiaaluminio'];
		$celosiahierro= $row['celosiahierro'];
		$celosiamadera= $row['celosiamadera'];
		$correderaaluminio= $row['correderaaluminio'];
		$correderahierro= $row['correderahierro'];
		$correderamadera= $row['correderamadera'];
		$panoramicaaluminio= $row['panoramicaaluminio'];
		$panoramicahierro= $row['panoramicahierro'];
		$panoramicamadera= $row['panoramicamadera'];
		$estadoconservacion= $row['estadoconservacion'];
		$anoconstruccion= $row['anoconstruccion'];
		$porcentajerefaccion= $row['porcentajerefaccion'];
		$numeroniveles= $row['numeroniveles'];
		$anorefaccion= $row['anorefaccion'];
		$edadefectiva= $row['edadefectiva'];
		$numeroedificacion= $row['numeroedificacion'];


	$html .= '
	<table cellspacing="0" cellpadding="1" border="1" width="100%">
		<thead>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Nombre del Propietario: </th>
				<th width = "70%">'.$ponacionalidad.'-'.$pocedula.' '.$pon1.' '.$pn2.' '.$poa1.' '.$poa2.'</th>
			</tr>
			<tr align="center" bgcolor="#eee" style="font-weight:bold;">
				<th width = "30%">Sector: </th>
				<th width = "70%">'.$sector_nombre.'</th>
				
			</tr>
			<tr align="center" bgcolor="#eee"  >
				<th width = "100%" style="font-weight:bold;">Condiciones Generales </th>
			</tr>
			<tr align="center" border="0">
			<td  width = "50%" style="font-weight:bold;">&nbsp; Terreno</td>
			<td  width = "50%" style="font-weight:bold;">&nbsp;Construccion</td>
		</tr>
		<thead>
		<tbody>';
		

	$html .= '
		<tr align="center" border="0">
			<td  width = "50%">
			<ul>
				<li>Topografía: '.$topografia.'</li>
				<li>Acceso: '.$acceso.'</li>
				<li>Forma: '.$forma.'</li>
				<li>Ubicación: '.$ubicacion.'</li>
				<li>Entorno Fisico: '.$entornofisico.'</li>
				<li>Mejoras al Terreno: '.$mejorasterreno.'</li>
				<li>Tenencia Terreno: '.$teneciaterreno.'</li>
				<li>Rég. Propieda: '.$registropropiedad.'</li>
				<li>Uso Actual: '.$usoactual.'</li>
				<li>Servicios Públicos:</li>
				
			</ul>

			</td>
			<td  width = "50%">
			
			 <ul>
				<li>Tipo Casa: '.$tipo.'</li>
				<li>Descripción de Uso: '.$descripcionuso.'</li>
				<li>Tenencia Construcción: '.$teneciaconstruccion.'</li>
				<li>Regimen Propiedad: '.$regimenpropiedad.'</li>
				<li>Soporte: '.$soporteestructural.'</li>
				<li>Techo: '.$techoestructural.'</li>
				<li>Cubierta Externa: '.$cubiertaexterna.'</li>
				<li>Cubierta Ixterna: '.$cubiertainterna.'</li>
				<li>Paredes</li>
				<ul>
					<li>Tipo: '.$paredestipo.'</li>
					<li>Acabado: '.$paredesacabado.'</li>
					<li>Pintura: '.$paredesacabado.'</li>
					<li> Inst. Electricas: '.$paredesinstelectricas.'</li>
				</ul>
				<li>Pisos: '.$pisotipo.'</li>
				<li>Baños (WC: '.$banowc.' - Baño Comunitario: '.$banocomunitario.' -  Ducha: '.$banoducha.' - Lavamanos: '.$banolavamanos.' - Bañera: '.$banobanera.' - Bidet: '.$banobidet.' - Letrina: '.$banoletrina.' - Cerámica 1ra: '.$banoceramica1.' - Cerámica 2ra: '.$banoceramica2.') </li>
				
				<li>Ventanas ( Ventanal: [A:'.$ventanalaluminio.' - M:'.$ventanalmadera.' - H: '.$ventanalhierro.'], Basculante: [A:'.$basculantealuminio.' - M:'.$basculantemadera.' - H: '.$basculantehierro.'], Batiente: [A:'.$satientealuminio.' - M:'.$satientemadera.' - H: '.$satientehierro.'], Celosía: [A:'.$celosiaaluminio.' - M:'.$celosiamadera.' - H: '.$celosiahierro.'], Corredera: [A:'.$correderaaluminio.' - M:'.$correderamadera.' - H: '.$correderahierro.'], Panorámica: [A:'.$panoramicaaluminio.' - M:'.$panoramicamadera.' - H: '.$panoramicahierro.'])</li>
				
				<li>Puertas (Metálica: '.$puertametalica.' - Madera Cepillada: '.$puertamaderacepillada.' -   Mad. Ent. Ecón.: '.$puertamadeconomica.' -  Entamborada Fina: '.$puertaentamboradafina.' -  De Seguridad: '.$puertaseguridad.' - Otra '.$puertaotra.': '.$puertaotrac.') </li>
				
				<li>Estado Conservación: '.$estadoconservacion.'</li>
				<li>Otros Datos</li>
				<ul>
					<li>Año Construcción: '.$anoconstruccion.'</li>
					<li>% Refacción: '.$porcentajerefaccion.'</li>
					<li>N° de Niveles: '.$numeroniveles.'</li>
					<li>Años Refacción: '.$anorefaccion.'</li>
					<li>Edad Efectiva: '.$edadefectiva.'</li>
					<li>N° de Edificaciones: '.$numeroedificacion.'</li>
				</ul>
			</ul>
			</td>
		</tr>
	<tbody> 
	</table> <br><br>';
	}
}


//$pdf->writeHTML($encabezado1, false, false, false, false, '');
$pdf->writeHTML($html, false, false, false, false, '');




// -----------------------------------------------------------------------------
$pdf->LastPage();

//Close and output PDF document
$pdf->Output('Estadisticas Generales.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+



