<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if(isset($_GET['id']))
{
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$id =  $_GET['id'];
	$title = "Ficha Catastral";
	$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					where
					fc. id = '$id'"
				);
	$row=pg_fetch_array($query);
	$numeroarchivo = $row['numeroarchivo'];
	$numerocatastral = $row['numerocatastral'];
	$fechainscripcion = $row['fechainscripcion'];
	$fechalevantamiento = $row['fechalevantamiento'];
	$fechavisita = $row['fechavisita'];
	$fichaobservacion = $row['fichaobservacion'];
	$idterreno = $row['idterreno'];
	$topografia = $row['topografia'];
	$acceso = $row['acceso'];
	$forma = $row['forma'];
	$ubicacion = $row['ubicacion'];
	$entornofisico = $row['entornofisico'];
	$mejorasterreno = $row['mejorasterreno'];
	$teneciaterreno = $row['teneciaterreno'];
	$registropropiedad= $row['registropropiedad'];
	$usoactual= $row['usoactual'];
	$linderonorte= $row['linderonorte'];
	$linderosur= $row['linderosur'];
	$linderoeste= $row['linderoeste'];
	$linderooeste= $row['linderooeste'];
	$coordenadasutmnorte= $row['coordenadasutmnorte'];
	$coordenadasutmeste= $row['coordenadasutmeste'];
	$coordenadasutmhuso= $row['coordenadasutmhuso'];
	$areametroscuadrados= $row['areametroscuadrados'];
	$valorunitariometroscuadrados= $row['valorunitariometroscuadrados'];
	$factorajustearea= $row['factorajustearea'];
	$factorajusteforma= $row['factorajusteforma'];
	$sector= $row['sector'];
	$numero_comuna = $row['numero_comuna'];
	$numero_comite_urbano = $row['numero_comite_urbano'];
	$numero_consejo_comunal = $row['numero_consejo_comunal'];
	$tipo_calle = $row['tipo_calle'];
	$nombre_calle = $row['nombre_calle'];
	$sector_nombre= $row['sector_nombre'];
	$valorajustadometroscuadrados= $row['valorajustadometroscuadrados'];
	$valortotal= $row['valortotal'];
	$observacionterreno = $row['observacionterreno'];
	$tipo= $row['tipo'];
	$descripcionuso= $row['descripcionuso'];
	$teneciaconstruccion= $row['teneciaconstruccion'];
	$regimenpropiedad= $row['regimenpropiedad'];
	$soporteestructural= $row['soporteestructural'];
	$techoestructural= $row['techoestructural'];
	$cubiertainterna= $row['cubiertainterna'];
	$cubiertaexterna= $row['cubiertaexterna'];
	$paredestipo= $row['paredestipo'];
	$paredesacabado= $row['paredesacabado'];
	$paredespintura= $row['paredespintura'];
	$paredesinstelectricas= $row['paredesinstelectricas'];
	$pisotipo= $row['pisotipo'];
	$banowc= $row['banowc'];
	$banocomunitario= $row['banocomunitario'];
	$banoducha= $row['banoducha'];
	$banolavamanos= $row['banolavamanos'];
	$banobanera= $row['banobanera'];
	$banobidet= $row['banobidet'];
	$banoletrina= $row['banoletrina'];
	$banoceramica1= $row['banoceramica1'];
	$banoceramica2= $row['banoceramica2'];
	$puertametalica= $row['puertametalica'];
	$puertamaderacepillada= $row['puertamaderacepillada'];
	$puertamadeconomica= $row['puertamadeconomica'];
	$puertaentamboradafina= $row['puertaentamboradafina'];
	$puertaseguridad= $row['puertaseguridad'];
	$puertaotra= $row['puertaotra'];
	$puertaotrac= $row['puertaotrac'];
	$ventanalaluminio= $row['ventanalaluminio'];
	$ventanalhierro= $row['ventanalhierro'];
	$ventanalmadera= $row['ventanalmadera'];
	$basculantealuminio= $row['basculantealuminio'];
	$basculantehierro= $row['basculantehierro'];
	$basculantemadera= $row['basculantemadera'];
	$satientealuminio= $row['satientealuminio'];
	$satientehierro= $row['satientehierro'];
	$satientemadera= $row['satientemadera'];
	$celosiaaluminio= $row['celosiaaluminio'];
	$celosiahierro= $row['celosiahierro'];
	$celosiamadera= $row['celosiamadera'];
	$correderaaluminio= $row['correderaaluminio'];
	$correderahierro= $row['correderahierro'];
	$correderamadera= $row['correderamadera'];
	$panoramicaaluminio= $row['panoramicaaluminio'];
	$panoramicahierro= $row['panoramicahierro'];
	$panoramicamadera= $row['panoramicamadera'];
	$estadoconservacion= $row['estadoconservacion'];
	$anoconstruccion= $row['anoconstruccion'];
	$porcentajerefaccion= $row['porcentajerefaccion'];
	$numeroniveles= $row['numeroniveles'];
	$anorefaccion= $row['anorefaccion'];
	$edadefectiva= $row['edadefectiva'];
	$numeroedificacion= $row['numeroedificacion'];
	$observacionconstruccion = $row['observacionconstruccion'];
	$tipologia= $row['tipologia'];
	$areametrocuadrado= $row['areametrocuadrado'];
	$valorareacuadrada= $row['valorareacuadrada'];
	$areatotal= $row['areatotal'];
	$porcentajedepreciacion= $row['porcentajedepreciacion'];
	$valoractual= $row['valoractual'];
	$valortotalconstruccion = $row['valortotalconstruccion'];
	$valorinmueble= $row['valorinmueble'];
	$valorconstruccion= $row['valorconstruccion'];
	$obsecoconstruccion = $row['obsecoconstruccion'];
	$numero= $row['numero'];
	$folio= $row['folio'];
	$tomo= $row['tomo'];
	$protocolo= $row['protocolo'];
	$fecharp= $row['fecharp'];
	$aream2terreno= $row['aream2terreno'];
	$aream2cont= $row['aream2cont'];
	$valorbs= $row['valorbs'];
	$numero_rmv= $row['numero_rmv'];
	$region_consejofg= $row['region_consejofg'];
	$comuna= $row['comuna'];
	$comite_tierrau= $row['comite_tierrau'];
	$consejo_comunal= $row['consejo_comunal'];
	$ciudad= $row['ciudad'];
	$localidad= $row['localidad'];
	$urbanizacion= $row['urbanizacion'];
	$consejo_residencial= $row['consejo_residencial'];
	$barrio= $row['barrio'];
	$numero_civico= $row['numero_civico'];
	$puntoreferencia= $row['puntoreferencia'];
	$manzana= $row['manzana'];
	$parroquia= $row['parroquia'];
	$nombre_parroquia= $row['nombre_parroquia'];
	$sector= $row['sector'];
	$ppnacionalidad = $row['ppnacionalidad'];
	$ppcedula = $row['ppcedula'];
	$ppn1 = $row['ppn1'];
	$ppn2 = $row['ppn2'];
	$ppa1 = $row['ppa1'];
	$ppa2= $row['ppa2'];
	$pptelefono1 = $row['telefono1pp'];
	$pptelefono2 = $row['telefono2pp'];
	$ppcorreo = $row['emailpp'];
	$ponacionalidad = $row['ponacionalidad'];
	$pocedula= $row['pocedula'];
	$pon1 = $row['pon1'];
	$pon2 = $row['pon2'];
	$poa1 = $row['poa1'];
	$poa2 = $row['poa2'];
	$potelefono1 = $row['telefono1po'];
	$potelefono2 = $row['telefono2po'];
	$pocorreo = $row['emailpo'];
	$penacionalidad= $row['penacionalidad'];
	$pecedula = $row['pecedula'];
	$pen1 = $row['pen1'];
	$pen2 = $row['pen2'];
	$pea1 = $row['pea1'];
	$pea2 = $row['pea2'];
	$prnacionalidad= $row['prnacionalidad'];
	$prcedula = $row['prcedula'];
	$prn1 = $row['prn1'];
	$prn2 = $row['prn2'];
	$pra1 = $row['pra1'];
	$pra2 = $row['pra2'];
	$dppciudad = $row['dppciudad'];
	$dpplocalidad = $row['dpplocalidad'];
	$dppurbanizacion = $row['dppurbanizacion'];
	$dppconsejo_residencial = $row['dppconsejo_residencial'];
	$dppbarrio = $row['dppbarrio'];
	$dppnumero_civico = $row['dppnumero_civico'];
	$dpppuntoreferencia = $row['dpppuntoreferencia'];
	$dppparroquia = $row['dppparroquia'];
	$dppsector = $row['dppsector'];
	$dpptipocalle = $row['dpptipocalle'];
	$dppnombrecalle = $row['dppnombrecalle'];

	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);
	
	/*$codigocatastral[0];
	$codigocatastral[1];
	$codigocatastral[2];
	$codigocatastral[3];*/

}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
/*$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle($title);
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');*/
$pdf->SetTitle($title);
$cintillo = "pie.png";

$pdf->SetHeaderData($cintillo, "184", "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'República Bolivariana de Venezuela', '', 0, 'C', true, 0, false, false, 0);

//$pdf->Write(0, 'Ficha Catastral', '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica', '', 8);



// -----------------------------------------------------------------------------
#DATOS GENERALES
$pt = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 50px;">
        	<b> Ficha Catastral </b>
        </td>
    </tr>   
</table>
';

$pdf->writeHTML($pt, true, false, false, false, '');

$dg = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td colspan="3"  align="center" valign="middle" bgcolor="#666666" color="#fff" >A.- Datos Generales</td>
    </tr>
    <tr>
    	<td rowspan="2">&nbsp;Control Archivo: <br />
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$numeroarchivo.' </td>
    	<td>&nbsp;Ficha Castastral &nbsp;N&deg;: <br /> &nbsp;'.$numerocatastral.'</td>
    	<td>&nbsp;Inscripción Catastral N&deg;:<br />
    		&nbsp;Fecha: '.$fecha_c.'
    	</td>
    </tr>
    
</table>
';

$pdf->writeHTML($dg, true, false, false, false, '');


// -----------------------------------------------------------------------------
#CODIGO ANTERIOR
$ca = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td colspan="3"  align="center" valign="middle" bgcolor="#666666" color="#fff" >B.- Código Anterior</td>
    </tr>
    <tr>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63">&nbsp;Sec</td>
    	<td width="63">&nbsp;Man </td>
    	<td width="63">&nbsp;Par </td>
    	<td width="63">&nbsp;Sbp</td>
    	<td width="63">&nbsp;Niv</td>
    	<td width="70">&nbsp;Und</td>
    </tr>
     <tr>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="63"></td>
    	<td width="70"></td>
    </tr>
    
</table>
';

$pdf->writeHTML($ca, true, false, false, false, '');


// -----------------------------------------------------------------------------
#CODIGO CATASTRAL
$cc = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td colspan="3"  align="center" valign="middle" bgcolor="#666666" color="#fff" >C.- Código Catastral</td>
    </tr>
    <tr>
    	<td width="63">&nbsp;Efed</td>
    	<td width="63">&nbsp;Mun</td>
    	<td width="63">&nbsp;Prr</td>
    	<td width="63">&nbsp;Ámb</td>
    	<td width="63">&nbsp;Sec</td>
    	<td width="63">&nbsp;Man </td>
    	<td width="63">&nbsp;Par </td>
    	<td width="63">&nbsp;Sbp</td>
    	<td width="63">&nbsp;Niv</td>
    	<td width="70">&nbsp;Und</td>
    </tr>
     <tr>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;U -'.$codigocatastral[0].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[1].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[2].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[3].'</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="70">&nbsp;</td>
    </tr>
    
</table>
';

$pdf->writeHTML($cc, true, false, false, false, '');

// -----------------------------------------------------------------------------

#DATOS DE UBICACION COMUNITARIA
$duc = '
<table border="1" cellpadding="1" nobr="true">
<tr>
	<td colspan="5"  align="center" valign="middle" bgcolor="#666666" color="#fff" >D.- Datos de Ubicación Comunitaria</td>
</tr>
 <tr>
	 <td width="198">&nbsp;Región Consejo Federal de Gobierno</td>
	 <td width="190" align="center">Comuna</td>
	 <td width="30" align="center">N&deg;</td>
	 <td width="190" align="center">Consejo Comunal</td>
	 <td width="30" align="center"> N&deg;</td>
 </tr>
 <tr>
	 <td width="198" rowspan="3">&nbsp;'.$region_consejofg.'</td>
	 <td width="190" align="center" rowspan="">'.$comuna.'</td>
	 <td width="30" align="center">'.$numero_comuna.'</td>
	 <td width="190" align="center">'.$consejo_comunal.'</td>
	 <td width="30" align="center">'.$numero_consejo_comunal.'</td>
 </tr>
 <tr>
	 <td width="190" align="center" rowspan="">Comité de Tierra Urbano</td>
	 <td width="30" align="center">N&deg;</td>
	 <td colspan="2" align="center">N&deg; Registro Misión Vivienda</td>
  </tr>
 <tr>
	 <td width="190" align="center" rowspan="">'.$comite_tierrau.'</td>
	 <td width="30" align="center">'.$numero_comite_urbano.'</td>
	 <td colspan="2" align="center">'.$numero_rmv.'</td>
  </tr>
</table>
';

$pdf->writeHTML($duc, true, false, false, false, '');

if($tipo_calle == "Av"){
	$Av ="x"; 
}
elseif ($tipo_calle == "Clle") {
	$Clle ="x"; 
}
elseif ($tipo_calle == "Crr") {
	$Crr ="x"; 
}
elseif ($tipo_calle == "Trav") {
	$Trav ="x"; 
}
elseif ($tipo_calle == "Prol") {
	$Prol ="x"; 
}
elseif ($tipo_calle == "Crrt") {
	$Crrt ="x"; 
}
elseif ($tipo_calle == "Cjn") {
	$Cjn ="x"; 
}
elseif ($tipo_calle == "Psje") {
	$Psje ="x"; 
}
elseif ($tipo_calle == "Blv") {
	$Blv ="x"; 
}
elseif ($tipo_calle == "Vda") {
	$Vda ="x"; 
}
elseif ($tipo_calle == "Esc") {
	$Esc ="x"; 
}
elseif ($tipo_calle == "Snd") {
	$Snd ="x"; 
}
elseif ($tipo_calle == "Tcal") {
	$Tcal ="x"; 
}
elseif ($tipo_calle == "Cno") {
	$Cno ="x"; 
}
#DATOS DE DIRECCION DEL INMUEBLE
$di = '
<table border="1" cellpadding="1" nobr="true">
<tr>
	<td colspan="28"  align="center" valign="middle" bgcolor="#666666" color="#fff" >E.- Dirección del Inmueble</td>
</tr>
 <tr>
	 <td colspan="4"  align="center">&nbsp;Parroquia</td>
	 <td colspan="4"  align="center">&nbsp;Ciudad</td>
	 <td colspan="4"  align="center">&nbsp;Localidad</td>
	 <td colspan="4"  align="center">&nbsp;Urbanización</td>
	 <td colspan="4"  align="center">&nbsp; Conj. Resid.</td>
	 <td colspan="4"  align="center">&nbsp; Barrio</td>
	 <td colspan="4"  align="center">&nbsp; Sector</td>
 </tr>
 <tr>
	 <td colspan="4"  align="center">&nbsp;'.$nombre_parroquia.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$ciudad.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$localidad.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$urbanizacion.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$consejo_residencial.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$barrio.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$sector_nombre.'</td>
 </tr>

 <tr>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Av.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Clle.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Crr.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Trav.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Prol.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Crrt.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Cjn.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Psje.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Blv.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Vda.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Esc.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Snd.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;'.$Tcal.'</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;'.$Cno.'</td>

 </tr>
 <tr>
	 <td  align="justify" colspan="28">&nbsp;'.$nombre_calle.'</td>
	 
 </tr>
</table>
';

$pdf->writeHTML($di, true, false, false, false, '');

$di1 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
   <td  align="center" style="font-size: 20px;">&nbsp;Entre</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
 
 <tr>
   <td  align="center" style="font-size: 25px;">&nbsp;Y</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($di1, true, false, false, false, '');

$di2 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="center" style="font-size: 25px;" >&nbsp;Edif</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Apto</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Qta</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Casa</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Rancho</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;C.C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Local C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Ofic</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 23px;">&nbsp;Galpón</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Otro</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

 </tr>
 <tr>
	 <td  align="center" colspan="20">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($di2, true, false, false, false, '');

$di3 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="justify" style="font-size: 25px;" >&nbsp;Nombre: </td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; N&deg; Cívico: '.$numero_civico.'</td>
 </tr>
 <tr>
	 <td  align="justify" style="font-size: 25px;" whidth="120" >&nbsp;Telefono: </td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; Punto de Referencia: '.$puntoreferencia.'</td>
 </tr>
</table>
';

$pdf->writeHTML($di3, true, false, false, false, '');

#DATOS DEL OCUPANTE

if($ponacionalidad =="J")
{
	$j="x";
}
else
{
	$v="x";
}
$do = '
<table border="1" cellpadding="1" nobr="true">
<tr>
	<td colspan="7"  align="center" valign="middle" bgcolor="#666666" color="#fff" >F.- Datos del Ocupante</td>
</tr>
 <tr>
	 <td colspan="5" align="justyfi">&nbsp;Nombres y Apellidos/Razón Social:
	 '.$pon1.' '.$pon2.' '.$poa1.' '.$poa2.'</td>
	 <td align="justyfi">&nbsp;Pers. Nat. ['.$v.']</td>
	 <td align="justyfi">&nbsp;Pers. Jurid. ['.$j.']</td>
 </tr>
 <tr>
	 <td colspan="4" align="justyfi" width="159">&nbsp;C&eacute;dula/Rif: 
	 '.$ponacionalidad.'-'.$pocedula.'</td>
	 <td align="justyfi" width="159">&nbsp;Ciudad/Localidad: <br>&nbsp;'.$ciudad.'/'.$localidad.'</td>
	 <td align="justyfi" width="159">&nbsp;Urbanizacion: '.$urbanizacion.'</td>
	 <td align="justyfi" width="160">&nbsp;Sector: <br>&nbsp;'.$sector_nombre.'</td>
 </tr>
 <tr>
   <td colspan="4" align="justyfi">&nbsp;Barrio: '.$barrio.'</td>
   <td colspan="3" align="justyfi">&nbsp;Direcci&oacute;n: '.$nombre_calle.'</td>
  </tr>
   <tr>
	 <td colspan="4" align="justyfi">&nbsp;Tel&eacute;fono: 
	 '.$potelefono1.'</td>
	 <td align="justyfi">&nbsp;Tel&eacute;fono Celular : '.$potelefono2.'</td>
	 <td align="justyfi">&nbsp;Correo Electronico : <br> &nbsp;'.$pocorreo.'</td>
	 <td align="justyfi">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($do, true, false, false, false, '');

$sql_valida= pg_query("SELECT * FROM  tb_inmueble 
	WHERE idfichacatastral='$id'");

$valida=pg_fetch_object($sql_valida);

if($valida->idocupante != $valida->idpropietario)
{


if($dpptipocalle == "Av"){
	$Avp ="x"; 
}
elseif ($dpptipocalle == "Clle") {
	$Cllep ="x"; 
}
elseif ($dpptipocalle == "Crr") {
	$Crrp ="x"; 
}
elseif ($dpptipocalle == "Trav") {
	$Travp ="x"; 
}
elseif ($dpptipocalle == "Prol") {
	$Prolp ="x"; 
}
elseif ($dpptipocalle == "Crrt") {
	$Crrtp ="x"; 
}
elseif ($dpptipocalle == "Cjn") {
	$Cjnp="x"; 
}
elseif ($dpptipocalle == "Psje") {
	$Psjep ="x"; 
}
elseif ($dpptipocalle == "Blv") {
	$Blvp ="x"; 
}
elseif ($dpptipocalle == "Vda") {
	$Vdap ="x"; 
}
elseif ($dpptipocalle == "Esc") {
	$Escp ="x"; 
}
elseif ($dpptipocalle == "Snd") {
	$Sndp ="x"; 
}
elseif ($dpptipocalle == "Tcal") {
	$Tcalp ="x"; 
}
elseif ($dpptipocalle == "Cno") {
	$Cnop ="x"; 
}

if($ppnacionalidad =="J")
{
	$jp="x";
}
else
{
	$vp="x";
}
#DATOS DEL PROPIETARIO
$dp = '
<table border="1" cellpadding="1" nobr="true">
<tr>
	<td colspan="7"  align="center" valign="middle" bgcolor="#666666" color="#fff" >G.- Datos del Propietario/Representante Legal/Administrador</td>
</tr>
 <tr>
	 <td colspan="5" align="justyfi">&nbsp;Nombres y Apellidos/Razón Social: '.$ppn1.' '.$ppn2.' '.$ppa1.' '.$ppa2.'</td>
	 <td align="justyfi">&nbsp;Pers. Nat. ['.$vp.']</td>
	 <td align="justyfi">&nbsp;Pers. Jurid. ['.$jp.']</td>
 </tr>
 <tr>
	 <td colspan="4" align="justyfi" width="159">&nbsp;Propietario</td>
	 <td align="justyfi" width="159">&nbsp;Rep. Legal</td>
	 <td align="justyfi" width="159">&nbsp;Administrador: </td>
	 <td align="justyfi" width="160">&nbsp;C&eacute;dula/Rif: '.$ppnacionalidad.'-'.$ppcedula.'</td>
 </tr>
</table>
';

$pdf->writeHTML($dp, true, false, false, false, '');

$dp1 = '
<table border="1" cellpadding="1" nobr="true">
 <tr>
	 <td colspan="4"  align="center">&nbsp;Parroquia</td>
	 <td colspan="4"  align="center">&nbsp;Ciudad</td>
	 <td colspan="4"  align="center">&nbsp;Localidad</td>
	 <td colspan="4"  align="center">&nbsp;Urbanización</td>
	 <td colspan="4"  align="center">&nbsp; Conj. Resid.</td>
	 <td colspan="4"  align="center">&nbsp; Barrio</td>
	 <td colspan="4"  align="center">&nbsp; Sector</td>
 </tr>
 <tr>
	 <td colspan="4"  align="center">&nbsp;'.$dppparroquia.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$dppciudad.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$dpplocalidad.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$dppurbanizacion.'</td>
	 <td colspan="4"  align="center">&nbsp; '.$dppconsejo_residencial.'</td>
	 <td colspan="4"  align="center">&nbsp; '.$dppbarrio.'</td>
	 <td colspan="4"  align="center">&nbsp;'.$dppsector.'</td>
 </tr>

  <tr>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Avp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Cllep.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Crrp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Travp.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Prolp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Crrtp.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Cjnp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Psjep.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Blvp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Vdap.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Escp.'</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;'.$Sndp.'</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;'.$Tcalp.'</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;'.$Cnop.'</td>

 </tr>
 <tr>
	 <td  align="justify" colspan="28">&nbsp;'.$dppnombrecalle.'</td>
	 
 </tr>
</table>
';

$pdf->writeHTML($dp1, true, false, false, false, '');

$dp2 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
   <td  align="center" style="font-size: 20px;">&nbsp;Entre</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
 
 <tr>
   <td  align="center" style="font-size: 25px;">&nbsp;Y</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dp2, true, false, false, false, '');

$dp3 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="center" style="font-size: 25px;" >&nbsp;Edif</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Apto</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Qta</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Casa</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Rancho</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;C.C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Local C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Ofic</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 23px;">&nbsp;Galpón</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Otro</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

 </tr>
 <tr>
	 <td  align="center" colspan="20">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dp3, true, false, false, false, '');

$dp4 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="justify" style="font-size: 25px;" >&nbsp;Nombre: </td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; N&deg; Cívico:'.$dppnumero_civico.'</td>
 </tr>
 <tr>
	 <td  align="justify" style="font-size: 25px;" whidth="120" >&nbsp;Telefono: '.$pptelefono1.'</td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; Punto de Referencia: '.$dpppuntoreferencia.'</td>
 </tr>
</table>
';

$pdf->writeHTML($dp4, true, false, false, false, '');

}else{

#DATOS DEL PROPIETARIO
$dp = '
<table border="1" cellpadding="1" nobr="true">
<tr>
	<td colspan="7"  align="center" valign="middle" bgcolor="#666666" color="#fff" >G.- Datos del Propietario/Representante Legal/Administrador</td>
</tr>
 <tr>
	 <td colspan="5" align="justyfi">&nbsp;Nombres y Apellidos/Razón Social: </td>
	 <td align="justyfi">&nbsp;Pers. Nat. []</td>
	 <td align="justyfi">&nbsp;Pers. Jurid. []</td>
 </tr>
 <tr>
	 <td colspan="4" align="justyfi" width="159">&nbsp;Propietario</td>
	 <td align="justyfi" width="159">&nbsp;Rep. Legal</td>
	 <td align="justyfi" width="159">&nbsp;Administrador: </td>
	 <td align="justyfi" width="160">&nbsp;C&eacute;dula/Rif: </td>
 </tr>
</table>
';

$pdf->writeHTML($dp, true, false, false, false, '');

$dp1 = '
<table border="1" cellpadding="1" nobr="true">
 <tr>
	 <td colspan="4"  align="center">&nbsp;Parroquia</td>
	 <td colspan="4"  align="center">&nbsp;Ciudad</td>
	 <td colspan="4"  align="center">&nbsp;Localidad</td>
	 <td colspan="4"  align="center">&nbsp;Urbanización</td>
	 <td colspan="4"  align="center">&nbsp; Conj. Resid.</td>
	 <td colspan="4"  align="center">&nbsp; Barrio</td>
	 <td colspan="4"  align="center">&nbsp; Sector</td>
 </tr>
 <tr>
	 <td colspan="4"  align="center">&nbsp;</td>
	 <td colspan="4"  align="center">&nbsp;</td>
	 <td colspan="4"  align="center">&nbsp;</td>
	 <td colspan="4"  align="center">&nbsp;</td>
	 <td colspan="4"  align="center">&nbsp; </td>
	 <td colspan="4"  align="center">&nbsp; </td>
	 <td colspan="4"  align="center">&nbsp;</td>
 </tr>

  <tr>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>

 </tr>
 <tr>
	 <td  align="justify" colspan="28">&nbsp;</td>
	 
 </tr>
</table>
';

$pdf->writeHTML($dp1, true, false, false, false, '');

$dp2 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
   <td  align="center" style="font-size: 20px;">&nbsp;Entre</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
 
 <tr>
   <td  align="center" style="font-size: 25px;">&nbsp;Y</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Av</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Clle</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Crr</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Trav</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Prol</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Crrt</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Cjn</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Psje</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Blv</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Vda</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Esc</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Snd</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Tcal</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
     <td  align="center" style="font-size: 25px;">&nbsp;Cno</td>
     <td  align="center" style="font-size: 25px;">&nbsp;</td>
 </tr>
 <tr>
	 <td  align="center" colspan="29">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dp2, true, false, false, false, '');

$dp3 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="center" style="font-size: 25px;" >&nbsp;Edif</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Apto</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 25px;">&nbsp;Qta</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 22px;">&nbsp;Casa</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Rancho</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;C.C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 20px;">&nbsp;Local C.</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Ofic</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

	 <td  align="center" style="font-size: 23px;">&nbsp;Galpón</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;Otro</td>
	 <td  align="center" style="font-size: 25px;">&nbsp;</td>

 </tr>
 <tr>
	 <td  align="center" colspan="20">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dp3, true, false, false, false, '');

$dp4 = '
<table border="1" cellpadding="1" nobr="true">

 <tr>
	 <td  align="justify" style="font-size: 25px;" >&nbsp;Nombre: </td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; N&deg; Cívico:</td>
 </tr>
 <tr>
	 <td  align="justify" style="font-size: 25px;" whidth="120" >&nbsp;Telefono: </td>
	 <td  align="justify" style="font-size: 25px;">&nbsp; Punto de Referencia: </td>
 </tr>
</table>
';

$pdf->writeHTML($dp4, true, false, false, false, '');

}

#DATOS DEL TERRENO
#topografia
if($topografia == "Plano"){
	$pl ="x"; 
}
elseif ($topografia == "Sobre Nivel") {
	$sn ="x"; 
}
elseif ($topografia == "Bajo Nivel") {
	$bn ="x"; 
}
elseif ($topografia == "Corte") {
	$ctt ="x"; 
}
elseif ($topografia == "Relleno") {
	$re ="x"; 
}
elseif ($topografia == "Inclinado/Pend. Suave") {
	$ps ="x"; 
}
elseif ($topografia == "Inclinado/Pend. Media") {
	$pm ="x"; 
}
elseif ($topografia == "Inclinado/Pend. Fuerte") {
	$pf ="x"; 
}
elseif ($topografia == "Irregular") {
	$ir ="x"; 
}

#acceso
if($acceso == "Calle Pavimentada"){
	$cp ="x"; 
}
elseif ($acceso == "Calle Engranzonada") {
	$ce ="x"; 
}
elseif ($acceso == "Calle de Tierra") {
	$ct ="x"; 
}
elseif ($acceso == "Escalera Pavimento") {
	$ep ="x"; 
}
elseif ($acceso == "Escalera de Tierra") {
	$et ="x"; 
}
elseif ($acceso == "Paso de Servidumbre") {
	$pas ="x"; 
}
elseif ($acceso == "Vereda") {
	$ve ="x"; 
}
elseif ($acceso == "Sendero") {
	$se ="x"; 
}
elseif ($acceso == "Otro") {
	$ot ="x"; 
}

#forma
if($forma == "Regular"){
	$reg ="x"; 
}
elseif ($forma == "Irregular") {
	$irr ="x"; 
}
elseif ($forma == "Muy Irregular") {
	$mi ="x"; 
}

#forma
if($ubicacion == "Convencional"){
	$con ="x"; 
}
elseif ($ubicacion == "Esquina") {
	$esq ="x"; 
}
elseif ($ubicacion == "Interior de Manzana") {
	$im ="x"; 
}
elseif ($ubicacion == "Oculta") {
	$oco ="x"; 
}
#entorno fisico
if($entornofisico == "Zona Urbanizada"){
	$zu ="x"; 
}
elseif ($entornofisico == "Zona no Urbanizada") {
	$znu ="x"; 
}
elseif ($entornofisico == "Rio/Quebrada") {
	$rq ="x"; 
}
elseif ($entornofisico == "Barranco/Talud") {
	$btd ="x"; 
}
elseif ($entornofisico == "Otro") {
	$otr ="x"; 
}

#mejoras al terreno
if($mejorasterreno == "Muro de Contencion"){
	$mc ="x"; 
}
elseif ($mejorasterreno == "Nivelacion") {
	$mn ="x"; 
}
elseif ($mejorasterreno == "Cercado") {
	$cerd ="x"; 
}
elseif ($mejorasterreno == "Pozo Septico") {
	$pz ="x"; 
}
elseif ($mejorasterreno == "Lagunas Artificiales") {
	$la ="x"; 
}
elseif ($mejorasterreno == "Otro") {
	$motr ="x"; 
}

#tenencia terreno
if($teneciaterreno == "Arrendamiento"){
	$tar ="x"; 
}
elseif ($teneciaterreno == "Propiedad") {
	$tp ="x"; 
}
elseif ($teneciaterreno == "Comodato") {
	$tco ="x"; 
}
elseif ($teneciaterreno == "Anticresis") {
	$tat ="x"; 
}
elseif ($teneciaterreno == "Enfiteusis") {
	$tet ="x"; 
}
elseif ($teneciaterreno == "Usufructo") {
	$tus ="x"; 
}
elseif ($teneciaterreno == "Derechho de Hab.") {
	$tdh ="x"; 
}
elseif ($teneciaterreno == "Concesion de Uso") {
	$tcu ="x"; 
}
elseif ($teneciaterreno == "Admin. de Estado N/E/M") {
	$tae ="x"; 
}
elseif ($teneciaterreno == "Posesion con Tit./Sin Tit.") {
	$ttp ="x"; 
}
elseif ($teneciaterreno == "Posesion Certificada TU") {
	$tpt ="x"; 
}
elseif ($teneciaterreno == "Ocupacion") {
	$toc ="x"; 
}
elseif ($teneciaterreno == "Otro") {
	$tot ="x"; 
}

#reg. propiedad terreno
if($registropropiedad == "Ejido"){
	$rpe ="x"; 
}
elseif ($registropropiedad == "Municipal Propio") {
	$rmp ="x"; 
}
elseif ($registropropiedad == "Nacional") {
	$rn ="x"; 
}
elseif ($registropropiedad == "Baldio") {
	$rb ="x"; 
}
elseif ($registropropiedad == "Estadal") {
	$rel ="x"; 
}
elseif ($registropropiedad == "Priv. Individual") {
	$rpi ="x"; 
}
elseif ($registropropiedad == "Priv. Condominio") {
	$rpco ="x"; 
}
elseif ($registropropiedad == "Priv. Posesion") {
	$rpp ="x"; 
}
elseif ($registropropiedad == "Familiar") {
	$rf ="x"; 
}
elseif ($registropropiedad == "Colectiva") {
	$rco ="x"; 
}
elseif ($registropropiedad == "Multifamiliar") {
	$rpmf ="x"; 
}
elseif ($registropropiedad == "Comunal") {
	$rpcc ="x"; 
}

elseif ($registropropiedad == "Otro") {
	$rpo ="x"; 
}

#Uso Actual
if($usoactual == "Residencial"){
	$usre ="x"; 
}
elseif ($usoactual == "Comercial") {
	$usco ="x"; 
}
elseif ($usoactual == "Industrial") {
	$usin ="x"; 
}
elseif ($usoactual == "Recreativo") {
	$usrec ="x"; 
}
elseif ($usoactual == "Deportivo") {
	$usde ="x"; 
}
elseif ($usoactual == "Asistencial/salud") {
	$usas ="x"; 
}
elseif ($usoactual == "Educacional") {
	$used ="x"; 
}
elseif ($usoactual == "Turistico") {
	$ustu ="x"; 
}
elseif ($usoactual == "Social/Cultural") {
	$uscs ="x"; 
}
elseif ($usoactual == "Religioso") {
	$usreg ="x"; 
}
elseif ($usoactual == "Alimentacion") {
	$usali ="x"; 
}
elseif ($usoactual == "Servicio Comunal") {
	$ussco ="x"; 
}

elseif ($usoactual == "Gubernamental/institucional") {
	$usgi ="x"; 
}
elseif ($usoactual == "Pesquero") {
	$uspe ="x"; 
}
elseif ($usoactual == "Agroindustrial") {
	$usag ="x"; 
}
elseif ($usoactual == "Agroforestal") {
	$usagf ="x"; 
}
elseif ($usoactual == "Agricola") {
	$usagr ="x"; 
}
elseif ($usoactual == "Pecuario") {
	$uspec ="x"; 
}
elseif ($usoactual == "Forestal") {
	$usfor ="x"; 
}

elseif ($usoactual == "Minero") {
	$usmin ="x"; 
}
elseif ($usoactual == "Sin Uso") {
	$usosin ="x"; 
}
elseif ($usoactual == "Otro") {
	$usuot ="x"; 
}


#Uso Actual
if($usoactual == "Residencial"){
	$usre ="x"; 
}
elseif ($usoactual == "Comercial") {
	$usco ="x"; 
}
elseif ($usoactual == "Industrial") {
	$usin ="x"; 
}
elseif ($usoactual == "Recreativo") {
	$usrec ="x"; 
}
elseif ($usoactual == "Deportivo") {
	$usde ="x"; 
}
elseif ($usoactual == "Asistencial/salud") {
	$usas ="x"; 
}
elseif ($usoactual == "Educacional") {
	$used ="x"; 
}
elseif ($usoactual == "Turistico") {
	$ustu ="x"; 
}
elseif ($usoactual == "Social/Cultural") {
	$uscs ="x"; 
}
elseif ($usoactual == "Religioso") {
	$usreg ="x"; 
}
elseif ($usoactual == "Alimentacion") {
	$usali ="x"; 
}
elseif ($usoactual == "Servicio Comunal") {
	$ussco ="x"; 
}

elseif ($usoactual == "Gubernamental/institucional") {
	$usgi ="x"; 
}
elseif ($usoactual == "Pesquero") {
	$uspe ="x"; 
}
elseif ($usoactual == "Agroindustrial") {
	$usag ="x"; 
}
elseif ($usoactual == "Agroforestal") {
	$usagf ="x"; 
}
elseif ($usoactual == "Agricola") {
	$usagr ="x"; 
}
elseif ($usoactual == "Pecuario") {
	$uspec ="x"; 
}
elseif ($usoactual == "Forestal") {
	$usfor ="x"; 
}

elseif ($usoactual == "Minero") {
	$usmin ="x"; 
}
elseif ($usoactual == "Sin Uso") {
	$usosin ="x"; 
}
elseif ($usoactual == "Otro") {
	$usuot ="x"; 
}
#datos de los servicios

$sql ="SELECT terr.*, serv.* from tb_terreno as terr
JOIN tb_terrenoservicios as tserv on tserv.idterreno = terr.id
JOIN tb_servicio as serv on serv.id = tserv.idservicios
WHERE terr.id = '$idterreno'";
$query = pg_query($sql);


while($servicio = pg_fetch_array($query)){
	if($servicio['tipo'] == "Acueducto"){
		$servac = "x";
	}elseif ($servicio['tipo'] == "Cloacas") {
		$servcl ="x"; 
	}elseif ($servicio['tipo'] == "Drenaje Artificial") {
		$servda ="x"; 
	}elseif ($servicio['tipo'] == "Alumbrado Publico") {
		$servap ="x"; 
	}elseif ($servicio['tipo'] == "Electricidad Residencial") {
		$server ="x"; 
	}elseif ($servicio['tipo'] == "Electricidad Industrial") {
		$servei ="x"; 
	}elseif ($servicio['tipo'] == "Vialidad") {
		$servv ="x"; 
	}elseif ($servicio['tipo'] == "Pavimento") {
		$servpa ="x"; 
	}elseif ($servicio['tipo'] == "Acera") {
		$servacr ="x"; 
	}elseif ($servicio['tipo'] == "Transp. Pub. Terrestre") {
		$servtpt ="x"; 
	}elseif ($servicio['tipo'] == "Transp. Pub. Subterr.") {
		$servtps ="x"; 
	}elseif ($servicio['tipo'] == "Transp. Pub. Aereo") {
		$servtpa ="x"; 
	}elseif ($servicio['tipo'] == "Telefono") {
		$servte ="x"; 
	}elseif ($servicio['tipo'] == "Cobertura Celular") {
		$servcc ="x"; 
	}elseif ($servicio['tipo'] == "TV Satelital") {
		$servtvs ="x"; 
	}elseif ($servicio['tipo'] == "Cable TV") {
		$servctv ="x"; 
	}elseif ($servicio['tipo'] == "Aseo Urbano") {
		$servau ="x"; 
	}elseif ($servicio['tipo'] == "Barrido de Calle") {
		$servbc ="x"; 
	}elseif ($servicio['tipo'] == "Correo y Telegrafo") {
		$servctl ="x"; 
	}elseif ($servicio['tipo'] == "Escuela") {
		$serves ="x"; 
	}elseif ($servicio['tipo'] == "Medicatura") {
		$servme ="x"; 
	}elseif ($servicio['tipo'] == "Gas Directo") {
		$servgas ="x"; 
	}elseif ($servicio['tipo'] == "Gas Bombona") {
		$servgasb ="x"; 
	}elseif ($servicio['tipo'] == "Riego") {
		$servrg ="x"; 
	}

}
$dt = '
<table border="1" cellpadding="1" nobr="true" >
<tr>
	<td colspan="15"  align="center" valign="middle" bgcolor="#666666" color="#fff" >H.- Datos del Terreno</td>
</tr>
 <tr>
	 <td colspan="2"  align="center" width="126" >&nbsp;<b>1.- Topografía</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>2.- Acceso</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>3.- Forma</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>4.- Ubicación</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>5.- Entorno Fisico</b></td>	 
 </tr>
 <tr>
	 <td  align="justyfi" width="114">&nbsp;Plano</td>
	 <td  align="center" width="12">'.$pl.'</td>

	 <td  align="justyfi" width="116">&nbsp;Calle Pavimentada</td>
	 <td  align="center" width="12">'.$cp.'</td>

	 <td  align="justyfi" width="116">&nbsp;Regular</td>
	 <td  align="center" width="12">&nbsp;'.$reg.'</td>

	 <td  align="justyfi" width="116">&nbsp;Convencional</td>
	 <td  align="center" width="12">&nbsp;'.$con.'</td>

	 <td  align="justyfi" width="116">&nbsp;Zona Urbanizada</td>
	 <td  align="center" width="12">&nbsp;'.$zu.'</td>
 </tr>

  <tr>
	 <td  align="justyfi" width="114">&nbsp;Sobre Nivel</td>
	 <td  align="center" width="12">&nbsp;'.$sn.'</td>

	 <td  align="justyfi" width="116">&nbsp;Calle Engranzonada</td>
	 <td  align="center" width="12">&nbsp;'.$ce.'</td>

	 <td  align="justyfi" width="116">&nbsp;Irregular</td>
	 <td  align="center" width="12">&nbsp;'.$irr.'</td>

	 <td  align="justyfi" width="116">&nbsp;Esquina</td>
	 <td  align="center" width="12">&nbsp;'.$esq.'</td>

	 <td  align="justyfi" width="116">&nbsp;Zona no Urbanizada</td>
	 <td  align="center" width="12">&nbsp;'.$znu.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Bajo Nivel</td>
	 <td  align="center" width="12">&nbsp;'.$bn.'</td>

	 <td  align="justyfi" width="116">&nbsp;Calle de Tierra</td>
	 <td  align="center" width="12">&nbsp;'.$ct.'</td>

	 <td  align="justyfi" width="116">&nbsp;Muy Irregular</td>
	 <td  align="center" width="12">&nbsp;'.$mi.'</td>

	 <td  align="justyfi" width="116">&nbsp;Interior de Manzana</td>
	 <td  align="center" width="12">&nbsp;'.$im.'</td>

	 <td  align="justyfi" width="116">&nbsp;Río/Quebrada</td>
	 <td  align="center" width="12">&nbsp;'.$rq.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Corte</td>
	 <td  align="center" width="12">&nbsp;'.$ctt.'</td>

	 <td  align="justyfi" width="116">&nbsp;Escalera Pavimento</td>
	 <td  align="center" width="12">&nbsp;'.$ep.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Oculta</td>
	 <td  align="center" width="12">&nbsp;'.$oco.'</td>

	 <td  align="justyfi" width="116">&nbsp;Barranco/Talud</td>
	 <td  align="center" width="12">&nbsp;'.$btd.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Relleno</td>
	 <td  align="center" width="12">&nbsp;'.$re.'</td>

	 <td  align="justyfi" width="116">&nbsp;Escalera de Tierra</td>
	 <td  align="center" width="12">&nbsp;'.$et.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$otr.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Inclinado/Pend. Suave</td>
	 <td  align="center" width="12">&nbsp;'.$ps.'</td>

	 <td  align="justyfi" width="116">&nbsp;Paso de Servidumbre</td>
	 <td  align="center" width="12">&nbsp;'.$pas.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Inclinado/Pend. Media</td>
	 <td  align="center" width="12">&nbsp;'.$pm.'</td>

	 <td  align="justyfi" width="116">&nbsp;Vereda</td>
	 <td  align="center" width="12">&nbsp;'.$ve.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Inclinado/Pend. Fuerte</td>
	 <td  align="center" width="12">&nbsp;'.$pf.'</td>

	 <td  align="justyfi" width="116">&nbsp;Sendero</td>
	 <td  align="center" width="12">&nbsp;'.$se.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Irregular</td>
	 <td  align="center" width="12">&nbsp;'.$ir.'</td>

	 <td  align="justyfi" width="116">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$ot.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td colspan="2"  align="center" width="126" >&nbsp;<b>6.- Mejoras al Terreno</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>7.- Tenencia Terreno</b></td>
	 <td colspan="2"  align="center" width="128">&nbsp;<b>8.- Rég. Propiedad</b></td>
	 <td colspan="4"  align="center">&nbsp;<b>9.- Uso Actual</b></td>
  </tr>
 
 <tr>
	 <td  align="justyfi" width="114">&nbsp;Muro de Contención</td>
	 <td  align="center" width="12">&nbsp;'.$mc.'</td>

	 <td  align="justyfi" width="116">&nbsp;Propiedad</td>
	 <td  align="center" width="12">&nbsp;'.$tp.'</td>

	 <td  align="justyfi" width="116">&nbsp;Ejido</td>
	 <td  align="center" width="12">&nbsp;'.$rpe.'</td>

	 <td  align="justyfi" width="116">&nbsp;Residencial</td>
	 <td  align="center" width="12">&nbsp;'.$usre.'</td>

	 <td  align="justyfi" width="116">&nbsp;Pesquero</td>
	 <td  align="center" width="12">&nbsp;'.$usoe.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Nivelación</td>
	 <td  align="center" width="12">&nbsp;'.$mn.'</td>

	 <td  align="justyfi" width="116">&nbsp;Arrendamiento</td>
	 <td  align="center" width="12">&nbsp;'.$tar.'</td>

	 <td  align="justyfi" width="116">&nbsp;Municipal Propio</td>
	 <td  align="center" width="12">&nbsp;'.$rmp.'</td>

	 <td  align="justyfi" width="116">&nbsp;Comercial</td>
	 <td  align="center" width="12">&nbsp;'.$usco.'</td>

	 <td  align="justyfi" width="116">&nbsp;Agroindustrial</td>
	 <td  align="center" width="12">&nbsp;'.$usag.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Cercado</td>
	 <td  align="center" width="12">&nbsp;'.$cerd.'</td>

	 <td  align="justyfi" width="116">&nbsp;Comodato</td>
	 <td  align="center" width="12">&nbsp;'.$tco.'</td>

	 <td  align="justyfi" width="116">&nbsp;Nacional</td>
	 <td  align="center" width="12">&nbsp;'.$rn.'</td>

	 <td  align="justyfi" width="116">&nbsp;Industrial</td>
	 <td  align="center" width="12">&nbsp;'.$usin.'</td>

	 <td  align="justyfi" width="116">&nbsp;Agroforestal</td>
	 <td  align="center" width="12">&nbsp;'.$usagf.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Pozo Séptico</td>
	 <td  align="center" width="12">&nbsp;'.$pz.'</td>

	 <td  align="justyfi" width="116">&nbsp;Anticresis</td>
	 <td  align="center" width="12">&nbsp;'.$tat.'</td>

	 <td  align="justyfi" width="116">&nbsp;Baldío</td>
	 <td  align="center" width="12">&nbsp;'.$rb.'</td>

	 <td  align="justyfi" width="116">&nbsp;Recreativo</td>
	 <td  align="center" width="12">&nbsp;'.$usrec.'</td>

	 <td  align="justyfi" width="116">&nbsp;Agricola</td>
	 <td  align="center" width="12">&nbsp;'.$usagr.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Lagunas Artificialaes</td>
	 <td  align="center" width="12">&nbsp;'.$la.'</td>

	 <td  align="justyfi" width="116">&nbsp;Enfiteusis</td>
	 <td  align="center" width="12">&nbsp;'.$tet.'</td>

	 <td  align="justyfi" width="116">&nbsp;Estatal</td>
	 <td  align="center" width="12">&nbsp;'.$rel.'</td>

	 <td  align="justyfi" width="116">&nbsp;Deportivo</td>
	 <td  align="center" width="12">&nbsp;'.$usde.'</td>

	 <td  align="justyfi" width="116">&nbsp;Pecuario</td>
	 <td  align="center" width="12">&nbsp;'.$uspec.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$motr.'</td>

	 <td  align="justyfi" width="116">&nbsp;Usufructo</td>
	 <td  align="center" width="12">&nbsp;'.$tus.'</td>

	 <td  align="justyfi" width="116">&nbsp;Priv. Individual</td>
	 <td  align="center" width="12">&nbsp;'.$rpl.'</td>

	 <td  align="justyfi" width="116">&nbsp;Asistencial/Salud</td>
	 <td  align="justyfi" width="12">&nbsp;'.$usas.'</td>

	 <td  align="justyfi" width="116">&nbsp;Forestal</td>
	 <td  align="center" width="12">&nbsp;'.$usfor.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Derecho de Hab.</td>
	 <td  align="center" width="12">&nbsp;'.$tdh.'</td>

	 <td  align="justyfi" width="116">&nbsp;Priv. Condominio</td>
	 <td  align="center" width="12">&nbsp;'.$rpco.'</td>

	 <td  align="justyfi" width="116">&nbsp;Educacional</td>
	 <td  align="center" width="12">&nbsp;'.$used.'</td>

	 <td  align="justyfi" width="116">&nbsp;Minero</td>
	 <td  align="center" width="12">&nbsp;'.$usmin.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Concesión de Uso</td>
	 <td  align="center" width="12">&nbsp;'.$tcu.'</td>

	 <td  align="justyfi" width="116">&nbsp;Priv. Posesión</td>
	 <td  align="center" width="12">&nbsp;'.$rpp.'</td>

	 <td  align="justyfi" width="116">&nbsp;Turístico</td>
	 <td  align="center" width="12">&nbsp;'.$ustu.'</td>

	 <td  align="justyfi" width="116">&nbsp;Sin Uso</td>
	 <td  align="center" width="12">&nbsp;'.$usosin.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Adm. de Estado N/E/M</td>
	 <td  align="center" width="12">&nbsp;'.$tae.'</td>

	 <td  align="justyfi" width="116">&nbsp;Familiar</td>
	 <td  align="center" width="12">&nbsp;'.$rf.'</td>

	 <td  align="justyfi" width="116">&nbsp;Social/Cultural</td>
	 <td  align="center" width="12">&nbsp;'.$uscs.'</td>

	 <td  align="justyfi" width="116">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$usuot.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Posesión con Tit./Sin Tit.</td>
	 <td  align="center" width="12">&nbsp;'.$ttp.'</td>

	 <td  align="justyfi" width="116">&nbsp;Colectiva</td>
	 <td  align="center" width="12">&nbsp;'.$rco.'</td>

	 <td  align="justyfi" width="116">&nbsp;Religioso</td>
	 <td  align="center" width="12">&nbsp;'.$usreg.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Posesión Certificada TU</td>
	 <td  align="center" width="12">&nbsp;'.$tpt.'</td>

	 <td  align="justyfi" width="116">&nbsp;Multifamiliar</td>
	 <td  align="center" width="12">&nbsp;'.$rpmf.'</td>

	 <td  align="justyfi" width="116">&nbsp;Alimentación</td>
	 <td  align="center" width="12">&nbsp;'.$usali.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="center" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Ocupación</td>
	 <td  align="center" width="12">&nbsp;'.$toc.'</td>

	 <td  align="justyfi" width="116">&nbsp;Comunal</td>
	 <td  align="center" width="12">&nbsp;'.$rpcc.'</td>

	 <td  align="justyfi" width="116">&nbsp;Servicio Comunal</td>
	 <td  align="center" width="12">&nbsp;'.$ussco.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>

	 <td  align="justyfi" width="116">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$tot.'</td>

	 <td  align="justyfi" width="116">&nbsp;Otro:</td>
	 <td  align="center" width="12">&nbsp;'.$rpo.'</td>

	 <td  align="justyfi" width="116" style="font-size: 25px;">&nbsp;Gubernamental/Institucional</td>
	 <td  align="center" width="12">&nbsp;'.$usgi.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>

 <tr>
	 <td colspan="10" align="center" >&nbsp;<b>10.-Servicios P&uacute;blicos </b></td>
  </tr>

  <tr>
	 <td  align="justyfi" width="114">&nbsp;Acueducto</td>
	 <td  align="center" width="12">&nbsp;'.$servac.'</td>

	 <td  align="justyfi" width="116">&nbsp;Electricidad Industrial</td>
	 <td  align="center" width="12">&nbsp;'.$servei.'</td>

	 <td  align="justyfi" width="116">&nbsp;Transp. Púb. Subterr.</td>
	 <td  align="center" width="12">&nbsp;'.$servtps.'</td>

	 <td  align="justyfi" width="116">&nbsp;Cable TV</td>
	 <td  align="center" width="12">&nbsp;'.$servctv.'</td>

	 <td  align="justyfi" width="116">&nbsp;Medicatura</td>
	 <td  align="center" width="12">'.$servme.'</td>
 </tr>
 <tr>
	 <td  align="justyfi" width="114">&nbsp;Cloacas</td>
	 <td  align="center" width="12">&nbsp;'.$servcl.'</td>

	 <td  align="justyfi" width="116">&nbsp;Vialidad</td>
	 <td  align="center" width="12">&nbsp;'.$servv.'</td>

	 <td  align="justyfi" width="116">&nbsp;Transp. Púb. Aéreo</td>
	 <td  align="center" width="12">&nbsp;'.$servtpa.'</td>

	 <td  align="justyfi" width="116">&nbsp;Aseo Urbano</td>
	 <td  align="center" width="12">&nbsp;'.$servau.'</td>

	 <td  align="justyfi" width="116">&nbsp;Gas Directo</td>
	 <td  align="center" width="12">&nbsp;'.$servgas.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Drenaje Artificial</td>
	 <td  align="center" width="12">&nbsp;'.$servda.'</td>

	 <td  align="justyfi" width="116">&nbsp;Pavimento</td>
	 <td  align="center" width="12">&nbsp;'.$servpa.'</td>

	 <td  align="justyfi" width="116">&nbsp;Teléfono</td>
	 <td  align="center" width="12">&nbsp;'.$servte.'</td>

	 <td  align="justyfi" width="116">&nbsp;Barrido de Calles</td>
	 <td  align="center" width="12">&nbsp;'.$servbc.'</td>

	 <td  align="justyfi" width="116">&nbsp;Gas Bombona</td>
	 <td  align="center" width="12">&nbsp;'.$servgasb.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Alumbrado Público</td>
	 <td  align="center" width="12">&nbsp;'.$servap.'</td>

	 <td  align="justyfi" width="116">&nbsp;Acera</td>
	 <td  align="center" width="12">&nbsp;'.$servacr.'</td>

	 <td  align="justyfi" width="116">&nbsp;Cobertura Celular</td>
	 <td  align="center" width="12">&nbsp;'.$servcc.'</td>

	 <td  align="justyfi" width="116">&nbsp;Correo y Telegráfo</td>
	 <td  align="center" width="12">&nbsp;'.$servctl.'</td>

	 <td  align="justyfi" width="116">&nbsp;Riego</td>
	 <td  align="center" width="12">&nbsp;'.$servrg.'</td>
 </tr>

 <tr>
	 <td  align="justyfi" width="114">&nbsp;Electricidad Residencial</td>
	 <td  align="center" width="12">&nbsp;'.$server.'</td>

	 <td  align="justyfi" width="116">&nbsp;Transp. Púb. Terrestre</td>
	 <td  align="center" width="12">&nbsp;'.$servtpt.'</td>

	 <td  align="justyfi" width="116">&nbsp;TV Satelital</td>
	 <td  align="center" width="12">&nbsp;'.$servtvs.'</td>

	 <td  align="justyfi" width="116">&nbsp;Escuela</td>
	 <td  align="center" width="12">&nbsp;'.$serves.'</td>

	 <td  align="justyfi" width="116">&nbsp;</td>
	 <td  align="justyfi" width="12">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dt, true, false, false, false, '');
#datos tipo construccion
if($tipo == "Quinta"){
		$tipq = "x";
	}elseif ($tipo == "Casa/Quinta") {
		$tipqc ="x"; 
	}elseif ($tipo == "Chalet") {
		$tipch ="x"; 
	}elseif ($tipo == "Town House") {
		$tipth ="x"; 
	}elseif ($tipo == "Casa Tradicional") {
		$tipct ="x"; 
	}elseif ($tipo == "Casa Convencional") {
		$tipcc ="x"; 
	}elseif ($tipo == "Casa Economica") {
		$tipce="x"; 
	}elseif ($tipo == "Rancho") {
		$tipra ="x"; 
	}elseif ($tipo == "Edificio") {
		$tiped ="x"; 
	}elseif ($tipo == "Apartamento") {
		$tipap ="x"; 
	}elseif ($tipo == "Centro Comercial") {
		$tipcenc ="x"; 
	}elseif ($tipo == "Local Comercial") {
		$tiplc ="x"; 
	}elseif ($tipo == "Galpon") {
		$tipga ="x"; 
	}elseif ($tipo == "Barraca") {
		$tipba ="x"; 
	}elseif ($tipo == "Vaqueras") {
		$tipvaq ="x"; 
	}elseif ($tipo == "Cochineras") {
		$tipcoch ="x"; 
	}elseif ($tipo == "Corrales y Anexos") {
		$tipca ="x"; 
	}elseif ($tipo == "Bebederos") {
		$tipbb ="x"; 
	}elseif ($tipo == "Comederos") {
		$tipcm ="x"; 
	}elseif ($tipo == "Tanques") {
		$tiptan ="x"; 
	}elseif ($tipo == "Otro") {
		$tipotr ="x"; 
	}

#datos descripcion de uso
if($descripcionuso == "Unifamiliar"){
		$dsunf = "x";
	}elseif ($descripcionuso == "Bifamiliar") {
		$dsubif ="x"; 
	}elseif ($descripcionuso == "Multifamiliar") {
		$dsumf ="x"; 
	}elseif ($descripcionuso == "Comercio al Detal") {
		$dsucd ="x"; 
	}elseif ($descripcionuso == "Comercio al Mayor") {
		$dsucm ="x"; 
	}elseif ($descripcionuso == "Mercado Libre") {
		$dsuml ="x"; 
	}elseif ($descripcionuso == "Oficinas") {
		$dsuof ="x"; 
	}elseif ($descripcionuso == "Industrial") {
		$dsuin ="x"; 
	}elseif ($descripcionuso == "Servicio") {
		$dsuser ="x"; 
	}elseif ($descripcionuso == "Agropecuario") {
		$dsuag ="x"; 
	}elseif ($descripcionuso == "Otro") {
		$dsuotr ="x"; 
	}

	#datos tenencia construccion

if($teneciaconstruccion == "Propiedad"){
		$tecp = "x";
	}elseif ($teneciaconstruccion == "Arrendamiento") {
		$tecar ="x"; 
	}elseif ($teneciaconstruccion == "Comodato") {
		$teco ="x"; 
	}elseif ($teneciaconstruccion == "Anticresis") {
		$tecan ="x"; 
	}elseif ($teneciaconstruccion == "Enfiteusis") {
		$tecen ="x"; 
	}elseif ($teneciaconstruccion == "Usufructo") {
		$tecus ="x"; 
	}elseif ($teneciaconstruccion == "Derecho de Hab.") {
		$tecdu ="x"; 
	}elseif ($teneciaconstruccion == "Concesion de Uso") {
		$teccu ="x"; 
	}elseif ($teneciaconstruccion == "Admin. de Estado N/E/M") {
		$tecae  ="x"; 
	}elseif ($teneciaconstruccion == "Posesion con Tit./Sin Tit.") {
		$tecpt ="x"; 
	}elseif ($teneciaconstruccion == "Ocupacion") {
		$tecop ="x"; 
	}elseif ($teneciaconstruccion == "Otro") {
		$tecotr  ="x"; 
	}

	#regimen propiedad construccion

if($regimenpropiedad == "Municipal Propio"){
		$regmp = "x";
	}elseif ($regimenpropiedad == "Nacional") {
		$regnac ="x"; 
	}elseif ($regimenpropiedad == "Estadal") {
		$reged = "x"; 
	}elseif ($regimenpropiedad == "Priv. Individual") {
		$regpi ="x"; 
	}elseif ($regimenpropiedad == "Priv. Condominio") {
		$regpc ="x"; 
	}elseif ($regimenpropiedad == "Posesion") {
		$regps ="x"; 
	}elseif ($regimenpropiedad == "Familiar") {
		$regfam ="x"; 
	}elseif ($regimenpropiedad == "Multifamiliar") {
		$regmf ="x"; 
	}elseif ($regimenpropiedad == "Otro") {
		$regotr ="x"; 
	}

#DATOS GENERALES DE LA CONSTRUCCION
$dgc = '
<table border="1" cellpadding="1" nobr="true" >
<tr>
	<td colspan="10"  align="center" valign="middle" bgcolor="#666666" color="#fff" >I.- Datos Generales de la Construcci&oacute;n </td>
</tr>
 <tr>
	 <td colspan="4"  width="194" align="center" >&nbsp;<b>1.- Tipo</b></td>
	 <td colspan="2"  width="150" align="center">&nbsp;<b>2.- Descripci&oacute;n de Uso </b></td>
	 <td colspan="2"   width="150" align="center">&nbsp;<b>3.- Tenencia Construcci&oacute;n</b></td>
	 <td colspan="2"   width="144" align="center">&nbsp;<b>4.- Regimen Propiedad </b></td>	 
 </tr>
 <tr>
	 <td   align="justyfi" width="87">&nbsp;Quinta</td>
	 <td   align="center" width="10">&nbsp;'.$tipq.'</td>
	 <td   align="justyfi" width="87">&nbsp;Galpón</td>
	 <td   align="center" width="10">&nbsp;'.$tipga.'</td>
	 <td   align="justyfi" width="140">&nbsp;Unifamiliar</td>
	 <td   align="center" width="10">&nbsp;'.$dsunf.'</td>
	 <td   align="justyfi" width="140">&nbsp;Propiedad</td>
	 <td   align="center" width="10">&nbsp;'.$tecp.'</td>
	 <td   align="justyfi" width="134">&nbsp;Municipal Propio</td>	 
     <td   align="center" width="10">&nbsp;'.$regmp.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Casa/Quinta</td>
	 <td   align="center" width="10">&nbsp;'.$tipqc.'</td>
	 <td   align="justyfi" width="87">&nbsp;Barraca</td>
	 <td   align="center" width="10">&nbsp;'.$tipba.'</td>
	 <td   align="justyfi" width="140">&nbsp;Bifamiliar</td>
	 <td   align="center" width="10">&nbsp;'.$dsubif.'</td>
	 <td   align="justyfi" width="140">&nbsp;Arrendamiento</td>
	 <td   align="center" width="10">&nbsp;'.$tecar.'</td>
	 <td   align="justyfi" width="134">&nbsp;Nacional</td>	 
     <td   align="center" width="10">&nbsp;'.$regnac.'</td>
 </tr>

  <tr>
	 <td   align="justyfi" width="87">&nbsp;Chalet</td>
	 <td   align="center" width="10">&nbsp;'.$tipch.'</td>
	 <td   align="justyfi" width="87">&nbsp;Vaqueras</td>
	 <td   align="center" width="10">&nbsp;'.$tipvaq.'</td>
	 <td   align="justyfi" width="140">&nbsp;Multifamiliar</td>
	 <td   align="center" width="10">&nbsp;'.$dsumf.'</td>
	 <td   align="justyfi" width="140">&nbsp;Comodato</td>
	 <td   align="center" width="10">&nbsp;'.$teco.'</td>
	 <td   align="justyfi" width="134">&nbsp;Estadal</td>	 
     <td   align="center" width="10">&nbsp;'.$reged.'</td>
 </tr>

  <tr>
	 <td   align="justyfi" width="87">&nbsp;Town House</td>
	 <td   align="center" width="10">&nbsp;'.$tipth.'</td>
	 <td   align="justyfi" width="87">&nbsp;Cochineras</td>
	 <td   align="center" width="10">&nbsp;'.$tipcoch.'</td>
	 <td   align="justyfi" width="140">&nbsp;Comercio al Detal</td>
	 <td   align="center" width="10">&nbsp;'.$dsucd.'</td>
	 <td   align="justyfi" width="140">&nbsp;Anticresis</td>
	 <td   align="center" width="10">&nbsp;'.$tecan.'</td>
	 <td   align="justyfi" width="134">&nbsp;Priv. Individual</td>	 
     <td   align="center" width="10">&nbsp;'.$regpi.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Casa Tradicional</td>
	 <td   align="center" width="10">&nbsp;'.$tipct.'</td>
	 <td   align="justyfi" width="87">&nbsp;Corrales y Anexos</td>
	 <td   align="center" width="10">&nbsp;'.$tipca.'</td>
	 <td   align="justyfi" width="140">&nbsp;Comercio al Mayor</td>
	 <td   align="center" width="10">&nbsp;'.$dsucm.'</td>
	 <td   align="justyfi" width="140">&nbsp;Enfiteusis</td>
	 <td   align="center" width="10">&nbsp;'.$tecen.'</td>
	 <td   align="justyfi" width="134">&nbsp;Priv. Condominio</td>	 
     <td   align="center" width="10">&nbsp;'.$regpc.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87" style="font-size: 25px;">&nbsp;Casa Convencional</td>
	 <td   align="center" width="10">&nbsp;'.$tipcc.'</td>
	 <td   align="justyfi" width="87">&nbsp;Bebederos</td>
	 <td   align="center" width="10">&nbsp;'.$tipbb.'</td>
	 <td   align="justyfi" width="140">&nbsp;Mercado Libre</td>
	 <td   align="center" width="10">&nbsp;'.$dsuml.'</td>
	 <td   align="justyfi" width="140">&nbsp;Usufructo</td>
	 <td   align="center" width="10">&nbsp;'.$tecus.'</td>
	 <td   align="justyfi" width="134">&nbsp;Posesión</td>	 
     <td   align="center" width="10">&nbsp;'.$regps.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Casa Económica</td>
	 <td   align="center" width="10">&nbsp;'.$tipce.'</td>
	 <td   align="justyfi" width="87">&nbsp;Comederos</td>
	 <td   align="center" width="10">&nbsp;'.$tipcm.'</td>
	 <td   align="justyfi" width="140">&nbsp;Oficinas</td>
	 <td   align="center" width="10">&nbsp;'.$dsuof.'</td>
	 <td   align="justyfi" width="140">&nbsp;Derecho de Hab.</td>
	 <td   align="center" width="10">&nbsp;'.$tecdu.'</td>
	 <td   align="justyfi" width="134">&nbsp;Familiar</td>	 
     <td   align="center" width="10">&nbsp;'.$regfam.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Rancho</td>
	 <td   align="center" width="10">&nbsp;'.$tipra.'</td>
	 <td   align="justyfi" width="87">&nbsp;Tanques</td>
	 <td   align="center" width="10">&nbsp;'.$tiptan.'</td>
	 <td   align="justyfi" width="140">&nbsp;Industrial</td>
	 <td   align="center" width="10">&nbsp;'.$dsuin.'</td>
	 <td   align="justyfi" width="140">&nbsp;Concesion de Uso.</td>
	 <td   align="center" width="10">&nbsp;'.$teccu.'</td>
	 <td   align="justyfi" width="134">&nbsp;Multifamiliar</td>	 
     <td   align="center" width="10">&nbsp;'.$regmf.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Edificio</td>
	 <td   align="center" width="10">&nbsp;'.$tiped.'</td>
	 <td   align="justyfi" width="87">&nbsp;Otro: </td>
	 <td   align="center" width="10">&nbsp;'.$tipotr.'</td>
	 <td   align="justyfi" width="140">&nbsp;Servicio</td>
	 <td   align="center" width="10">&nbsp;'.$dsuser.'</td>
	 <td   align="justyfi" width="140">&nbsp;Adm. del Estado N/E/M</td>
	 <td   align="center" width="10">&nbsp;'.$tecae.'</td>
	 <td   align="justyfi" width="134">&nbsp;Otro</td>	 
     <td   align="center" width="10">&nbsp;'.$regotr.'</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Apartamento</td>
	 <td   align="center" width="10">&nbsp;'.$tipap.'</td>
	 <td   align="justyfi" width="87">&nbsp;</td>
	 <td   align="center" width="10">&nbsp;</td>
	 <td   align="justyfi" width="140">&nbsp;Agropecuario</td>
	 <td   align="center" width="10">&nbsp;'.$dsuag.'</td>
	 <td   align="justyfi" width="140">&nbsp;Posesión con Tit./Sin Tit.</td>
	 <td   align="center" width="10">&nbsp;'.$tecpt.'</td>
	 <td   align="justyfi" width="134">&nbsp;</td>	 
     <td   align="center" width="10">&nbsp;</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Centro Comercial</td>
	 <td   align="center" width="10">&nbsp;'.$tipcenc.'</td>
	 <td   align="justyfi" width="87">&nbsp;</td>
	 <td   align="center" width="10">&nbsp;</td>
	 <td   align="justyfi" width="140">&nbsp;Otro: </td>
	 <td   align="center" width="10">&nbsp;'.$dsuotr.'</td>
	 <td   align="justyfi" width="140">&nbsp;Ocupación</td>
	 <td   align="center" width="10">&nbsp;'.$tecop.'</td>
	 <td   align="justyfi" width="134">&nbsp;</td>	 
     <td   align="center" width="10">&nbsp;</td>
 </tr>

 <tr>
	 <td   align="justyfi" width="87">&nbsp;Local Comercial</td>
	 <td   align="center" width="10">&nbsp;'.$tiplc.'</td>
	 <td   align="justyfi" width="87">&nbsp;</td>
	 <td   align="center" width="10">&nbsp;</td>
	 <td   align="justyfi" width="140">&nbsp;</td>
	 <td   align="center" width="10">&nbsp;</td>
	 <td   align="justyfi" width="140">&nbsp;Otro: </td>
	 <td   align="center" width="10">&nbsp;'.$tecotr.'</td>
	 <td   align="justyfi" width="134">&nbsp;</td>	 
     <td   align="center" width="10">&nbsp;</td>
 </tr>
</table>
';

$pdf->writeHTML($dgc, true, false, false, false, '');
	
#soporte de la construccion

if($soporteestructural == "Concreto Armado"){
		$sptca = "x";
	}elseif ($soporteestructural == "Metalica") {
		$sptme  ="x"; 
	}elseif ($soporteestructural == "Madera") {
		$sptma = "x"; 
	}elseif ($soporteestructural == "Paredes de Carga") {
		$sptpc ="x"; 
	}elseif ($soporteestructural == "Prefabricado") {
		$sptpf ="x"; 
	}elseif ($soporteestructural == "Machones") {
		$sptmach ="x"; 
	}elseif ($soporteestructural == "Otro") {
		$sptotr  ="x"; 
	}

	

#techo de la construccion

if($techoestructural == "Concreto Armado"){
		$tcsca = "x";
	}elseif ($techoestructural == "Metalica") {
		$tcsme  ="x"; 
	}elseif ($techoestructural == "Madera") {
		$tcsma = "x"; 
	}elseif ($techoestructural == "Varas") {
		$tcsva ="x"; 
	}elseif ($techoestructural == "Cerchas") {
		$tcscer ="x"; 
	}elseif ($techoestructural == "Otro") {
		$tcsotr   ="x"; 
	}

#cubierta externa
if($cubiertaexterna == "Madera/Teja"){
		$cuemt = "x";
	}elseif ($cubiertaexterna == "Placa/Teja") {
		$cuept  ="x"; 
	}elseif ($cubiertaexterna == "Platabanda") {
		$cuepl = "x"; 
	}elseif ($cubiertaexterna == "Tejas") {
		$cuetj ="x"; 
	}elseif ($cubiertaexterna == "Caña Brava") {
		$cuecb  ="x"; 
	}elseif ($cubiertaexterna == "Asbesto") {
		$cueab   ="x"; 
	}elseif ($cubiertaexterna == "Aluminio") {
		$cueal ="x"; 
	}elseif ($cubiertaexterna == "Zinc") {
		$cuezi  ="x"; 
	}elseif ($cubiertaexterna == "Acerolit") {
		$cueac    ="x"; 
	}elseif ($cubiertaexterna == "Tabelon") {
		$cuetab ="x"; 
	}elseif ($cubiertaexterna == "Acero Galbanizado") {
		$cueag  ="x"; 
	}elseif ($cubiertaexterna == "Otro") {
		$cueotr    ="x"; 
	}

#cubierta interna
if($cubiertainterna == "Plafon"){
		$cipl = "x";
	}elseif ($cubiertainterna == "Cielo Raso (Laminas)") {
		$cicrl  ="x"; 
	}elseif ($cubiertainterna == "Cielo Raso (Econom.)") {
		$cicre = "x"; 
	}elseif ($cubiertainterna == "Machihembrado (Madera)") {
		$cimm ="x"; 
	}elseif ($cubiertainterna == "Otro") {
		$ciotr  ="x"; 
	}

#DATOS GENERALES DE LA CONSTRUCCION
$dgc1 = '
<table border="1" cellpadding="1" nobr="true">
  <tr align="center">
    <th width="214" colspan="4" bgcolor="#666666" color="#fff">&nbsp; J.- Datos Estructurales de la Construcción</th>
    <th width="214" colspan="4" bgcolor="#666666" color="#fff">&nbsp; K.- Cubierta Externa</th>
    <th width="210" colspan="2" bgcolor="#666666" color="#fff">&nbsp; L.- Cubierta Interna</th>
  </tr>
  <tr>
    <td colspan="2" width="107" align="center"><b>1.- Soporte</b></td>
    <td colspan="2" width="107" align="center"><b>2.- Techo</b></td>
    <td width="97">&nbsp;Madera/Teja </td>
    <td width="10">'.$cuemt.'</td>
    <td width="97">&nbsp;Acerolit</td>
    <td width="10">'.$cueac.'</td>
    <td width="200" align="justyfi">&nbsp;Plaf&oacute;n</td>
    <td width="10">'.$cipl.'</td>
  </tr>
  <tr align="justyfi">
    <td width="97">&nbsp;Concreto Armado </td>
    <td width="10">&nbsp;'.$sptca.'</td>
    <td width="97">&nbsp;Concreto Armado</td>
    <td width="10">&nbsp;'.$tcsca.'</td>
    <td >&nbsp;Placa/Teja</td>
    <td >'.$cuept.'</td>
    <td >&nbsp;Tabel&oacute;n</td>
    <td >'.$cuetab.'</td>
    <td>&nbsp;Cielo Raso (L&aacute;minas) </td>
    <td>'.$cicrl.'</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Met&aacute;lica</td>
    <td>&nbsp;'.$sptme.'</td>
    <td>&nbsp;Met&aacute;lica</td>
    <td>&nbsp;'.$tcsme.'</td>
    <td >&nbsp;Platabanda</td>
    <td >'.$cuepl.'</td>
    <td >&nbsp;Acero Galbanizo </td>
    <td >'.$cueag.'</td>
    <td>&nbsp;Cielo Raso (Ecn&oacute;m.)</td>
    <td>'.$cicre.'</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Madera</td>
    <td>&nbsp;'.$sptma.'</td>
    <td>&nbsp;Madera</td>
    <td>&nbsp;'.$tcsma.'</td>
    <td >&nbsp;Tejas</td>
    <td >'.$cuetj.'</td>
    <td >&nbsp;Otro:</td>
    <td >'.$cueotr.'</td>
    <td>&nbsp;Machihembrado Mad. </td>
    <td>'.$cimm.'</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Paredes de Carga </td>
    <td>&nbsp;'.$sptpc.'</td>
    <td>&nbsp;Varas</td>
    <td>&nbsp;'.$tcsva.'</td>
    <td >&nbsp;Ca&ntilde;a Brava </td>
    <td >'.$cuecb.'</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td>&nbsp;Otro:</td>
    <td>'.$ciotr.'</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Prefabricado</td>
    <td>&nbsp;'.$sptpf.'</td>
    <td>&nbsp;Cerchas</td>
    <td>&nbsp;'.$tcscer.'</td>
    <td >&nbsp;Asbesto</td>
    <td >'.$cueab.'</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Machones</td>
    <td>&nbsp;'.$sptmach.'</td>
    <td>&nbsp;Otro:</td>
    <td>&nbsp;'.$tcsotr.'</td>
    <td >&nbsp;Aluminio</td>
    <td >'.$cueal.'</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr align="justyfi">
    <td>&nbsp;Otro:</td>
    <td>&nbsp;'.$sptotr.'</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td >&nbsp;Zinc</td>
    <td >'.$cuezi.'</td>
    <td >&nbsp;</td>
    <td >&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
';

$pdf->writeHTML($dgc1, true, false, false, false, '');

#Paredes tipo
	if ($paredestipo == "Bloque de Arcilla") {
		$ptiba   ="x"; 
	}elseif ($paredestipo == "Bloque de Cemento") {
		$ptibc  = "x"; 
	}elseif ($paredestipo == "Madera Aserrada") {
		$ptima ="x"; 
	}elseif ($paredestipo == "Laton") {
		$ptil   ="x"; 
	}elseif ($paredestipo == "Ladrillo") {
		$ptila   ="x"; 
	}elseif ($paredestipo == "Prefabricada") {
		$ptipf  = "x"; 
	}elseif ($paredestipo == "Bahareque") {
		$ptibq ="x"; 
	}elseif ($paredestipo == "Adobe") {
		$ptiad   ="x"; 
	}elseif ($paredestipo == "Tapia") {
		$ptita   ="x"; 
	}elseif ($paredestipo == "Vidrio") {
		$ptivd  = "x"; 
	}elseif ($paredestipo == "Otro") {
		$ptiotr ="x"; 
	}

#Paredes acabado
	if ($paredesacabado == "Sin Friso") {
		$pacsf   ="x"; 
	}elseif ($paredesacabado == "Friso Liso") {
		$pacfl  = "x"; 
	}elseif ($paredesacabado == "Friso Rustico") {
		$pacfr ="x"; 
	}elseif ($paredesacabado == "Obra Limpia") {
		$pacol  ="x"; 
	}elseif ($paredesacabado == "Cemento Blanco") {
		$paccb   ="x"; 
	}elseif ($paredesacabado == "Ceramica") {
		$pacce  = "x"; 
	}elseif ($paredesacabado == "Vidrio Molido") {
		$pacvm ="x"; 
	}elseif ($paredesacabado == "Yeso/Cal") {
		$pacyc   ="x"; 
	}elseif ($paredesacabado == "Otro") {
		$pacotr  ="x"; 
	}
	
	#Paredes pintura
	if ($paredespintura == "Caucho") {
		$parpca   ="x"; 
	}elseif ($paredespintura == "Oleo") {
		$parpol  = "x"; 
	}elseif ($paredespintura == "Lechada") {
		$parple ="x"; 
	}elseif ($paredespintura == "Pasta") {
		$parpat  ="x"; 
	}elseif ($paredespintura == "Asbestina") {
		$parpab   ="x"; 
	}elseif ($paredespintura == "Sin Pintura") {
		$parpsp  = "x"; 
	}elseif ($paredespintura == "Papel Tapiz") {
		$parpta ="x"; 
	}

	#instalaciones electricas
	if ($paredesinstelectricas == "Embutidas") {
		$paem   ="x"; 
	}elseif ($paredesinstelectricas == "Externa") {
		$paex  = "x"; 
	}elseif ($paredesinstelectricas == "Industrial") {
		$paei ="x"; 
	}

	#Pisos
	if ($pisotipo == "Cemento Pulido") {
		$piscp   ="x"; 
	}elseif ($pisotipo == "Cemento Rustico") {
		$piscr  = "x"; 
	}elseif ($pisotipo == "Sin Piso (Tierra)") {
		$pispt ="x"; 
	}elseif ($pisotipo == "Ceramica") {
		$pisce  ="x"; 
	}elseif ($pisotipo == "Granito") {
		$pisgr   ="x"; 
	}elseif ($pisotipo == "Ladrillos") {
		$pisla  = "x"; 
	}elseif ($pisotipo == "Parque") {
		$pispa ="x"; 
	}elseif ($pisotipo == "Marmol") {
		$pisma   ="x"; 
	}elseif ($pisotipo == "Vinil") {
		$pisvi  = "x"; 
	}elseif ($pisotipo == "Mosaico") {
		$pismo ="x"; 
	}elseif ($pisotipo == "Otro") {
		$pisot   ="x"; 
	}	

	#estado conservacion de construccion
	if ($estadoconservacion == "Excelente") {
		$edex   ="x"; 
	}elseif ($estadoconservacion == "Bueno") {
		$edbu  = "x"; 
	}elseif ($estadoconservacion == "Regular") {
		$edre ="x"; 
	}elseif ($estadoconservacion == "Malo") {
		$edma   ="x"; 
	}elseif ($estadoconservacion == "Otro") {
		$edot  = "x"; 
	}	


#DATOS GENERALES DE LA CONSTRUCCION
$dgc2 = '
<table border="1" cellpadding="1" nobr="true" >
<tr>
	<td colspan="17"  align="center" valign="middle" bgcolor="#666666" color="#fff" >M.- Datos Coplementarios de la Construcción</td>
</tr>
 <tr>
   <td colspan="8"  align="center" width="370">&nbsp;<b>1.- Paredes </b></td>
   <td colspan="2"  align="center" width="128">&nbsp;<b>2.- Pisos </b></td>
   <td  align="center" width="119">&nbsp;<b>3.- Ba&ntilde;os </b></td>
   <td  align="center" width="20">&nbsp;<strong>N&deg;</strong></td>
 </tr>
 <tr>
	 <td colspan="2"  align="center" width="124" >&nbsp;<b>1.1- Tipo  </b></td>
	 <td colspan="4"  align="center" width="124">&nbsp;<b>&nbsp;<b>1.2</b>.- Acabado</b></td>
	 <td colspan="2"  align="center" width="122">&nbsp;&nbsp;<b>1.</b><b>3.- Pintura </b></td>
	 <td  align="justyfi" width="116">&nbsp;Cemento Pulido </td>
	 <td  align="justyfi" width="12">'.$piscp.'</td>
   <td width="119" align="justyfi">&nbsp;WC&nbsp;</td>
   <td  align="center" width="20">'.$banowc.'</td>	 
 </tr>
 <tr>
	 <td width="112"  align="justyfi">&nbsp;Bloque de Arcilla </td>
	 <td  align="justyfi" width="12">'.$ptiba.'</td>

	 <td  align="justyfi" width="112">&nbsp;Sin Friso </td>
	 <td width="12" colspan="3"  align="justyfi">'.$pacsf.'</td>

	 <td  align="justyfi" width="104">&nbsp;Caucho</td>
	 <td  align="justyfi" width="18">'.$parpca.'</td>

	 <td  align="justyfi" width="116">&nbsp;Cemento R&uacute;stico </td>
	 <td  align="justyfi" width="12">'.$piscr.'</td>

	 <td width="119" align="justyfi">&nbsp;Ba&ntilde;o Comunitario </td>
   <td  align="center" width="20">'.$banocomunitario.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Bloque de Cemento </td>
   <td  align="justyfi">'.$ptibc.'</td>
   <td  align="justyfi">&nbsp;Friso Liso </td>
   <td colspan="3"  align="justyfi">'.$pacfl.'</td>
   <td width="104"  align="justyfi">&nbsp;&Oacute;leo</td>
   <td width="18"  align="justyfi">'.$parpol.'</td>
   <td  align="justyfi">&nbsp;Sin Piso (Tierra) </td>
   <td  align="justyfi">'.$pispt.'</td>
   <td align="justyfi">&nbsp;Ducha</td>
   <td  align="center">'.$banoducha.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Madera Aserrada </td>
   <td  align="justyfi">'.$ptima.'</td>
   <td  align="justyfi">&nbsp;Friso R&uacute;stico </td>
   <td colspan="3"  align="justyfi">'.$pacfr.'</td>
   <td width="104"  align="justyfi">&nbsp;Lechada</td>
   <td width="18"  align="justyfi">'.$parple.'</td>
   <td  align="justyfi">&nbsp;Cer&aacute;mica</td>
   <td  align="justyfi">'.$pisce.'</td>
   <td align="justyfi">&nbsp;Lavamanos</td>
   <td  align="center">'.$banolavamanos.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Lat&oacute;n</td>
   <td  align="justyfi">'.$ptil.'</td>
   <td  align="justyfi">&nbsp;Obra Limpia </td>
   <td colspan="3"  align="justyfi">'.$pacol.'</td>
   <td width="104"  align="justyfi">&nbsp;Pasta</td>
   <td width="18"  align="justyfi">'.$parpat.'</td>
   <td  align="justyfi">&nbsp;Granito</td>
   <td  align="justyfi">'.$pisgr.'</td>
   <td align="justyfi">&nbsp;Ba&ntilde;era</td>
   <td  align="center">'.$banobanera.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Ladrillo</td>
   <td  align="justyfi">'.$ptila.'</td>
   <td  align="justyfi">&nbsp;Cemento Blanco </td>
   <td colspan="3"  align="justyfi">'.$paccb.'</td>
   <td width="104"  align="justyfi">&nbsp;Asbestina</td>
   <td width="18"  align="justyfi">'.$parpab.'</td>
   <td  align="justyfi">&nbsp;Ladrillos</td>
   <td  align="justyfi">'.$pisla.'</td>
   <td align="justyfi">&nbsp;Bidet</td>
   <td  align="center">'.$banobidet.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Prefabricada</td>
   <td  align="justyfi">'.$ptipf.'</td>
   <td  align="justyfi">&nbsp;Cer&aacute;mica</td>
   <td colspan="3"  align="justyfi">'.$pacce.'</td>
   <td width="104"  align="justyfi">&nbsp;Sin Pintura </td>
   <td width="18"  align="justyfi">'.$parpsp.'</td>
   <td  align="justyfi">&nbsp;Parque</td>
   <td  align="justyfi">'.$pispa.'</td>
   <td align="justyfi">&nbsp;Letrina</td>
   <td  align="center">'.$banoletrina.'</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Bahareque</td>
   <td  align="justyfi">'.$ptibq.'</td>
   <td  align="justyfi">&nbsp;Vidrio Molido </td>
   <td colspan="3"  align="justyfi">'.$pacvm.'</td>
   <td width="104"  align="justyfi">&nbsp;Papel Tapiz </td>
   <td width="18"  align="justyfi">'.$parpta.'</td>
   <td  align="justyfi">&nbsp;M&aacute;rmol</td>
   <td  align="justyfi">'.$pisma.'</td>
   <td align="justyfi">&nbsp;Cer&aacute;mica 1ra </td>
   <td  align="center">'.$banoceramica1.'</td>
 </tr>
 <tr>
	 <td  align="justyfi" width="112">&nbsp;Adobe </td>
	 <td  align="justyfi" width="12">'.$ptiad.'</td>

	 <td  align="justyfi" width="112">&nbsp;Yeso/cal </td>
	 <td width="12" colspan="3"  align="justyfi">'.$pacyc.'</td>

	 <td colspan="2"  align="center" width="122">&nbsp;<b>1.</b><b>4.- Inst. Electricas </b></td>
	 <td  align="justyfi" width="116">&nbsp;Vinil</td>
	 <td  align="justyfi" width="12">'.$pisvi.'</td>

	 <td width="119" align="justyfi">&nbsp;Cer&aacute;mica 2da </td>
     <td  align="center" width="20">'.$banoceramica2.'</td>
 </tr>
 <tr>
	 <td  align="justyfi" width="112">&nbsp;Tapia</td>
	 <td  align="justyfi" width="12">'.$ptita.'</td>

	 <td  align="justyfi" width="112">&nbsp;Otro: </td>
	 <td width="12" colspan="3"  align="justyfi">'.$pacotr.'</td>

	 <td  align="justyfi" width="104">&nbsp;Embutidas</td>
	 <td  align="justyfi" width="18">'.$paem.'</td>

	 <td  align="justyfi" width="116">&nbsp;Mosaico</td>
	 <td  align="justyfi" width="12">'.$pismo.'</td>

	 <td  align="center" width="119">&nbsp;&nbsp;</td>
   <td  align="center" width="20">&nbsp;&nbsp;</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Vidrio</td>
   <td  align="justyfi">'.$ptivd.'</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td colspan="3"  align="justyfi">&nbsp;&nbsp;</td>
   <td width="104"  align="justyfi">&nbsp;Externa</td>
   <td width="18"  align="justyfi">'.$paex.'</td>
   <td  align="justyfi">&nbsp;Otro:</td>
   <td  align="justyfi">'.$pisot.'</td>
   <td  align="center">&nbsp;&nbsp;</td>
   <td  align="center">&nbsp;&nbsp;</td>
 </tr>
 <tr>
   <td  align="justyfi">&nbsp;Otro: </td>
   <td  align="justyfi">'.$ptiotr.'</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td colspan="3"  align="justyfi">&nbsp;&nbsp;</td>
   <td width="104"  align="justyfi">&nbsp;Industrial</td>
   <td width="18"  align="justyfi">'.$paei.'</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="center">&nbsp;&nbsp;</td>
   <td  align="center">&nbsp;&nbsp;</td>
 </tr>
 <tr>
   <td colspan="3"  align="center" width="188">&nbsp;<b>4.- Ventanas </b></td>
   <td width="60" colspan="3"  align="center">&nbsp;<strong>N&deg;</strong></td>
   <td  align="center" width="104">&nbsp;<b>5.- Puertas </b></td>
   <td  align="center" width="18">N°</td>
   <td colspan="2"  align="center" width="128">&nbsp;<b>6.-Estado Conservaci&oacute;n </b></td>
   <td colspan="2"  align="center" width="139">&nbsp;<b>7.- Otros Datos </b></td>
  </tr>
 <tr>
	 <td colspan="3"  align="center" width="188">&nbsp;Material</td>
	 <td  align="center" width="20">&nbsp;<strong>A</strong></td>

	 <td  align="center" width="20">&nbsp;<strong>H</strong></td>
	 <td  align="center" width="20">&nbsp;<strong>M</strong></td>
	 <td  align="justyfi" width="104">&nbsp;Met&aacute;lica</td>
	 <td  align="center" width="18">'.$puertametalica.'</td>

	 <td  align="justyfi" width="116">&nbsp;Excelente </td>
	 <td  align="justyfi" width="12">'.$edex.'</td>

	 <td  align="justyfi" width="119">&nbsp;A&ntilde;o Construcci&oacute;n </td>
   <td  align="center" width="20">'.$anoconstruccion.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Ventanal</td>
   <td  align="center">'.$ventanalaluminio.'</td>
   <td  align="center">'.$ventanalhierro.'</td>
   <td  align="center">'.$ventanalmadera.'</td>
   <td width="104"  align="justyfi">&nbsp;Madera Cepillada </td>
   <td align="center" width="18">'.$puertamaderacepillada.'</td>
   <td  align="justyfi">&nbsp;Bueno</td>
   <td  align="justyfi">'.$edbu.'</td>
   <td  align="justyfi">&nbsp;% Refacci&oacute;n </td>
   <td  align="center">'.$porcentajerefaccion.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Basculante</td>
   <td  align="center">'.$basculantealuminio.'</td>
   <td  align="center">'.$basculantehierro.'</td>
   <td  align="center">'.$basculantemadera.'</td>
   <td width="104"  align="justyfi">&nbsp;Mad. Ent. Ec&oacute;n. </td>
   <td align="center" width="18">'.$puertamadeconomica.'</td>
   <td  align="justyfi">&nbsp;Regular</td>
   <td  align="justyfi">'.$edre.'</td>
   <td  align="justyfi">&nbsp;N&deg; de Niveles </td>
   <td  align="center">'.$numeroniveles.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Batiente</td>
   <td  align="center">'.$satientealuminio.'</td>
   <td  align="center">'.$satientehierro.'</td>
   <td  align="center">'.$satientemadera.'</td>
   <td width="104"  align="justyfi">&nbsp;Entamborada Fina </td>
   <td align="center" width="18">'.$puertaentamboradafina.'</td>
   <td  align="justyfi">&nbsp;Malo</td>
   <td  align="justyfi">'.$edma.'</td>
   <td  align="justyfi">&nbsp;A&ntilde;os Refacci&oacute;n </td>
   <td  align="center">'.$anorefaccion.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Celos&iacute;a</td>
   <td  align="center">'.$celosiaaluminio.'</td>
   <td  align="center">'.$celosiahierro.'</td>
   <td  align="center">'.$celosiamadera.'</td>
   <td width="104"  align="justyfi">&nbsp;De Seguridad </td>
   <td align="center" width="18">'.$puertaseguridad.'</td>
   <td  align="justyfi">&nbsp;Otro:</td>
   <td  align="justyfi">'.$edot.'</td>
   <td  align="justyfi">&nbsp;Edad Efectiva </td>
   <td  align="center">'.$edadefectiva.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Corredera</td>
   <td  align="center">'.$correderaaluminio.'</td>
   <td  align="center">'.$correderahierro.'</td>
   <td  align="center">'.$correderamadera.'</td>
   <td width="104"  align="justyfi">&nbsp;Otra: '.$puertaotra.'</td>
   <td align="center" width="18">'.$puertaotrac.'</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="justyfi">&nbsp;N&deg; de Edificaciones </td>
   <td  align="center">'.$numeroedificacion.'</td>
 </tr>
 <tr>
   <td colspan="3"  align="justyfi">&nbsp;Panor&aacute;mica</td>
   <td  align="center">'.$panoramicaaluminio.'</td>
   <td  align="center">'.$panoramicahierro.'</td>
   <td  align="center">'.$panoramicamadera.'</td>
   <td width="104"  align="justyfi">&nbsp;&nbsp;</td>
   <td width="18"  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="justyfi">&nbsp;&nbsp;</td>
   <td  align="center">&nbsp;&nbsp;</td>
   <td  align="center">&nbsp;&nbsp;</td>
 </tr>
 <tr>
   <td colspan="12"  align="justyfi">&nbsp;<strong>Observaci&oacute;n: &nbsp;</strong> 
   '.$observacionconstruccion.'</td>
  </tr>
</table>
';

$pdf->writeHTML($dgc2, true, false, false, false, '');

#DATOS DEL REGISTRO PUBLICO
$drp = '
<table border="1" cellpadding="1" nobr="true" >
<tr>
	<td colspan="17"  align="center" valign="middle" bgcolor="#666666" color="#fff" >N.- Datos del Registro P&uacute;blico </td>
</tr>
 <tr>
   <td width="80" colspan="5" rowspan="2"  align="center" valign="middle"><b> N&uacute;mero </b></td>
   <td width="80" rowspan="2"  align="center" valign="middle"><b>Folio</b></td>
   <td width="80" rowspan="2"  align="center" valign="middle"><b>Tomo</b></td>
   <td width="80" rowspan="2"  align="center" valign="middle"><b>Protocolo</b></td>
    <td width="83" rowspan="2"  align="center" valign="middle"><b>Fecha</b></td>
   <td width="134" colspan="2"  align="center" valign="middle"><strong>&Aacute;rea (M2) </strong></td>
   <td width="100" colspan="2" rowspan="2"  align="center" valign="middle"><b>Monto (Bs. S) </b><strong></strong></td>
  </tr>
 <tr>
   <td width="67"  align="center" valign="middle">Terreno</td>
   <td width="67"  align="center" valign="middle">Construcci&oacute;n</td>
 </tr>
 
 <tr>
   <td width="80" colspan="5"  align="center">&nbsp;'.$numero.'</td>
   <td width="80"  align="center">&nbsp;'.$folio.'</td>
   <td width="80"  align="center">&nbsp;'.$tomo.'</td>
   <td width="80"  align="center">&nbsp;'.$protocolo.'</td>
   <td width="83"  align="center">&nbsp;'.$fecharp.'</td>
   <td  align="center" width="67">&nbsp;'.$aream2terreno.'</td>
   <td  align="center" width="67">'.$aream2cont.'</td>
   <td colspan="2"  align="center" width="100">'.$valorbs.'</td>
 </tr>
</table>
';

$pdf->writeHTML($drp, true, false, false, false, '');


#VALORACION ECONOMICA
$drp = '
<table border="1" cellpadding="1" nobr="true" >
<tr>
	<td colspan="17"  align="center" valign="middle" bgcolor="#666666" color="#fff" >&Ntilde;.- Valoraci&oacute;n Econ&oacute;mica </td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" ><b>1.- Terreno </b></td>
</tr>
 <tr>
   <td width="92" colspan="5" rowspan="2"  align="center" valign="middle"><strong>&Aacute;rea (M2) </strong></td>
   <td width="92" rowspan="2"  align="center" valign="middle"><b>Valor Unitario (Bs. S/M2) </b></td>
   <td width="92" rowspan="2"  align="center" valign="middle"><b>Sector</b></td>
   <td width="134" colspan="2"  align="center" valign="middle"><strong>Factor Ajuste  </strong></td>
   <td width="110" rowspan="2"  align="center" valign="middle"><b>Valor Ajustado (Bs. S/M2) </b></td>
   <td width="117" colspan="2" rowspan="2"  align="center" valign="middle"><b>Valor Total  (Bs. S) </b><strong></strong></td>
  </tr>
  
  
  
 <tr>
   <td width="67"  align="center" valign="middle">&Aacute;rea</td>
   <td width="67"  align="center" valign="middle">Forma</td>
 </tr>
 <tr>
   <td colspan="5"  align="center" valign="middle">'.$areametroscuadrados.'</td>
   <td width="92"  align="center" valign="middle">'.$valorunitariometroscuadrados.'</td>
   <td width="92"  align="center" valign="middle">'.$sector_nombre.'</td>
   <td width="67"  align="center" valign="middle">&nbsp;</td>
   <td width="67"  align="center" valign="middle">&nbsp;</td>
   <td  align="center" valign="middle">'.$valorajustadometroscuadrados.'</td>
   <td colspan="2"  align="center" valign="middle">'.$valortotal.'</td>
 </tr>
 
 
 <tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b>2.- Construcci&oacute;n </b></td>
</tr>
<tr>
   <td width="92" colspan="5"  align="center"><strong>Tipolog&iacute;a</strong></td>
   <td width="92"  align="center"><strong>&Aacute;rea (M2) </strong></td>
   <td width="92"  align="center"><strong>Valor (M2) </strong></td>
   <td width="67"  align="center"><strong>% Depresiaci&oacute;n </strong></td>
   <td  align="center" width="67"><strong>Valor Actual (Bs. S) </strong></td>
   <td  colspan="3" align="center" width=""><b>Valor Total  (Bs. S) </b><strong></strong></td>
 </tr>

 <tr>
   <td width="92" colspan="5"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="center" width="67">&nbsp;</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>

 <tr>
   <td width="92" colspan="5"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="center" width="67">&nbsp;</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>
 <tr>
   <td width="92" colspan="5"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="center" width="67">&nbsp;</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>
 <tr>
   <td width="92" colspan="5"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="center" width="67">&nbsp;</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>
 <tr>
   <td width="92" colspan="5"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="right" width="67">&nbsp;Construcción</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>
 <tr>
   <td width="92" colspan="5"  align="center">&nbsp; Área Total</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="92"  align="center">&nbsp;</td>
   <td width="67"  align="center">&nbsp;</td>
   <td  align="right" width="67">&nbsp;Inmueble</td>
   <td  colspan="3" align="center" width="">&nbsp;</td>
 </tr>
 <tr>
  <td colspan="17"  align="Justyfi" valign="middle" width="637"><b> Observaciones: </b></td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b> </b></td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b> </b></td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b> </b></td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b> </b></td>
</tr>
<tr>
  <td colspan="17"  align="center" valign="middle" width="637"><b> </b></td>
</tr>


</table>
';

$pdf->writeHTML($drp, true, false, false, false, '');

// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
#DATOS DE LA PARCELA
$up = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td colspan="3"  align="center" valign="middle" bgcolor="#666666" color="#fff" >O.- Ubicación de la Parcela</td>
    </tr>
    <tr>
        <td colspan="3"  align="center" valign="middle" >1.- Datos Generales</td>
    </tr>
    <tr>
    	<td rowspan="2">&nbsp;Control Archivo: <br />
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '.$numeroarchivo.'</td>
    	<td>&nbsp;Ficha Castastral &nbsp;N&deg;: <br /> &nbsp;'.$numerocatastral.'</td>
    	<td>&nbsp;Inscrpcion Castastral N&deg;:<br />
    		&nbsp;Fecha:&nbsp;'.$fechainscripcion.'
    	</td>
    </tr>
    
</table>
';

$pdf->writeHTML($up, true, false, false, false, '');


#DATOS UBICACION PARCELA
$up = '
<table cellspacing="0" cellpadding="1" border="1">
    <tr>
        <td colspan="10"  align="center" valign="middle" bgcolor="#666666" color="#fff" width="637" >P.- Datos de Ubicaci&oacute;n</td>
    </tr>
	<tr>
        <td colspan="10"  align="center" valign="middle" width="637"><strong>1.- Código Catastral Ámbito Urbano</strong></td>
    </tr>
    <tr>
    	<td width="63">&nbsp;Efed</td>
    	<td width="63">&nbsp;Mun</td>
    	<td width="63">&nbsp;Prr</td>
    	<td width="63">&nbsp;Ámb</td>
    	<td width="63">&nbsp;Sec</td>
    	<td width="63">&nbsp;Man </td>
    	<td width="63">&nbsp;Par </td>
    	<td width="63">&nbsp;Sbp</td>
    	<td width="63">&nbsp;Niv</td>
    	<td width="70">&nbsp;Und</td>
    </tr>
     <tr>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;U -'.$codigocatastral[0].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[1].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[2].'</td>
    	<td width="63">&nbsp;'.$codigocatastral[3].'</td>
    	<td width="63">&nbsp;</td>
    	<td width="63">&nbsp;</td>
    	<td width="70">&nbsp;</td>
    </tr>
	<tr>
        <td colspan="10"  align="center" valign="middle" width="637"><strong>Linderos Actuales </strong></td>
    </tr>
    <tr>
    	<td width="637" colspan="10">&nbsp;Norte:&nbsp;'.$linderonorte.'</td>
    	
    </tr>
	<tr>
    	<td width="637" colspan="10">&nbsp;Sur:&nbsp;'.$linderosur.'</td>
    	
    </tr>
	<tr>
    	<td width="637" colspan="10">&nbsp;Este: &nbsp;'.$linderoeste.'</td>
    	
    </tr><tr>
    	<td width="637" colspan="10">&nbsp;Oeste: &nbsp;'.$linderooeste.'</td>
    	
    </tr>
	<tr>
        <td colspan="10"  align="center" valign="middle" width="637" ><strong>2.- Coordenadas UTM (REGVEN)</strong></td>
    </tr>
	<tr>
        <td colspan="8"  align="center" valign="middle"  width="212">&nbsp;Norte (m):</td>
        <td  align="center" valign="middle"  width="212">&nbsp;Este (m):</td>
        <td  align="center" valign="middle"  width="213">&nbsp; Huso</td>
	</tr>
	<tr>
        <td colspan="8"  align="center" valign="middle"  width="212">&nbsp;'.$coordenadasutmnorte.'</td>
        <td  align="center" valign="middle"  width="212">&nbsp;'.$coordenadasutmeste.'</td>
        <td  align="center" valign="middle"  width="213">&nbsp;'.$coordenadasutmhuso.'</td>
	</tr>
</table>


';

$pdf->writeHTML($up, true, false, false, false, '');



$tbl = '
<table cellspacing="0" cellpadding="1" border="1">
    
	<tr>
	  <td  align="center" valign="middle" ><strong>3.- Situacion Relativa de la Parcela </strong></td>
  </tr>
	<tr>
	  <td   align="center" valign="middle" >&nbsp;<img src="./parcela.png" width="" height="200"></td>
  </tr>
</table>
';

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

$tbl = '
<table cellspacing="0" cellpadding="1" border="1">
    
	<tr>
	  <td colspan="5"  align="center" valign="middle" ><strong>4.- Croquis de Levantamiento Parcelero </strong></td>
  </tr>
	<tr>
	  <td colspan="5"   align="center" valign="middle">&nbsp;
		  <br />
		  <br />
		  <br />
		   <br />
		  <br />
		  <br />
		   <br />
		  <br />
		  <br />
		   <br />
		  <br />
		  <br />
		   <br />
		  <br />
		  <br />	  
	  </td>
  </tr>
	<tr>
	  <td colspan="5" align="justify" valign="middle">&nbsp;Observaci&oacute;n: '.$fichaobservacion.'</td>
  </tr>
	
	<tr>
	  <td width="212" rowspan="2"   align="justyfi" valign="middle">&nbsp;Fecha de Primera Visita:<br>&nbsp;'.$fechavisita.'</td>
      <td colspan="2"   align="center" valign="middle" width="212">Elaborado Por: </td>
      <td colspan="2"   align="center" valign="middle" width="213">Revisado Por:</td>
  </tr>
	<tr>
	  <td width="106"   align="center" valign="middle"  h>Nombre</td>
      <td width="106"   align="center" valign="middle"  h>C.I.</td>
      <td width="106"   align="center" valign="middle"  h>Nombre</td>
      <td width="107"   align="center" valign="middle"  h>C.I.</td>
  </tr>
	<tr>
	  <td width="212"   align="justyfi" valign="middle" >&nbsp;Fecha del Levantamiento: <br>&nbsp;'.$fechalevantamiento.'</td>
      <td   align="center" valign="middle"  width="106">&nbsp;'.$pen1.' '.$pen2.' '.$pea1.' '.$pea2.'</td>
      <td   align="center" valign="middle"  width="106">&nbsp;'.$penacionalidad.'-'.$pecedula.'</td>
      <td   align="center" valign="middle" width="106">&nbsp;'.$prn1.' '.$prn2.' '.$pra1.' '.$pra2.'</td>
      <td   align="center" valign="middle"  width="107">&nbsp;'.$prnacionalidad.'-'.$prcedula.'</td>
  </tr>
</table>

';

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->AddPage();

$avl = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 40px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 40px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 40px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 40px;">
        	<b> OFicina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 50px;">
        	<b> Acta de Verificación de Linderos </b>
        </td>
    </tr>   
</table>
<br><br>
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="justify" valign="middle" style="font-size: 40px;">Por medio de la presente se deja constancia que el día 
        	'.$fechalevantamiento.', los funcionarios:&nbsp;'.$pen1.' '.$pen2.' '.$pea1.' '.$pea2.'; C.I.N°&nbsp;'.$penacionalidad.'-'.$pecedula.'; y '.$prn1.' '.$prn2.' '.$pra1.' '.$pra2.'; C.I.N°&nbsp;'.$prnacionalidad.'-'.$prcedula.'; en representación de la Dirección de Catastro Municipal del Municipio: Ribero, del Estado: Sucre, ejecutaron la verificación de los linderos del inmueble identificado  con el Código Catastral N°:&nbsp; U-'.$numerocatastral.', ubiado: en la Parroquia: 
        		'.$nombre_parroquia.', Sector:&nbsp;'.$sector_nombre.', en la '.$nombre_calle.', Urbanizacion: '.$urbanizacion.', todo de conformidad con lo estipulado en los artículos 33 y 35 de la ley de geografía, Cartografía y Catastro Nacional, publicada en Gaceta Oficial N° 37.002.<br><br>
        	Se deja constancia de todo lo observado, además de lo asentado en la ficha catastral, en relación a:<br><br>
        	Servidumbre: ____________________________________________________________________<br><br>
        	Alteraciones de linderos: ___________________________________________________________<br><br>
        	Accidentes geográficos: ___________________________________________________________<br><br>
        	Otras circunstancias interés: ________________________________________________________<br><br>
        	Los abajo firmantes manifiestan su conformidad o inconformidad en cuanto a la exactitud y contenido de los datos reflejados en la ficha catastral.<br>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="2" width="50%" align="justyfi" valign="middle" style="font-size: 40px;">En se&ntilde;al de Conformidad: </td>
        
        <td align="justyfi" width="50%" valign="middle" style="font-size: 40px;">En se&ntilde;al de Inconformidad:</td>
    </tr>
    <tr>
       
        <td colspan="2" bordercolor="#000000" width="50%" align="justyfi" valign="middle" style="font-size: 40px; border:solid 1px #000;">&nbsp;Firma: <br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Propietario u Ocupante</td>
        
        <td align="justyfi" bordercolor="#000000" width="50%" valign="middle" style="font-size: 40px; border:solid 1px #000;">&nbsp;Firma: <br><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Propietario u Ocupante</td>
    </tr> 
   
    <tr>
        
        <td colspan="3"  align="justyfi" valign="middle" style="font-size: 40px;">
        	 <br><br>En caso de inconformidad explique el (los) motivo (s)________________________________________________________________________________________________________________________________________________________________________________________________________<br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3" width="100%"  align="justyfi" style="font-size: 35px; border:solid 1px #000;">
        	<br><br><br><b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Firma de Auxiliares Catastrales Responsables y Sello de la OFicina Municipal de Catastro </b>
        </td>
    </tr> 
    <tr>
        
        <td colspan="3"  align="justyfi" valign="middle" style="font-size:40px;">
        	<br><br><b>Nota:</b>&nbsp; La información suministrada por el Propietario u Ocupante tendrá carácter de Declaración Jurada y será cptejada con la documentación respectiva, para todos los efectos legales correspondientes.
        </td>
    </tr> 
    <!-- <tr>
        
        <td colspan="3"  align="justyfi" valign="middle" style="font-size:40px;">
        	<img src="./pie.png">
        </td>
    </tr>   -->
</table>';

$pdf->writeHTML($avl, true, false, false, false, '');

// -----------------------------------------------------------------------------
$pdf->LastPage();

//Close and output PDF document
$pdf->Output('Ficha Catastral.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+



