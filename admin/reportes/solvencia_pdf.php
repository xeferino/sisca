<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if(isset($_GET['id']))
{
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$id =  $_GET['id'];
	$title = "Solvencia Municipal";
	$query=pg_query("SELECT * FROM view_solvencia_municipal where id='$id'");
	$reg=pg_fetch_array($query);
					$id=$reg['id'];
					$nacionalidad=$reg['nacionalidad'];
					$cedula=$reg['cedula'];
					$nombre1=$reg['nombre1'];
					$nombre1=$reg['nombre2'];
					$apellido1=$reg['apellido1'];
					$apellido1=$reg['apellido2'];

					$monto=$reg['monto'];
					$estatus=$reg['estatus'];
					$fecha=$reg['fecha'];
					$anualidad=$reg['anualidad'];
					
}


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);

$pdf->AddPage();

// Set some content to print
$html = '
	<img src="./cintillo.png">
	<h5 align="center">Solvencia municipal</h5>
	<br>
	<h5 align="center">Sobre propiedad inmobiliaria</h5>
	
	<p align="justify">El suscrito director de hacienda publica municipal de la alcaldía del municipio ribero estado sucre, una vez revisado estado de cuentas presentado por la oficina de tributación.
</p>
<h5 align="center">HACE CONSTAR</h5>
<br>
	<p align="justify">
	Que el o la ciudadadano(a): <b>'.$nombre1.' '.$nombre2.' '.$apellido1.' '.$apellido2.'</b>, Titular de la Cedula de identidad Nº <b>'.$nacionalidad.'-'.$cedula.', SE ENCUENTRA SOLVENTE DE IMPUESTOS MUNICIPALES CON ESTA ADMINISTRACION PARA EL AÑO: '.$anualidad.'.</b>		
		<br>
		<br>

		Constancia que se expide a solicitud de parte interesada en Cariaco, a los '.$d.' días del mes de '.$m.' del '.$a.'.
		
		<br>
		<br>
		<div align="center">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<b>Atentamente,

			<br>
			<br>
		</div>
	</p>
	
	<img src="./pie.png">
';

	
	



// Print text using writeHTMLCell()
$pdf->writeHTML($html, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Solvencia.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
