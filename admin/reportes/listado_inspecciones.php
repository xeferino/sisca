<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if (isset($_POST['submit']) && $_POST['submit'] == 'pdf') {

	$fechainicio = $_POST['fechainicio'];
	$fechafin = $_POST['fechafin'];
	$tipo = $_POST['tipo'];

	if($tipo=="todos"){
		$tipo_nombre = "Todos";
		$query=pg_query("SELECT
							ip.tipo,
							ip.observaciones,
							ip.fecha,
							pp.nacionalidad as ppn,
							pp.cedula as ppc,
							pp.nombre1 as ppn1,
							pp.nombre2  as ppn2,
							pp.apellido1 as ppa1,
							pp.apellido2 as ppa2,
							pp1.nacionalidad as pp1n,
							pp1.cedula as pp1c,
							pp1.nombre1 as pp1n1,
							pp1.nombre2  as pp1n2,
							pp1.apellido1 as pp1a1,
							pp1.apellido2 as pp1a2
						FROM
							tb_inspecciones as ip
						LEFT JOIN tb_persona as pp on pp.id = ip.id_inspector
						LEFT JOIN tb_persona_inspeccion as ppi on ppi.id_inspeccion = ip.id
						LEFT JOIN tb_persona as pp1 on pp1.id = ppi.id_persona
						WHERE
							ip.fecha BETWEEN '".$fechainicio."'
						AND '".$fechafin."'"
				);
	}elseif ($tipo!="todos") {
		# code...
		$tipo_nombre =  $tipo;
		$query=pg_query("SELECT
							ip.tipo,
							ip.observaciones,
							ip.fecha,
							pp.nacionalidad as ppn,
							pp.cedula as ppc,
							pp.nombre1 as ppn1,
							pp.nombre2  as ppn2,
							pp.apellido1 as ppa1,
							pp.apellido2 asppa2,
							pp1.nacionalidad as pp1n,
							pp1.cedula as pp1c,
							pp1.nombre1 as pp1n1,
							pp1.nombre2  as pp1n2,
							pp1.apellido1 as pp1a1,
							pp1.apellido2 as pp1a2
						FROM
							tb_inspecciones as ip
						LEFT JOIN tb_persona as pp on pp.id = ip.id_inspector
						LEFT JOIN tb_persona_inspeccion as ppi on ppi.id_inspeccion = ip.id
						LEFT JOIN tb_persona as pp1 on pp1.id = ppi.id_persona
						WHERE
							ip.fecha BETWEEN '".$fechainicio."'
						AND '".$fechafin."' and 
						ip.tipo = '".$tipo."'");
	}

	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$title = "Listado de Inspecciones";
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sisca');
$pdf->SetSubject('PDF');
$pdf->SetTitle($title);
$pdf->SetKeywords('Listado de Inspecciones');
$cintillo = "pie.png";

$pdf->SetHeaderData($cintillo, "184", "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'República Bolivariana de Venezuela', '', 0, 'C', true, 0, false, false, 0);

//$pdf->Write(0, 'Ficha Catastral', '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica', '', 8);



// -----------------------------------------------------------------------------
#DATOS GENERALES
$encabezado = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<br><br><b> Listado de Inspecciones para el rango de fecha: '.$fechainicio.' - '.$fechafin.' Tipo de Inspeccion: '.$tipo_nombre.'</b>
        </td>
    </tr>   
</table>
';

$html .= '
<table cellspacing="0" cellpadding="1" border="1" width="100%">
	<thead>
		<tr align="center" bgcolor="#eee" style="font-weight:bold;">
			<th width = "5%">ID</th>
			<th width = "20%">Inspector</th>
			<th width = "15%">Tipo</th>
			<th width = "20%">Observaciones</th>
			<th width = "20%">Persona</th>
			<th width = "20%">Fecha</th>
		</tr>
	<thead>
	<tbody>';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		
		$tipo = $row['tipo'];
		$observaciones = $row['observaciones'];
		$fecha = $row['fecha'];
		$ppn = $row['ppn'];
		$ppc = $row['ppc'];
		$ppn1 = $row['ppn1'];
		$ppn2 = $row['ppn2'];
		$ppa1 = $row['ppa1'];
		$ppa2 = $row['ppa2'];
		$pp1n = $row['pp1n'];
		$pp1c = $row['pp1c'];
		$pp1n1 = $row['pp1n1'];
		$pp1n2 = $row['pp1n2'];
		$pp1a1 = $row['pp1a1'];
		$pp1a2 = $row['pp1a2'];

		$fecha1 = explode("-", $fecha);
		$fecha_c = $fecha1[1]."/".$fecha1[2]."/".$fecha1[0];

		$id ++;
		$total +=1;
$html .= '
	<tr align="center">
		<td width = "5%">'.$id.'</td>
		<td width = "20%">'.$ppn.'-'.$ppc.' '.$ppn1.' '.$ppn2.' '.$ppa1.' '.$ppa2.'</td>
		<td width = "15%">'.$tipo.'</td>
		<td width = "20%">'.$observaciones.'</td>
		<td width = "20%">'.$pp1n.'-'.$pp1c.' '.$pp1n1.' '.$pp1n2.' '.$pp1a1.' '.$pp1a2.'</td>
		<td width = "20%">'.$fecha_c.'</td>
	</tr>';
	}
$html .= '

	<tr align="center" border="0">
		<td  width = "">&nbsp; Total General ('.$total.') de las Inspecciones consultadas segun los criterios de busqueda del reporte</td>
	</tr>
<tbody> 
</table>';

$pdf->writeHTML($encabezado, true, false, false, false, '');
$pdf->writeHTML($html, true, false, false, false, '');




// -----------------------------------------------------------------------------
$pdf->LastPage();

//Close and output PDF document
$pdf->Output('Listado de Inspecciones.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+



