<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');
/*if ($_POST) {
*/	
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$fechainicio = $_POST['fechainicio'];
	$fechafin = $_POST['fechafin'];
	$sector = $_POST['sector'];

	$ptopografia = $_POST['topografia'];
	$pacceso = $_POST['acceso'];
	$pforma = $_POST['forma'];
	$pubicacion = $_POST['ubicacion'];
	$pentorno = $_POST['entorno_fisico'];
	$pmejoras = $_POST['mejoras_terreno'];
	$ptenencia = $_POST['tenencia_terreno'];
	$ppropiedad = $_POST['regimen_propiedad'];
	$puso = $_POST['uso_actual_terreno'];
	$pgraficatorta = $_POST['graficatorta'];
	$pgraficabarra = $_POST['graficabarrahorizontal'];
	$pgraficalinea = $_POST['graficalinea'];

	//var_dump($pgraficabarra);die();

	$title = "Graficas de Terrenos";					
	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_terreno AS tr ON tr. ID = im.idterreno
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");

	$pedul= pg_query("SELECT * FROM  tb_pedul WHERE id='".$sector."'");
														
	$reg=pg_fetch_object($pedul);

	$nombre = $reg->nombre;


	$validar = pg_num_rows($sql);

	##GRAFICA DE BARRA DE TORTA TERRENO
	if($validar>0 and $pgraficatorta =="1"){

	require_once('libchart/libchart/classes/libchart.php');
	$chart = new PieChart(1000, 500);
	$dataSet = new XYDataSet();

	$topografia = 0;
	$acceso = 0;
	$forma = 0;
	$ubicacion = 0;
	$entorno = 0;
	$mejoras = 0;
	$tenencia = 0;
	$propiedad = 0;
	$uso = 0;

	while($fila = pg_fetch_array($sql)){
		foreach ($_POST as $key => $value) {
			if($fila['topografia'] == $value){$topografia++;}
			if($fila['acceso'] == $value){$acceso++;}
			if($fila['forma'] == $value){$forma++;}
			if($fila['ubicacion'] == $value){$ubicacion++;}
			if($fila['entornofisico'] == $value){$entorno++;}
			if($fila['mejorasterreno'] == $value){$mejoras++;}
			if($fila['teneciaterreno'] == $value){$tenencia++;}
			if($fila['registropropiedad'] == $value){$propiedad++;}
			if($fila['usoactual'] == $value){$uso++;}
		}
	}

	$dataSet->addPoint(new Point("TOPOGRAFIA :".$ptopografia." (".$topografia.")", $topografia));
	$dataSet->addPoint(new Point("ACCESO :".$pacceso." (".$acceso.")", $acceso));
	$dataSet->addPoint(new Point("FORMA :".$pforma." (".$forma.")", $forma));
	$dataSet->addPoint(new Point("UBICACION :".$pubicacion." (".$ubicacion.")", $ubicacion));
	$dataSet->addPoint(new Point("ENTORNO FISICO :".$pentorno." (".$entorno.")", $entorno));
	$dataSet->addPoint(new Point("MEJORAS AL TERRENO :".$pmejoras." (".$mejoras.")", $mejoras));
	$dataSet->addPoint(new Point("TENENCIA TERRENO :".$ptenencia." (".$tenencia.")", $tenencia));
	$dataSet->addPoint(new Point("REG. DE PROPIEDAD :".$ppropiedad." (".$propiedad.")", $propiedad));
	$dataSet->addPoint(new Point("USO ACTUAL :".$puso." (".$uso.")", $uso));
	$chart->setDataSet($dataSet);
	$chart->getPlot()->setGraphPadding(new Padding(5, 30, 100, 160));
	$chart->setTitle("");
	$grafica1 = $chart->render("./libchart/demo/generated/grafica_torta.png");
	}//fin grafica torta

	##GRAFICA DE BARRA HORIZONTAL TERRENO
	if($validar>0 and $pgraficabarra =="2"){


	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_terreno AS tr ON tr. ID = im.idterreno
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");


	require_once('libchart/libchart/classes/libchart.php');
	$chart2 = new VerticalBarChart();
	$dataSet2 = new XYDataSet();

	$topografia = 0;
	$acceso = 0;
	$forma = 0;
	$ubicacion = 0;
	$entorno = 0;
	$mejoras = 0;
	$tenencia = 0;
	$propiedad = 0;
	$uso = 0;

	while($row = pg_fetch_array($sql)){
		foreach ($_POST as $key => $value) {
			//echo $key.": ".$value."<br>";
			if($row['topografia'] == $value){$topografia++;}
			if($row['acceso'] == $value){$acceso++;}
			if($row['forma'] == $value){$forma++;}
			if($row['ubicacion'] == $value){$ubicacion++;}
			if($row['entornofisico'] == $value){$entorno++;}
			if($row['mejorasterreno'] == $value){$mejoras++;}
			if($row['teneciaterreno'] == $value){$tenencia++;}
			if($row['registropropiedad'] == $value){$propiedad++;}
			if($row['usoactual'] == $value){$uso++;}
		}
	}

	$dataSet2->addPoint(new Point("ENT. FIS. :".$pentorno."", $entorno));
	$dataSet2->addPoint(new Point("ACCESO :".$pacceso."", $acceso));
	$dataSet2->addPoint(new Point("FORMA :".$pforma."", $forma));
	$dataSet2->addPoint(new Point("UBICACION :".$pubicacion."", $ubicacion));
	$dataSet2->addPoint(new Point("TOPOGRAFIA :".$ptopografia."", $topografia));
	$dataSet2->addPoint(new Point("MEJORAS :".$pmejoras."", $mejoras));
	$dataSet2->addPoint(new Point("TENENCIA :".$ptenencia."", $tenencia));
	$dataSet2->addPoint(new Point("PROPIEDAD :".$ppropiedad."", $propiedad));
	$dataSet2->addPoint(new Point("USO :".$puso."", $uso));
	
	$chart2->setDataSet($dataSet2);
	$chart2->getPlot()->setGraphPadding(new Padding(5, 30, 100, 160));
	

	$chart2->setTitle("");
	$grafica2 = $chart2->render("./libchart/demo/generated/grafica_barra_horizontal.png");

	}//fin grafica DE BARRA


	##GRAFICA DE linea TERRENO
	if($validar>0 and $pgraficalinea =="3"){


	$sql = pg_query("SELECT
						*
						FROM
						tb_inmueble AS im
						LEFT JOIN tb_ficha_catastral AS fc ON fc. ID = im.idfichacatastral
						LEFT JOIN tb_terreno AS tr ON tr. ID = im.idterreno
						LEFT JOIN tb_ubicacion_comunitaria AS uc ON uc. ID = im.idubicacioncomunitaria
						LEFT JOIN tb_pedul AS sc ON sc. ID = uc.sector
						WHERE
						uc.sector = '".$sector."'
						AND fc.fechainscripcion BETWEEN '".$fechainicio."'
						AND '".$fechafin."'
	");


	require_once('libchart/libchart/classes/libchart.php');
	$chart3 = new LineChart();
	$dataSet3 = new XYDataSet();

	$topografia = 0;
	$acceso = 0;
	$forma = 0;
	$ubicacion = 0;
	$entorno = 0;
	$mejoras = 0;
	$tenencia = 0;
	$propiedad = 0;
	$uso = 0;

	while($row = pg_fetch_array($sql)){
		foreach ($_POST as $key => $value) {
			//echo $key.": ".$value."<br>";
			if($row['topografia'] == $value){$topografia++;}
			if($row['acceso'] == $value){$acceso++;}
			if($row['forma'] == $value){$forma++;}
			if($row['ubicacion'] == $value){$ubicacion++;}
			if($row['entornofisico'] == $value){$entorno++;}
			if($row['mejorasterreno'] == $value){$mejoras++;}
			if($row['teneciaterreno'] == $value){$tenencia++;}
			if($row['registropropiedad'] == $value){$propiedad++;}
			if($row['usoactual'] == $value){$uso++;}
		}
	}

	$dataSet3->addPoint(new Point("ENT. FIS. :".$pentorno."", $entorno));
	$dataSet3->addPoint(new Point("ACCESO :".$pacceso."", $acceso));
	$dataSet3->addPoint(new Point("FORMA :".$pforma."", $forma));
	$dataSet3->addPoint(new Point("UBICACION :".$pubicacion."", $ubicacion));
	$dataSet3->addPoint(new Point("TOPOGRAFIA :".$ptopografia."", $topografia));
	$dataSet3->addPoint(new Point("MEJORAS :".$pmejoras."", $mejoras));
	$dataSet3->addPoint(new Point("TENENCIA :".$ptenencia."", $tenencia));
	$dataSet3->addPoint(new Point("PROPIEDAD :".$ppropiedad."", $propiedad));
	$dataSet3->addPoint(new Point("USO :".$puso."", $uso));
	
	$chart3->setDataSet($dataSet3);
	$chart3->getPlot()->setGraphPadding(new Padding(5, 20, 110, 100));
	

	$chart3->setTitle("");
	$grafica3 = $chart3->render("./libchart/demo/generated/grafica_lineas.png");

	}//fin grafica DE lineas

/*}*/


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);




$pdf->AddPage();

// Set some content to print
	
if($validar>0){

	###grafica de torta

	if($pgraficatorta ==NULL)
	{
		$html = '';
	}elseif($pgraficatorta =="1"){
		$html = '
			<center>
			<h3>Condiciones de Terreno para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_torta.png">
			</center>
		';
	}

	if($pgraficabarra == NULL)
	{
		$html1 = '';
	}elseif($pgraficabarra =="2"){
		$html1 = '
			<center>
			<h3>Condiciones de Terreno para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_barra_horizontal.png">
			</center>
		';
	}

	if($pgraficalinea == NULL)
	{
		$html2 = '';
	}elseif($pgraficalinea =="3"){
		$html2 = '
			<center>
			<h3>Condiciones de Terreno para el Sector '.$nombre.'</h3>
			<h5>Para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.'</h5>
			<img src="./libchart/demo/generated/grafica_lineas.png">
			</center>
		';
	}
}
else{

	$html = '
	<p aling="center">No hay Resultados Obtenidos para el Rango de Fecha Desde: '.$fechainicio.' Hasta: '.$fechafin.' para el sector '.$nombre.'</p>
';
}

if($pgraficatorta ==NULL and $pgraficalinea == NULL and $pgraficabarra == NULL){

	$html4 = '
	<p aling="center">No hay Resultados Obtenidos, Debido a que no Seleccionó Ningún tipo de Gráfica a Mostrar... </p>';

}



	



// Print text using writeHTMLCell()
$pdf->writeHTML($html4, true, 0, true, 0);
$pdf->writeHTML($html, true, 0, true, 0);
$pdf->writeHTML($html1, true, 0, true, 0);

	if($pgraficatorta =="1" and $pgraficabarra==2){
		$pdf->AddPage();
	}

$pdf->writeHTML($html2, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('graficas.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
