<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if(isset($_GET['id']))
{
	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$id =  $_GET['id'];
	$title = "Certificacion de Linderos";
	$query=pg_query("SELECT
					fc.numeroarchivo,
					fc.numerocatastral,
					fc.fechainscripcion,
					fc.fechalevantamiento,
					fc.fechavisita,
					fc.observaciones AS fichaobservacion,
					tr. ID AS idterreno,
					tr.topografia,
					tr.acceso,
					tr.forma,
					tr.ubicacion,
					tr.entornofisico,
					tr.mejorasterreno,
					tr.teneciaterreno,
					tr.registropropiedad,
					tr.usoactual,
					tr.linderonorte,
					tr.linderosur,
					tr.linderoeste,
					tr.linderooeste,
					tr.coordenadasutmnorte,
					tr.coordenadasutmeste,
					tr.coordenadasutmhuso,
					tr.areametroscuadrados,
					tr.valorunitariometroscuadrados,
					tr.factorajustearea,
					tr.factorajusteforma,
					tr.sector,
					tr.valorajustadometroscuadrados,
					tr.valortotal,
					tr.largo_terreno,
					tr.ancho_terreno,
					tr.observacioneconomica as observacionterreno,
					ct.tipo,
					ct.descripcionuso,
					ct.teneciaconstruccion,
					ct.regimenpropiedad,
					ct.soporteestructural,
					ct.techoestructural,
					ct.cubiertainterna,
					ct.cubiertaexterna,
					ct.paredestipo,
					ct.paredesacabado,
					ct.paredespintura,
					ct.paredesinstelectricas,
					ct.pisotipo,
					ct.banowc,
					ct.banocomunitario,
					ct.banoducha,
					ct.banolavamanos,
					ct.banobanera,
					ct.banobidet,
					ct.banoletrina,
					ct.banoceramica1,
					ct.banoceramica2,
					ct.puertametalica,
					ct.puertamaderacepillada,
					ct.puertamadeconomica,
					ct.puertaentamboradafina,
					ct.puertaseguridad,
					ct.puertaotra,
					ct.puertaotrac,
					ct.ventanalaluminio,
					ct.ventanalhierro,
					ct.ventanalmadera,
					ct.basculantealuminio,
					ct.basculantehierro,
					ct.basculantemadera,
					ct.satientealuminio,
					ct.satientehierro,
					ct.satientemadera,
					ct.celosiaaluminio,
					ct.celosiahierro,
					ct.celosiamadera,
					ct.correderaaluminio,
					ct.correderahierro,
					ct.correderamadera,
					ct.panoramicaaluminio,
					ct.panoramicahierro,
					ct.panoramicamadera,
					ct.estadoconservacion,
					ct.anoconstruccion,
					ct.porcentajerefaccion,
					ct.numeroniveles,
					ct.anorefaccion,
					ct.edadefectiva,
					ct.numeroedificacion,
					ct.observacionesconts as observacionconstruccion,
					ct.tipologia,
					ct.areametrocuadrado,
					ct.valorareacuadrada,
					ct.areatotal,
					ct.porcentajedepreciacion,
					ct.valoractual,
					ct.valortotal as valortotalconstruccion,
					ct.valorinmueble,
					ct.valorconstruccion,
					ct.observacioneconomica as obsecoconstruccion,
					rp.numero,
					rp.folio,
					rp.tomo,
					rp.protocolo,
					rp.fecha as fecharp,
					rp.aream2terreno,
					rp.aream2cont,
					rp.valorbs,
					uc.numero_rmv,
					uc.region_consejofg,
					uc.comuna,
					uc.comite_tierrau,
					uc.consejo_comunal,
					uc.ciudad,
					uc.localidad,
					uc.urbanizacion,
					uc.consejo_residencial,
					uc.barrio,
					uc.numero_civico,
					uc.puntoreferencia,
					uc.manzana,
					uc.parroquia,
					uc.sector,
					uc.numero_comuna,
					uc.numero_comite_urbano,
					uc.numero_consejo_comunal,
					uc.tipo_calle,
					uc.nombre_calle,
					sec.nombre as sector_nombre,
					pa.nombre as nombre_parroquia,
					pp.nacionalidad as ppnacionalidad,
					pp.cedula as ppcedula,
					pp.nombre1 as ppn1,
					pp.nombre2 as ppn2,
					pp.apellido1 as ppa1,
					pp.apellido2 as ppa2,
					pp.telefono1 as telefono1pp,
					pp.telefono2 as telefono2pp,
					pp.correo as emailpp,
					po.nacionalidad as ponacionalidad,
					po.cedula as pocedula,
					po.nombre1 as pon1,
					po.nombre2 as pon2,
					po.apellido1 as poa1,
					po.apellido2 as poa2,
					po.telefono1 as telefono1po,
					po.telefono2 as telefono2po,
					po.correo as emailpo,
					pe.nacionalidad as penacionalidad,
					pe.cedula as pecedula,
					pe.nombre1 as pen1,
					pe.nombre2 as pen2,
					pe.apellido1 as pea1,
					pe.apellido2 as pea2,
					pr.nacionalidad as prnacionalidad,
					pr.cedula as prcedula,
					pr.nombre1 as prn1,
					pr.nombre2 as prn2,
					pr.apellido1 as pra1,
					pr.apellido2 as pra2,
					dpp.ciudad as dppciudad,
					dpp.localidad as dpplocalidad,
					dpp.urbanizacion as dppurbanizacion,
					dpp.consejo_residencial as dppconsejo_residencial,
					dpp.barrio as dppbarrio,
					dpp.numero_civico as dppnumero_civico,
					dpp.puntoreferencia as dpppuntoreferencia,
					dpp.parroquia as dppparroquia,
					dpp.sector as dppsector,
					dpp.tipo_calle as dpptipocalle,
					dpp.nombre_calle as dppnombrecalle
					FROM
					tb_inmueble as ib
					LEFT JOIN tb_ficha_catastral as fc on fc. id = ib.idfichacatastral
					LEFT JOIN tb_terreno as tr on tr. id = ib.idterreno
					LEFT JOIN tb_construccion as ct on ct. id = ib.idconstruccion
					LEFT JOIN tb_registro_publico as rp on rp. id = ib.idregistro
					LEFT JOIN tb_ubicacion_comunitaria as uc on uc. id = ib.idubicacioncomunitaria
					LEFT JOIN tb_pedul as sec on sec.id = uc.sector
					LEFT JOIN tb_parroquia as pa on pa.codigo = uc.parroquia
					LEFT JOIN tb_persona as pp on pp. id = ib.idpropietario
					LEFT JOIN tb_persona as po on po. id = ib.idocupante
					LEFT JOIN tb_persona as pe on pe. id = fc.elaboradopor
					LEFT JOIN tb_persona as pr on pr. id = fc.revisadopor
					LEFT JOIN tb_direccion_persona as dpp on dpp.idpersona = pp. id
					where
					fc. id = '$id'"
				);
	$row=pg_fetch_array($query);
	$numeroarchivo = $row['numeroarchivo'];
	$numerocatastral = $row['numerocatastral'];
	$fechainscripcion = $row['fechainscripcion'];
	$fechalevantamiento = $row['fechalevantamiento'];
	$fechavisita = $row['fechavisita'];
	$fichaobservacion = $row['fichaobservacion'];
	$idterreno = $row['idterreno'];
	$topografia = $row['topografia'];
	$acceso = $row['acceso'];
	$forma = $row['forma'];
	$ubicacion = $row['ubicacion'];
	$entornofisico = $row['entornofisico'];
	$mejorasterreno = $row['mejorasterreno'];
	$teneciaterreno = $row['teneciaterreno'];
	$registropropiedad= $row['registropropiedad'];
	$usoactual= $row['usoactual'];
	$linderonorte= $row['linderonorte'];
	$linderosur= $row['linderosur'];
	$linderoeste= $row['linderoeste'];
	$linderooeste= $row['linderooeste'];
	$coordenadasutmnorte= $row['coordenadasutmnorte'];
	$coordenadasutmeste= $row['coordenadasutmeste'];
	$coordenadasutmhuso= $row['coordenadasutmhuso'];
	$areametroscuadrados= $row['areametroscuadrados'];
	$valorunitariometroscuadrados= $row['valorunitariometroscuadrados'];
	$factorajustearea= $row['factorajustearea'];
	$factorajusteforma= $row['factorajusteforma'];
	$sector= $row['sector'];
	$numero_comuna = $row['numero_comuna'];
	$numero_comite_urbano = $row['numero_comite_urbano'];
	$numero_consejo_comunal = $row['numero_consejo_comunal'];
	$tipo_calle = $row['tipo_calle'];
	$nombre_calle = $row['nombre_calle'];
	$sector_nombre= $row['sector_nombre'];
	$valorajustadometroscuadrados= $row['valorajustadometroscuadrados'];
	$valortotal= $row['valortotal'];
	$observacionterreno = $row['observacionterreno'];
	$tipo= $row['tipo'];
	$descripcionuso= $row['descripcionuso'];
	$teneciaconstruccion= $row['teneciaconstruccion'];
	$regimenpropiedad= $row['regimenpropiedad'];
	$soporteestructural= $row['soporteestructural'];
	$techoestructural= $row['techoestructural'];
	$cubiertainterna= $row['cubiertainterna'];
	$cubiertaexterna= $row['cubiertaexterna'];
	$paredestipo= $row['paredestipo'];
	$paredesacabado= $row['paredesacabado'];
	$paredespintura= $row['paredespintura'];
	$paredesinstelectricas= $row['paredesinstelectricas'];
	$pisotipo= $row['pisotipo'];
	$banowc= $row['banowc'];
	$banocomunitario= $row['banocomunitario'];
	$banoducha= $row['banoducha'];
	$banolavamanos= $row['banolavamanos'];
	$banobanera= $row['banobanera'];
	$banobidet= $row['banobidet'];
	$banoletrina= $row['banoletrina'];
	$banoceramica1= $row['banoceramica1'];
	$banoceramica2= $row['banoceramica2'];
	$puertametalica= $row['puertametalica'];
	$puertamaderacepillada= $row['puertamaderacepillada'];
	$puertamadeconomica= $row['puertamadeconomica'];
	$puertaentamboradafina= $row['puertaentamboradafina'];
	$puertaseguridad= $row['puertaseguridad'];
	$puertaotra= $row['puertaotra'];
	$puertaotrac= $row['puertaotrac'];
	$ventanalaluminio= $row['ventanalaluminio'];
	$ventanalhierro= $row['ventanalhierro'];
	$ventanalmadera= $row['ventanalmadera'];
	$basculantealuminio= $row['basculantealuminio'];
	$basculantehierro= $row['basculantehierro'];
	$basculantemadera= $row['basculantemadera'];
	$satientealuminio= $row['satientealuminio'];
	$satientehierro= $row['satientehierro'];
	$satientemadera= $row['satientemadera'];
	$celosiaaluminio= $row['celosiaaluminio'];
	$celosiahierro= $row['celosiahierro'];
	$celosiamadera= $row['celosiamadera'];
	$correderaaluminio= $row['correderaaluminio'];
	$correderahierro= $row['correderahierro'];
	$correderamadera= $row['correderamadera'];
	$panoramicaaluminio= $row['panoramicaaluminio'];
	$panoramicahierro= $row['panoramicahierro'];
	$panoramicamadera= $row['panoramicamadera'];
	$estadoconservacion= $row['estadoconservacion'];
	$anoconstruccion= $row['anoconstruccion'];
	$porcentajerefaccion= $row['porcentajerefaccion'];
	$numeroniveles= $row['numeroniveles'];
	$anorefaccion= $row['anorefaccion'];
	$edadefectiva= $row['edadefectiva'];
	$numeroedificacion= $row['numeroedificacion'];
	$observacionconstruccion = $row['observacionconstruccion'];
	$tipologia= $row['tipologia'];
	$areametrocuadrado= $row['areametrocuadrado'];
	$valorareacuadrada= $row['valorareacuadrada'];
	$areatotal= $row['areatotal'];
	$porcentajedepreciacion= $row['porcentajedepreciacion'];
	$valoractual= $row['valoractual'];
	$valortotalconstruccion = $row['valortotalconstruccion'];
	$valorinmueble= $row['valorinmueble'];
	$valorconstruccion= $row['valorconstruccion'];
	$obsecoconstruccion = $row['obsecoconstruccion'];
	$numero= $row['numero'];
	$folio= $row['folio'];
	$tomo= $row['tomo'];
	$protocolo= $row['protocolo'];
	$fecharp= $row['fecharp'];
	$aream2terreno= $row['aream2terreno'];
	$aream2cont= $row['aream2cont'];
	$valorbs= $row['valorbs'];
	$numero_rmv= $row['numero_rmv'];
	$region_consejofg= $row['region_consejofg'];
	$comuna= $row['comuna'];
	$comite_tierrau= $row['comite_tierrau'];
	$consejo_comunal= $row['consejo_comunal'];
	$ciudad= $row['ciudad'];
	$localidad= $row['localidad'];
	$urbanizacion= $row['urbanizacion'];
	$consejo_residencial= $row['consejo_residencial'];
	$barrio= $row['barrio'];
	$numero_civico= $row['numero_civico'];
	$puntoreferencia= $row['puntoreferencia'];
	$manzana= $row['manzana'];
	$parroquia= $row['parroquia'];
	$nombre_parroquia= $row['nombre_parroquia'];
	$sector= $row['sector'];
	$ppnacionalidad = $row['ppnacionalidad'];
	$ppcedula = $row['ppcedula'];
	$ppn1 = $row['ppn1'];
	$ppn2 = $row['ppn2'];
	$ppa1 = $row['ppa1'];
	$ppa2= $row['ppa2'];
	$pptelefono1 = $row['telefono1pp'];
	$pptelefono2 = $row['telefono2pp'];
	$ppcorreo = $row['emailpp'];
	$ponacionalidad = $row['ponacionalidad'];
	$pocedula= $row['pocedula'];
	$pon1 = $row['pon1'];
	$pon2 = $row['pon2'];
	$poa1 = $row['poa1'];
	$poa2 = $row['poa2'];
	$potelefono1 = $row['telefono1po'];
	$potelefono2 = $row['telefono2po'];
	$pocorreo = $row['emailpo'];
	$penacionalidad= $row['penacionalidad'];
	$pecedula = $row['pecedula'];
	$pen1 = $row['pen1'];
	$pen2 = $row['pen2'];
	$pea1 = $row['pea1'];
	$pea2 = $row['pea2'];
	$prnacionalidad= $row['prnacionalidad'];
	$prcedula = $row['prcedula'];
	$prn1 = $row['prn1'];
	$prn2 = $row['prn2'];
	$pra1 = $row['pra1'];
	$pra2 = $row['pra2'];
	$dppciudad = $row['dppciudad'];
	$dpplocalidad = $row['dpplocalidad'];
	$dppurbanizacion = $row['dppurbanizacion'];
	$dppconsejo_residencial = $row['dppconsejo_residencial'];
	$dppbarrio = $row['dppbarrio'];
	$dppnumero_civico = $row['dppnumero_civico'];
	$dpppuntoreferencia = $row['dpppuntoreferencia'];
	$dppparroquia = $row['dppparroquia'];
	$dppsector = $row['dppsector'];
	$dpptipocalle = $row['dpptipocalle'];
	$dppnombrecalle = $row['dppnombrecalle'];

	$largo_terreno = $row['largo_terreno'];
	$ancho_terreno = $row['ancho_terreno'];

	$fecha = explode("-", $fechainscripcion);
	$dia = $fecha[2];
	$mes = $fecha[1];
	$anio = $fecha[0];

	$fecha_c = $fecha[1]."/".$fecha[2]."/".$fecha[0];

	$codigocatastral =   explode("-", $numerocatastral);

	$hectaria = $areametroscuadrados/10000;
}


$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
$cintillo = "cintillo.png";

$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set document information
$pdf->SetTitle($title);
$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 14);

$pdf->AddPage();

// Set some content to print

//var_dump(."==".);die();
$cpo = $ponacionalidad."-".$pocedula;
$cpp = $ppnacionalidad."-".$ppcedula;
if($cpo == $cpp)
{
$html = '
	<img src="./cintillo.png">
	<h5 align="right">Oficio N° 56656</h5>
	<br>
	<h3 align="center">A QUIEN PUEDA INTERESAR</h3>
	<p align="justify">Quien suscribe Ing. Félix Aristimuño Coordinador de Catastro de la Alcaldía del Municipio Ribero del Estado Sucre, por medio de la presente CERTIFICA: la existencia de un lote de terreno <b>Municipal,</b> &nbsp; ocupado por el (los) Ciudadano (a): <b>'.$pon1.' '.$pon2.' '.$poa1.' '.$poa2.',</b>&nbsp;&nbsp; Titular de la Cedula de identidad Nº <b>'.$ponacionalidad.'-'.$pocedula.', &nbsp; Ubicado:</b> en la '.$nombre_calle.' N° '.$numero_civico.'de la comunidad de '.$nombre_parroquia.' Parroquia: '.$nombre_parroquia.' Municipio Ribero del Estado Sucre, asignado con el Nº Catastral <b>'.$numerocatastral.'</b> y Alinderado de la manera siguiente: 

		<ul>
			<li><b>NORTE:</b> '.$linderonorte.' ('.$largo_terreno.' mts.)</li>
			<li><b>SUR:</b> '.$linderosur.' ('.$largo_terreno.' mts.)</li>
			<li><b>ESTE:</b> '.$linderoeste.' ('.$ancho_terreno.' mts.)</li>
			<li><b>OESTE:</b> '.$linderooeste.' ('.$ancho_terreno.' mts.)</li>
		</ul>

	<b>Nota:</b> El Inmueble ante descrito esta enclavado en un area total de '.$areametroscuadrados.'.
	<br><br><br><br>
	
		Constancia que se expide a solicitud de parte interesada en Cariaco, a los '.$d.' días del mes de '.$m.' del '.$a.'.
		<br>
		<div align="center">
			<b>Atentamente,
			
			<br>
			<br>
		</div>
	</p>
	
	<img src="./pie.png">
';
}elseif($cpo != $cpp)
{
$html = '
	<img src="./cintillo.png">
	<h5 align="right">Oficio N° 56656</h5>
	<br>
	<h3 align="center">A QUIEN PUEDA INTERESAR</h3>
	<p align="justify">Quien suscribe Ing. Félix Aristimuño Coordinador de Catastro de la Alcaldía del Municipio Ribero del Estado Sucre, por medio de la presente CERTIFICA: la existencia de un lote de terreno <b>Municipal,</b> &nbsp; ocupado por el (los) Ciudadano (a): <b>'.$ppn1.' '.$ppn2.' '.$ppa1.' '.$ppa2.',</b>&nbsp;&nbsp; Titular de la Cedula de identidad Nº <b>'.$ponacionalidad.'-'.$ppcedula.', &nbsp; Ubicado:</b> en la '.$nombre_calle.' N° '.$numero_civico.'de la comunidad de '.$nombre_parroquia.' Parroquia: '.$nombre_parroquia.' Municipio Ribero del Estado Sucre, asignado con el Nº Catastral <b>'.$numerocatastral.'</b> y Alinderado de la manera siguiente: 

		<ul>
			<li><b>NORTE:</b> '.$linderonorte.' ('.$largo_terreno.' mts.)</li>
			<li><b>SUR:</b> '.$linderosur.' ('.$largo_terreno.' mts.)</li>
			<li><b>ESTE:</b> '.$linderoeste.' ('.$ancho_terreno.' mts.)</li>
			<li><b>OESTE:</b> '.$linderooeste.' ('.$ancho_terreno.' mts.)</li>
		</ul>

	<b>Nota:</b> El Inmueble ante descrito esta enclavado en un area total de '.$areametroscuadrados.'.
	<br><br><br><br>
	
		Constancia que se expide a solicitud de parte interesada en Cariaco, a los '.$d.' días del mes de '.$m.' del '.$a.'.
		<br>
		<div align="center">
			<b>Atentamente,
			
			<br>
			<br>
		</div>
	</p>
	
	<img src="./pie.png">
';
}


	
	



// Print text using writeHTMLCell()
$pdf->writeHTML($html, true, 0, true, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('Certificacion de Linderos.pdf', 'I');
$pdf->LastPage();

//============================================================+
// END OF FILE
//============================================================+
