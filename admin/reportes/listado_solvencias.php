<?php

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');
require_once('../clases/conexion.php');
require_once('../clases/linderos.php');

if (isset($_POST['submit']) && $_POST['submit'] == 'pdf') {

	$fechainicio = $_POST['fechainicio'];
	$fechafin = $_POST['fechafin'];

			$query=pg_query("SELECT
								sm.estatus,
								sm.anualidad,
								sm.fecha,
								sm.monto,
								pp.nacionalidad as ppn,
								pp.cedula as ppc,
								pp.nombre1 as ppn1,
								pp.nombre2  as ppn2,
								pp.apellido1 as ppa1,
								pp.apellido2 as ppa2
							FROM
								tb_solvencia_municipal as sm
							INNER JOIN tb_persona as pp on pp.id = sm.id_persona
							WHERE
								sm.fecha BETWEEN '".$fechainicio."'
							AND '".$fechafin."'"
			);

	$d = date('d');
	$m = date('m');
	$a = date('Y');

	if($m == 1){ $m = "Enero";}
	if($m == 2){ $m = "Febrero";}
	if($m == 3){ $m = "Marzo";}
	if($m == 4){ $m = "Abril";}
	if($m == 5){ $m = "Mayo";}
	if($m == 6){ $m = "Junio";}
	if($m == 7){ $m = "Julio";}
	if($m == 8){ $m = "Agosto";}
	if($m == 9){ $m = "Septiembre";}
	if($m == 10){ $m = "Octubre";}
	if($m == 11){ $m = "Noviembre";}
	if($m == 12){ $m = "Diciembre";}

	$title = "Listado Solvencias";
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Sisca');
$pdf->SetSubject('PDF');
$pdf->SetTitle($title);
$pdf->SetKeywords('Listado Solvencias');
$cintillo = "pie.png";

$pdf->SetHeaderData($cintillo, "184", "", array(0,64,255), array(0,64,128));
//$pdf->SetHeaderData($cintillo, "", "", array(0,64,255), array(0,64,128));
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 20);

// add a page
$pdf->AddPage();

//$pdf->Write(0, 'República Bolivariana de Venezuela', '', 0, 'C', true, 0, false, false, 0);

//$pdf->Write(0, 'Ficha Catastral', '', 0, 'C', true, 0, false, false, 0);


$pdf->SetFont('helvetica', '', 8);



// -----------------------------------------------------------------------------
#DATOS GENERALES
$encabezado = '
<table cellspacing="0" cellpadding="1" border="">
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> República Bolivariana de Venezuela  </b>
        </td>
        
    </tr>
    <tr>
       
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b>	Estado: Sucre </b>
        </td>
        
    </tr> 
    <tr>
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Alcaldía del Municipio: Ribero </b>
        </td>
       
    </tr> 
    <tr>
        
        <td colspan="3"  align="center" valign="middle" style="font-size: 35px;">
        	<b> Oficina Municipal de Catastro </b><br>
        </td>
     
    </tr> 
    <tr>
        
        <td colspan="3"  align="right" valign="middle" style="font-size: 35px;">
        	<b> Cariaco, '.$d.' de '.$m.' de '.$a.' </b>
        </td>
    </tr>

    <tr>
        
        <td colspan="3"  align="justify" valign="middle" style="font-size: 35px;">
        	<br><br><b> Listado de Solvencias para el rango de fecha: '.$fechainicio.' - '.$fechafin.'</b>
        </td>
    </tr>   
</table>
';

$html .= '
<table cellspacing="0" cellpadding="1" border="1" width="100%">
	<thead>
		<tr align="center" bgcolor="#eee" style="font-weight:bold;">
			<th width = "5%">ID</th>
			<th width = "20%">Persona</th>
			<th width = "15%">Anualidad</th>
			<th width = "20%">Monto</th>
			<th width = "20%">Estatus</th>
			<th width = "20%">Fecha</th>
		</tr>
	<thead>
	<tbody>';
	$id = 0;
	$total = 0;
	while ($row = pg_fetch_array($query)){
		
		$estatus = $row['estatus'];
		$anualidad = $row['anualidad'];
		$fecha = $row['fecha'];
		$monto = $row['monto'];
		$ppn = $row['ppn'];
		$ppc = $row['ppc'];
		$ppn1 = $row['ppn1'];
		$ppn2 = $row['ppn2'];
		$ppa1 = $row['ppa1'];
		$ppa2 = $row['ppa2'];
		

		$fecha1 = explode("-", $fecha);
		$fecha_c = $fecha1[1]."/".$fecha1[2]."/".$fecha1[0];

		$id ++;
		$total +=1;
$html .= '
	<tr align="center">
		<td width = "5%">'.$id.'</td>
		<td width = "20%">'.$ppn.'-'.$ppc.' '.$ppn1.' '.$ppn2.' '.$ppa1.' '.$ppa2.'</td>
		<td width = "15%">'.$anualidad.'</td>
		<td width = "20%">'.$monto.'</td>
		<td width = "20%">'.$estatus.'</td>
		<td width = "20%">'.$fecha_c.'</td>
	</tr>';
	}
$html .= '

	<tr align="center" border="0">
		<td  width = "">&nbsp; Total General ('.$total.') de las Solvencias consultadas segun los criterios de busqueda del reporte</td>
	</tr>
<tbody> 
</table>';

$pdf->writeHTML($encabezado, true, false, false, false, '');
$pdf->writeHTML($html, true, false, false, false, '');




// -----------------------------------------------------------------------------
$pdf->LastPage();

//Close and output PDF document
$pdf->Output('Listado Solvencias.pdf', 'I');

//============================================================+
// END OF FILE                                                
//============================================================+



