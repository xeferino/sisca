<?php 
include_once("seguridad.php");
include_once("clases/conexion.php");
include_once("clases/ficha.php");
include_once("clases/linderos.php");

$ficha = new Ficha();
$ficha->fichaEstadistica();

$lindero = new Linderos();
$lindero->linderoEstadistica();


?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

	<?php include('../layout/banner.php');?>

	<div class="main-container ace-save-state" id="main-container">
		<script type="text/javascript">
			try{ace.settings.loadState('main-container')}catch(e){}
		</script>

		<?php $menu=10; include('../layout/menu.php');?>

		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs ace-save-state" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="ace-icon fa fa-home home-icon"></i>
							<a href="#">Home</a>							</li>
							<li class="active">Reporte Ficha</li>
						</ul><!-- /.breadcrumb -->
					</div>

		<div class="page-content">
			<!-- /.ace-settings-container -->
			<!-- /.page-header -->
			<div class="row">
				<div class="col-xs-12">
					<!-- PAGE CONTENT BEGINS -->
					<div class="alert alert-block alert-info">
						<i class="ace-icon fa fa-check"></i>
						Bienvenido Módulo de Reporte donde podra realizar los reportes correspodientes segun sus necesidades.
						<strong class="green">
							<small></small>									</strong>
						</div>
						<!-- /.row -->
						<!-- /.row -->
			<div class="row" style="font-weight: ">
				<form action="reportes/listado_ficha_catastral.php" method="post" id="fom-tereno-estadistica" target="_blank">
					<input type="hidden" name="submit" value="pdf" />
					<div class="col-sm-12">
						<div class="widget-box transparent" id="recent-box">
							<div class="col-sm-12" style="text-transform: uppercase;">
								<div class="panel panel-default">
									<div class="panel-heading">
										<b><i class="ace-icon fa fa-filter"></i> Filtros</b>
									</div>
									<div class="panel-body">
										<div class="col-md-12">
											<div class="col-sm-4">
												<div class="form-group">
													<label>Fecha de Inicio</label>
													<input class="form-control " id="fecha-visita" name="fechainicio" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
												</div>
											</div>

											<div class="col-sm-4">
												<div class="form-group">
													<label>Fecha Fin</label>
													<input class="form-control " id="fecha-levantamiento" name="fechafin" value="" placeholder="00-00-0000" type="text" required="required"  readonly="">
												</div>
											</div>

											<div class="col-sm-4">
												<div class="form-group">
													<label>Sector</label>
													<select class="form-control  select2" id="sector" name="sector" required="required">
														<option value="todos">Todos</option>
														<?php 
														$i=0;
														$pedul= pg_query("SELECT * FROM  tb_pedul");
														?>			
														<?php while($reg=pg_fetch_object($pedul)){?>
														<?php $i++;?>

														<option value="<?php echo $reg->id;?>"><?php echo $reg->nombre;?></option>
														<?php }?>
													</select>
												</div>
											</div>
										</div>
										<div align="right"><br>
											<button type="submit" class="btn btn-primary" title=""><i class="fa fa-cubes"></i> Generar</button>

										</div>
									</div>

								</div>
							</div>

						</div>
					</form>			


					<!-- /.widget-body -->
				</div><!-- /.widget-box -->
			</div><!-- /.col -->
						<!-- /.col -->
					</div><!-- /.row -->


					<!-- /.col -->
				</div><!-- /.row -->
				<!-- PAGE CONTENT ENDS -->
			</div><!-- /.col -->
		</div><!-- /.row -->
				</div><!-- /.page-content -->
			</div>
		</div><!-- /.main-content -->

		<?php include('../layout/footer.php');?>

		<script>
			$(".select2").select2();
			$(".select22").select2();


			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#registro-fecha").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});

			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#fecha-ficha").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});

			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#fecha-visita").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});

			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#fecha-levantamiento").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});



			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#fechaci").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});

			$(function (){
				$.datepicker.setDefaults($.datepicker.regional["es"]);
				$("#fechacf").datepicker({
					firstDay: 0,
					changeMonth: true,
					changeYear : true,
					dateFormat: "dd-mm-yy",
					yearRange: "1900:2099"
				});
			});

		</script>
		<script type="text/javascript">

			Morris.Bar({
				element: 'morris-bar-chart',
				data: [
				{ y: 'Ene', a: 100}, 
				{ y: 'Feb', a: 75},
				{ y: 'Mar', a: 50},
				{ y: 'Abr', a: 75},
				{ y: 'May', a: 50},
				{ y: 'Jun', a: 75},
				{ y: 'Jul', a: 100},
				{ y: 'Ago', a: 100},
				{ y: 'Sep', a: 100},
				{ y: 'Oct', a: 100},
				{ y: 'Nov', a: 100},
				{ y: 'Dic', a: 100}
				],
				xkey: 'y',
				ykeys: ['a'],
				labels: ['Ficha Registradas'],
				barColors: ['#0b62a4'],
				hideHover: 'auto',
				resize: true
			});


			Morris.Line({
				element: 'morris-line-chart',
				data: [{
					y: '2006',
					a: 100,
					b: 90
				}, {
					y: '2007',
					a: 75,
					b: 65
				}, {
					y: '2008',
					a: 50,
					b: 40
				}, {
					y: '2009',
					a: 75,
					b: 65
				}, {
					y: '2010',
					a: 50,
					b: 40
				}, {
					y: '2011',
					a: 75,
					b: 65
				}, {
					y: '2012',
					a: 100,
					b: 90
				}],
				xkey: 'y',
				ykeys: ['a', 'b'],
				labels: ['Series A', 'Series B'],
				lineColors: ['#e91313', '#0b62a4'],
				hideHover: 'auto',
				resize: true
			});

			Morris.Area({
				element: 'morris-area-chart',
				data: [{
					period: '2010 Q1',
					iphone: 2666,
					ipad: null,
					itouch: 2647
				}, {
					period: '2010 Q2',
					iphone: 2778,
					ipad: 2294,
					itouch: 2441
				}, {
					period: '2010 Q3',
					iphone: 4912,
					ipad: 1969,
					itouch: 2501
				}, {
					period: '2010 Q4',
					iphone: 3767,
					ipad: 3597,
					itouch: 5689
				}, {
					period: '2011 Q1',
					iphone: 6810,
					ipad: 1914,
					itouch: 2293
				}, {
					period: '2011 Q2',
					iphone: 5670,
					ipad: 4293,
					itouch: 1881
				}, {
					period: '2011 Q3',
					iphone: 4820,
					ipad: 3795,
					itouch: 1588
				}, {
					period: '2011 Q4',
					iphone: 15073,
					ipad: 5967,
					itouch: 5175
				}, {
					period: '2012 Q1',
					iphone: 10687,
					ipad: 4460,
					itouch: 2028
				}, {
					period: '2012 Q2',
					iphone: 8432,
					ipad: 5713,
					itouch: 1791
				}],
				xkey: 'period',
				ykeys: ['iphone', 'ipad', 'itouch'],
				labels: ['iPhone', 'iPad', 'iPod Touch'],
				pointSize: 2,
				hideHover: 'auto',
				resize: true
			});


		</script>

		<script type="text/javascript">

			Morris.Donut({
				element: 'morris-donut-chart',
				data: [{
					value: <?php echo $lindero->estadistica[0];?>,
					color: "#e91313",
					highlight: "#000",
					label: "Certificacion de Linderos"
				},
				{
					value: <?php echo $ficha->estadistica[0];?>,
					color: "#0b62a4",
					highlight: "#000",
					label: "Fichas Catastrales"
				}],
				resize: true
			});

		</script>

	</body>
	</html>
