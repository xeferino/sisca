<?php include_once("seguridad.php"); ?>
<?php
	include_once("clases/motor.php");
	
	$objeto = new Persona();
		//if($url="http://localhost:8080/sisca/admin/edit-usuario.php?id=".$_GET['id'].""){
				
 /*   // Variable to check
    $url="http://localhost:8080/sisca/admin/edit-usuario.php?id=".$_GET['id']."";

    // Remover los caracteres ilegales de la url
    $url = filter_var($url, FILTER_SANITIZE_URL);
    echo $url;
    // Validar url
    if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
        echo("$url es una URL valida");
    } else {
        echo("$url no es una URL valida");
    }*/
	
		if(isset($_GET['id']))
		{
			
			$id= $_GET['id'];
			$objeto->data_usuario($id);
		}
		//}
		else{
			header("Location: ./");
		}
			
		if (isset($_POST['submit']) && $_POST['submit'] == 'edit') {	
			
			$nacionalidad =  $_POST['nacionalidad'];
			$cedula =  $_POST['cedula'];
			$nombre1 =  $_POST['nombre1'];
			$nombre2 =  $_POST['nombre2'];
			$apellido1 =  $_POST['apellido1'];
			$apellido2 =  $_POST['apellido2'];
			$role =  $_POST['role'];
			$cuenta =  $_POST['cuenta'];
			$clave =  $_POST['clave'];
			$clave2 =  $_POST['clave2'];
			
			$estatus =  $_POST['estatus'];
			if ($estatus== "0"){$estatus='Activo';}
			if ($estatus== "1"){$estatus='Inactivo';}
			
			$id= $_POST['id'];
			
			$ci =  $_POST['ci'];
			$n =  $_POST['n'];
			$user =  $_POST['user'];
				
			$objeto->update_cuenta($id, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $role, $cuenta, $clave, $clave2, $estatus, $ci, $user, $n);
		}

?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=7; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-user"></i> Usuarios </li> <li class="active"><i class="ace-icon fa fa-user"></i> Editar </li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="usuarios.php"><span class="btn btn-primary pull-right" title="Lista de Usuarios"><i class="ace-icon fa fa-users"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Actualizar Usuario</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Actualize los Datos del Usuario.</strong>
														</p>
													
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Usuario Actualizado Exitosamente.",
																  type: "success",
																  confirmButtonText: "Aceptar"
																},
																function(){
																	$(location).attr('href','http:usuarios.php');
																});
															
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==6){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos en Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Cuenta no Disponible",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "2000"
																}/*,
																function(){
																	$(location).attr('href','http:inspectores.php');
																}*/);
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==4){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Cedula: <?php echo $objeto->nacionalidad."-".$objeto->cedula;?> No Disponilbe, Verifique!",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "2000"
																}/*,
																function(){
																	$(location).attr('href','http:inspectores.php');
																}*/);
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==5){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Las Claves no Coiciden.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->error=$objeto->mensaje==8){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Actualizacion no Exitosa, Verifique.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "1000"
																},
																function(){
																	$(location).attr('href','http:usuarios.php');
																});
														});
														</script>
														<?php }?>
							<form action="" method="post" id="form-usuario">
								<input type="hidden" name="submit" value="edit" />
								<input type="hidden" name="id" value="<?php echo $objeto->id;?>" />
								<input type="hidden" name="n" value="<?php echo $objeto->nacionalidad;?>" /><strong>
								<input type="hidden" name="ci" value="<?php echo $objeto->cedula;?>" />
								<input type="hidden" name="user" value="<?php echo $objeto->cuenta;?>" /></strong>

									<fieldset>
									<div class="form-group col-sm-4">
										<div class="form-group">
										<?php 
										$nac = array('V' => 'V',
											'E' => 'E',
											'P' => 'P',
											'T' => 'T',
											'J' => 'J'
										);
										?>
											<label>Nacionalidad</label>

											<select name="nacionalidad" id="nacionalidad" class="form-control select2">
											<option value="">Seleccione</option>
											<?php foreach ($nac as $key => $value){?>
	
												<option <?php if ($key == $objeto->nacionalidad){echo "selected='selected'";}?> value="<?php echo $key?>"><?php echo $key?></option>
											<?php }?>
												
											</select>
										</div>
									</div>
									<div class="form-group col-sm-8">
										<div class="form-group">
											<label>Cedula de Identidad</label>
											<input class="form-control" name="cedula" id="cedula" value="<?php echo $objeto->cedula;?>" placeholder="EJ:12345678" type="text" >
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Nombre</label>
											<input class="form-control" name="nombre1" id="nombre1" value="<?php echo $objeto->nombre1;?>" placeholder="EJ: Juan" type="text">
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Nombre</label>
											<input class="form-control" name="nombre2" id="nombre2"  value="<?php echo $objeto->nombre2;?>" placeholder="EJ: Miguel" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Apellido</label>
											<input class="form-control" name="apellido1" id="apellido1"  value="<?php echo $objeto->apellido1;?>" placeholder="EJ: Lara" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Apellido</label>
											<input class="form-control" name="apellido2" id="apellido2" value="<?php echo $objeto->apellido2;?>" placeholder="EJ: Lopez" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Cuenta</label>
											<input class="form-control" name="cuenta" id="cuenta" value="<?php echo $objeto->cuenta;?>" placeholder="EJ: Lara" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Tipo</label>
											<select class="form-control select2" name="role" id="Perfil">
												<?php if ($objeto->role=="Administrador"){?>
												<option value="<?php echo $objeto->role;?>"><?php echo $objeto->role;?></option>
												<option value="Director">Director</option>
												<option value="Coordinador">Coordinador</option>
												<option value="Secretaria">Secretaria</option>
												<?php }?>
												<?php if ($objeto->role=="Director"){?>
												<option value="<?php echo $objeto->role;?>"><?php echo $objeto->role;?></option>
												<option value="Administrador">Administrador</option>
												<option value="Coordinador">Coordinador</option>
												<option value="Secretaria">Secretaria</option>
												<?php }?>
												
												<?php if ($objeto->role=="Coordinador"){?>
												<option value="<?php echo $objeto->role;?>"><?php echo $objeto->role;?></option>
												<option value="Administrador">Administrador</option>
												<option value="Director">Director</option>
												<option value="Secretaria">Secretaria</option>
												<?php }?>
												
												<?php if ($objeto->role=="Secretaria"){?>
												<option value="<?php echo $objeto->role;?>"><?php echo $objeto->role;?></option>
												<option value="Administrador">Administrador</option>
												<option value="Director">Director</option>
												<option value="Coordinador">Coordinador</option>
												<?php }?>
													
										  </select>
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Clave</label>
											<input class="form-control" name="clave" id="clave" value=""  placeholder="EJ: 12345" type="password">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Repetir Clave</label>
											<input class="form-control" name="clave2" id="clave2" value=""  placeholder="EJ: 12345" type="password">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Cambiar Estatus</label>
											<select class="form-control select2" name="estatus">
											<?php if ($objeto->estatus=="Activo"){?>
													<option value="0">--Seleccione--</option>
													<option value="Inactivo">Inactivo</option>
											<?php } else {?>
											<option value="1">--Seleccione--</option>
											<option value="Activo">Activo</option>
											<?php }?>
												</select>
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Estatus</label>
											<input class="form-control" name="" value="<?php echo $objeto->estatus;?>" placeholder="EJ: Lopez" type="text" disabled="disabled">
										</div>
									</div>
										
									</fieldset>
									<div>
										<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-pencil"></i> Actualizar</button>
									</div>
									<br><br>
								</form>
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ\s]+$/.test(value);
				});

				$.validator.addMethod('cuenta', function(value, element){
					return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
				});
			    $("#form-usuario").validate({
			        rules: {
			        	nacionalidad: { required: true},
			            cedula: { required: true, digits: true, minlength: 7, maxlength:9 },
			            nombre1: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            nombre2: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            apellido1: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            apellido2: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            cuenta: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			            clave: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			            clave2: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			        },
			        messages: {
			            nacionalidad: {
			            		required: 'la nacionalidad es requerida'
			            },
			            cedula: {
			            		required: 'la cedula es requerida',
			            		digits: 'la cedula solo acepta numeros ',
			            		minlength:'la cedula debe tener como minimo 7 caracter',
			            		maxlength:'el maximo permitido son 9 caracteres'
			            },
			            nombre1: {
			            		required: 'el primer nombre es requerido',
			            		minlength:'el primer nombre debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el primer nombre acepta solo letras'
			            },
			            nombre2: {
			            		required: 'el segundo nombre es requerido',
			            		minlength:'el segundo nombre debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el segundo nombre acepta solo letras'
			            },
			            apellido1: {
			            		required: 'el primer apellido es requerido',
			            		minlength:'el primer apellido debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el primer apellido acepta solo letras'
			            },
			            apellido2: {
			            		required: 'el segundo apellido es requerido',
			            		minlength:'el segundo apellido debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el segundo apellido acepta solo letras'
			            },
			            cuenta: {
			            		required: 'la cuenta es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la cuenta solo acepta letras y numeros'
			            },
			            clave: {
			            		required: 'la clave 1 es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la clave 1 solo acepta letras y numeros'
			            },
			            clave2: {
			            		required: 'la clave 2 es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la clave 2 solo acepta letras y numeros'
			            },
			            
			        }
			    });
			    $("#form-usuario").submit(function(){
			        $post("edit-usuario.php.php");
			        return false;
			    });
			});
		</script>
			<?php include('../layout/footer.php');?>
			<script type="text/javascript">
				$(".select2").select2();
			</script>
	</body>
</html>
