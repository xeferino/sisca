<?php include_once("seguridad.php"); ?>
<?php
	include_once("clases/motor.php");
	
	$objeto = new Persona();
		
		if (isset($_POST['submit']) && $_POST['submit'] == 'new') {	
			
			$nacionalidad =  $_POST['nacionalidad'];
			$cedula =  $_POST['cedula'];
			$nombre1 =  $_POST['nombre1'];
			$nombre2 =  $_POST['nombre2'];
			$apellido1 =  $_POST['apellido1'];
			$apellido2 =  $_POST['apellido2'];
			$role =  $_POST['role'];
			$cuenta =  $_POST['cuenta'];
			$clave =  $_POST['clave'];
			$clave2 =  $_POST['clave2'];
			$estatus =  "Activo";
			$tipo =  "Usuario";
			
			
			$id =  @$_POST['id '];
			$telefono1 =  @$_POST['telefono1'];
			$telefono2 =  @$_POST['telefono2'];
			$estado_civil =  @$_POST['estado_civil'];
			$correo =  @$_POST['correo'];
							
			$objeto->cargar ($id, $nacionalidad, $cedula, $nombre1, $nombre2, $apellido1, $apellido2, $estado_civil, 
			$correo, $telefono1, $telefono2, $tipo, $role, $cuenta, $clave, $clave2, $estatus);
			$objeto->new_cuenta();
		}

?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=7; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-user"></i> Usuarios </li> <li class="active"><i class="ace-icon fa fa-user"></i> Nuevo </li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
								<a href="usuarios.php"><span class="btn btn-primary pull-right" title="Lista de Usuario"><i class="ace-icon fa fa-users"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-desktop"></i> Formulario de Registro</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<span class=" mensaje glyphicon glyphicon-exclamation-sign"></span> <strong>Ingrese los Datos del Nuevo Usuario a Registrar.</strong>
														</p>
													
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Registro de Usuario Exitoso.",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
															
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "<?php echo "la Cuenta de Usuario: (".$objeto->cuenta.") no Esta Disponible.";?> ",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==4){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "<?php echo "Cedula: ".$objeto->nacionalidad."-".$objeto->cedula." no Disponible";?>",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==5){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Las Claves no Coiciden.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
														<?php if($objeto->mensaje==6){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos en Blancos.",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														
							<form action="" method="POST" id="form-usuario">
								<input type="hidden" name="submit" value="new" />
								
									<fieldset>
									<div class="form-group col-sm-4">
										<div class="form-group">
											<?php 
										$nac = array('V' => 'V',
											'E' => 'E',
											'P' => 'P',
											'T' => 'T',
											'J' => 'J'
										);
										?>
											<label>Nacionalidad</label>

											<select name="nacionalidad" id="nacionalidad" class="form-control select2">
											<option value="">Seleccione</option>
											<?php foreach ($nac as $key => $value){?>
	
												<option value="<?php echo $key?>"><?php echo $key?></option>
											<?php }?>
												
											</select>
										</div>
									</div>
									<div class="form-group col-sm-8">
										<div class="form-group">
											<label>Cedula de Identidad</label>
											<input class="form-control" name="cedula" id="cedula" value="<?php echo $objeto->cedula;?>" placeholder="EJ:12345678" type="text" >
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Nombre</label>
											<input class="form-control" name="nombre1" id="nombre1" value="<?php echo $objeto->nombre1;?>" placeholder="EJ: Juan" type="text">
										</div>
									</div>
										<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Nombre</label>
											<input class="form-control" name="nombre2" id="nombre2" value="<?php echo $objeto->nombre2;?>" placeholder="EJ: Miguel" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Primer Apellido</label>
											<input class="form-control" name="apellido1" id="apellido1" value="<?php echo $objeto->apellido1;?>" placeholder="EJ: Lara" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Segundo Apellido</label>
											<input class="form-control" name="apellido2" id="apellido2"  value="<?php echo $objeto->apellido2;?>" placeholder="EJ: Lopez" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Cuenta</label>
											<input class="form-control" name="cuenta" id="cuenta" value="<?php echo $objeto->cuenta;?>" placeholder="EJ: Lara" type="text">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Tipo</label>
											<select class="form-control select2" name="role" id="perfil">
												<option value="0">--Selecccione--</option>
												<option value="Administrador"></option>
												<option value="Director">Director</option>
												<option value="Coordinador">Coordinador</option>
												<option value="Secretaria"></option>
										  </select>
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Clave</label>
											<input class="form-control" name="clave" id="clave" value=""  placeholder="EJ: 12345" type="password">
										</div>
									</div>
									
									<div class="form-group col-sm-6">
										<div class="form-group">
											<label>Repetir Clave</label>
											<input class="form-control" name="clave2" id="clave2" value=""  placeholder="EJ: 12345" type="password">
										</div>
									</div>
										
									</fieldset>
									<div>
										<button type="submit" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-ok-sign"></i> Registrar</button>
									</div>
									<br><br>
								</form>
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ\s]+$/.test(value);
				});

				$.validator.addMethod('cuenta', function(value, element){
					return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
				});
			    $("#form-usuario").validate({
			        rules: {
			        	nacionalidad: { required: true},
			            cedula: { required: true, digits: true, minlength: 7, maxlength:9 },
			            nombre1: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            nombre2: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            apellido1: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            apellido2: { required:true, letras: true, minlength: 4,  maxlength:50 },
			            cuenta: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			            clave: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			            clave2: { required:true, cuenta: true, minlength: 4,  maxlength:8 },
			            perfil: { required: true},
			        },
			        messages: {
			            nacionalidad: {
			            		required: 'la nacionalidad es requerida'
			            },
			            cedula: {
			            		required: 'la cedula es requerida',
			            		digits: 'la cedula solo acepta numeros ',
			            		minlength:'la cedula debe tener como minimo 7 caracter',
			            		maxlength:'el maximo permitido son 9 caracteres'
			            },
			            nombre1: {
			            		required: 'el primer nombre es requerido',
			            		minlength:'el primer nombre debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el primer nombre acepta solo letras'
			            },
			            nombre2: {
			            		required: 'el segundo nombre es requerido',
			            		minlength:'el segundo nombre debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el segundo nombre acepta solo letras'
			            },
			            apellido1: {
			            		required: 'el primer apellido es requerido',
			            		minlength:'el primer apellido debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el primer apellido acepta solo letras'
			            },
			            apellido2: {
			            		required: 'el segundo apellido es requerido',
			            		minlength:'el segundo apellido debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el segundo apellido acepta solo letras'
			            },
			            cuenta: {
			            		required: 'la cuenta es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la cuenta solo acepta letras y numeros'
			            },
			            clave: {
			            		required: 'la clave 1 es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la clave 1 solo acepta letras y numeros'
			            },
			            clave2: {
			            		required: 'la clave 2 es requerida',
			            		minlength:'la cuenta debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 8 caracteres',
			            		cuenta:'la clave 2 solo acepta letras y numeros'
			            },
			            perfil: {
			            		required: 'el tipo es requerido'
			            },
			            
			        }
			    });
			    $("#form-usuario").submit(function(){
			        $post("nuevo_usuario.php.php");
			        return false;
			    });
			});
		</script>
			<?php include('../layout/footer.php');?>
			<script type="text/javascript">
				$(".select2").select2();
			</script>
	</body>
</html>
