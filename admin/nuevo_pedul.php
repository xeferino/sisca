<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/motor.php");
include_once ('clases/pedul.php');

$manzana = new Manzana();
$manzana->manzanas();

$objeto = new Pedul();

if (isset($_POST['submit']) && $_POST['submit'] == 'new'){	

$id = @$_POST['id'];
$codigo =  $_POST['codigo'];
$nombre =  $_POST['nombre'];
$descripcion =  $_POST['descripcion'];
$cantidad_manzana =  $_POST['cantidad_manzana'];
$calle = @$_POST['calle'];

$objeto->cargar ($id, $codigo, $nombre, $descripcion, $cantidad_manzana, $calle);
$objeto->registrar();
}

?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
	try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=8; include('../layout/menu.php');?>

<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="./">Home</a>							</li>
					<li class="active"><i class="ace-icon fa fa-map"></i> Sector</li> <li class="active"><i class="ace-icon fa fa-map"></i>  Nuevo</li>
				</ul><!-- /.breadcrumb -->
			</div>

			<div class="page-content">
				<!-- /.ace-settings-container -->
				<!-- /.page-header -->
				<div class="row">
					<div class="col-xs-12">
						<!-- PAGE CONTENT BEGINS -->
						<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">

							<a href="pedul.php"><span class="btn btn-primary pull-right" title="Lista de Pedul"><i class="ace-icon fa fa-list"></i></span></a><br><br><br>
							<div class="widget-box" id="widget-box-4">
								<div class="widget-header widget-header-large">
									<h4 class="widget-title"><i class="ace-icon fa 	fa-map"></i> Formulario de Registro</h4>
								</div>

								<div class="widget-body">
									<div class="widget-main">
										<p class="alert alert-info">
											<i class="ace-icon fa 	fa-edit"></i> <strong>Ingrese los Datos para el nuevo Registro.</strong>
										</p>
										<?php if($objeto->mensaje==1){?>
										<script>
											$(document).ready(function() {
												swal({
													title: "",
													text: "Sector Registrado Exitosamente",
													type: "success",
													confirmButtonText: "Aceptar",
													timer: "3000"
												});

											});
										</script>
										<?php }?>
										<?php if($objeto->mensaje==2){?>
										<script>
											$(document).ready(function() {
												swal({
													title: "Error!",
													text: "Codigo (<?php echo $objeto->codigo;?>) no Disponible, Pertenece a otro Sector",
													type: "error",
													confirmButtonText: "Aceptar",
													timer: "3000"
												});
											});
										</script>
										<?php }?>
										<?php if($objeto->mensaje==3){?>
										<script>
											$(document).ready(function() {
												swal({
													title: "Error!",
													text: "Campos En Blancos",
													type: "error",
													confirmButtonText: "Aceptar",
													timer: "3000"
												});
											});
										</script>
										<?php }?>
										<form action="" method="post" id="form-sector">
											<input type="hidden" name="submit" value="new" />
											<fieldset>
												<div class="form-group col-sm-2"></div>
												<div class="form-group col-sm-8">
													<label class="block clearfix"> Codigo 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="codigo" id="codigo" value="<?php echo $objeto->codigo;?>"  placeholder="" type="text">
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>
												</div>
												<div class="form-group col-sm-2"></div>

											</fieldset>
											<fieldset>
												<div class="form-group col-sm-2"></div>
												<div class="form-group col-sm-8">
													<label class="block clearfix"> Nombre 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="nombre" id="nombre" value="<?php echo $objeto->nombre;?>"  placeholder="" type="text" >
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>											
												</div>
												<div class="form-group col-sm-2"></div>

											</fieldset>
											<fieldset>
												<div class="form-group col-sm-2"></div>
												<div class="form-group col-sm-8">
													<label class="block clearfix"> Calles 
														<div class="form-group">
															<select name="calle[]" id="calle" class="form-control select2" multiple="multiple" data-placeholder="--Seleccione--" style="width: 100%;">
																<?php while($reg=pg_fetch_object($manzana->consulta)){?>
																<?php $i++;?>
																<option value="<?php  echo $reg->id;?>"> <?php echo $reg->nombre;?></option>
																<?php }?>
															</select>
														</div>

													</label>											
												</div>
												<div class="form-group col-sm-2"></div>

											</fieldset>
											<fieldset>
												<div class="form-group col-sm-2"></div>
												<div class="form-group col-sm-8">
													<label class="block clearfix"> Cantidad de Manzanas 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="cantidad_manzana" id="manzana" value="<?php echo $objeto->cantidad_manzana;?>"  placeholder="" type="text" requerid>
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>
												</div>
												<div class="form-group col-sm-2"></div>
												
											</fieldset>
											<fieldset>
												<div class="form-group col-sm-2"></div>
												<div class="form-group col-sm-8">
													<label class="block clearfix"> Descripci&oacute;n 
														<span class="block input-icon input-icon-right">
															<textarea class="form-control" name="descripcion" id="descripcion" cols="" rows=""><?php echo $objeto->descripcion;?></textarea>
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>											
												</div>
												<div class="form-group col-sm-2"></div>

											</fieldset>



											<div class="form-group col-sm-2"></div>
											<div class="form-group col-sm-8">

												<button type="submit" class="btn btn-primary pull-right" title="Actualizar"><i class="fa fa-check-circle-o"></i> Registrar</button>
											</div>
											<div class="form-group col-sm-2"></div>
											<div>
												<br><br><br>
											</div>
										</form>
									</div>

								</div>
							</div>
						</div>
						<!-- PAGE CONTENT ENDS -->
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.page-content -->
		</div>
	</div><!-- /.main-content -->
	<script type="text/javascript"> 
		$(document).ready(function(event) {

			$.validator.addMethod('letras', function(value, element){
				return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ0-9.,\s]+$/.test(value);
			});
			$("#form-sector").validate({
				rules: {
					codigo: { required: true, digits: true, minlength: 1, maxlength:2 },
					manzana: { required: true, digits: true, minlength: 1, maxlength:4 },
					nombre: { required:true, letras: true, minlength: 4,  maxlength:50 },
					calle: { required:true },
					descripcion: { required:true, letras: true, minlength: 10,  maxlength:1000 },
				},
				messages: {
					codigo: {
						required: 'el codigo es requerido',
						digits: 'el codigo solo acepta numeros ',
						minlength:'el codigo debe tener como minimo 1 caracter',
						maxlength:'el maximo permitido son 2 caracteres'
					},
					manzana: {
						required: 'la cantidad es requerida',
						digits: 'la cantidad solo acepta numeros ',
						minlength:'la cantidad debe tener como minimo 1 caracter',
						maxlength:'el maximo permitido son 4 caracteres'
					},
					nombre: {
						required: 'el nombre es requerido',
						minlength:'el nombre debe tener como minimo 4 caracter',
						maxlength:'el maximo permitido son 50 caracteres',
						letras:'el nombre acepta solo letras y numeros (.,)'
					},
					calle: {
						required: 'la calle es requerida'
					},
					descripcion: {
						required: 'la descripcion es requerida',
						minlength:'la descripcion debe tener como minimo 10 caracteres',
						maxlength:'el maximo permitido son 1000 caracteres',
						letras:'la descripcion acepta solo letras y numeros (.,)'
					},
				}
			});
			$("#form-sector").submit(function(){
				$post("nueva_pedul.php");
				return false;
			});
		});
	</script>
	<?php include('../layout/footer.php');?>
	<script>
		$(".select2").select2();
	//$(".select22").select2();
</script>
</body>
</html>
