<?php include_once("seguridad.php"); ?>
<?php 
	include_once("clases/motor.php");
	$objeto = new Manzana();
	
	if (isset($_POST['submit']) && $_POST['submit'] == 'new') {	
			
		$id = @$_POST['id'];
		$codigo =  $_POST['codigo'];
		$nombre =  $_POST['nombre'];
		
		$objeto->cargar ($id, $codigo, $nombre);
		$objeto->registrar();

		//echo $codigo."-".$nombre;
	}

?>
<!DOCTYPE html>
<html lang="en">

	<?php include('../layout/head.php');?>
	
	<body class="no-skin">
	
		<?php include('../layout/banner.php');?>
		
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
			<?php $menu=8; include('../layout/menu.php');?>
			
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="./">Home</a>							</li>
							<li class="active"><i class="ace-icon fa fa-map"></i> Calles</li> <li class="active"><i class="ace-icon fa fa-map"></i>  Nueva</li>
						</ul><!-- /.breadcrumb -->
					</div>

					<div class="page-content">
						<!-- /.ace-settings-container -->
						<!-- /.page-header -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 widget-container-col" id="widget-container-col-4">
									<a href="calles.php"><span class="btn btn-primary pull-right" title="Lista de Calles"><i class="ace-icon fa fa-list"></i></span></a><br><br><br>
											<div class="widget-box" id="widget-box-4">
												<div class="widget-header widget-header-large">
													<h4 class="widget-title"><i class="ace-icon fa 	fa-map"></i> Formulario de Registro</h4>
												</div>

												<div class="widget-body">
													<div class="widget-main">
														<p class="alert alert-info">
															<i class="ace-icon fa 	fa-edit"></i> <strong>Ingrese los Datos para el Nuevo Registro.</strong>
														</p>
														<?php if($objeto->mensaje==1){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "",
																  text: "Calle Registrada Exitosamente",
																  type: "success",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
															
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==2){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Codigo (<?php echo $objeto->codigo;?>) no Disponible, Pertenece a otra Calle",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
														<?php if($objeto->mensaje==3){?>
														<script>
														$(document).ready(function() {
															swal({
																  title: "Error!",
																  text: "Campos En Blancos",
																  type: "error",
																  confirmButtonText: "Aceptar",
																  timer: "3000"
																});
														});
														</script>
														<?php }?>
															
							<form action="" method="post" id="form-calle">
								<input type="hidden" name="submit" value="new" />
									<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											<label class="block clearfix"> Codigo 
														<span class="block input-icon input-icon-right">
															<input class="form-control" name="codigo" id="codigo" value="<?php echo $objeto->codigo;?>"  placeholder="" type="text">
															<i class="ace-icon fa fa-pencil"></i>
														</span>
													</label>
										</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
								<fieldset>
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
								<label class="block clearfix"> Nombre 
									<textarea class="form-control" name="nombre" id="nombre" cols="" rows=""><?php echo $objeto->nombre;?></textarea>
								</label>											
									</div>
										<div class="form-group col-sm-2"></div>
										
								</fieldset>
							
									<div class="form-group col-sm-2"></div>
								<div class="form-group col-sm-8">
											
											<button type="submit" class="btn btn-primary pull-right" title="Actualizar"><i class="fa fa-check-circle-o"></i> Registrar</button>
										</div>
										<div class="form-group col-sm-2"></div>
									<div>
										<br><br><br>
									</div>
								</form>
													</div>
													
												</div>
											</div>
										</div>
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
			<script type="text/javascript"> 
				$(document).ready(function(event) {

				$.validator.addMethod('letras', function(value, element){
					return this.optional(element) || /^[a-zA-ZáéóóúñÁÉÓÓÚÑ0-9-\s]+$/.test(value);
				});
			    $("#form-calle").validate({
			        rules: {
			            codigo: { required: true, digits: true, minlength: 1, maxlength:2 },
			            nombre: { required:true, letras: true, minlength: 4,  maxlength:50 },
			        },
			        messages: {
			            codigo: {
			            		required: 'el codigo es requerido',
			            		digits: 'el codigo solo acepta numeros ',
			            		minlength:'el codigo debe tener como minimo 1 caracter',
			            		maxlength:'el maximo permitido son 2 caracteres'
			            },
			            nombre: {
			            		required: 'el nombre es requerido',
			            		minlength:'el nombre debe tener como minimo 4 caracter',
			            		maxlength:'el maximo permitido son 50 caracteres',
			            		letras:'el nombre acepta solo letras y numeros'
			            },
			        }
			    });
			    $("#form-calle").submit(function(){
			        $post("nueva_calle.php");
			        return false;
			    });
			});
		</script>
			<?php include('../layout/footer.php');?>
	</body>
</html>
