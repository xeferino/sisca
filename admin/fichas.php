<?php include_once("seguridad.php"); ?>
<?php 
include_once("clases/conexion.php");
include_once("clases/ficha.php");

$objeto = new Ficha();
$objeto->fichasCatastrales();
?>
<!DOCTYPE html>
<html lang="en">

<?php include('../layout/head.php');?>

<body class="no-skin">

<?php include('../layout/banner.php');?>

<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<?php $menu=2; include('../layout/menu.php');?>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
	<i class="ace-icon fa fa-home home-icon"></i>
	<a href="./">Home</a>							</li>
<li class="active"><i class="ace-icon fa fa-map"></i> Ficha Catastral</li> 
</ul><!-- /.breadcrumb -->
</div>

<div class="page-content">
<!-- /.ace-settings-container -->
<!-- /.page-header -->
<div class="row">

<div class="col-xs-12" >
	<a href="#" class="btnNew"><span class="btn btn-primary pull-right" title="Nueva Ficha Catastral"><i class="ace-icon fa fa-plus"></i></span></a><br><br><br>
	<!-- PAGE CONTENT BEGINS -->
	<table id="dynamic-t" class="table table-striped">
					<thead>
						<tr>
							<th class="center">ID</th>
							<th><i class="fa fa-credit-card"></i> Cedula/Propietario</th>
							<th><i class="fa fa-credit-card"></i> Numero Archivo</th>
							<th><i class="fa fa-pencil"></i> Numero Castastral</th>
							<th><i class="fa fa-calendar"></i> Fecha Inscripci&oacute;n</th>
							<th><i class="fa fa-check-square-o"></i> Acciones</th>
						</tr>
					</thead>
					
					<tbody>
					<?php $i=0;?>			
					<?php while($reg=pg_fetch_object($objeto->ficha)){?>
					<?php $i++;?>
						<tr>
							<td><?php echo $i;?></td>
							<td><?php echo $reg->ppnacionalidad."-".$reg->ppcedula;?> <a class="pull-right" data-toggle="modal" data-target="#cedula<?php echo $reg->ficha;?>" href="#" title="Ver">
								<i class="ace-icon fa fa-search bigger-130 red" ></i>
						</a></td>
							<td><?php echo $reg->numeroarchivo;?></td>
							<td><?php echo $reg->numerocatastral;?></td>
							<td><?php echo $reg->fechainscripcion;?></td>
							<td>	
							<a class="btn btn-primary btn-xs btnEdit<?php echo $i;?>" href="editar-ficha-catastral.php?ficha=<?php echo $reg->ficha;?>">
									<i class="ace-icon fa fa-pencil bigger-130" title="Editar"></i>
							</a>
							<a class="btn btn-primary btn-xs" href="reportes/fichaCatastralPdf.php?id=<?php echo $reg->ficha; ?>" target="_blank" title="Ficha Catastral">
									<i class="ace-icon fa fa-file-pdf-o bigger-130"></i>
							</a>
							<a class="btn btn-primary btn-xs" href="reportes/fichaLinderos.php?id=<?php echo $reg->ficha; ?>" target="_blank" title="Certificacion de Linderos">
									<i class="ace-icon fa fa-file-text-o bigger-130"></i>
							</a>
							</td>
						</tr>
						<?php include('modales.php');?>
					<?php }?>
					</tbody>
				</table>
	<!-- PAGE CONTENT ENDS -->
</div>
<!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
<?php include('../layout/footer.php');?>
<script>
$(document).ready(function() {	
$('#dynamic-t').DataTable({
responsive: true,
aoColumnDefs:[{'bSortable':false,'aTargets':[4]}]
});	
$('.btnNew').click(function(){

swal({
title: "Alerta!",
text: "Nueva Ficha Catastral",
type: "info",
showCancelButton: true,
closeOnConfirm: false,
confirmButtonText: 'Aceptar'
},
function(){
$(location).attr('href','http:ficha_catastral.php');
});//swal

});//click

});///dom
</script>
</body>
</html>
