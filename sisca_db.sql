--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: tb_bano_const; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_bano_const (
    codigo integer NOT NULL,
    tipo character varying(30),
    cantidad integer
);


ALTER TABLE public.tb_bano_const OWNER TO postgres;

--
-- Name: tb_bano_const_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_bano_const_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_bano_const_codigo_seq OWNER TO postgres;

--
-- Name: tb_bano_const_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_bano_const_codigo_seq OWNED BY tb_bano_const.codigo;


--
-- Name: tb_certificacion_linderos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_certificacion_linderos (
    id integer NOT NULL,
    lnorte integer,
    lsur integer,
    leste integer,
    loeste integer,
    area character varying(30),
    norte text,
    sur text,
    este text,
    oeste text,
    ubicacion text,
    fecha character varying(20),
    observacion text,
    id_persona integer,
    id_parroquia integer
);


ALTER TABLE public.tb_certificacion_linderos OWNER TO postgres;

--
-- Name: tb_certificacion_linderos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_certificacion_linderos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_certificacion_linderos_id_seq OWNER TO postgres;

--
-- Name: tb_certificacion_linderos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_certificacion_linderos_id_seq OWNED BY tb_certificacion_linderos.id;


--
-- Name: tb_const_bano; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_const_bano (
    id integer NOT NULL,
    codigo_const integer,
    codigo_bano integer
);


ALTER TABLE public.tb_const_bano OWNER TO postgres;

--
-- Name: tb_const_bano_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_const_bano_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_const_bano_id_seq OWNER TO postgres;

--
-- Name: tb_const_bano_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_const_bano_id_seq OWNED BY tb_const_bano.id;


--
-- Name: tb_const_puerta; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_const_puerta (
    id integer NOT NULL,
    codigo_const integer,
    codigo_puerta integer
);


ALTER TABLE public.tb_const_puerta OWNER TO postgres;

--
-- Name: tb_const_puerta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_const_puerta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_const_puerta_id_seq OWNER TO postgres;

--
-- Name: tb_const_puerta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_const_puerta_id_seq OWNED BY tb_const_puerta.id;


--
-- Name: tb_const_ventana; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_const_ventana (
    id integer NOT NULL,
    codigo_const integer,
    codigo_ventana integer
);


ALTER TABLE public.tb_const_ventana OWNER TO postgres;

--
-- Name: tb_const_ventana_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_const_ventana_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_const_ventana_id_seq OWNER TO postgres;

--
-- Name: tb_const_ventana_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_const_ventana_id_seq OWNED BY tb_const_ventana.id;


--
-- Name: tb_construccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_construccion (
    id integer NOT NULL,
    tipo character varying(255),
    descripcionuso character varying(255),
    teneciaconstruccion character varying(255),
    regimenpropiedad character varying(255),
    soporteestructural character varying(255),
    techoestructural character varying(255),
    cubiertainterna character varying(255),
    cubiertaexterna character varying(255),
    paredestipo character varying(255),
    paredesacabado character varying(255),
    paredespintura character varying(255),
    paredesinstelectricas character varying(255),
    pisotipo character varying(255),
    banowc character varying(255),
    banocomunitario character varying(255),
    banoducha character varying(255),
    banolavamanos character varying(255),
    banobanera character varying(255),
    banobidet character varying(255),
    banoletrina character varying(255),
    banoceramica1 character varying(255),
    banoceramica2 character varying(255),
    puertametalica character varying(255),
    puertamaderacepillada character varying(255),
    puertamadeconomica character varying(255),
    puertaentamboradafina character varying(255),
    puertaseguridad character varying(255),
    puertaotra character varying(255),
    puertaotrac character varying(255),
    ventanalaluminio character varying(255),
    ventanalhierro character varying(255),
    ventanalmadera character varying(255),
    basculantealuminio character varying(255),
    basculantehierro character varying(255),
    basculantemadera character varying(255),
    satientealuminio character varying(255),
    satientehierro character varying(255),
    satientemadera character varying(255),
    celosiaaluminio character varying(255),
    celosiahierro character varying(255),
    celosiamadera character varying(255),
    correderaaluminio character varying(255),
    correderahierro character varying(255),
    correderamadera character varying(255),
    panoramicaaluminio character varying(255),
    panoramicahierro character varying(255),
    panoramicamadera character varying(255),
    estadoconservacion character varying(255),
    anoconstruccion character varying(255),
    porcentajerefaccion character varying(255),
    numeroniveles character varying(255),
    anorefaccion character varying(255),
    edadefectiva character varying(255),
    numeroedificacion character varying(255),
    observacionesconts text,
    tipologia character varying(255),
    areametrocuadrado character varying(255),
    valorareacuadrada character varying(255),
    areatotal character varying(255),
    porcentajedepreciacion character varying(255),
    valoractual character varying(255),
    valortotal character varying(255),
    valorinmueble character varying(255),
    valorconstruccion character varying(255),
    observacioneconomica text
);


ALTER TABLE public.tb_construccion OWNER TO postgres;

--
-- Name: tb_construccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_construccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_construccion_id_seq OWNER TO postgres;

--
-- Name: tb_construccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_construccion_id_seq OWNED BY tb_construccion.id;


--
-- Name: tb_dato_const_complemento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_dato_const_complemento (
    codigo integer NOT NULL,
    tipo character varying(30),
    cantidad integer
);


ALTER TABLE public.tb_dato_const_complemento OWNER TO postgres;

--
-- Name: tb_dato_construccion_com_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_dato_construccion_com_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_dato_construccion_com_codigo_seq OWNER TO postgres;

--
-- Name: tb_dato_construccion_com_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_dato_construccion_com_codigo_seq OWNED BY tb_dato_const_complemento.codigo;


--
-- Name: tb_datos_construccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_datos_construccion (
    id integer NOT NULL,
    codigo_construccion integer,
    codigo_dato integer
);


ALTER TABLE public.tb_datos_construccion OWNER TO postgres;

--
-- Name: tb_datos_construccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_datos_construccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_datos_construccion_id_seq OWNER TO postgres;

--
-- Name: tb_datos_construccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_datos_construccion_id_seq OWNED BY tb_datos_construccion.id;


--
-- Name: tb_direccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_direccion (
    id integer NOT NULL,
    numero_rmv integer,
    region_consejofg character varying(30),
    comuna character varying(30),
    comite_tierrau character varying(30),
    consejo_comunal character varying(30),
    ciudad character varying(100),
    localidad character varying(100),
    urbanizacion character varying(100),
    consejo_residencial character varying(100),
    barrio character varying(100),
    numero_civico integer,
    puntoreferencia text,
    idpersona integer
);


ALTER TABLE public.tb_direccion OWNER TO postgres;

--
-- Name: tb_direccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_direccion_id_seq OWNER TO postgres;

--
-- Name: tb_direccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_direccion_id_seq OWNED BY tb_direccion.id;


--
-- Name: tb_ubicacion_comunitaria; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_ubicacion_comunitaria (
    id integer NOT NULL,
    numero_rmv integer,
    region_consejofg text,
    comuna text,
    comite_tierrau text,
    consejo_comunal text,
    ciudad text,
    localidad text,
    urbanizacion text,
    consejo_residencial text,
    barrio text,
    numero_civico integer,
    puntoreferencia text,
    manzana character varying(255),
    parroquia character varying(255),
    sector integer,
    numero_comuna integer,
    numero_comite_urbano integer,
    numero_consejo_comunal integer,
    tipo_calle character varying(255),
    nombre_calle character varying(255)
);


ALTER TABLE public.tb_ubicacion_comunitaria OWNER TO postgres;

--
-- Name: tb_ubicacion_comunitaria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_ubicacion_comunitaria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_ubicacion_comunitaria_id_seq OWNER TO postgres;

--
-- Name: tb_ubicacion_comunitaria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_ubicacion_comunitaria_id_seq OWNED BY tb_ubicacion_comunitaria.id;


--
-- Name: tb_direccion_persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_direccion_persona (
    id integer DEFAULT nextval('tb_ubicacion_comunitaria_id_seq'::regclass) NOT NULL,
    ciudad text,
    localidad text,
    urbanizacion text,
    consejo_residencial text,
    barrio text,
    numero_civico integer,
    puntoreferencia text,
    parroquia character varying(255),
    sector character varying(255),
    tipo_calle character varying(255),
    nombre_calle character varying(255),
    idpersona smallint
);


ALTER TABLE public.tb_direccion_persona OWNER TO postgres;

--
-- Name: tb_ficha_catastral; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_ficha_catastral (
    id integer NOT NULL,
    numerocatastral character varying(255),
    numeroarchivo integer,
    fechainscripcion date,
    croquis character varying(255),
    fechavisita character varying(255),
    fechalevantamiento character varying(255),
    elaboradopor integer,
    revisadopor integer,
    observaciones text
);


ALTER TABLE public.tb_ficha_catastral OWNER TO postgres;

--
-- Name: tb_ficha_catastral_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_ficha_catastral_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_ficha_catastral_id_seq OWNER TO postgres;

--
-- Name: tb_ficha_catastral_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_ficha_catastral_id_seq OWNED BY tb_ficha_catastral.id;


--
-- Name: tb_inmueble; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_inmueble (
    id integer NOT NULL,
    idfichacatastral integer,
    idocupante integer,
    idpropietario integer,
    idterreno integer,
    idconstruccion integer,
    idregistro integer,
    idubicacioncomunitaria integer
);


ALTER TABLE public.tb_inmueble OWNER TO postgres;

--
-- Name: tb_inmueble_copy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_inmueble_copy (
    id integer NOT NULL,
    idfichacatastral integer,
    idocupante integer,
    idpropietario character varying(32),
    idterreno integer,
    idconstruccion integer,
    idregistro integer,
    idubicacioncomunitaria integer
);


ALTER TABLE public.tb_inmueble_copy OWNER TO postgres;

--
-- Name: tb_inmueble_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_inmueble_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_inmueble_id_seq OWNER TO postgres;

--
-- Name: tb_inmueble_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_inmueble_id_seq OWNED BY tb_inmueble.id;


--
-- Name: tb_inspecciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_inspecciones (
    id integer NOT NULL,
    tipo character varying(30),
    fecha date,
    observaciones text,
    id_inspector integer
);


ALTER TABLE public.tb_inspecciones OWNER TO postgres;

--
-- Name: tb_inspeciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_inspeciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_inspeciones_id_seq OWNER TO postgres;

--
-- Name: tb_inspeciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_inspeciones_id_seq OWNED BY tb_inspecciones.id;


--
-- Name: tb_manzana; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_manzana (
    id integer NOT NULL,
    codigo integer NOT NULL,
    nombre text
);


ALTER TABLE public.tb_manzana OWNER TO postgres;

--
-- Name: tb_manzana_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_manzana_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_manzana_id_seq OWNER TO postgres;

--
-- Name: tb_manzana_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_manzana_id_seq OWNED BY tb_manzana.id;


--
-- Name: tb_parcela; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_parcela (
    codigo integer NOT NULL,
    nombre text,
    descripcion text,
    id_m integer
);


ALTER TABLE public.tb_parcela OWNER TO postgres;

--
-- Name: tb_parcela_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_parcela_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_parcela_codigo_seq OWNER TO postgres;

--
-- Name: tb_parcela_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_parcela_codigo_seq OWNED BY tb_parcela.codigo;


--
-- Name: tb_parroquia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_parroquia (
    id integer NOT NULL,
    codigo character varying(5) NOT NULL,
    nombre character varying(20)
);


ALTER TABLE public.tb_parroquia OWNER TO postgres;

--
-- Name: tb_parroquia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_parroquia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_parroquia_id_seq OWNER TO postgres;

--
-- Name: tb_parroquia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_parroquia_id_seq OWNED BY tb_parroquia.id;


--
-- Name: tb_pedul; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_pedul (
    id integer NOT NULL,
    codigo integer NOT NULL,
    nombre text,
    descripcion text,
    cantidad_manzana integer
);


ALTER TABLE public.tb_pedul OWNER TO postgres;

--
-- Name: tb_pedul_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_pedul_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pedul_id_seq OWNER TO postgres;

--
-- Name: tb_pedul_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_pedul_id_seq OWNED BY tb_pedul.id;


--
-- Name: tb_pedul_manzana; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_pedul_manzana (
    id integer NOT NULL,
    id_p integer,
    id_m integer
);


ALTER TABLE public.tb_pedul_manzana OWNER TO postgres;

--
-- Name: tb_pedul_manzana_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_pedul_manzana_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_pedul_manzana_id_seq OWNER TO postgres;

--
-- Name: tb_pedul_manzana_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_pedul_manzana_id_seq OWNED BY tb_pedul_manzana.id;


--
-- Name: tb_persona; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_persona (
    id integer NOT NULL,
    nacionalidad character(1) NOT NULL,
    cedula integer NOT NULL,
    nombre1 character varying(20) NOT NULL,
    nombre2 character varying(20) NOT NULL,
    apellido1 character varying(20) NOT NULL,
    apellido2 character varying(20) NOT NULL,
    estado_civil character varying(10),
    correo character varying(30),
    telefono1 character varying(11),
    telefono2 character(11),
    tipo_p character varying(25),
    role character varying(15),
    cuenta character varying(15),
    clave character varying(10),
    estatus character varying(15),
    tipo character varying(20),
    chequeado bigint
);


ALTER TABLE public.tb_persona OWNER TO postgres;

--
-- Name: tb_persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_persona_id_seq OWNER TO postgres;

--
-- Name: tb_persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_persona_id_seq OWNED BY tb_persona.id;


--
-- Name: tb_persona_inspeccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_persona_inspeccion (
    id integer NOT NULL,
    id_persona integer,
    id_inspeccion integer
);


ALTER TABLE public.tb_persona_inspeccion OWNER TO postgres;

--
-- Name: tb_persona_inspeccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_persona_inspeccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_persona_inspeccion_id_seq OWNER TO postgres;

--
-- Name: tb_persona_inspeccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_persona_inspeccion_id_seq OWNED BY tb_persona_inspeccion.id;


--
-- Name: tb_puerta_const; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_puerta_const (
    codigo integer NOT NULL,
    tipo character varying(30),
    cantidad integer
);


ALTER TABLE public.tb_puerta_const OWNER TO postgres;

--
-- Name: tb_puerta_const_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_puerta_const_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_puerta_const_codigo_seq OWNER TO postgres;

--
-- Name: tb_puerta_const_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_puerta_const_codigo_seq OWNED BY tb_puerta_const.codigo;


--
-- Name: tb_referencia_direccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_referencia_direccion (
    id integer NOT NULL,
    nombre character varying(100)
);


ALTER TABLE public.tb_referencia_direccion OWNER TO postgres;

--
-- Name: tb_referencia_direccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_referencia_direccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_referencia_direccion_id_seq OWNER TO postgres;

--
-- Name: tb_referencia_direccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_referencia_direccion_id_seq OWNED BY tb_referencia_direccion.id;


--
-- Name: tb_registro_publico; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_registro_publico (
    id integer NOT NULL,
    numero integer NOT NULL,
    folio character varying(255) NOT NULL,
    tomo character varying(255) NOT NULL,
    protocolo character varying(255) NOT NULL,
    fecha character varying(255) NOT NULL,
    aream2terreno real,
    aream2cont real,
    valorbs real
);


ALTER TABLE public.tb_registro_publico OWNER TO postgres;

--
-- Name: tb_registro_publico_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_registro_publico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_registro_publico_id_seq OWNER TO postgres;

--
-- Name: tb_registro_publico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_registro_publico_id_seq OWNED BY tb_registro_publico.id;


--
-- Name: tb_servicio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_servicio (
    id integer NOT NULL,
    tipo character varying(30)
);


ALTER TABLE public.tb_servicio OWNER TO postgres;

--
-- Name: tb_servicio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_servicio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_servicio_id_seq OWNER TO postgres;

--
-- Name: tb_servicio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_servicio_id_seq OWNED BY tb_servicio.id;


--
-- Name: tb_solvencia_municipal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_solvencia_municipal (
    id integer NOT NULL,
    monto integer,
    estatus character varying(30),
    fecha date,
    id_persona integer,
    anualidad character varying(4) NOT NULL
);


ALTER TABLE public.tb_solvencia_municipal OWNER TO postgres;

--
-- Name: tb_solvencia_municipal_anualidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_solvencia_municipal_anualidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_solvencia_municipal_anualidad_seq OWNER TO postgres;

--
-- Name: tb_solvencia_municipal_anualidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_solvencia_municipal_anualidad_seq OWNED BY tb_solvencia_municipal.anualidad;


--
-- Name: tb_solvencia_municipal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_solvencia_municipal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_solvencia_municipal_id_seq OWNER TO postgres;

--
-- Name: tb_solvencia_municipal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_solvencia_municipal_id_seq OWNED BY tb_solvencia_municipal.id;


--
-- Name: tb_terreno; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_terreno (
    id integer NOT NULL,
    topografia character varying(255),
    acceso character varying(255),
    forma character varying(255),
    ubicacion character varying(255),
    entornofisico character varying(255),
    mejorasterreno character varying(255),
    teneciaterreno character varying(255),
    registropropiedad character varying(255),
    usoactual character varying(255),
    linderonorte character varying(255),
    linderosur character varying(255),
    linderoeste character varying(255),
    linderooeste character varying(255),
    coordenadasutmnorte character varying(255),
    coordenadasutmeste character varying(255),
    coordenadasutmhuso character varying(255),
    areametroscuadrados character varying(255),
    valorunitariometroscuadrados character varying(255),
    factorajustearea character varying(255),
    factorajusteforma character varying(255),
    sector character varying(255),
    valorajustadometroscuadrados character varying(255),
    valortotal character varying(255),
    observacioneconomica text,
    largo_terreno real,
    ancho_terreno real,
    unidad_tributaria real
);


ALTER TABLE public.tb_terreno OWNER TO postgres;

--
-- Name: tb_terreno_coordenadas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_terreno_coordenadas (
    id integer NOT NULL,
    codigo_terreno integer,
    norte character varying(30),
    este character varying(30),
    uso character varying(30)
);


ALTER TABLE public.tb_terreno_coordenadas OWNER TO postgres;

--
-- Name: tb_terreno_coordenadas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_terreno_coordenadas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_terreno_coordenadas_id_seq OWNER TO postgres;

--
-- Name: tb_terreno_coordenadas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_terreno_coordenadas_id_seq OWNED BY tb_terreno_coordenadas.id;


--
-- Name: tb_terreno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_terreno_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_terreno_id_seq OWNER TO postgres;

--
-- Name: tb_terreno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_terreno_id_seq OWNED BY tb_terreno.id;


--
-- Name: tb_terreno_linderos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_terreno_linderos (
    id integer NOT NULL,
    codigo_terreno integer,
    norte character varying(30),
    sur character varying(30),
    este character varying(30),
    oeste character varying(30)
);


ALTER TABLE public.tb_terreno_linderos OWNER TO postgres;

--
-- Name: tb_terreno_linderos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_terreno_linderos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_terreno_linderos_id_seq OWNER TO postgres;

--
-- Name: tb_terreno_linderos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_terreno_linderos_id_seq OWNED BY tb_terreno_linderos.id;


--
-- Name: tb_terreno_valoracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_terreno_valoracion (
    id integer NOT NULL,
    codigo_terreno integer,
    valor_unitario integer,
    factor_ajuste character varying,
    sector character varying(30),
    area_metros integer,
    valor_total integer
);


ALTER TABLE public.tb_terreno_valoracion OWNER TO postgres;

--
-- Name: tb_terreno_valoracion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_terreno_valoracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_terreno_valoracion_id_seq OWNER TO postgres;

--
-- Name: tb_terreno_valoracion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_terreno_valoracion_id_seq OWNED BY tb_terreno_valoracion.id;


--
-- Name: tb_terrenoservicios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_terrenoservicios (
    id integer NOT NULL,
    idservicios integer,
    idterreno integer
);


ALTER TABLE public.tb_terrenoservicios OWNER TO postgres;

--
-- Name: tb_terrenoservicios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_terrenoservicios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_terrenoservicios_id_seq OWNER TO postgres;

--
-- Name: tb_terrenoservicios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_terrenoservicios_id_seq OWNED BY tb_terrenoservicios.id;


--
-- Name: tb_unidad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_unidad (
    codigo integer NOT NULL,
    precio real,
    fecha character varying(20),
    estatus character varying(20)
);


ALTER TABLE public.tb_unidad OWNER TO postgres;

--
-- Name: tb_unidad_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_unidad_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_unidad_codigo_seq OWNER TO postgres;

--
-- Name: tb_unidad_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_unidad_codigo_seq OWNED BY tb_unidad.codigo;


--
-- Name: tb_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_usuario (
    id integer DEFAULT nextval('tb_persona_id_seq'::regclass) NOT NULL,
    nacionalidad character(1) NOT NULL,
    cedula integer NOT NULL,
    nombre1 character varying(20) NOT NULL,
    nombre2 character varying(20) NOT NULL,
    apellido1 character varying(20) NOT NULL,
    apellido2 character varying(20) NOT NULL,
    estado_civil character varying(10),
    correo character varying(30),
    telefono1 character varying(11),
    telefono2 character(11),
    tipo_p character varying(25),
    role character varying(15),
    cuenta character varying(15),
    clave character varying(255),
    estatus character varying(15),
    tipo character varying(20),
    chequeado bigint
);


ALTER TABLE public.tb_usuario OWNER TO postgres;

--
-- Name: tb_valoracion_construccion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_valoracion_construccion (
    id integer NOT NULL,
    codigo_construccion integer,
    tipologia character varying(30),
    area_mts_cuadrados character varying(30),
    valor_mts_cuadrados character varying(30),
    descripcion text,
    valor_actual integer
);


ALTER TABLE public.tb_valoracion_construccion OWNER TO postgres;

--
-- Name: tb_valoracion_construccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_valoracion_construccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_valoracion_construccion_id_seq OWNER TO postgres;

--
-- Name: tb_valoracion_construccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_valoracion_construccion_id_seq OWNED BY tb_valoracion_construccion.id;


--
-- Name: tb_ventana_const; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tb_ventana_const (
    codigo integer NOT NULL,
    tipo character varying(30),
    cantidad integer
);


ALTER TABLE public.tb_ventana_const OWNER TO postgres;

--
-- Name: tb_ventana_const_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tb_ventana_const_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_ventana_const_codigo_seq OWNER TO postgres;

--
-- Name: tb_ventana_const_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tb_ventana_const_codigo_seq OWNED BY tb_ventana_const.codigo;


--
-- Name: view_certificacion_linderos; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW view_certificacion_linderos AS
    SELECT tb_certificacion_linderos.id, tb_certificacion_linderos.area, tb_certificacion_linderos.norte, tb_certificacion_linderos.sur, tb_certificacion_linderos.este, tb_certificacion_linderos.oeste, tb_certificacion_linderos.lnorte, tb_certificacion_linderos.lsur, tb_certificacion_linderos.leste, tb_certificacion_linderos.loeste, tb_certificacion_linderos.fecha, tb_certificacion_linderos.observacion, tb_certificacion_linderos.ubicacion, tb_persona.nacionalidad, tb_persona.cedula, tb_persona.nombre1, tb_persona.nombre2, tb_persona.apellido1, tb_persona.apellido2 FROM tb_certificacion_linderos, tb_persona WHERE (tb_certificacion_linderos.id_persona = tb_persona.id);


ALTER TABLE public.view_certificacion_linderos OWNER TO postgres;

--
-- Name: view_solvencia_municipal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW view_solvencia_municipal AS
    SELECT tb_solvencia_municipal.id, tb_solvencia_municipal.monto, tb_solvencia_municipal.estatus, tb_solvencia_municipal.fecha, tb_solvencia_municipal.anualidad, tb_solvencia_municipal.id_persona, tb_persona.nacionalidad, tb_persona.cedula, tb_persona.nombre1, tb_persona.nombre2, tb_persona.apellido1, tb_persona.apellido2, tb_persona.telefono1, tb_persona.telefono2, tb_persona.tipo_p FROM tb_solvencia_municipal, tb_persona WHERE (tb_solvencia_municipal.id_persona = tb_persona.id);


ALTER TABLE public.view_solvencia_municipal OWNER TO postgres;

--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_bano_const ALTER COLUMN codigo SET DEFAULT nextval('tb_bano_const_codigo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_certificacion_linderos ALTER COLUMN id SET DEFAULT nextval('tb_certificacion_linderos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_const_bano ALTER COLUMN id SET DEFAULT nextval('tb_const_bano_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_const_puerta ALTER COLUMN id SET DEFAULT nextval('tb_const_puerta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_const_ventana ALTER COLUMN id SET DEFAULT nextval('tb_const_ventana_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_construccion ALTER COLUMN id SET DEFAULT nextval('tb_construccion_id_seq'::regclass);


--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_dato_const_complemento ALTER COLUMN codigo SET DEFAULT nextval('tb_dato_construccion_com_codigo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_datos_construccion ALTER COLUMN id SET DEFAULT nextval('tb_datos_construccion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_direccion ALTER COLUMN id SET DEFAULT nextval('tb_direccion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_ficha_catastral ALTER COLUMN id SET DEFAULT nextval('tb_ficha_catastral_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_inmueble ALTER COLUMN id SET DEFAULT nextval('tb_inmueble_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_inspecciones ALTER COLUMN id SET DEFAULT nextval('tb_inspeciones_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_manzana ALTER COLUMN id SET DEFAULT nextval('tb_manzana_id_seq'::regclass);


--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_parcela ALTER COLUMN codigo SET DEFAULT nextval('tb_parcela_codigo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_parroquia ALTER COLUMN id SET DEFAULT nextval('tb_parroquia_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pedul ALTER COLUMN id SET DEFAULT nextval('tb_pedul_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_pedul_manzana ALTER COLUMN id SET DEFAULT nextval('tb_pedul_manzana_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_persona ALTER COLUMN id SET DEFAULT nextval('tb_persona_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_persona_inspeccion ALTER COLUMN id SET DEFAULT nextval('tb_persona_inspeccion_id_seq'::regclass);


--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_puerta_const ALTER COLUMN codigo SET DEFAULT nextval('tb_puerta_const_codigo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_referencia_direccion ALTER COLUMN id SET DEFAULT nextval('tb_referencia_direccion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_registro_publico ALTER COLUMN id SET DEFAULT nextval('tb_registro_publico_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_servicio ALTER COLUMN id SET DEFAULT nextval('tb_servicio_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_solvencia_municipal ALTER COLUMN id SET DEFAULT nextval('tb_solvencia_municipal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_terreno ALTER COLUMN id SET DEFAULT nextval('tb_terreno_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_terreno_coordenadas ALTER COLUMN id SET DEFAULT nextval('tb_terreno_coordenadas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_terreno_linderos ALTER COLUMN id SET DEFAULT nextval('tb_terreno_linderos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_terreno_valoracion ALTER COLUMN id SET DEFAULT nextval('tb_terreno_valoracion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_terrenoservicios ALTER COLUMN id SET DEFAULT nextval('tb_terrenoservicios_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_ubicacion_comunitaria ALTER COLUMN id SET DEFAULT nextval('tb_ubicacion_comunitaria_id_seq'::regclass);


--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_unidad ALTER COLUMN codigo SET DEFAULT nextval('tb_unidad_codigo_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_valoracion_construccion ALTER COLUMN id SET DEFAULT nextval('tb_valoracion_construccion_id_seq'::regclass);


--
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tb_ventana_const ALTER COLUMN codigo SET DEFAULT nextval('tb_ventana_const_codigo_seq'::regclass);


--
-- Data for Name: tb_bano_const; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_bano_const_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_bano_const_codigo_seq', 1, false);


--
-- Data for Name: tb_certificacion_linderos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_certificacion_linderos VALUES (6, 12, 12, 14, 14, '168', 'fdfdfdfdff', 'dff', 'dfdf', 'dfdf', 'catuaro', '01-11-2018', 'todo fino', 16, 40);
INSERT INTO tb_certificacion_linderos VALUES (1, 4, 4, 3, 3, '12', 'casa 1', 'casa 2', 'casa 3', 'casa 4', 'las violetas casa del vecino', '17-06-2018', 'el aera  total esta enmarcada en la veracidad de los datos del propietario', 4, 40);
INSERT INTO tb_certificacion_linderos VALUES (3, 12, 12, 14, 14, '168', 'vecino 1', 'vecino 2', 'vecino 3', 'vecino 4', 'paraiso k los trigales', '01-11-2018', 'todo bien aqui', 16, 40);
INSERT INTO tb_certificacion_linderos VALUES (4, 12, 12, 14, 14, '168', 'dfdfd', 'dfdfd', 'dfdf', 'dff', 'dfdfdfdfdfdfdfdffddf', '05-11-2018', 'dfdfdfdfdfdfdfdffddf', 21, 40);
INSERT INTO tb_certificacion_linderos VALUES (2, 12, 12, 6, 6, '72', 'el vecino 123', 'el vecino 2', 'el vecino 3', 'el vecino 4', 'la santa ana', '24-06-2018', 'la santa ana', 21, 40);
INSERT INTO tb_certificacion_linderos VALUES (5, 12, 12, 12, 12, '144', 'dfdf', 'dfdf', 'dfdf', 'dfd', 'dfdfffgfg', '05-11-2018', 'dfdfdfgfgfgfgfgfgfgfg', 21, 39);


--
-- Name: tb_certificacion_linderos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_certificacion_linderos_id_seq', 6, true);


--
-- Data for Name: tb_const_bano; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_const_bano_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_const_bano_id_seq', 1, false);


--
-- Data for Name: tb_const_puerta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_const_puerta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_const_puerta_id_seq', 1, false);


--
-- Data for Name: tb_const_ventana; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_const_ventana_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_const_ventana_id_seq', 1, false);


--
-- Data for Name: tb_construccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_construccion VALUES (52, 'Quinta', 'Bifamiliar', 'Arrendamiento', 'Nacional', 'Prefabricado', 'Cerchas', 'Plafon', 'Madera/Teja', 'Ladrillo', 'Cemento Blanco', 'Caucho', 'Externa', 'Cemento Pulido', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', '0', 'Malo', '1', '1', '1', '1', '1', '1', 'dfdfdf', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
INSERT INTO tb_construccion VALUES (53, 'Quinta', 'Unifamiliar', 'Propiedad', 'Municipal Propio', 'Concreto Armado', 'Concreto Armado', 'Cielo Raso (Econom.)', 'Madera/Teja', 'Bloque de Cemento', 'Friso Liso', 'Oleo', 'Embutidas', 'Ceramica', '1', '2', '3', '1', '2', '3', '3', '3', '1', '1', '1', '1', '1', '2', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Excelente', '1', '2', '1', '2', '1', '2', 'cdfdfd', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
INSERT INTO tb_construccion VALUES (54, 'Chalet', 'Bifamiliar', 'Arrendamiento', 'Municipal Propio', 'Prefabricado', 'Cerchas', 'Machihembrado (Madera)', 'Platabanda', 'Bloque de Arcilla', 'Friso Liso', 'Caucho', 'Embutidas', 'Granito', '1', '2', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '2', '3', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '0', 'Regular', '1', '2', '1', '2', '1', '2', 'ninguna', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
INSERT INTO tb_construccion VALUES (55, 'Quinta', 'Unifamiliar', 'Propiedad', 'Municipal Propio', 'Metalica', 'Metalica', 'Plafon', 'Madera/Teja', 'Bloque de Arcilla', 'Sin Friso', 'Caucho', 'Embutidas', 'Ceramica', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2', '2', 'Excelente', '1', '2', '1', '2', '1', '2', 'fggfg', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
INSERT INTO tb_construccion VALUES (51, 'Casa Tradicional', 'Unifamiliar', 'Propiedad', 'Municipal Propio', 'Concreto Armado', 'Concreto Armado', 'Plafon', 'Madera/Teja', 'Bloque de Arcilla', 'Yeso/Cal', 'Caucho', 'Industrial', 'Cemento Pulido', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2', '0', '0', '0', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '3', '2', '0', 'Excelente', '4', '2', '1', '3', '4', '1', 'la construccion esta fina', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
INSERT INTO tb_construccion VALUES (56, 'Casa Tradicional', 'Multifamiliar', 'Propiedad', 'Municipal Propio', 'Concreto Armado', 'Concreto Armado', 'Machihembrado (Madera)', 'Caña Brava', 'Bloque de Arcilla', 'Sin Friso', 'Caucho', 'Embutidas', 'Cemento Pulido', '2', '1', '3', '3', '3', '3', '3', '1', '1', '2', '0', '0', '1', '2', 'ninguna', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Regular', '1930', '40', '1', '2', '3', '1', 'ninguno ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');


--
-- Name: tb_construccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_construccion_id_seq', 56, true);


--
-- Data for Name: tb_dato_const_complemento; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_dato_construccion_com_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_dato_construccion_com_codigo_seq', 1, false);


--
-- Data for Name: tb_datos_construccion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_datos_construccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_datos_construccion_id_seq', 1, false);


--
-- Data for Name: tb_direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_direccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_direccion_id_seq', 3, true);


--
-- Data for Name: tb_direccion_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_direccion_persona VALUES (48, 'cariaco', 'cariaco', 'cariaco', 'ninguno', 'ninguno', 0, 'al lado de rodolfito', 'cariaco', 'campo alegre ', 'Av', 'calle andres eloy blanco ', 21);
INSERT INTO tb_direccion_persona VALUES (38, 'dfdf', 'dfdf', 'dfd', 'dfdf', 'dfd', 344, 'dfdfdfdf', 'Cariaco', 'BARRIO CARINICUAO', 'Av', 'dfdf', 17);
INSERT INTO tb_direccion_persona VALUES (37, 'Muelle de Cariaco', 'Ribero', 'Las palmas', 'los cocos', 'el cortijo', 232, 'cerca del cementerio', 'Cariaco', 'BARRIO CARINICUAO', 'Av', 'las pameras', 16);
INSERT INTO tb_direccion_persona VALUES (40, 'dfdf', 'dfdf', 'dfdf', 'dffd', 'df', 3434, 'dfdfff', 'sfddf', 'dfdf', 'Prol', 'fdf', 18);
INSERT INTO tb_direccion_persona VALUES (42, 'erer', 'er', 'er', 'err', 'er', 344, 'dfff', 'erer', 'dff', 'Trav', 'dfdf', 15);
INSERT INTO tb_direccion_persona VALUES (43, 'Generico', 'Generico', 'Generico', 'Generico', 'Generico', 9999, 'Generico', 'Generico', 'Generico', 'Generico', 'Generico', 13);
INSERT INTO tb_direccion_persona VALUES (36, 'Cumana', 'la llanada', 'los mangos', 'los mangos bajos', 'el mango alto', 123, 'cerca de la policial municipal', 'Santa Cruz', 'BARRIO CAMPO ALEGRE ', 'Av', 'las pumalacas', 4);


--
-- Data for Name: tb_ficha_catastral; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_ficha_catastral VALUES (71, '01-8-1-04', 2018, '2018-10-02', 'CROQUIS', '01-10-2018', '08-10-2018', 8, 8, 'wedwewe');
INSERT INTO tb_ficha_catastral VALUES (72, '01-8-5-01', 2018332, '2018-10-02', 'CROQUIS', '01-10-2018', '02-10-2018', 8, 8, 'scdfdf');
INSERT INTO tb_ficha_catastral VALUES (73, '01-8-3-01', 43434, '2018-10-16', 'CROQUIS', '01-10-2018', '10-10-2018', 8, 8, 'ninguna');
INSERT INTO tb_ficha_catastral VALUES (74, '01-10-5-04', 43434, '2018-10-02', 'CROQUIS', '01-10-2018', '09-10-2018', 3, 3, 'dfdfdfd');
INSERT INTO tb_ficha_catastral VALUES (70, '01-9-4-01', 2018332, '2018-10-01', 'CROQUIS', '01-10-2018', '14-10-2018', 3, 6, 'la ficha esta verificada');
INSERT INTO tb_ficha_catastral VALUES (75, '01-7-3-02', 4, '2018-11-15', 'CROQUIS', '01-11-2018', '01-11-2018', 7, 8, 'ninguno ');


--
-- Name: tb_ficha_catastral_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_ficha_catastral_id_seq', 75, true);


--
-- Data for Name: tb_inmueble; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_inmueble VALUES (16, 71, 17, 4, 72, 52, 30, 39);
INSERT INTO tb_inmueble VALUES (17, 72, 18, 18, 73, 53, 31, 44);
INSERT INTO tb_inmueble VALUES (18, 73, 16, 16, 74, 54, 32, 45);
INSERT INTO tb_inmueble VALUES (19, 74, 15, 15, 75, 55, 33, 46);
INSERT INTO tb_inmueble VALUES (15, 70, 13, 13, 71, 51, 29, 35);
INSERT INTO tb_inmueble VALUES (20, 75, 21, 21, 76, 56, 34, 47);


--
-- Data for Name: tb_inmueble_copy; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_inmueble_copy VALUES (2, 11, 2, '10', 15, 5, 10, 4);
INSERT INTO tb_inmueble_copy VALUES (3, 13, 2, '10', 15, 5, 10, 4);
INSERT INTO tb_inmueble_copy VALUES (4, 54, 10, '10', 55, 15, 14, 11);


--
-- Name: tb_inmueble_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_inmueble_id_seq', 20, true);


--
-- Data for Name: tb_inspecciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_inspecciones VALUES (12, 'Terreno', '2018-10-07', 'ninguna por los momentos', 7);
INSERT INTO tb_inspecciones VALUES (13, 'Lindero', '2018-10-01', 'gtytytytytrytytyty', 8);
INSERT INTO tb_inspecciones VALUES (14, 'Terreno', '2018-11-01', 'ninguna', 3);


--
-- Name: tb_inspeciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_inspeciones_id_seq', 14, true);


--
-- Data for Name: tb_manzana; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_manzana VALUES (7, 3, 'Calle romulo gallego');
INSERT INTO tb_manzana VALUES (8, 4, 'Calle sucre');
INSERT INTO tb_manzana VALUES (9, 5, 'Calle la plaza');
INSERT INTO tb_manzana VALUES (10, 6, 'calle Bermúdez');
INSERT INTO tb_manzana VALUES (11, 7, 'calle las flores');
INSERT INTO tb_manzana VALUES (13, 9, 'Calle bolivar');
INSERT INTO tb_manzana VALUES (14, 10, 'Calle Ayacucho');
INSERT INTO tb_manzana VALUES (15, 11, 'Calle Carabobo');
INSERT INTO tb_manzana VALUES (16, 12, 'Calle angel maria arcia');
INSERT INTO tb_manzana VALUES (17, 13, 'Calle Araure');
INSERT INTO tb_manzana VALUES (18, 14, 'Calle junin');
INSERT INTO tb_manzana VALUES (19, 15, 'Calle jesus guzman');
INSERT INTO tb_manzana VALUES (20, 16, 'Calle principal');
INSERT INTO tb_manzana VALUES (21, 17, 'Calle las animas');
INSERT INTO tb_manzana VALUES (22, 18, 'Calle dolores benita de luna');
INSERT INTO tb_manzana VALUES (23, 19, 'Calle santa ana ');
INSERT INTO tb_manzana VALUES (24, 20, 'Calle la pas ');
INSERT INTO tb_manzana VALUES (25, 21, 'Calle diego Carbonell');
INSERT INTO tb_manzana VALUES (26, 22, 'Calle 10 de junio');
INSERT INTO tb_manzana VALUES (27, 23, 'Calle zamara');
INSERT INTO tb_manzana VALUES (28, 24, 'Terreno lino olivieri');
INSERT INTO tb_manzana VALUES (29, 29, 'Urbanización Venezuela');
INSERT INTO tb_manzana VALUES (30, 30, 'Urbanizacion  jose franciasco  bermudez');
INSERT INTO tb_manzana VALUES (31, 31, 'Barrio democracia');
INSERT INTO tb_manzana VALUES (32, 32, 'Calle san isidro ');
INSERT INTO tb_manzana VALUES (33, 33, 'Calle buenos aires ');
INSERT INTO tb_manzana VALUES (35, 35, 'Barrio 22 de octubre');
INSERT INTO tb_manzana VALUES (36, 36, 'Calle las catalinas');
INSERT INTO tb_manzana VALUES (37, 37, 'Calle enrrique brekermans');
INSERT INTO tb_manzana VALUES (38, 38, 'Calle san Felipe');
INSERT INTO tb_manzana VALUES (39, 39, 'Calle las pelencia');
INSERT INTO tb_manzana VALUES (40, 40, 'Canal de riego');
INSERT INTO tb_manzana VALUES (41, 41, 'Antigua carretera cumana-cariaco');
INSERT INTO tb_manzana VALUES (42, 42, 'Calle las palmeras');
INSERT INTO tb_manzana VALUES (43, 43, 'Calle las margaritas');
INSERT INTO tb_manzana VALUES (44, 44, 'Barrio ribero');
INSERT INTO tb_manzana VALUES (45, 45, 'Calle ribero');
INSERT INTO tb_manzana VALUES (46, 46, 'Calle Estanislao rendon ');
INSERT INTO tb_manzana VALUES (47, 47, 'Calle sanjuen de dios ');
INSERT INTO tb_manzana VALUES (48, 49, 'Calle miranda');
INSERT INTO tb_manzana VALUES (49, 50, 'Barrio juan Quijano');
INSERT INTO tb_manzana VALUES (50, 51, 'Urbanización carlos andres peres ');
INSERT INTO tb_manzana VALUES (51, 52, 'Calle elojio velazquez');
INSERT INTO tb_manzana VALUES (52, 53, 'Calle andres Eloy balnco ');
INSERT INTO tb_manzana VALUES (6, 2, 'Calle Congresillo');
INSERT INTO tb_manzana VALUES (5, 1, 'Calle Juncal');
INSERT INTO tb_manzana VALUES (53, 80, 'Calle Jesus Aguilera');


--
-- Name: tb_manzana_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_manzana_id_seq', 53, true);


--
-- Data for Name: tb_parcela; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_parcela VALUES (1, 'Parcela cariaco', 'Los linderos', 3);
INSERT INTO tb_parcela VALUES (2, 'Parcela 4', 'casco de los cariaco', 4);


--
-- Name: tb_parcela_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_parcela_codigo_seq', 2, true);


--
-- Data for Name: tb_parroquia; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_parroquia VALUES (41, '04', 'Santa Cruz');
INSERT INTO tb_parroquia VALUES (39, '02', 'Rendon');
INSERT INTO tb_parroquia VALUES (42, '05', 'Santa Maria');
INSERT INTO tb_parroquia VALUES (40, '03', 'Catuaro');
INSERT INTO tb_parroquia VALUES (49, '07', 'Sucre');
INSERT INTO tb_parroquia VALUES (38, '01', 'Cariaco');
INSERT INTO tb_parroquia VALUES (50, '12', 'Cantaura');


--
-- Name: tb_parroquia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_parroquia_id_seq', 50, true);


--
-- Data for Name: tb_pedul; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_pedul VALUES (12, 6, 'EL PORVENIR DE CARIACO', 'no posee nombre las calles o este sector no tiene actualizada la información y posee 10 manzana', 10);
INSERT INTO tb_pedul VALUES (8, 2, 'BARRIO CAMPO ALEGRE ', 'Este sector tiene 22 Mazana del 1 al 22  ', 22);
INSERT INTO tb_pedul VALUES (10, 4, 'BARRIO CARIAQUITO', 'Este sector esta constituido por  21 manzanas del 1 al 21 ', 21);
INSERT INTO tb_pedul VALUES (11, 5, 'VALLE VERDE ', 'Las manoas no posee nombre las calles o este sector no tiene actualizada la información y posee 12 manzanas   ', 12);
INSERT INTO tb_pedul VALUES (7, 1, 'CENTRO DE CARIACO', 'Este sector esta constituido por 44  manzana, y corresponden  del  número 1 al 44   ', 44);
INSERT INTO tb_pedul VALUES (9, 3, 'BARRIO CARINICUAO', 'Este sector esta constutuido por 15 manzanas del 1 al 15  ', 15);
INSERT INTO tb_pedul VALUES (13, 12, 'TRONCAL 9', 'Carretera nacional cariaco cumana', 12);


--
-- Name: tb_pedul_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_pedul_id_seq', 13, true);


--
-- Data for Name: tb_pedul_manzana; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_pedul_manzana VALUES (124, 7, 7);
INSERT INTO tb_pedul_manzana VALUES (125, 7, 8);
INSERT INTO tb_pedul_manzana VALUES (126, 7, 9);
INSERT INTO tb_pedul_manzana VALUES (127, 7, 10);
INSERT INTO tb_pedul_manzana VALUES (128, 7, 11);
INSERT INTO tb_pedul_manzana VALUES (129, 7, 13);
INSERT INTO tb_pedul_manzana VALUES (130, 7, 14);
INSERT INTO tb_pedul_manzana VALUES (131, 7, 15);
INSERT INTO tb_pedul_manzana VALUES (132, 7, 16);
INSERT INTO tb_pedul_manzana VALUES (133, 7, 17);
INSERT INTO tb_pedul_manzana VALUES (134, 7, 18);
INSERT INTO tb_pedul_manzana VALUES (135, 7, 19);
INSERT INTO tb_pedul_manzana VALUES (136, 7, 48);
INSERT INTO tb_pedul_manzana VALUES (137, 7, 6);
INSERT INTO tb_pedul_manzana VALUES (138, 7, 5);
INSERT INTO tb_pedul_manzana VALUES (139, 9, 33);
INSERT INTO tb_pedul_manzana VALUES (140, 9, 37);
INSERT INTO tb_pedul_manzana VALUES (141, 9, 38);
INSERT INTO tb_pedul_manzana VALUES (142, 9, 39);
INSERT INTO tb_pedul_manzana VALUES (143, 9, 40);
INSERT INTO tb_pedul_manzana VALUES (144, 9, 41);
INSERT INTO tb_pedul_manzana VALUES (77, 8, 20);
INSERT INTO tb_pedul_manzana VALUES (78, 8, 21);
INSERT INTO tb_pedul_manzana VALUES (79, 8, 22);
INSERT INTO tb_pedul_manzana VALUES (80, 8, 23);
INSERT INTO tb_pedul_manzana VALUES (81, 8, 24);
INSERT INTO tb_pedul_manzana VALUES (82, 8, 25);
INSERT INTO tb_pedul_manzana VALUES (83, 8, 26);
INSERT INTO tb_pedul_manzana VALUES (84, 8, 27);
INSERT INTO tb_pedul_manzana VALUES (85, 8, 28);
INSERT INTO tb_pedul_manzana VALUES (86, 8, 29);
INSERT INTO tb_pedul_manzana VALUES (87, 8, 31);
INSERT INTO tb_pedul_manzana VALUES (88, 8, 32);
INSERT INTO tb_pedul_manzana VALUES (89, 8, 33);
INSERT INTO tb_pedul_manzana VALUES (90, 8, 35);
INSERT INTO tb_pedul_manzana VALUES (91, 8, 48);
INSERT INTO tb_pedul_manzana VALUES (92, 10, 42);
INSERT INTO tb_pedul_manzana VALUES (93, 10, 43);
INSERT INTO tb_pedul_manzana VALUES (94, 10, 44);
INSERT INTO tb_pedul_manzana VALUES (95, 10, 45);
INSERT INTO tb_pedul_manzana VALUES (96, 10, 46);
INSERT INTO tb_pedul_manzana VALUES (97, 10, 47);
INSERT INTO tb_pedul_manzana VALUES (98, 10, 48);
INSERT INTO tb_pedul_manzana VALUES (99, 10, 49);
INSERT INTO tb_pedul_manzana VALUES (100, 10, 50);
INSERT INTO tb_pedul_manzana VALUES (101, 10, 51);
INSERT INTO tb_pedul_manzana VALUES (102, 10, 52);


--
-- Name: tb_pedul_manzana_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_pedul_manzana_id_seq', 144, true);


--
-- Data for Name: tb_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_persona VALUES (8, 'E', 21321456, 'Juan', 'Jose', 'Jimiez', 'Espinoza', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (5, 'V', 14787312, 'Juan', 'Manuel', 'Jimiez', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (7, 'V', 20737890, 'Fabian', 'Eduardo', 'Espinoza', 'Espinoza', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (6, 'V', 15883731, 'Luis', 'Manuel', 'Perez', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (3, 'V', 24344284, 'Maria', 'Jose', 'Brito', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (9, 'V', 14787365, 'Maria', 'Manuel', 'Rodriguez', 'Espinoza', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (17, 'V', 4296072, 'Jose', 'Miguel', 'Hernandez', 'Acosta', 'NULL', 'joser@gmail.com', '04167890543', '04167890543', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', '', 1);
INSERT INTO tb_persona VALUES (13, 'V', 12345678, 'Petra', 'Josefa', 'Perez', 'Perez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', NULL, 1);
INSERT INTO tb_persona VALUES (18, 'E', 4296075, 'Luis ', 'Miguel', 'Bello', 'Acosta', 'NULL', 'joser@gmail.com', '04167890543', '04167890543', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', '', 1);
INSERT INTO tb_persona VALUES (16, 'V', 9087654, 'Rosbelys', 'Carolina', 'Hernandez', 'Rojas', 'NULL', 'joser@gmail.com', '04167890590', '04167890593', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', '', 1);
INSERT INTO tb_persona VALUES (15, 'P', 20432789, 'Jose', 'Eduardo', 'Bello', 'Acosta', 'NULL', 'josebello@gmail.com', '04167890590', '04167890543', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', NULL, 1);
INSERT INTO tb_persona VALUES (4, 'T', 12402040, 'Maria', 'Luisa', 'Jimiez', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', NULL, 0);
INSERT INTO tb_persona VALUES (10, 'E', 117654321, 'Julia', 'Maria', 'Garate', 'Veliz', 'NULL', 'julia@gmail.com', '04167890543', '04167890512', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', NULL, 0);
INSERT INTO tb_persona VALUES (20, 'V', 20022993, 'Julian', 'Jose', 'Vera', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Inspector', 'NULL', 'NULL', 'NULL', 'Activo', NULL, NULL);
INSERT INTO tb_persona VALUES (21, 'V', 20022994, 'Julio', 'Jose', 'Lara', 'Mata', 'NULL', 'julio@gmail.com', '04143214567', '04143214567', 'Propietario/Ocupante', 'NULL', 'NULL', 'NULL', 'Activo', '', 1);


--
-- Name: tb_persona_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_persona_id_seq', 21, true);


--
-- Data for Name: tb_persona_inspeccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_persona_inspeccion VALUES (3, 13, 12);
INSERT INTO tb_persona_inspeccion VALUES (4, 10, 13);
INSERT INTO tb_persona_inspeccion VALUES (5, 21, 14);


--
-- Name: tb_persona_inspeccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_persona_inspeccion_id_seq', 5, true);


--
-- Data for Name: tb_puerta_const; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_puerta_const_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_puerta_const_codigo_seq', 1, false);


--
-- Data for Name: tb_referencia_direccion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_referencia_direccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_referencia_direccion_id_seq', 1, false);


--
-- Data for Name: tb_registro_publico; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_registro_publico VALUES (30, 2323, 'pg001', 'pg009', 'pv000', '07-10-2018', 34, 34, 34344);
INSERT INTO tb_registro_publico VALUES (31, 3434, 'po001', 't001', 'p009', '02-10-2018', 123, 23, 2323);
INSERT INTO tb_registro_publico VALUES (32, 2333, 'poo09', 't0009', 'p0009', '16-10-2018', 12.3400002, 23.8999996, 2333);
INSERT INTO tb_registro_publico VALUES (33, 3343, 'p009', 't0009', 'p009', '02-10-2018', 13.8000002, 13.8000002, 4345);
INSERT INTO tb_registro_publico VALUES (29, 90023, 'F0001', 'T001', 'P0009', '01-10-2018', 12, 12, 133);
INSERT INTO tb_registro_publico VALUES (34, 0, '0', '0', '0', '07-11-2018', 450, 400, 30000);


--
-- Name: tb_registro_publico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_registro_publico_id_seq', 34, true);


--
-- Data for Name: tb_servicio; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_servicio VALUES (1, 'Acueducto');
INSERT INTO tb_servicio VALUES (2, 'Cloacas');
INSERT INTO tb_servicio VALUES (3, 'Drenaje Artificial');
INSERT INTO tb_servicio VALUES (4, 'Alumbrado Publico');
INSERT INTO tb_servicio VALUES (5, 'Electricidad Residencial');
INSERT INTO tb_servicio VALUES (6, 'Electricidad Industrial');
INSERT INTO tb_servicio VALUES (8, 'Pavimento');
INSERT INTO tb_servicio VALUES (9, 'Acera');
INSERT INTO tb_servicio VALUES (10, 'Transp. Pub. Terrestre');
INSERT INTO tb_servicio VALUES (11, 'Transp. Pub. Subterr.');
INSERT INTO tb_servicio VALUES (12, 'Transp. Pub. Aereo');
INSERT INTO tb_servicio VALUES (13, 'Telefono');
INSERT INTO tb_servicio VALUES (14, 'Cobertura Celular');
INSERT INTO tb_servicio VALUES (15, 'TV Satelital');
INSERT INTO tb_servicio VALUES (16, 'Cable TV');
INSERT INTO tb_servicio VALUES (17, 'Aseo Urbano');
INSERT INTO tb_servicio VALUES (18, 'Barrido de Calle');
INSERT INTO tb_servicio VALUES (19, 'Correo y Telegrafo');
INSERT INTO tb_servicio VALUES (20, 'Escuela');
INSERT INTO tb_servicio VALUES (21, 'Medicatura');
INSERT INTO tb_servicio VALUES (22, 'Gas Directo');
INSERT INTO tb_servicio VALUES (23, 'Gas Bombona');
INSERT INTO tb_servicio VALUES (24, 'Riego');
INSERT INTO tb_servicio VALUES (7, 'Vialidad');


--
-- Name: tb_servicio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_servicio_id_seq', 24, true);


--
-- Data for Name: tb_solvencia_municipal; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_solvencia_municipal VALUES (1, 12345, 'Pagada', '2018-10-06', 13, '2019');
INSERT INTO tb_solvencia_municipal VALUES (4, 990, 'Pagada', '2018-11-01', 15, '2018');
INSERT INTO tb_solvencia_municipal VALUES (5, 990, 'Pagada', '2018-11-02', 15, '2019');
INSERT INTO tb_solvencia_municipal VALUES (6, 990, 'Pagada', '2018-11-02', 13, '2020');
INSERT INTO tb_solvencia_municipal VALUES (7, 990, 'Pagada', '2018-11-01', 16, '2018');
INSERT INTO tb_solvencia_municipal VALUES (3, 900, 'Pagada', '2018-06-03', 10, '2018');


--
-- Name: tb_solvencia_municipal_anualidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_solvencia_municipal_anualidad_seq', 1, true);


--
-- Name: tb_solvencia_municipal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_solvencia_municipal_id_seq', 7, true);


--
-- Data for Name: tb_terreno; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_terreno VALUES (72, 'Plano', 'Calle Pavimentada', 'Regular', 'Convencional', 'Zona Urbanizada', 'Muro de Contencion', 'Propiedad', 'Ejido', 'Residencial', 'dd 12', 'fdf 11', 'dvdfdf 11', 'dfdff 12', '121', '121', '12121', '144', '', '', '', '', '', '', 'ererre', 12, 12, NULL);
INSERT INTO tb_terreno VALUES (73, 'Plano', 'Calle Engranzonada', 'Irregular', 'Convencional', 'Zona Urbanizada', 'Nivelacion', 'Propiedad', 'Ejido', 'Residencial', '121', '1212', '121', '121', '1212', '21', '121', '144', '', '', '', '', '', '', 'fdfdfdf', 12, 12, NULL);
INSERT INTO tb_terreno VALUES (74, 'Corte', 'Calle de Tierra', 'Irregular', 'Interior de Manzana', 'Rio/Quebrada', 'Cercado', 'Arrendamiento', 'Municipal Propio', 'Comercial', 'sddfdf', 'dfdfd', 'dff', 'dfdf', '122', '1212', '323', '166.41', '6.18', '', '', '8', '', '0', 'ninguna', 12.8999996, 12.8999996, 6.17999983);
INSERT INTO tb_terreno VALUES (75, 'Plano', 'Calle Pavimentada', 'Regular', 'Convencional', 'Zona Urbanizada', 'Muro de Contencion', 'Anticresis', 'Nacional', 'Recreativo', 'xcxc', 'xcxc', 'xcxc', 'xcxc', '223', '3434', '344', '830.27', '6.18', '', '', '10', '20.9', '107239.33', 'fggfg', 20.2999992, 40.9000015, 6.17999983);
INSERT INTO tb_terreno VALUES (71, 'Plano', 'Calle Pavimentada', 'Regular', 'Convencional', 'Zona Urbanizada', 'Muro de Contencion', 'Propiedad', 'Municipal Propio', 'Residencial', 'ddfgf', 'fgfg', 'fg', 'fgf', '123', '123', '1234', '144', '6.18', '', '', '9', '8.9', '7920.28', 'esta fino el terreno', 12, 12, 6.17999983);
INSERT INTO tb_terreno VALUES (76, 'Plano', 'Calle Pavimentada', 'Regular', 'Convencional', 'Zona Urbanizada', 'Muro de Contencion', 'Propiedad', 'Municipal Propio', 'Residencial', 'casa que es o fue de petra ', 'casa que fue o es d margot ', 'cassa que fue o es de santa ', 'casa que es o fue de yugdelis', '30.0', '20.0', '4', '34000', '6.18', '', '', '7', '2.5', '525300.00', 'el terreno se encuentra en buen estado ', 340, 100, 6.17999983);


--
-- Data for Name: tb_terreno_coordenadas; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_terreno_coordenadas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_terreno_coordenadas_id_seq', 1, false);


--
-- Name: tb_terreno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_terreno_id_seq', 76, true);


--
-- Data for Name: tb_terreno_linderos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_terreno_linderos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_terreno_linderos_id_seq', 1, false);


--
-- Data for Name: tb_terreno_valoracion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_terreno_valoracion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_terreno_valoracion_id_seq', 1, false);


--
-- Data for Name: tb_terrenoservicios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_terrenoservicios VALUES (506, 3, 72);
INSERT INTO tb_terrenoservicios VALUES (507, 4, 72);
INSERT INTO tb_terrenoservicios VALUES (508, 5, 72);
INSERT INTO tb_terrenoservicios VALUES (509, 6, 72);
INSERT INTO tb_terrenoservicios VALUES (510, 15, 72);
INSERT INTO tb_terrenoservicios VALUES (511, 16, 72);
INSERT INTO tb_terrenoservicios VALUES (512, 19, 72);
INSERT INTO tb_terrenoservicios VALUES (513, 7, 72);
INSERT INTO tb_terrenoservicios VALUES (514, 2, 73);
INSERT INTO tb_terrenoservicios VALUES (515, 3, 73);
INSERT INTO tb_terrenoservicios VALUES (516, 2, 74);
INSERT INTO tb_terrenoservicios VALUES (517, 3, 74);
INSERT INTO tb_terrenoservicios VALUES (518, 4, 74);
INSERT INTO tb_terrenoservicios VALUES (519, 5, 74);
INSERT INTO tb_terrenoservicios VALUES (520, 6, 74);
INSERT INTO tb_terrenoservicios VALUES (521, 12, 74);
INSERT INTO tb_terrenoservicios VALUES (522, 14, 74);
INSERT INTO tb_terrenoservicios VALUES (532, 2, 75);
INSERT INTO tb_terrenoservicios VALUES (533, 4, 75);
INSERT INTO tb_terrenoservicios VALUES (534, 5, 75);
INSERT INTO tb_terrenoservicios VALUES (535, 1, 71);
INSERT INTO tb_terrenoservicios VALUES (536, 2, 71);
INSERT INTO tb_terrenoservicios VALUES (537, 1, 76);
INSERT INTO tb_terrenoservicios VALUES (538, 2, 76);
INSERT INTO tb_terrenoservicios VALUES (539, 3, 76);
INSERT INTO tb_terrenoservicios VALUES (540, 4, 76);
INSERT INTO tb_terrenoservicios VALUES (541, 5, 76);
INSERT INTO tb_terrenoservicios VALUES (542, 13, 76);


--
-- Name: tb_terrenoservicios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_terrenoservicios_id_seq', 542, true);


--
-- Data for Name: tb_ubicacion_comunitaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_ubicacion_comunitaria VALUES (39, 12, 'agua blanca', 'la esmeralda 2', 'los tierruo', 'la trinchera', 'Ciudad', 'Ribero adentro', 'Los bloques', 'el refugio', 'Barrio', 24324, 'el refugio de los mangos', '1', '04', 8, 1, 3, 2, 'Av', 'Calle principal');
INSERT INTO tb_ubicacion_comunitaria VALUES (44, 21323, 'casanay', 'la esmeralda', 'los tierruo', 'la izquierdad', 'Cariaco', 'Ribero adentro', 'Los bloques', 'el refugio', 'Barrio', 123, 'el refugio  de las flores', '5', '01', 8, 1, 3, 2, 'Trav', 'Calle la pas ');
INSERT INTO tb_ubicacion_comunitaria VALUES (45, 2, 'Los cartujos', 'los riendos', 'los frailes', 'los firulais', 'Cariaco', 'Ribero', 'Los quesos', 'el jamon', 'queso duro', 223, '2.5', '3', '01', 8, 1, 2, 3, 'Esc', 'Calle principal');
INSERT INTO tb_ubicacion_comunitaria VALUES (46, 2, 'Los cartujos', 'los riendos', 'los frailes', 'los firulais', 'Cariaco', 'Ribero', 'Los quesos', 'el jamon', 'queso duro', 223, 'la mortadela', '5', '04', 10, 1, 2, 3, 'Av', 'Calle Estanislao rendon ');
INSERT INTO tb_ubicacion_comunitaria VALUES (35, 12, 'las manoas', 'la esmeralda', 'los tierruo', 'la trinchera', 'Cariaco', 'Ribero', 'Los bloques', 'el refugio', 'Barrio carinicuo', 123, 'el refugio  de las flores', '4', '01', 9, 1, 3, 2, 'Av', 'Antigua carretera cumana-cariaco');
INSERT INTO tb_ubicacion_comunitaria VALUES (47, 0, 'Ninguna ', 'la trinchera ', 'ninguno ', 'la trinchera ', 'cariaco', 'cariaco', 'cariaco', 'nonguno', 'ninguno', 0, 'al lado del hospital ', '3', '02', 7, 0, 0, 0, 'Clle', 'Calle romulo gallego');


--
-- Name: tb_ubicacion_comunitaria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_ubicacion_comunitaria_id_seq', 48, true);


--
-- Data for Name: tb_unidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_unidad VALUES (6, 6.17999983, '25/10/2018', 'Aperturada');


--
-- Name: tb_unidad_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_unidad_codigo_seq', 6, true);


--
-- Data for Name: tb_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tb_usuario VALUES (1, 'V', 14787367, 'Juan', 'Jose', 'Pereira', 'Lopez', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Usuario', 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Activo', NULL, NULL);
INSERT INTO tb_usuario VALUES (11, 'E', 20372123, 'Kelvin', 'Jose', 'Diaz', 'Diaz', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Usuario', 'Administrador', 'kelvin12', 'fcea920f7412b5da7be0cf42b8c93759', 'Activo', NULL, NULL);
INSERT INTO tb_usuario VALUES (19, 'V', 20022993, 'Yadira', 'Josefina', 'Garate', 'Pereda', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Usuario', 'Secretaria', 'yadira', '2ccb2f0293a1d5f7e06f7ef57a652b4f', 'Activo', NULL, NULL);
INSERT INTO tb_usuario VALUES (12, 'V', 22921276, 'Robert', 'Jose', 'Rodriguez', 'Centeno', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Usuario', 'Coordinador', 'robert', '684c851af59965b680086b7b4896ff98', 'Activo', NULL, NULL);
INSERT INTO tb_usuario VALUES (14, 'P', 4296074, 'Efrain', 'Miguel', 'Torres', 'Antioque', 'NULL', 'NULL', 'NULL', 'NULL       ', 'Usuario', 'Director', 'etorres', '080f04c94b418375331c97a822945156', 'Inactivo', NULL, NULL);


--
-- Data for Name: tb_valoracion_construccion; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_valoracion_construccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_valoracion_construccion_id_seq', 1, false);


--
-- Data for Name: tb_ventana_const; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Name: tb_ventana_const_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tb_ventana_const_codigo_seq', 1, false);


--
-- Name: tb_bano_const_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_bano_const
    ADD CONSTRAINT tb_bano_const_pkey PRIMARY KEY (codigo);


--
-- Name: tb_certificacion_linderos_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_certificacion_linderos
    ADD CONSTRAINT tb_certificacion_linderos_id_key UNIQUE (id);


--
-- Name: tb_const_bano_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_const_bano
    ADD CONSTRAINT tb_const_bano_pkey PRIMARY KEY (id);


--
-- Name: tb_const_puerta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_const_puerta
    ADD CONSTRAINT tb_const_puerta_pkey PRIMARY KEY (id);


--
-- Name: tb_const_ventana_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_const_ventana
    ADD CONSTRAINT tb_const_ventana_pkey PRIMARY KEY (id);


--
-- Name: tb_construccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_construccion
    ADD CONSTRAINT tb_construccion_pkey PRIMARY KEY (id);


--
-- Name: tb_dato_const_complemento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_dato_const_complemento
    ADD CONSTRAINT tb_dato_const_complemento_pkey PRIMARY KEY (codigo);


--
-- Name: tb_ficha_catastral_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_ficha_catastral
    ADD CONSTRAINT tb_ficha_catastral_pkey PRIMARY KEY (id);


--
-- Name: tb_inmueble_copy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_inmueble_copy
    ADD CONSTRAINT tb_inmueble_copy_pkey PRIMARY KEY (id);


--
-- Name: tb_inmueble_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_inmueble
    ADD CONSTRAINT tb_inmueble_pkey PRIMARY KEY (id);


--
-- Name: tb_inspeciones_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_inspecciones
    ADD CONSTRAINT tb_inspeciones_id_key UNIQUE (id);


--
-- Name: tb_parcela_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_parcela
    ADD CONSTRAINT tb_parcela_pkey PRIMARY KEY (codigo);


--
-- Name: tb_parroquia_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_parroquia
    ADD CONSTRAINT tb_parroquia_id_key UNIQUE (id);


--
-- Name: tb_parroquia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_parroquia
    ADD CONSTRAINT tb_parroquia_pkey PRIMARY KEY (codigo);


--
-- Name: tb_persona_copy_id_correo_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_persona_copy_id_correo_key UNIQUE (id, correo);


--
-- Name: tb_persona_copy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_usuario
    ADD CONSTRAINT tb_persona_copy_pkey PRIMARY KEY (nacionalidad, cedula);


--
-- Name: tb_persona_id_correo_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_persona
    ADD CONSTRAINT tb_persona_id_correo_key UNIQUE (id, correo);


--
-- Name: tb_persona_inspeccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_persona_inspeccion
    ADD CONSTRAINT tb_persona_inspeccion_pkey PRIMARY KEY (id);


--
-- Name: tb_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_persona
    ADD CONSTRAINT tb_persona_pkey PRIMARY KEY (nacionalidad, cedula);


--
-- Name: tb_puerta_const_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_puerta_const
    ADD CONSTRAINT tb_puerta_const_pkey PRIMARY KEY (codigo);


--
-- Name: tb_referencia_direccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_referencia_direccion
    ADD CONSTRAINT tb_referencia_direccion_pkey PRIMARY KEY (id);


--
-- Name: tb_registro_publico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_registro_publico
    ADD CONSTRAINT tb_registro_publico_pkey PRIMARY KEY (id);


--
-- Name: tb_servicio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_servicio
    ADD CONSTRAINT tb_servicio_pkey PRIMARY KEY (id);


--
-- Name: tb_solvencia_municipal_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_solvencia_municipal
    ADD CONSTRAINT tb_solvencia_municipal_id_key UNIQUE (id);


--
-- Name: tb_terreno_coordenadas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_terreno_coordenadas
    ADD CONSTRAINT tb_terreno_coordenadas_pkey PRIMARY KEY (id);


--
-- Name: tb_terreno_linderos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_terreno_linderos
    ADD CONSTRAINT tb_terreno_linderos_pkey PRIMARY KEY (id);


--
-- Name: tb_terreno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_terreno
    ADD CONSTRAINT tb_terreno_pkey PRIMARY KEY (id);


--
-- Name: tb_terreno_valoracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_terreno_valoracion
    ADD CONSTRAINT tb_terreno_valoracion_pkey PRIMARY KEY (id);


--
-- Name: tb_unidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_unidad
    ADD CONSTRAINT tb_unidad_pkey PRIMARY KEY (codigo);


--
-- Name: tb_ventana_const_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tb_ventana_const
    ADD CONSTRAINT tb_ventana_const_pkey PRIMARY KEY (codigo);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

