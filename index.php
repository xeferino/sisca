
<?php 
include_once("admin/clases/motor.php");
$objeto = new Persona();
if (isset($_POST['submit']) && $_POST['submit'] == 'login') {	

	$cuenta =  $_POST['cuenta'];
	$clave =  $_POST['clave'];

	$objeto->login($cuenta,$clave);
} 
?>
<!doctype html>
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta charset="utf-8" />
	<title>Login - Admin</title>
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/img/pa.ico">

	<meta name="description" content="User login page" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

	<!-- bootstrap & fontawesome -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

	<!-- text fonts -->
	<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

	<!-- ace styles -->
	<link rel="stylesheet" href="assets/css/ace.min.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
		
		<script src="assets/js/jquery-2.1.4.min.js"></script>
		<!-- sweetalert -->
		<script src="assets/sweetalert/dist/sweetalert-dev.js"></script>
		<!--<link rel="stylesheet" href="../assets/sweetalert/example/example.css">-->
		<link rel="stylesheet" href="assets/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" href="assets/sweetalert/themes/facebook/facebook.css">
  <style type="text/css">
 	body {
		width: 100%;
		margin: 0;
		padding: 0;
		-webkit-font-smoothing: antialiased;
	}
	@media only screen and (max-width: 600px) {
		table[class="table-row"] {
			float: none !important;
			width: 98% !important;
			padding-left: 20px !important;
			padding-right: 20px !important;
		}
		table[class="table-row-fixed"] {
			float: none !important;
			width: 98% !important;
		}
		table[class="table-col"], table[class="table-col-border"] {
			float: none !important;
			width: 100% !important;
			padding-left: 0 !important;
			padding-right: 0 !important;
			table-layout: fixed;
		}
		td[class="table-col-td"] {
			width: 100% !important;
		}
		table[class="table-col-border"] + table[class="table-col-border"] {
			padding-top: 12px;
			margin-top: 12px;
			border-top: 1px solid #E8E8E8;
		}
		table[class="table-col"] + table[class="table-col"] {
			margin-top: 15px;
		}
		td[class="table-row-td"] {
			padding-left: 0 !important;
			padding-right: 0 !important;
		}
		table[class="navbar-row"] , td[class="navbar-row-td"] {
			width: 100% !important;
		}
		img {
			max-width: 100% !important;
			display: inline !important;
		}
		img[class="pull-right"] {
			float: right;
			margin-left: 11px;
            max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		img[class="pull-left"] {
			float: left;
			margin-right: 11px;
			max-width: 125px !important;
			padding-bottom: 0 !important;
		}
		table[class="table-space"], table[class="header-row"] {
			float: none !important;
			width: 98% !important;
		}
		td[class="header-row-td"] {
			width: 100% !important;
		}
	}
	@media only screen and (max-width: 480px) {
		table[class="table-row"] {
			padding-left: 16px !important;
			padding-right: 16px !important;
		}
	}
	@media only screen and (max-width: 320px) {
		table[class="table-row"] {
			padding-left: 12px !important;
			padding-right: 12px !important;
		}
	}
	@media only screen and (max-width: 608px) {
		td[class="table-td-wrap"] {
			width: 100% !important;
		}
	}
  </style>
 </head>
 <body style="font-family: Arial, sans-serif; font-size:13px; color: #444444; min-height: 200px;" bgcolor="#E4E6E9" leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">
 <table width="100%" height="100%" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0">
 <tr><td width="100%" align="center" valign="top" bgcolor="#E4E6E9" style="background-color:#E4E6E9; min-height: 200px;">
<table><tr><td class="table-td-wrap" align="center" width="608"><div style="font-family: Arial, sans-serif; line-height: 32px; color: #444444; font-size: 13px;">
  <a href="#" style="color: #478fca; text-decoration: none; font-size: 14px; background-color: transparent;">
  </a>
</div>
<br>
<table class="table-row" style="table-layout: auto; padding-right: 24px; padding-left: 24px; width: 600px; background-color: #ffffff;" bgcolor="#FFFFFF" width="600" cellspacing="0" cellpadding="0" border="0"><tbody><tr height="55px" style="font-family: Arial, sans-serif; line-height: 19px; color: #fff; font-size: 13px; height: 55px;">
   <td class="table-row-td" style="height: 55px; padding-right: 16px; font-family: Arial, sans-serif; line-height: 19px;  background-color: #E4E6E9; color: #fff; font-size: 13px; font-weight: bold; vertical-align: middle;" valign="middle" align="left">
     <a href="#" style="color: #fff; font-weight: bold; text-decoration: none; padding: 0px; font-size: 25px; line-height: 20px; height: 50px;">
     		<h3>&nbsp;&nbsp; <!-- <i class="ace-icon fa fa-lock"></i> --> <!-- Ingrese su datos de acceso --> </h3> 
  	 </a>
   </td>
 
   <td class="table-row-td" style="height: 55px; font-family: Arial, sans-serif; line-height: 19px; background-color: #E4E6E9; color: #fff; font-size: 13px; font-weight: bold; text-align: right; vertical-align: middle;" align="right" valign="middle">
     <a href="#" style="color: #fff;  text-decoration: none; font-size: 15px; background-color: transparent;">
	   
	 </a>
	 &nbsp;
	 <a href="#" style="color: #fff; text-decoration: none; font-size: 25px; background-color: transparent;" class="pull-left">
	   <!-- <i class="ace-icon fa fa-lock"></i> -->
	 </a>
   </td>
</tr></tbody></table>


<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;</td></tr></tbody></table>
<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>

<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 24px; padding-right: 24px;" valign="top" align="left">
 <table class="table-col" align="left" width="552" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="552" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">	
	<div style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; text-align: center;">
		<img src="assets/images/fondo.png" style="border: 0px none #444444; vertical-align: middle; display: block; padding-bottom: 9px;" hspace="0" vspace="0" border="0" width="550" height="280">
	</div>
 </td></tr></tbody></table>
</td></tr></tbody></table>

<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
   <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
	 <table class="header-row" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="528" style="font-size: 24px; margin: 0px; font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #999; padding-bottom: 10px; padding-top: 15px;" valign="top" align="left"><span class="red"><i class="ace-icon fa fa-desktop"></i> Acceder al Sistema</span></td></tr></tbody></table>
	 <table class="header-row" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="528" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #444444; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left">
	 	<?php if($objeto->mensaje==1){?>

	 	<script>
	 		$(document).ready(function() {
	 			swal({
	 				title: "",
	 				text: "Usuario Temporalmente Inactivo.",
	 				type: "error",
	 				confirmButtonText: "Aceptar",
	 				timer: "3000",
	 				allowEscapeKey: false
	 			});
	 		});
	 	</script>
	 	<?php }?>
	 	<?php if($objeto->mensaje==2){?>

	 	<script>
	 		$(document).ready(function() {
	 			swal({
	 				title: "",
	 				text: "Usuario o Clave Invalidos.",
	 				type: "error",
	 				confirmButtonText: "Aceptar",
	 				timer: "3000",
	 				allowEscapeKey: false
	 			});
	 		});
	 	</script>
	 	<?php }?>
	 	<?php if($objeto->mensaje==3){?>

	 	<script>
	 		$(document).ready(function() {
	 			swal({
	 				title: "",
	 				text: "Campos En Blancos",
	 				type: "error",
	 				confirmButtonText: "Aceptar",
	 				timer: "3000",
	 				allowEscapeKey: false
	 			});
	 		});
	 	</script>
	 	<?php }?>
	 	<?php if($objeto->mensaje==4){?>

	 	<script>
	 		$(document).ready(function() {
	 			swal({
	 				title: "Bienvenido, <?php echo @$_SESSION['nombre']." ".@$_SESSION['apellido'];?>",
	 				text: "<h5><b>Inicio de sesion correctamente</b></h5>",
	 				type: "success",
	 				showConfirmButton: false,
	 				/*confirmButtonText: "Aceptar",*/
	 				timer: "3000",
	 				allowEscapeKey: false,
	 				html: true
	 			},
					function(){
						$(location).attr('href','http:admin/');
					}
				);
	 		});
	 	</script>
	 	<?php }?>
	 	<form action="" method="post">
	 		<input type="hidden" name="submit" value="login" />
	 		<fieldset>
	 			<label class="block clearfix">
	 				<span class="block input-icon input-icon-right">
	 					<input type="text" name="cuenta" class="form-control" placeholder="Username" />
	 					<i class="ace-icon fa fa-user"></i>
	 				</span>
	 			</label>

	 			<label class="block clearfix">
	 				<span class="block input-icon input-icon-right">
	 					<input type="password" name="clave" class="form-control" placeholder="Password" />
	 					<i class="ace-icon fa fa-lock"></i>
	 				</span>
	 			</label>

	 			<div class="space"></div>

	 			<div class="clearfix">


	 				<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
	 					<i class="ace-icon fa fa-sign-in"></i>
	 					<span class="bigger-110">Login</span>
	 				</button>
	 			</div>

	 			<div class="space-4"></div>
	 		</fieldset>
	 	</form>

	 </td></tr></tbody></table>
   </td></tr></tbody></table>
</td></tr></tbody></table>


<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
   <table class="table-col" align="left" width="528" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="528" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
	 <table width="100%" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td width="100%" bgcolor="#d3dade" style="font-family: Arial, sans-serif; line-height: 19px; color: #31708f; font-size: 14px; font-weight: normal; padding: 15px; border: 1px solid #c5d1d4; background-color: #d3dade;" valign="top" align="left">
	   Si Presenta Problemas para Ingresar, Ponganse en Contacto con el Administrador del Sistema.
	   <br>
	 </td></tr></tbody></table>
   </td></tr></tbody></table>
</td></tr></tbody></table>
<table class="table-space" height="24" style="height: 24px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="24" style="height: 24px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>


<table class="table-space" height="12" style="height: 12px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="12" style="height: 12px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>
<table class="table-space" height="24" style="height: 24px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="24" style="height: 24px; width: 600px; padding-left: 18px; padding-right: 18px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="center">&nbsp;<table bgcolor="#e91313" height="0" width="100%" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td bgcolor="#e91313" height="1" width="100%" style="height: 1px; font-size:0;" valign="top" align="left">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>
	
	

<table class="table-row" width="600" bgcolor="#FFFFFF" style="table-layout: fixed; background-color: #ffffff;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-row-td" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal; padding-left: 36px; padding-right: 36px;" valign="top" align="left">
   <table class="table-col" align="left" width="273" style="padding-right: 18px; table-layout: fixed;" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-col-td" width="255" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
	<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left"><span class="red">Desarrallado Por: </span></td></tr></tbody></table>
	
	<div style="font-family: Arial, sans-serif; line-height: 36px; color: #444444; font-size: 13px;">
		<span>Estudiantes de la UPTOS "Cariaco" </span>
	</div>
   </td></tr></tbody></table>
   
   <table class="table-col" align="left" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="table-col-td" width="255" style="font-family: Arial, sans-serif; line-height: 19px; color: #444444; font-size: 13px; font-weight: normal;" valign="top" align="left">
	<table class="header-row" width="255" cellspacing="0" cellpadding="0" border="0" style="table-layout: fixed;"><tbody><tr><td class="header-row-td" width="255" style="font-family: Arial, sans-serif; font-weight: normal; line-height: 19px; color: #478fca; margin: 0px; font-size: 18px; padding-bottom: 8px; padding-top: 10px;" valign="top" align="left"><span class="red"></span></td></tr></tbody></table>
	
	<br>
	
   </td></tr></tbody></table>	   
</td></tr></tbody></table>
<table class="table-space" height="16" style="height: 16px; font-size: 0px; line-height: 0; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="16" style="height: 16px; width: 600px; background-color: #ffffff;" width="600" bgcolor="#FFFFFF" align="left">&nbsp;</td></tr></tbody></table>


<table class="table-space" height="6" style="height: 6px; font-size: 0px; line-height: 0; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" cellspacing="0" cellpadding="0" border="0"><tbody><tr><td class="table-space-td" valign="middle" height="6" style="height: 6px; width: 600px; background-color: #e4e6e9;" width="600" bgcolor="#E4E6E9" align="left">&nbsp;</td></tr></tbody></table>

</td></tr>
 </table>
 </body>
 </html>